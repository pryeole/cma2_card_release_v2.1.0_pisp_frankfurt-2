package com.capgemini.psd2.pisp.isp.controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.CustomISPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomISPaymentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBWriteInternationalScheduled1;
import com.capgemini.psd2.pisp.domain.OBWriteInternationalScheduledResponse1;
import com.capgemini.psd2.pisp.isp.service.ISPaymentsService;
import com.fasterxml.jackson.databind.ObjectMapper;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-12-03T16:58:19.566+05:30")

@Controller
public class InternationalScheduledPaymentsApiController implements InternationalScheduledPaymentsApi {

	private final ObjectMapper objectMapper;

	private final HttpServletRequest request;

	@Autowired
	private ISPaymentsService service;

	@org.springframework.beans.factory.annotation.Autowired
	public InternationalScheduledPaymentsApiController(ObjectMapper objectMapper, HttpServletRequest request) {
		this.objectMapper = objectMapper;
		this.request = request;
	}

	@Override
	public Optional<ObjectMapper> getObjectMapper() {
		return Optional.ofNullable(objectMapper);
	}

	@Override
	public Optional<HttpServletRequest> getRequest() {
		return Optional.ofNullable(request);
	}

	@Override
	public ResponseEntity<OBWriteInternationalScheduledResponse1> createInternationalScheduledPayments(
			@RequestBody OBWriteInternationalScheduled1 obWriteInternationalScheduled1Param, String xFapiFinancialId,
			String authorization, String xIdempotencyKey, String xJwsSignature, String xFapiCustomerLastLoggedTime,
			String xFapiCustomerIpAddress, String xFapiInteractionId, String xCustomerUserAgent) {
		if (getObjectMapper().isPresent()) {
			CustomISPaymentsPOSTRequest customRequest = new CustomISPaymentsPOSTRequest();
			customRequest.setData(obWriteInternationalScheduled1Param.getData());
			customRequest.setRisk(obWriteInternationalScheduled1Param.getRisk());
			CustomISPaymentsPOSTResponse customResponse = service
					.createInternationalScheduledPaymentsResource(customRequest);
			OBWriteInternationalScheduledResponse1 tppResponse = new OBWriteInternationalScheduledResponse1();
			tppResponse.setData(customResponse.getData());
			tppResponse.setLinks(customResponse.getLinks());
			tppResponse.setMeta(customResponse.getMeta());
			return new ResponseEntity<>(tppResponse, HttpStatus.CREATED);

		}
		throw PSD2Exception.populatePSD2Exception(
				new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_INVALIDFORMAT, ErrorMapKeys.RESOURCE_INVALIDFORMAT));
	}

	@Override
	public ResponseEntity<OBWriteInternationalScheduledResponse1> getInternationalScheduledPaymentsInternationalScheduledPaymentId(
			@PathVariable("InternationalScheduledPaymentId") String internationalScheduledPaymentId,
			String xFapiFinancialId, String authorization, String xFapiCustomerLastLoggedTime,
			String xFapiCustomerIpAddress, String xFapiInteractionId, String xCustomerUserAgent) {
		if (getObjectMapper().isPresent()) {

			CustomISPaymentsPOSTResponse customResponse = service
					.retrieveInternationalScheduledPaymentsResource(internationalScheduledPaymentId);
			OBWriteInternationalScheduledResponse1 tppResponse = new OBWriteInternationalScheduledResponse1();
			tppResponse.setData(customResponse.getData());
			tppResponse.setLinks(customResponse.getLinks());
			tppResponse.setMeta(customResponse.getMeta());
			return new ResponseEntity<>(tppResponse, HttpStatus.OK);
		}
		throw PSD2Exception.populatePSD2Exception(
				new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_INVALIDFORMAT, ErrorMapKeys.RESOURCE_INVALIDFORMAT));
	}

}
