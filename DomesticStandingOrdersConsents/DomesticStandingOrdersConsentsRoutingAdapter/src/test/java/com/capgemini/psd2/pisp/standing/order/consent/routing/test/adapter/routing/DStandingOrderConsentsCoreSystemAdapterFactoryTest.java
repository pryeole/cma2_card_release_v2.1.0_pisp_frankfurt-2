package com.capgemini.psd2.pisp.standing.order.consent.routing.test.adapter.routing;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.pisp.stage.operations.adapter.DomesticStandingOrdersPaymentStagingAdapter;
import com.capgemini.psd2.pisp.standing.order.consent.routing.adapter.impl.DStandingOrderConsentStagingRoutingAdapterImpl;
import com.capgemini.psd2.pisp.standing.order.consent.routing.adapter.routing.DStandingOrderConsentsCoreSystemAdapterFactory;

@RunWith(SpringJUnit4ClassRunner.class)
public class DStandingOrderConsentsCoreSystemAdapterFactoryTest {

	@Mock
	private ApplicationContext applicationContext;

	@InjectMocks
	private DStandingOrderConsentsCoreSystemAdapterFactory factory;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testGetDomesticStandingOrdersSetupStagingInstance() {
		DomesticStandingOrdersPaymentStagingAdapter adapter = new DStandingOrderConsentStagingRoutingAdapterImpl();
		when(applicationContext.getBean(anyString())).thenReturn(adapter);

		DomesticStandingOrdersPaymentStagingAdapter dsoAdapter = factory
				.getDomesticStandingOrdersSetupStagingInstance(anyString());
		assertNotNull(dsoAdapter);

	}

	@Test
	public void testsetApplicationContext() {
		factory.setApplicationContext(new ApplicationContextMock());
	}

	@After
	public void tearDown() throws Exception {
		factory = null;
		applicationContext = null;
	}

}
