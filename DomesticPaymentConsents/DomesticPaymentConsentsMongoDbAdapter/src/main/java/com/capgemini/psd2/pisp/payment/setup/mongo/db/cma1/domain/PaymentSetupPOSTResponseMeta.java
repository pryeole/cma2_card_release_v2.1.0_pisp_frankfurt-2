package com.capgemini.psd2.pisp.payment.setup.mongo.db.cma1.domain;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Meta Data Relevant to the payload
 */
@ApiModel(description = "Meta Data Relevant to the payload")
public class PaymentSetupPOSTResponseMeta   {
  @JsonProperty("TotalPages")
  private Integer totalPages = null;

  public PaymentSetupPOSTResponseMeta totalPages(Integer totalPages) {
    this.totalPages = totalPages;
    return this;
  }

   /**
   * Get totalPages
   * @return totalPages
  **/
  @ApiModelProperty(value = "")


  public Integer getTotalPages() {
    return totalPages;
  }

  public void setTotalPages(Integer totalPages) {
    this.totalPages = totalPages;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PaymentSetupPOSTResponseMeta paymentSetupPOSTResponseMeta = (PaymentSetupPOSTResponseMeta) o;
    return Objects.equals(this.totalPages, paymentSetupPOSTResponseMeta.totalPages);
  }

  @Override
  public int hashCode() {
    return Objects.hash(totalPages);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PaymentSetupPOSTResponseMeta {\n");
    
    sb.append("    totalPages: ").append(toIndentedString(totalPages)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

