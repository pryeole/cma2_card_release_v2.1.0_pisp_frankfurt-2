package com.capgemini.psd2.tppinformation.adaptor.pf.config;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "pf")
public class PfConfiguration {

	private Map<String, String> prefix = new HashMap<>();

	private Map<String, String> adminuser = new HashMap<>();

	private Map<String, String> adminpwdV2 = new HashMap<>();

	public String getTenantSpecificPrfix(String id) {
		return prefix.get(id);
	}

	public String getTenantSpecificAdminUser(String id) {
		return adminuser.get(id);
	}

	public String getTenantSpecificAdminPwd(String id) {
		return adminpwdV2.get(id);
	}

	public Map<String, String> getPrefix() {
		return prefix;
	}

	public void setPrefix(Map<String, String> prefix) {
		this.prefix = prefix;
	}

	public Map<String, String> getAdminuser() {
		return adminuser;
	}

	public void setAdminuser(Map<String, String> adminuser) {
		this.adminuser = adminuser;
	}

	public Map<String, String> getAdminpwdV2() {
		return adminpwdV2;
	}

	public void setAdminpwdV2(Map<String, String> adminpwdV2) {
		this.adminpwdV2 = adminpwdV2;
	}

}
