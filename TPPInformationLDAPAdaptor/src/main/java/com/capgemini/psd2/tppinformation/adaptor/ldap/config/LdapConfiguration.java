package com.capgemini.psd2.tppinformation.adaptor.ldap.config;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ldap.core.ContextSource;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;

@Configuration
@ConfigurationProperties(prefix = "ldap")
public class LdapConfiguration {

	@Value("${ldap.url}")
	private String url;

	@Value("${ldap.userDn}")
	private String userDn;
	
	@Value("${ldap.read.password}")
	private String ldapPassword;
	
	private Map<String, String> clientAppgroupbasedn = new HashMap<>();
	
	private Map<String, String> tppgroupbasedn = new HashMap<>();

	public String getTenantSpecificClientGroupBaseDn(String key) {
		return clientAppgroupbasedn.get(key);
	}
	
	public String getTenantSpecificTppGroupBaseDn(String key) {
		return tppgroupbasedn.get(key);
	}
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUserDn() {
		return userDn;
	}
	

	public void setUserDn(String userDn) {
		this.userDn = userDn;
	}

	public String getLdapPassword() {
		return ldapPassword;
	}

	public void setLdapPassword(String ldapPassword) {
		this.ldapPassword = ldapPassword;
	}

	@Bean
	public ContextSource contextSource() {
		LdapContextSource contextSource = new LdapContextSource();
		contextSource.setUrl(url);
		contextSource.setUserDn(userDn);
		contextSource.setPassword(ldapPassword);
		return contextSource;
	}

	@Bean
	public LdapTemplate ldapTemplate() {
		return new LdapTemplate(contextSource());
	}

	public Map<String, String> getClientAppgroupbasedn() {
		return clientAppgroupbasedn;
	}

	public void setClientAppgroupbasedn(Map<String, String> clientAppgroupbasedn) {
		this.clientAppgroupbasedn = clientAppgroupbasedn;
	}

	public Map<String, String> getTppgroupbasedn() {
		return tppgroupbasedn;
	}

	public void setTppgroupbasedn(Map<String, String> tppgroupbasedn) {
		this.tppgroupbasedn = tppgroupbasedn;
	}

}
