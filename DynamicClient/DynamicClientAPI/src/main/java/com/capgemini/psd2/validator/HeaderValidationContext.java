package com.capgemini.psd2.validator;

import java.util.Iterator;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.tpp.ssa.model.SSAModel;
/**
 * 
 * @author nagoswam
 *
 */
@Component
public class HeaderValidationContext {
	private static final Logger LOGGER = LoggerFactory.getLogger(HeaderValidationContext.class);
	
	@Autowired
	RequestHeaderAttributes headerAttributes;
	
	private Set<ValidationStrategy> headerValidationStrategies;
	

	public HeaderValidationContext(Set<ValidationStrategy> headerValidationStrategies) {
		this.headerValidationStrategies = headerValidationStrategies;
	}

	/*
	 * This method performs validation for fields one by one and return the
	 * invalid one if found. Otherwise, it will continue validating remaining
	 * fields. If all the fields are valid then will return null.
	 */
	public void execute(SSAModel ssaModel) {
		LOGGER.info("Enter header validations");
		ValidationStrategy headerValidation;
		for (Iterator<ValidationStrategy> iterator = headerValidationStrategies.iterator(); iterator.hasNext();) {
			headerValidation = iterator.next();
			headerValidation.validate(headerAttributes, ssaModel);
		}
		LOGGER.info("Exit header validations successfully");
	}

}
