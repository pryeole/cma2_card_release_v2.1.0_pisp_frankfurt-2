package com.capgemini.psd2.foundationservice.domestic.payment.submission.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.delegate.DomesticScheduledPaymentSubmissionFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.ScheduledPaymentInstructionComposite;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.transformer.DomesticScheduledPaymentSubmissionFoundationServiceTransformer;
import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTRequest;
import com.capgemini.psd2.rest.client.model.RequestInfo;

@RunWith(SpringJUnit4ClassRunner.class)
public class DomesticScheduledPaymentsFoundationServiceDelegateTest {

	@InjectMocks
	DomesticScheduledPaymentSubmissionFoundationServiceDelegate delegate;

	@Mock
	DomesticScheduledPaymentSubmissionFoundationServiceTransformer transformer;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}

	@Test
	public void testcreatePaymentRequestHeaders() {
		ReflectionTestUtils.setField(delegate, "transactionReqHeader", "transactionReqHeader");
		ReflectionTestUtils.setField(delegate, "correlationMuleReqHeader", "correlation");
		ReflectionTestUtils.setField(delegate, "sourceUserReqHeader", "sorceuser");
		ReflectionTestUtils.setField(delegate, "sourcesystem", "source");
		ReflectionTestUtils.setField(delegate, "apiChannelCode", "channel");
		ReflectionTestUtils.setField(delegate, "partySourceId", "sourceid");
		ReflectionTestUtils.setField(delegate, "domesticScheduledPaymentSubmissionBaseURL", "baseUrl");
		ReflectionTestUtils.setField(delegate, "apiChannelBrand", "ROI");
		ReflectionTestUtils.setField(delegate,"systemApiVersion","baseUrl");

		RequestInfo requestInfo = new RequestInfo();
		Map<String, String> params = new HashMap<>();
		params.put("transactionReqHeader", "boi");
		params.put("tenant_id","BOIUK");
		params.put("X-CORRELATION-ID","correlation");
		params.put("X-BOI-USER","boiuser");
		params.put("X-BOI-CHANNEL","boichannel");
		HttpHeaders header = delegate.createPaymentRequestHeaders(params);
		HttpHeaders header1 = delegate.createPaymentRequestHeadersPost(params);

		assertNotNull(header);
		assertNotNull(header1);

	}
	
	@Test
	public void testcreatePaymentRequestHeaders1() {
		ReflectionTestUtils.setField(delegate, "transactionReqHeader", "transactionReqHeader");
		ReflectionTestUtils.setField(delegate, "correlationMuleReqHeader", "correlation");
		ReflectionTestUtils.setField(delegate, "sourceUserReqHeader", "sorceuser");
		ReflectionTestUtils.setField(delegate, "sourcesystem", "source");
		ReflectionTestUtils.setField(delegate, "apiChannelCode", "channel");
		ReflectionTestUtils.setField(delegate, "partySourceId", "sourceid");
		ReflectionTestUtils.setField(delegate, "domesticScheduledPaymentSubmissionBaseURL", "baseUrl");
		ReflectionTestUtils.setField(delegate, "apiChannelBrand", "NIGB");
		ReflectionTestUtils.setField(delegate,"systemApiVersion","baseUrl");
		RequestInfo requestInfo = new RequestInfo();
		Map<String, String> params = new HashMap<>();
		params.put("transactionReqHeader", "boi");
		params.put("tenant_id","BOIROI");
		
		HttpHeaders header = delegate.createPaymentRequestHeaders(params);

		assertNotNull(header);

	}

	@Test
	public void testgetPaymentFoundationServiceURL() {
		String version = "1.0";
		String PaymentInstuctionProposalId = "1007";
		delegate.getPaymentFoundationServiceURL(PaymentInstuctionProposalId);
		assertNotNull(delegate);
	}

	@Test
	public void testgetPaymentFoundationServiceURL1() {
		String version = "1.0";
		String PaymentInstuctionProposalId = "1007";
		delegate.postPaymentFoundationServiceURL(version);
		assertNotNull(delegate);
	}
	
	@Test
	public void transformDomesticScheduledPaymentsFromAPIToFDForInsertTest(){
		CustomDSPaymentsPOSTRequest paymentConsentsRequest = new CustomDSPaymentsPOSTRequest();
		ScheduledPaymentInstructionComposite comp = new ScheduledPaymentInstructionComposite();
		Map<String, String> params=new HashMap<>();
		params.put("transactioReqHeader", "boi");
		params.put("tenant_id", "BOIUK");
		Mockito.when(transformer.transformDomesticScheduledPaymentsFromAPIToFDForInsert(paymentConsentsRequest,params)).thenReturn(comp);
		delegate.transformDomesticScheduledPaymentsFromAPIToFDForInsert(paymentConsentsRequest,params);
		
	}
}
