package com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.client.DomesticScheduledPaymentSubmissionFoundationServiceClient;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.delegate.DomesticScheduledPaymentSubmissionFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.ScheduledPaymentInstructionComposite;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.transformer.DomesticScheduledPaymentSubmissionFoundationServiceTransformer;
import com.capgemini.psd2.pisp.adapter.DomesticScheduledPaymentsAdapter;
import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.GenericPaymentsResponse;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.rest.client.model.RequestInfo;

@Component("domesticScheduledPaymentsFoundationServiceAdapter")
public class DomesticScheduledPaymentsFoundationServiceAdapter implements DomesticScheduledPaymentsAdapter  {

	@Autowired
	private DomesticScheduledPaymentSubmissionFoundationServiceDelegate domSchPaySubDelegate;

	@Autowired
	private DomesticScheduledPaymentSubmissionFoundationServiceClient domSchPaySubClient;

	@Autowired
	private DomesticScheduledPaymentSubmissionFoundationServiceTransformer domSchPaySubTransformer;

	@Value("${foundationService.domesticScheduledPaymentSubmissionBaseURL}")
	private String domesticScheduledPaymentSubmissionBaseURL;


	@Override
	public CustomDSPaymentsPOSTResponse retrieveStagedDSPaymentsResponse(
			CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = domSchPaySubDelegate.createPaymentRequestHeaders(params);
		String domesticPaymentURL = domSchPaySubDelegate.getPaymentFoundationServiceURL(customPaymentStageIdentifiers.getPaymentSubmissionId());
		requestInfo.setUrl(domesticPaymentURL);
		ScheduledPaymentInstructionComposite paymentInstructionProposal = domSchPaySubClient.restTransportForDomesticScheduledPaymentFoundationService(requestInfo, ScheduledPaymentInstructionComposite.class, httpHeaders);
		return domSchPaySubTransformer.transformDomesticScheduledPaymentResponse(paymentInstructionProposal);

	}

	@Override
	public GenericPaymentsResponse processDSPayments(CustomDSPaymentsPOSTRequest customRequest,
			Map<String, OBExternalStatus1Code> paymentStatusMap, Map<String, String> params) {
		RequestInfo requestInfo=new RequestInfo();
		HttpHeaders httpHeaders=domSchPaySubDelegate.createPaymentRequestHeadersPost(params);
		String domesticPaymentPostURL = domSchPaySubDelegate.postPaymentFoundationServiceURL(domesticScheduledPaymentSubmissionBaseURL);
		requestInfo.setUrl(domesticPaymentPostURL);
		ScheduledPaymentInstructionComposite instructionProposalRequest=domSchPaySubDelegate.transformDomesticScheduledPaymentsFromAPIToFDForInsert(customRequest,params);
		ScheduledPaymentInstructionComposite instructionProposalResponse =domSchPaySubClient.restTransportForDomesticScheduledPaymentsFoundationServicePost(requestInfo, instructionProposalRequest,ScheduledPaymentInstructionComposite.class , httpHeaders);
		return domSchPaySubTransformer.transformDomesticScheduledPaymentsFromFDToAPIForInsert(instructionProposalResponse);


	}

}
