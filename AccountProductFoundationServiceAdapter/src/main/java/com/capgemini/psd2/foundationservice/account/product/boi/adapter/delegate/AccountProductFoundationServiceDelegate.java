/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.product.boi.adapter.delegate;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountProductsResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.product.boi.adapter.constants.AccountProductFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.product.boi.adapter.raml.domain.Account;
import com.capgemini.psd2.foundationservice.account.product.boi.adapter.transformer.AccountProductFoundationServiceTransformer;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;

/**
 * The Class AccountProductFoundationServiceDelegate.
 */
@EnableConfigurationProperties
@ConfigurationProperties("foundationService")
@Component
public class AccountProductFoundationServiceDelegate {

	/** The account Product FS transformer. */
	@Autowired
	AccountProductFoundationServiceTransformer accountProductFSTransformer;

	@Value("${foundationService.userInReqHeader:#{X-BOI-USER}}")
	private String userInReqHeader;

	@Value("${foundationService.channelInReqHeader:#{X-BOI-CHANNEL}}")
	private String channelInReqHeader;

	@Value("${foundationService.platformInReqHeader:#{X-BOI-PLATFORM}}")
	private String platformInReqHeader;

	@Value("${foundationService.correlationReqHeader:#{X-CORRELATION-ID}}")
	private String correlationReqHeader;

	/** The user in req header. */
	@Value("${foundationService.userInReqHeadeRaml:#{x-api-source-user}}")
	private String userInReqHeadeRaml;

	/** The platform in req header. */
	@Value("${foundationService.platformInReqHeaderRaml:#{x-api-source-system}}")
	private String platformInReqHeaderRaml;

	/** The correlation id in req header. */
	@Value("${foundationService.transactionReqHeaderRaml:#{x-api-transaction-id}}")
	private String transactionReqHeaderRaml;

	/** The correlation id in req header. */
	@Value("${foundationService.correlationReqHeaderRaml:#{x-api-correlation-id}}")
	private String correlationReqHeaderRaml;
	
	@Value("${foundationService.channelInReqHeaderRaml:#{x-api-channel-Code}}")
	private String channelInReqHeaderRaml;
	
	@Value("${foundationService.partySourceNumber:#{X-API-PARTY-SOURCE-ID-NUMBER}}")
	private String partySourceNumber;

	/** The platform. */
	@Value("${foundationService.platform:#{PSD2API}}")
	private String platform;

	/**
	 * Gets the foundation service URL.
	 *
	 * @param channelId
	 *            the channel id
	 * @param accountNSC
	 *            the account NSC
	 * @return the foundation service URL
	 */

	public String getFoundationServiceURL(String baseURL,String version, String nsc, String accountNumber) {

		if (NullCheckUtils.isNullOrEmpty(nsc)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_NSC_IN_REQUEST);
		}
		if (NullCheckUtils.isNullOrEmpty(accountNumber)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}

		if (NullCheckUtils.isNullOrEmpty(baseURL)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.TECHNICAL_ERROR);
		}
		return baseURL + "/" + version + "/accounts/" + nsc + "/" + accountNumber;
	}

	/**
	 * Transform response from FD to API.
	 *
	 * 
	 * @param params
	 *            the params
	 * @return the Product GET response
	 */
	public PlatformAccountProductsResponse transformResponseFromFDToAPI(Account product, Map<String, String> params) {
		return accountProductFSTransformer.transformAccountProducts(product, params);
	}

	/**
	 * Creates the request headers.
	 *
	 * @param requestInfo
	 *            the request info
	 * @param accountMapping
	 *            the account mapping
	 * @return the http headers
	 */
	
	public HttpHeaders createRequestHeaders(RequestInfo requestInfo, AccountMapping accountMapping,
			Map<String, String> params) {
		HttpHeaders httpHeaders = new HttpHeaders();
		String channelCode = params.get(AccountProductFoundationServiceConstants.CHANNEL_ID).toUpperCase();
		httpHeaders.add(userInReqHeadeRaml, accountMapping.getPsuId());
		httpHeaders.add(channelInReqHeaderRaml, channelCode);
		httpHeaders.add(platformInReqHeaderRaml, platform);
		httpHeaders.add(transactionReqHeaderRaml, accountMapping.getCorrelationId());
		httpHeaders.add(correlationReqHeaderRaml, "");
		if(channelCode.equalsIgnoreCase("BOL"))
		httpHeaders.add(partySourceNumber, params.get(PSD2Constants.PARTY_IDENTIFIER));
		return httpHeaders;
	}

}
