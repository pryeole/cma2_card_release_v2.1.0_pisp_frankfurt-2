package com.capgemini.psd2.domestic.scheduled.payments.submission.mock.foundationservice.constants;

public class DomesticScheduledPaymentSubmissionFoundationConstants {
	
	public static final String RECORD_NOT_FOUND = "Payment Instruction Proposal Record Not Found";
	
	public static final String INSTRUCTION_ENDTOEND_REF="FRESCO.21302.GFX.20";
	
	

}
