package com.capgemini.psd2.pisp.international.standing.order.consent.controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.CustomIStandingOrderConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIStandingOrderConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBWriteInternationalStandingOrderConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteInternationalStandingOrderConsentResponse1;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.international.standing.order.consent.service.InternationalStandingOrderConsentsService;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.ApiParam;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-02-11T16:32:00.031+05:30")

@Controller
public class InternationalStandingOrderConsentsApiController implements InternationalStandingOrderConsentsApi {

	@Autowired
	private InternationalStandingOrderConsentsService iStandingOrderConsentsService;

	@Value("${app.responseErrorMessageEnabled}")
	private String responseErrorMessageEnabled;
	private final ObjectMapper objectMapper;

	private final HttpServletRequest request;

	@org.springframework.beans.factory.annotation.Autowired
	public InternationalStandingOrderConsentsApiController(ObjectMapper objectMapper, HttpServletRequest request) {
		this.objectMapper = objectMapper;
		this.request = request;
	}

	@Override
	public Optional<ObjectMapper> getObjectMapper() {
		return Optional.ofNullable(objectMapper);
	}

	@Override
	public Optional<HttpServletRequest> getRequest() {
		return Optional.ofNullable(request);
	}

	/*@Override
	public ResponseEntity<OBWriteInternationalStandingOrderConsentResponse1> createInternationalStandingOrderConsents(
			@ApiParam(value = "Default", required = true) @Valid @RequestBody OBWriteInternationalStandingOrderConsent1 obWriteInternationalStandingOrderConsent1Param,
			@ApiParam(value = "The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB.", required = true) @RequestHeader(value = "x-fapi-financial-id", required = true) String xFapiFinancialId,
			@ApiParam(value = "An Authorisation Token as per https://tools.ietf.org/html/rfc6750", required = true) @RequestHeader(value = "Authorization", required = true) String authorization,
			@ApiParam(value = "Every request will be processed only once per x-idempotency-key.  The Idempotency Key will be valid for 24 hours.", required = true) @RequestHeader(value = "x-idempotency-key", required = true) String xIdempotencyKey,
			@ApiParam(value = "A detached JWS signature of the body of the payload.", required = true) @RequestHeader(value = "x-jws-signature", required = true) String xJwsSignature,
			@ApiParam(value = "The time when the PSU last logged in with the TPP.  All dates in the HTTP headers are represented as RFC 7231 Full Dates. An example is below:  Sun, 10 Sep 2017 19:43:31 UTC") @RequestHeader(value = "x-fapi-customer-last-logged-time", required = false) String xFapiCustomerLastLoggedTime,
			@ApiParam(value = "The PSU's IP address if the PSU is currently logged in with the TPP.") @RequestHeader(value = "x-fapi-customer-ip-address", required = false) String xFapiCustomerIpAddress,
			@ApiParam(value = "An RFC4122 UID used as a correlation id.") @RequestHeader(value = "x-fapi-interaction-id", required = false) String xFapiInteractionId,
			@ApiParam(value = "Indicates the user-agent that the PSU is using.") @RequestHeader(value = "x-customer-user-agent", required = false) String xCustomerUserAgent) {

		if (getObjectMapper().isPresent()) {
			CustomIStandingOrderConsentsPOSTRequest customRequest = new CustomIStandingOrderConsentsPOSTRequest();
			customRequest.setData(obWriteInternationalStandingOrderConsent1Param.getData());
			customRequest.setRisk(obWriteInternationalStandingOrderConsent1Param.getRisk());
			CustomIStandingOrderConsentsPOSTResponse customResponse = iStandingOrderConsentsService
					.createInternationalStandingOrderConsentResource(customRequest);

			return new ResponseEntity<>(customResponse, HttpStatus.CREATED);
		}
		throw PSD2Exception.populatePSD2Exception(
				new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_INVALIDFORMAT, ErrorMapKeys.RESOURCE_INVALIDFORMAT));
	}*/

	@Override
	public ResponseEntity<OBWriteInternationalStandingOrderConsentResponse1> getInternationalStandingOrderConsentsConsentId(
			@ApiParam(value = "ConsentId", required = true) @PathVariable("ConsentId") String consentId,
			@ApiParam(value = "The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB.", required = true) @RequestHeader(value = "x-fapi-financial-id", required = true) String xFapiFinancialId,
			@ApiParam(value = "An Authorisation Token as per https://tools.ietf.org/html/rfc6750", required = true) @RequestHeader(value = "Authorization", required = true) String authorization,
			@ApiParam(value = "The time when the PSU last logged in with the TPP.  All dates in the HTTP headers are represented as RFC 7231 Full Dates. An example is below:  Sun, 10 Sep 2017 19:43:31 UTC") @RequestHeader(value = "x-fapi-customer-last-logged-time", required = false) String xFapiCustomerLastLoggedTime,
			@ApiParam(value = "The PSU's IP address if the PSU is currently logged in with the TPP.") @RequestHeader(value = "x-fapi-customer-ip-address", required = false) String xFapiCustomerIpAddress,
			@ApiParam(value = "An RFC4122 UID used as a correlation id.") @RequestHeader(value = "x-fapi-interaction-id", required = false) String xFapiInteractionId,
			@ApiParam(value = "Indicates the user-agent that the PSU is using.") @RequestHeader(value = "x-customer-user-agent", required = false) String xCustomerUserAgent) {

		// validate id here
		if (getObjectMapper().isPresent()) {
			PaymentRetrieveGetRequest paymentRetrieveRequest = new PaymentRetrieveGetRequest();
			paymentRetrieveRequest.setConsentId(consentId);
			CustomIStandingOrderConsentsPOSTResponse paymentConsentsResponse = iStandingOrderConsentsService
					.retrieveInternationalStandingOrderConsentsResource(paymentRetrieveRequest);
			return new ResponseEntity<>(paymentConsentsResponse, HttpStatus.OK);
		}
		throw PSD2Exception.populatePSD2Exception(
				new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_INVALIDFORMAT, ErrorMapKeys.RESOURCE_INVALIDFORMAT));
	}
/*
	@Override
	public ResponseEntity<OBWriteInternationalStandingOrderConsentResponse1> createInternationalStandingOrderConsents(
			OBWriteInternationalStandingOrderConsent1 obWriteInternationalStandingOrderConsent1Param,
			String xFapiFinancialId, String authorization, String xIdempotencyKey, String xJwsSignature,
			String xFapiCustomerLastLoggedTime, String xFapiCustomerIpAddress, String xFapiInteractionId,
			String xCustomerUserAgent) {
		// TODO Auto-generated method stub
		return null;
	}*/

	@Override
	public ResponseEntity<OBWriteInternationalStandingOrderConsentResponse1> createInternationalStandingOrderConsents(
			@RequestBody  OBWriteInternationalStandingOrderConsent1 obWriteInternationalStandingOrderConsent1Param,
			String xFapiFinancialId, String authorization, String xIdempotencyKey, String xJwsSignature,
			String xFapiCustomerLastLoggedTime, String xFapiCustomerIpAddress, String xFapiInteractionId,
			String xCustomerUserAgent) {
		if (getObjectMapper().isPresent()) {
			CustomIStandingOrderConsentsPOSTRequest customRequest = new CustomIStandingOrderConsentsPOSTRequest();
			customRequest.setData(obWriteInternationalStandingOrderConsent1Param.getData());
			customRequest.setRisk(obWriteInternationalStandingOrderConsent1Param.getRisk());
			CustomIStandingOrderConsentsPOSTResponse customResponse = iStandingOrderConsentsService
					.createInternationalStandingOrderConsentResource(customRequest);

			return new ResponseEntity<>(customResponse, HttpStatus.CREATED);
		}
		throw PSD2Exception.populatePSD2Exception(
				new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_INVALIDFORMAT, ErrorMapKeys.RESOURCE_INVALIDFORMAT));
	}

}
