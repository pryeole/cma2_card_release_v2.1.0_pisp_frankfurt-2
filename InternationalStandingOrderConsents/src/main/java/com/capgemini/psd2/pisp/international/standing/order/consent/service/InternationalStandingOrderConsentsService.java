package com.capgemini.psd2.pisp.international.standing.order.consent.service;

import com.capgemini.psd2.pisp.domain.CustomIStandingOrderConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIStandingOrderConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;

public interface InternationalStandingOrderConsentsService {

	CustomIStandingOrderConsentsPOSTResponse createInternationalStandingOrderConsentResource(
			CustomIStandingOrderConsentsPOSTRequest customInternationalStandingOrderPOSTRequest);

	CustomIStandingOrderConsentsPOSTResponse retrieveInternationalStandingOrderConsentsResource(
			PaymentRetrieveGetRequest paymentRetrieveRequest);

}
