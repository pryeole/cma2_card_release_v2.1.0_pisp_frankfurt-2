package com.capgemini.psd2.pisp.international.standing.order.consent.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMethod;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.CustomIStandingOrderConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIStandingOrderConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPlatformDetails;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentResponseInfo;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.international.standing.order.consent.comparator.InternationalStandingOrderConsentsPayloadComparator;
import com.capgemini.psd2.pisp.international.standing.order.consent.service.InternationalStandingOrderConsentsService;
import com.capgemini.psd2.pisp.processing.adapter.service.PaymentConsentProcessingAdapter;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.operations.adapter.InternationalStandingOrdersPaymentStagingAdapter;
import com.capgemini.psd2.pisp.status.PaymentConstants;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Service
public class InternationalStandingOrderConsentsServiceImpl implements InternationalStandingOrderConsentsService {

	@Autowired
	private InternationalStandingOrderConsentsPayloadComparator iStandingOrderConsentsComparator;

	@Autowired
	@Qualifier("iStandingOrderConsentsStagingRoutingAdapter")
	private InternationalStandingOrdersPaymentStagingAdapter iStandingOrderStagingAdapter;

	@SuppressWarnings("rawtypes")
	@Autowired
	private PaymentConsentProcessingAdapter paymemtRequestAdapter;
	
	@Value("${cmaVersion}")
	private String cmaVersion;

	@SuppressWarnings("unchecked")
	@Override
	public CustomIStandingOrderConsentsPOSTResponse createInternationalStandingOrderConsentResource(
			CustomIStandingOrderConsentsPOSTRequest customInternationalStandingOrderPOSTRequest) {

		CustomIStandingOrderConsentsPOSTResponse paymentConsentsFoundationResponse = null;
		String requestType = PSD2Constants.PAYMENT_RESOURCE_IDEMPOTENT;

		PaymentConsentsPlatformResource paymentSetupPlatformResource = paymemtRequestAdapter.preConsentProcessFlows(
				customInternationalStandingOrderPOSTRequest,
				populatePlatformDetails(customInternationalStandingOrderPOSTRequest));
		/* Non Idempotent Request */
		if (paymentSetupPlatformResource.getPaymentConsentId() == null) {
			requestType = PSD2Constants.PAYMENT_RESOURCE_NON_IDEMPOTENT;
			/* Create stage call to Adapter */
			paymentConsentsFoundationResponse = processStandingOrderStagingResource(
					customInternationalStandingOrderPOSTRequest, paymentSetupPlatformResource.getCreatedAt());
		}

		/* Idempotent Request */
		else
			paymentConsentsFoundationResponse = retrieveAndCompareDomesticStageResponse(
					customInternationalStandingOrderPOSTRequest, paymentSetupPlatformResource);

		PaymentResponseInfo stageDetails = populateStageDetails(paymentConsentsFoundationResponse);
		stageDetails.setRequestType(requestType);

		return (CustomIStandingOrderConsentsPOSTResponse) paymemtRequestAdapter.postConsentProcessFlows(stageDetails,
				paymentConsentsFoundationResponse, paymentSetupPlatformResource, RequestMethod.POST.toString());

	}

	private CustomPaymentSetupPlatformDetails populatePlatformDetails(
			CustomIStandingOrderConsentsPOSTRequest customInternationalStandingOrderPOSTRequest) {
		CustomPaymentSetupPlatformDetails standingConsentsPlatformDetails = new CustomPaymentSetupPlatformDetails();
		standingConsentsPlatformDetails.setPaymentType(PaymentTypeEnum.INTERNATIONAL_ST_ORD);
		standingConsentsPlatformDetails.setSetupCmaVersion(cmaVersion);
		standingConsentsPlatformDetails = checkDebtorDetails(customInternationalStandingOrderPOSTRequest,
				standingConsentsPlatformDetails);
		return standingConsentsPlatformDetails;

	}

	public CustomPaymentSetupPlatformDetails checkDebtorDetails(
			CustomIStandingOrderConsentsPOSTRequest standingOrderConsentsRequest,
			CustomPaymentSetupPlatformDetails platformDetails) {
		String tppDebtorDetailsStatus = String.valueOf(Boolean.FALSE);
		String tppDebtorNameDetailsStatus = String.valueOf(Boolean.FALSE);
		if (standingOrderConsentsRequest.getData() != null
				&& standingOrderConsentsRequest.getData().getInitiation() != null
				&& standingOrderConsentsRequest.getData().getInitiation().getDebtorAccount() != null) {
			tppDebtorDetailsStatus = String.valueOf(Boolean.TRUE);
			if (!NullCheckUtils
					.isNullOrEmpty(standingOrderConsentsRequest.getData().getInitiation().getDebtorAccount().getName()))
				tppDebtorNameDetailsStatus = String.valueOf(Boolean.TRUE);
		}
		platformDetails.setTppDebtorDetails(tppDebtorDetailsStatus);
		platformDetails.setTppDebtorNameDetails(tppDebtorNameDetailsStatus);
		return platformDetails;
	}

	private CustomIStandingOrderConsentsPOSTResponse processStandingOrderStagingResource(
			CustomIStandingOrderConsentsPOSTRequest internationalPaymentConsentsRequest, String setupCreationDate) {

		/* new fields added */
		internationalPaymentConsentsRequest.setCreatedOn(setupCreationDate);

		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentSetupVersion(cmaVersion);
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.INTERNATIONAL_ST_ORD);

		Map<String, String> paramMap = null;
		return iStandingOrderStagingAdapter.processStandingOrderConsents(internationalPaymentConsentsRequest,
				stageIdentifiers, paramMap, OBExternalConsentStatus1Code.AWAITINGAUTHORISATION,
				OBExternalConsentStatus1Code.REJECTED);
	}

	private CustomIStandingOrderConsentsPOSTResponse retrieveAndCompareDomesticStageResponse(
			CustomIStandingOrderConsentsPOSTRequest customInternationalStandingOrderPOSTRequest,
			PaymentConsentsPlatformResource paymentSetupPlatformResource) {
		CustomIStandingOrderConsentsPOSTResponse paymentConsentsFoundationResponse;

		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentConsentId(paymentSetupPlatformResource.getPaymentConsentId());
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_ST_ORD);
		stageIdentifiers.setPaymentSetupVersion(paymentSetupPlatformResource.getSetupCmaVersion());

		paymentConsentsFoundationResponse = iStandingOrderStagingAdapter
				.retrieveStagedInternationalStandingOrdersConsents(stageIdentifiers, null);

		if (iStandingOrderConsentsComparator.compareStandingOrderDetails(paymentConsentsFoundationResponse,
				customInternationalStandingOrderPOSTRequest, paymentSetupPlatformResource) > 0)
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_CONSENTMISMATCH,
					ErrorMapKeys.RESOURCE_CONSENT_MISMATCH));

		return paymentConsentsFoundationResponse;
	}

	private PaymentResponseInfo populateStageDetails(
			CustomIStandingOrderConsentsPOSTResponse internationalStagingResource) {
		PaymentResponseInfo stageDetails = new PaymentResponseInfo();
		stageDetails.setConsentProcessStatus(internationalStagingResource.getConsentProcessStatus());
		stageDetails.setPaymentConsentId(internationalStagingResource.getData().getConsentId());
		return stageDetails;
	}

	@SuppressWarnings("unchecked")
	@Override
	public CustomIStandingOrderConsentsPOSTResponse retrieveInternationalStandingOrderConsentsResource(
			PaymentRetrieveGetRequest paymentRetrieveRequest) {
		paymentRetrieveRequest.setPaymentTypeEnum(PaymentTypeEnum.INTERNATIONAL_ST_ORD);
		PaymentConsentsPlatformResource paymentConsentsPlatformResponse = paymemtRequestAdapter
				.preConsentProcessGETFlow(paymentRetrieveRequest);

		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentConsentId(paymentRetrieveRequest.getConsentId());
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.INTERNATIONAL_ST_ORD);
		stageIdentifiers.setPaymentSetupVersion(paymentConsentsPlatformResponse.getSetupCmaVersion());

		CustomIStandingOrderConsentsPOSTResponse paymentConsentsFoundationResponse = iStandingOrderStagingAdapter
				.retrieveStagedInternationalStandingOrdersConsents(stageIdentifiers, null);

		if (paymentConsentsFoundationResponse == null) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND,
					ErrorMapKeys.NO_PAYMENT_SETUP_RESOURCES_FOUND));
		}

		PaymentResponseInfo stageDetails = new PaymentResponseInfo();
		stageDetails.setRequestType(PSD2Constants.PAYMENT_RESOURCE_IDEMPOTENT);
		return (CustomIStandingOrderConsentsPOSTResponse) paymemtRequestAdapter.postConsentProcessFlows(stageDetails,
				paymentConsentsFoundationResponse, paymentConsentsPlatformResponse, RequestMethod.GET.toString());
	}

}
