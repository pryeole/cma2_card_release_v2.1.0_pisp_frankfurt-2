/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.delegate;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountSchedulePaymentsResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.constants.AccountSchedulePaymentsFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.raml.domain.PartiesOneOffPaymentInstructionsresponse;
import com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.transformer.AccountSchedulePaymentsFoundationServiceTransformer;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;

/**
 * The Class AccountSchedulePaymentsFoundationServiceDelegate.
 */
@Component
public class AccountSchedulePaymentsFoundationServiceDelegate {
	
	/** The account balance FS transformer. */
	@Autowired
	private AccountSchedulePaymentsFoundationServiceTransformer accountSchedulePaymentsFSTransformer;
	
	@Value("${foundationService.transactioReqHeader:#{X-API-TRANSACTION-ID}}")
	private String transactionReqHeader;
	
	@Value("${foundationService.version}")
	private String version;
	
	@Value("${foundationService.sourcesystem:#{X-API-SOURCE-SYSTEM}}")
	private String sourcesystem;
	
	@Value("${foundationService.sourceUserReqHeader:#{X-API-SOURCE-USER}}")
	private String sourceUserReqHeader;
	
    @Value("${foundationService.channelCodeReqHeader:#{X-API-CHANNEL-CODE}}")
    private String channelCodeReqHeader;
    
    @Value("${foundationService.partySourceNumber:#{X-API-PARTY-SOURCE-ID-NUMBER}}")
	private String partySourceNumber;

	
	/**
	 * Gets the foundation service URL.
	 *
	 * @param accountNSC the account NSC
	 * @param accountNumber the account number
	 * @param baseURL the base URL
	 * @return the foundation service URL
	 */
	public String getFoundationServiceURL(String accountNSC, String accountNumber, String UserId,String baseURL , String endUrl){
		if(NullCheckUtils.isNullOrEmpty(accountNSC)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}
		if(NullCheckUtils.isNullOrEmpty(accountNumber)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}
		if(NullCheckUtils.isNullOrEmpty(baseURL)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}
		if(NullCheckUtils.isNullOrEmpty(version)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}
		if(NullCheckUtils.isNullOrEmpty(endUrl)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}
		if(NullCheckUtils.isNullOrEmpty(UserId)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}
		//return baseURL + "/" + UserId + "/" + "accounts" + "/" +accountNSC + "/" + accountNumber + "/" + endUrl ;
		return baseURL + "/" + version +  "/parties/" + UserId +  "/" + endUrl ;
	}
	
	
	/**
	 * Transform response from FD to API.
	 *
	 * @param accounts the accounts
	 * @param params the params
	 * @return the balances GET response
	 */
	public PlatformAccountSchedulePaymentsResponse transformResponseFromFDToAPI(PartiesOneOffPaymentInstructionsresponse response, Map<String, String> params){
		return accountSchedulePaymentsFSTransformer.transformSchedulePayments(response, params);
	}
	
	/**
	 * Creates the request headers.
	 *
	 * @param requestInfo the request info
	 * @param accountMapping the account mapping
	 * @return the http headers
	 */
	public HttpHeaders createRequestHeaders(RequestInfo requestInfo, AccountMapping accountMapping,Map<String, String> params) {
		HttpHeaders httpHeaders = new HttpHeaders();
		String channelCode = params.get(AccountSchedulePaymentsFoundationServiceConstants.CHANNEL_ID).toUpperCase();
		httpHeaders.add(transactionReqHeader,accountMapping.getCorrelationId());
		httpHeaders.add(sourcesystem, "PSD2API");
		httpHeaders.add(sourceUserReqHeader, accountMapping.getPsuId());
		httpHeaders.add(channelCodeReqHeader, channelCode);
		if(channelCode.equalsIgnoreCase("BOL"))
		httpHeaders.add(partySourceNumber, params.get(PSD2Constants.PARTY_IDENTIFIER));
		return httpHeaders;
	}

}
