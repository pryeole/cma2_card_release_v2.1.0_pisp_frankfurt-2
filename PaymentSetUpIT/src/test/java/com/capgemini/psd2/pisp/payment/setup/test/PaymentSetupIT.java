package com.capgemini.psd2.pisp.payment.setup.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.capgemini.psd2.aisp.domain.AccountGETResponse;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse.StatusEnum;
import com.capgemini.psd2.pisp.payment.setup.config.PaymentSetUpITConfig;
import com.capgemini.psd2.pisp.payment.setup.mock.data.PaymentSetUpITMockData;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = PaymentSetUpITConfig.class)
@WebIntegrationTest
public class PaymentSetupIT {

	@Autowired
	protected RestClientSyncImpl restClient;

	@Value("${app.edgeserverURL}")
	private String edgeserverURL;
	@Value("${app.paymentSetUpURL}")
	private String paymentSetUpURL;

	@Value("${app.refToken}")
	private String refToken;
	@Value("${app.expiredToken}")
	private String expiredToken;
	@Value("${app.refTokenRevoked}")
	private String refTokenRevoked;

	@Value("${app.paymentId}")
	private String paymentId;
	@Value("${app.invalidPaymentId}")
	private String invalidPaymentId;
	@Value("${app.notInDatabasePaymentId}")
	private String notInDatabasePaymentId;
	@Value("${app.diffTppPaymentId}")
	private String diffTppPaymentId;
	
	@Autowired
	protected RestTemplate restTemplate;

	// ----------------- Payment SetUp Post Request IT Cases ----------------

	// SucessFully running CreatePaymentSetupResource method with a valid
	// payment set up request
	@Test
	public void CreatePaymentSetupResourceIsValidPaymentSetupRequestTrueTest() {
		String url = edgeserverURL +paymentSetUpURL  + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.78");
		PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
				.getPaymentSetupPOSTRequestMockData();
		HttpEntity<PaymentSetupPOSTRequest> requestEntity = new HttpEntity<PaymentSetupPOSTRequest>(
				paymentSetupPOSTRequestMockData, headers);

		ResponseEntity<PaymentSetupPOSTResponse> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
				PaymentSetupPOSTResponse.class);
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		PaymentSetupPOSTResponse paymentSetupPOSTResponse = (PaymentSetupPOSTResponse) response.getBody();
		assertNotNull(paymentSetupPOSTResponse);
		PaymentSetupResponse paymentSetupResponse = paymentSetupPOSTResponse.getData();
		assertNotNull(paymentSetupResponse.getCreationDateTime());
		assertNotNull(paymentSetupResponse.getPaymentId());
		assertEquals(paymentSetupPOSTRequestMockData.getData().getInitiation().getInstructedAmount().getAmount(),
				paymentSetupResponse.getInitiation().getInstructedAmount().getAmount());
		assertEquals(0, paymentSetupResponse.getStatus().compareTo(StatusEnum.ACCEPTEDTECHNICALVALIDATION));
		assertEquals(paymentSetupPOSTRequestMockData.getRisk().getDeliveryAddress().getPostCode(),
				paymentSetupPOSTResponse.getRisk().getDeliveryAddress().getPostCode());
	}

	@Test(expected = HttpClientErrorException.class)
	public void revokedAccessTokenTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refTokenRevoked);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.78");
		PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
				.getPaymentSetupPOSTRequestMockData();
		HttpEntity<PaymentSetupPOSTRequest> requestEntity = new HttpEntity<PaymentSetupPOSTRequest>(
				paymentSetupPOSTRequestMockData, headers);
		try {
			restTemplate.exchange(url, HttpMethod.POST,
					requestEntity, PaymentSetupPOSTResponse.class);
		} catch (HttpClientErrorException e) {
			assertEquals(HttpStatus.UNAUTHORIZED, e.getStatusCode());
			throw e;
		}
	}

	@Test(expected = HttpClientErrorException.class)
	public void otherThanPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.78");
		PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
				.getPaymentSetupPOSTRequestMockData();
		HttpEntity<PaymentSetupPOSTRequest> requestEntity = new HttpEntity<PaymentSetupPOSTRequest>(
				paymentSetupPOSTRequestMockData, headers);
		try {
			restTemplate.exchange(url, HttpMethod.PUT,
					requestEntity, PaymentSetupPOSTResponse.class);
		} catch (HttpClientErrorException e) {
			assertEquals(HttpStatus.METHOD_NOT_ALLOWED, e.getStatusCode());
			throw e;
		}
	}

	// data mismatch at the bank side and the in the sent payload
	// when same request is sent twice
	@Test
	public void idempotencyFailureTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.79");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
				.getPaymentSetupPOSTRequestMockData();
		try {
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			paymentSetupPOSTRequestMockData.getData().getInitiation().getDebtorAccount()
					.setSecondaryIdentification("ABCDEFGHIJKLMNOPQRSTUVWX");

			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("While doing payment idempotency check, payment request & bank response are not matched.",
					e.getErrorInfo().getErrorMessage());
		}
	}

	// executing CreatePaymentSetupResource method with invalid payment set up
	// request
	// i.e. mandatory header x-idempotency-key not passed in the request
	@Test(expected = PSD2Exception.class)
	public void idempotencyKeyHeaderMissingTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			restClient.callForPost(requestInfo, PaymentSetUpITMockData.getPaymentSetupPOSTRequestMockData(),
					PaymentSetupPOSTResponse.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(HttpStatus.BAD_REQUEST.value(), Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Header 'x-idempotency-key' can not be blank.", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	// executing CreatePaymentSetupResource method with invalid payment set up
	// request
	// i.e. mandatory header x-idempotency-key header length > 40
	@Test(expected = PSD2Exception.class)
	public void invalidIdempotencyKeyTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCOABCDEFGH.213021234.GFXLOBIJ.5712345");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			restClient.callForPost(requestInfo, PaymentSetUpITMockData.getPaymentSetupPOSTRequestMockData(),
					PaymentSetupPOSTResponse.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(HttpStatus.BAD_REQUEST.value(), Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Length of header 'x-idempotency-key' can not be more than defined size.",
					e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	// executing CreatePaymentSetupResource method with invalid payment set up
	// request
	// i.e. Null fields in PaymentSetupPOSTRequest payload
	@Test(expected = PSD2Exception.class)
	public void nullDataInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			restClient.callForPost(requestInfo, new PaymentSetupPOSTRequest(), PaymentSetupPOSTResponse.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(HttpStatus.BAD_REQUEST.value(), Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Field 'Data' is missing from payment request payload.", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	// executing CreatePaymentSetupResource method with invalid payment set up
	// request
	// i.e. Null PaymentSetupPOSTRequest payload
	@Test(expected = PSD2Exception.class)
	public void nullPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequest = null;
			restClient.callForPost(requestInfo, paymentSetupPOSTRequest, PaymentSetupPOSTResponse.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), Integer.parseInt(e.getErrorInfo().getStatusCode()));
			throw e;
		}
	}

	/** Value of x-fapifinancial-id is missing in the request header. */
	@Test(expected = PSD2Exception.class)
	public void fapiMandatoryHeaderMissingTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			restClient.callForPost(requestInfo, PaymentSetUpITMockData.getPaymentSetupPOSTRequestMockData(),
					PaymentSetupPOSTResponse.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(HttpStatus.BAD_REQUEST.value(), Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("x-fapi-financial-id is missing in headers.", e.getErrorInfo().getDetailErrorMessage());
			throw e;
		}
	}

	// Auth code is absent //401 UNAUTHORIZED
	/** Authorization header is null or blank and sent in the request */
	@Test(expected = HttpClientErrorException.class)
	public void noAuthTokenGivenTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			HttpEntity<?> requestEntity = new HttpEntity<>(headers);
			restTemplate.exchange(url, HttpMethod.GET, requestEntity, Object.class);
		} catch (HttpClientErrorException e) {
			assertEquals(HttpStatus.UNAUTHORIZED, e.getStatusCode());
			// assertEquals("Full authentication is required to access this
			// resource", e.getResponseBodyAsString());
			throw e;
		}
	}

	/** Expired Authorization code is sent in the request */
	@Test(expected = PSD2Exception.class)
	public void expiredAuthorizationTokenTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", expiredToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			restClient.callForGet(requestInfo, AccountGETResponse.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(HttpStatus.UNAUTHORIZED.toString(), e.getErrorInfo().getStatusCode());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void nullRiskInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.setRisk(null);
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Field 'Risk' is missing from request payload", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test
	public void blankAddressLineInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.76");
		PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
				.getPaymentSetupPOSTRequestMockData();
		List<String> adrressLineList = new ArrayList<>();
		paymentSetupPOSTRequestMockData.getRisk().getDeliveryAddress().setAddressLine(adrressLineList);
		HttpEntity<PaymentSetupPOSTRequest> requestEntity = new HttpEntity<PaymentSetupPOSTRequest>(
				paymentSetupPOSTRequestMockData, headers);

		ResponseEntity<PaymentSetupPOSTResponse> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
				PaymentSetupPOSTResponse.class);
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		assertNotNull(response.getBody().getData().getPaymentId());
	}

	@Test
	public void blankValueForCountrySubDivisionInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.80");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
				.getPaymentSetupPOSTRequestMockData();
		List<String> countrySubDivisionList = new ArrayList<>();
		paymentSetupPOSTRequestMockData.getRisk().getDeliveryAddress().setCountrySubDivision(countrySubDivisionList);
		HttpEntity<PaymentSetupPOSTRequest> requestEntity = new HttpEntity<PaymentSetupPOSTRequest>(
				paymentSetupPOSTRequestMockData, headers);

		ResponseEntity<PaymentSetupPOSTResponse> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
				PaymentSetupPOSTResponse.class);
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		assertNotNull(response.getBody().getData().getPaymentId());
	}

	@Test(expected = PSD2Exception.class)
	public void moreThan16CharacterForPostCodeInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getRisk().getDeliveryAddress().setPostCode("7412589630123654789");
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void blankValueForPostCodeInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getRisk().getDeliveryAddress().setPostCode("");
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test
	public void entered16CharacterForPostCodeInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.75");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
				.getPaymentSetupPOSTRequestMockData();
		paymentSetupPOSTRequestMockData.getRisk().getDeliveryAddress().setPostCode("7418529637418529");
		HttpEntity<PaymentSetupPOSTRequest> requestEntity = new HttpEntity<PaymentSetupPOSTRequest>(
				paymentSetupPOSTRequestMockData, headers);

		ResponseEntity<PaymentSetupPOSTResponse> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
				PaymentSetupPOSTResponse.class);
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		assertNotNull(response.getBody().getData().getPaymentId());
	}

	@Test
	public void numericValueForPostCodeInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.74");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);

		PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
				.getPaymentSetupPOSTRequestMockData();
		paymentSetupPOSTRequestMockData.getRisk().getDeliveryAddress().setPostCode("789654");
		HttpEntity<PaymentSetupPOSTRequest> requestEntity = new HttpEntity<PaymentSetupPOSTRequest>(
				paymentSetupPOSTRequestMockData, headers);

		ResponseEntity<PaymentSetupPOSTResponse> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
				PaymentSetupPOSTResponse.class);
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		assertNotNull(response.getBody().getData().getPaymentId());
	}

	@Test
	public void exactOneValueForPostCodeInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.73");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);

		PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
				.getPaymentSetupPOSTRequestMockData();
		paymentSetupPOSTRequestMockData.getRisk().getDeliveryAddress().setPostCode("1");
		HttpEntity<PaymentSetupPOSTRequest> requestEntity = new HttpEntity<PaymentSetupPOSTRequest>(
				paymentSetupPOSTRequestMockData, headers);

		ResponseEntity<PaymentSetupPOSTResponse> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
				PaymentSetupPOSTResponse.class);
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		assertNotNull(response.getBody().getData().getPaymentId());
	}

	@Test
	public void combinationOfNumberAndCharacterForPostCodeInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.72");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);

		PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
				.getPaymentSetupPOSTRequestMockData();
		paymentSetupPOSTRequestMockData.getRisk().getDeliveryAddress().setPostCode("A234F45Y7");
		HttpEntity<PaymentSetupPOSTRequest> requestEntity = new HttpEntity<PaymentSetupPOSTRequest>(
				paymentSetupPOSTRequestMockData, headers);

		ResponseEntity<PaymentSetupPOSTResponse> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
				PaymentSetupPOSTResponse.class);
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		assertNotNull(response.getBody().getData().getPaymentId());
	}

	@Test(expected = PSD2Exception.class)
	public void nullCountryNameInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getRisk().getDeliveryAddress().setCountry(null);
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void moreThanTwoCharactersForCountryNameInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getRisk().getDeliveryAddress().setCountry("INDI");
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void lessThanTwoCharactersForCountryNameInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getRisk().getDeliveryAddress().setCountry("I");
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void countryNameNotAsPerIsoInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getRisk().getDeliveryAddress().setCountry("INDIA");
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test
	public void countryNameAsPerIsoAnd2CharactersInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.71");

		PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
				.getPaymentSetupPOSTRequestMockData();
		paymentSetupPOSTRequestMockData.getRisk().getDeliveryAddress().setCountry("IN");
		HttpEntity<PaymentSetupPOSTRequest> requestEntity = new HttpEntity<PaymentSetupPOSTRequest>(
				paymentSetupPOSTRequestMockData, headers);

		ResponseEntity<PaymentSetupPOSTResponse> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
				PaymentSetupPOSTResponse.class);
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		assertNotNull(response.getBody().getData().getPaymentId());

	}

	@Test(expected = PSD2Exception.class)
	public void nullSchemeNameInDebtorAgentInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getData().getInitiation().getDebtorAgent().setSchemeName(null);
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void nullDebtorAgentInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getData().getInitiation().setDebtorAgent(null);

			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Debtor account details are found but field 'DebtorAgent' details are missing.",
					e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void setMoreThan35CharIdentificationInDebtorAgentInPaymentSetupResourcePayloadTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getData().getInitiation().getDebtorAgent()
					.setIdentification("SCUYGJGLHYFRDSDJNDE#$@%*&**YHHJGDRHKJ");

			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void nullIdentificationInDebtorAgentInPaymentSetUpITMockData() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getData().getInitiation().getDebtorAgent().setIdentification("");
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test
	public void set35CharInIdentificationInDebtorAgentInPaymentSetUpITMockData() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.70");
		PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
				.getPaymentSetupPOSTRequestMockData();
		paymentSetupPOSTRequestMockData.getData().getInitiation().getDebtorAgent()
				.setIdentification("SCUYGJGLHYFRDSDJNDE#$@%*&**YHHJGDRH");

		HttpEntity<PaymentSetupPOSTRequest> requestEntity = new HttpEntity<PaymentSetupPOSTRequest>(
				paymentSetupPOSTRequestMockData, headers);

		ResponseEntity<PaymentSetupPOSTResponse> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
				PaymentSetupPOSTResponse.class);
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		assertNotNull(response.getBody().getData().getPaymentId());
	}

	@Test(expected = PSD2Exception.class)
	public void setMoreThan35CharEndToEndIdentificationInPaymentSetupInPaymentSetUpPayload() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getData().getInitiation()
					.setEndToEndIdentification("FS_PMPSV_002YDSHKZONRDRDZJABNJL:AYYCSLSLA");

			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void blankEndToEndIdentificationInPaymentSetupInPaymentSetUpPayload() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getData().getInitiation().setEndToEndIdentification("");
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test
	public void setChar1to35EndToEndIdentificationInPaymentSetupInPaymentSetUpITMockData() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.69");

		PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
				.getPaymentSetupPOSTRequestMockData();
		paymentSetupPOSTRequestMockData.getData().getInitiation()
				.setEndToEndIdentification("FS_PMPSV_002YDSHKZONRDRDZJABNJL:AYY");
		HttpEntity<PaymentSetupPOSTRequest> requestEntity = new HttpEntity<PaymentSetupPOSTRequest>(
				paymentSetupPOSTRequestMockData, headers);

		ResponseEntity<PaymentSetupPOSTResponse> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
				PaymentSetupPOSTResponse.class);
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		assertNotNull(response.getBody().getData().getPaymentId());
	}

	@Test(expected = PSD2Exception.class)
	public void nullCurrencyInInstructedAmountInPaymentSetupInPaymentSetUpITMockData() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getData().getInitiation().getInstructedAmount().setCurrency(null);

			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("currency : may not be null;", e.getErrorInfo().getDetailErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void blankAmountInInstructedAmountInPaymentSetupInPaymentSetUpITPayload() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getData().getInitiation().getInstructedAmount().setAmount("");
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void setNegativeAmountInInstructedAmountInPaymentSetupInPaymentSetUpITPayload() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getData().getInitiation().getInstructedAmount().setAmount("-7777");

			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void setFiveDigitAmountInInstructedAmountInPaymentSetupInPaymentSetUpITMockData() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getData().getInitiation().getInstructedAmount().setAmount(".888881");

			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void setThirteenDigitAmountInInstructedAmountInPaymentSetupInPaymentSetUpPayload() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getData().getInitiation().getInstructedAmount().setAmount("8888876548976");

			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void setThirteenDigitPrecisionMoreThanFiveAmountInInstructedAmountInPaymentSetupPayload() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getData().getInitiation().getInstructedAmount()
					.setAmount("8888876548976.2345567");

			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test
	public void setThirteenDigitPrecisionFiveAmountInInstructedAmountInPaymentSetupInPaymentSetUpPayload() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.68");

		PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
				.getPaymentSetupPOSTRequestMockData();
		paymentSetupPOSTRequestMockData.getData().getInitiation().getInstructedAmount()
				.setAmount("8888876548976.23455");

		HttpEntity<PaymentSetupPOSTRequest> requestEntity = new HttpEntity<PaymentSetupPOSTRequest>(
				paymentSetupPOSTRequestMockData, headers);

		ResponseEntity<PaymentSetupPOSTResponse> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
				PaymentSetupPOSTResponse.class);
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		assertNotNull(response.getBody().getData().getPaymentId());

	}

	@Test
	public void setCurrencyInInstructedAmountInPaymentSetupInPaymentSetUpPayload() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.67");

		PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
				.getPaymentSetupPOSTRequestMockData();
		paymentSetupPOSTRequestMockData.getData().getInitiation().getInstructedAmount().setCurrency("EUR");

		HttpEntity<PaymentSetupPOSTRequest> requestEntity = new HttpEntity<PaymentSetupPOSTRequest>(
				paymentSetupPOSTRequestMockData, headers);

		ResponseEntity<PaymentSetupPOSTResponse> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
				PaymentSetupPOSTResponse.class);
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		assertNotNull(response.getBody().getData().getPaymentId());
	}

	@Test(expected = PSD2Exception.class)
	public void setCurrencyValueMoreThanThreeInInstructedAmountInPaymentSetupInPaymentSetUpITMockData() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getData().getInitiation().getInstructedAmount().setCurrency("GBPR");

			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void setCurrencyValueLessThanThreeInInstructedAmountInPaymentSetupInPaymentSetUpITMockData() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getData().getInitiation().getInstructedAmount().setCurrency("12");

			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test
	public void setInstructionIdentificationMoreThan35CharInPaymentSetupInPaymentSetUpITMockData() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
				.getPaymentSetupPOSTRequestMockData();
		paymentSetupPOSTRequestMockData.getData().getInitiation()
				.setInstructionIdentification("ABDDCNSGDFNC:2RHXKJKLFKMfciyuncbiwe");
		try {
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void BlankInstructionIdentificationInPaymentSetupInPaymentSetUpITMockData() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getData().getInitiation().setInstructionIdentification("");
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test
	public void setInstructionIdentificationToThirtyFiveCharInPaymentSetupInPaymentSetUpITMockData() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.66");
		PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
				.getPaymentSetupPOSTRequestMockData();
		paymentSetupPOSTRequestMockData.getData().getInitiation()
				.setInstructionIdentification("ABDDCNSGDFNC:2RHXKJKLFKMCLJHCXUGCUI");
		HttpEntity<PaymentSetupPOSTRequest> requestEntity = new HttpEntity<PaymentSetupPOSTRequest>(
				paymentSetupPOSTRequestMockData, headers);

		ResponseEntity<PaymentSetupPOSTResponse> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
				PaymentSetupPOSTResponse.class);
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		assertNotNull(response.getBody().getData().getPaymentId());
	}

	@Test
	public void nullRemittanceInformationInPaymentSetupInPaymentSetUpITMockData() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.84");

		PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
				.getPaymentSetupPOSTRequestMockData();
		paymentSetupPOSTRequestMockData.getData().getInitiation().setRemittanceInformation(null);
		HttpEntity<PaymentSetupPOSTRequest> requestEntity = new HttpEntity<PaymentSetupPOSTRequest>(
				paymentSetupPOSTRequestMockData, headers);

		ResponseEntity<PaymentSetupPOSTResponse> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
				PaymentSetupPOSTResponse.class);
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		assertNotNull(response.getBody().getData().getPaymentId());

	}

	@Test(expected = PSD2Exception.class)
	public void setReferenceMoreThanThirtyFiveCharInRemittanceInformationInPaymentSetupInPaymentSetUpITMockData() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getData().getInitiation().getRemittanceInformation()
					.setReference("AJWHDVCSBNSKDLDUOSDJ.LSMDKSDGYUAGDUYKWND");
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void blankReferenceInRemittanceInformationInPaymentSetupInPaymentSetUpITMockData() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getData().getInitiation().getRemittanceInformation().setReference("");

			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test
	public void setReferenceThirtyFiveCharInRemittanceInformationInPaymentSetupInPaymentSetUpITMockData() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.65");
		PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
				.getPaymentSetupPOSTRequestMockData();
		paymentSetupPOSTRequestMockData.getData().getInitiation().getRemittanceInformation()
				.setReference("AJWHDVCSBNSKDLDUOSDJ.LSMDKSDGY");

		HttpEntity<PaymentSetupPOSTRequest> requestEntity = new HttpEntity<PaymentSetupPOSTRequest>(
				paymentSetupPOSTRequestMockData, headers);

		ResponseEntity<PaymentSetupPOSTResponse> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
				PaymentSetupPOSTResponse.class);
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		assertNotNull(response.getBody().getData().getPaymentId());
	}

	@Test(expected = PSD2Exception.class)
	public void setReferenceMoreThanOneFortyCharInRemittanceInformationInPaymentSetupInPaymentSetUpITMockData() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getData().getInitiation().getRemittanceInformation().setReference(
					"java.lang.AssertionError:Expectedexceptioncom.capgemini.psd2.exceptions.PSD2Exceptionatorg.junit.internal.runners.statements.ExpectExcept");

			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void blankUnstructuredInRemittanceInformationInPaymentSetupInPaymentSetUpITMockData() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getData().getInitiation().getRemittanceInformation().setUnstructured("");

			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test
	public void setReferenceOneFortyCharInRemittanceInformationInPaymentSetupInPaymentSetUpITMockData() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.64");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
				.getPaymentSetupPOSTRequestMockData();
		paymentSetupPOSTRequestMockData.getData().getInitiation().getRemittanceInformation()
				.setReference("140 characters");
		HttpEntity<PaymentSetupPOSTRequest> requestEntity = new HttpEntity<PaymentSetupPOSTRequest>(
				paymentSetupPOSTRequestMockData, headers);

		ResponseEntity<PaymentSetupPOSTResponse> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
				PaymentSetupPOSTResponse.class);
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		assertNotNull(response.getBody().getData().getPaymentId());
	}

	// remove initiation
	@Test(expected = PSD2Exception.class)
	public void removeInitiationWithNullPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getData().setInitiation(null);
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Field 'Initiation' is missing from payment request payload.",
					e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	// ---------------- Creditor Account Validations test -------------

	// remove creditor account
	@Test(expected = PSD2Exception.class)
	public void removeCreditorAccountWithNullInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getData().getInitiation().setCreditorAccount(null);
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Field 'CreditorAccount' is missing from request payload", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	// remove Identification in CreditorAccount
	@Test(expected = PSD2Exception.class)
	public void removeIdentificationinCreditorAccountWithNullInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getData().getInitiation().getCreditorAccount().setIdentification(null);
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	// set Identification in CreditorAccount with more than 34 characters
	@Test(expected = PSD2Exception.class)
	public void setIdentificationinCreditorAccountWithMoreThan34CharactersInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getData().getInitiation().getCreditorAccount()
					.setIdentification("RRTSPXYZABCILOPGEMIBDVHUBHREYHIKPWZRS");
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	// blank value for Identification in CreditorAccount
	@Test(expected = PSD2Exception.class)
	public void setIdentificationinCreditorAccounttWithBlankInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getData().getInitiation().getCreditorAccount().setIdentification("");
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	// set Identification in CreditorAccount with 34 characters
	@Test
	public void setIdentificationinCreditorAccountWith34CharactersInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.63");
		PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
				.getPaymentSetupPOSTRequestMockData();
		paymentSetupPOSTRequestMockData.getData().getInitiation().getCreditorAccount()
				.setIdentification("RRTSPXYZABCILOPGEMIBDVHUBHREYHIYUV");
		HttpEntity<PaymentSetupPOSTRequest> requestEntity = new HttpEntity<PaymentSetupPOSTRequest>(
				paymentSetupPOSTRequestMockData, headers);

		ResponseEntity<PaymentSetupPOSTResponse> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
				PaymentSetupPOSTResponse.class);
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		assertNotNull(response.getBody().getData().getPaymentId());
	}

	// more than 70 characters name in CreditorAccount
	@Test(expected = PSD2Exception.class)
	public void setNameinCreditorAccountWithMoreThan70CharactersInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getData().getInitiation().getCreditorAccount()
					.setName("RRTSPXYZABCILOPGEMIBDVHUBHREYHIYURHDGJGDGGDGGEDGGDJGDGHGDGGGHBVHDOIOPLOJBDVYFYF");
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}

	}

	// blank name in CreditorAccount
	@Test(expected = PSD2Exception.class)
	public void setNameinCreditorAccountWithBlankInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getData().getInitiation().getCreditorAccount().setName("");
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}

	}

	// 1- 70 characters name in CreditorAccount
	@Test
	public void setNameinCreditorAccountWith70CharactersInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.62");

		PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
				.getPaymentSetupPOSTRequestMockData();
		paymentSetupPOSTRequestMockData.getData().getInitiation().getCreditorAccount()
				.setName("ABCDEFGHIJKLMNOPQRSTUVWXYZYUOPKHGTRESDFGYHUWBGCVFRTEGYUSJOPH");
		HttpEntity<PaymentSetupPOSTRequest> requestEntity = new HttpEntity<PaymentSetupPOSTRequest>(
				paymentSetupPOSTRequestMockData, headers);
		ResponseEntity<PaymentSetupPOSTResponse> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
				PaymentSetupPOSTResponse.class);
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		assertNotNull(response.getBody().getData().getPaymentId());
	}

	// set SecondaryIdentification in CreditorAccount With >34 Characters
	@Test(expected = PSD2Exception.class)
	public void setSecondaryIdentificationinCreditorAccountsWithMoreThan34CharactersInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getData().getInitiation().getCreditorAccount()
					.setSecondaryIdentification("ABCDEFGHIJKLMNOPQRSTUVWXYZvmsbvjkhdsvh");
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}

	}

	// set Secondary Identification in CreditorAccount With Blank
	@Test(expected = PSD2Exception.class)
	public void setSecondaryIdentificationinCreditorAccountWithBlankInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getData().getInitiation().getCreditorAccount()
					.setSecondaryIdentification("");
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}

	}

	// set Secondary Identification in CreditorAccount With 1-34 Characters
	@Test
	public void setSecondaryIdentificationinCreditorAccountWith34CharactersInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.61");
		PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
				.getPaymentSetupPOSTRequestMockData();
		paymentSetupPOSTRequestMockData.getData().getInitiation().getCreditorAccount()
				.setSecondaryIdentification("ABCDEFGHIJKLMNOPQRSTUVWX");
		HttpEntity<PaymentSetupPOSTRequest> requestEntity = new HttpEntity<PaymentSetupPOSTRequest>(
				paymentSetupPOSTRequestMockData, headers);
		ResponseEntity<PaymentSetupPOSTResponse> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
				PaymentSetupPOSTResponse.class);
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		assertNotNull(response.getBody().getData().getPaymentId());
	}

	// not Enter Some Mandatory Fields Of Creditor Agent
	@Test(expected = PSD2Exception.class)
	public void notEnterSomeMandatoryFieldsOfCreditorAgentInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getData().getInitiation().getCreditorAgent().setIdentification(null);
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}

	}

	// set Identification of CreditorAgent With More Than 35 Characters
	@Test(expected = PSD2Exception.class)
	public void setIdentificationOfCreditorAgentWithMoreThan35CharactersInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getData().getInitiation().getCreditorAgent()
					.setIdentification("ABCDEFGHIJKLMNOPQRSTUVWXYZABHC123456");
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	// set Identification of CreditorAgent With Blank
	@Test(expected = PSD2Exception.class)
	public void setIdentificationOfCreditorAgentWithNullInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getData().getInitiation().getCreditorAgent().setIdentification("");
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}

	}

	// set Identification of CreditorAgent With 35 Characters
	@Test
	public void setIdentificationOfCreditorAgentWith35CharactersInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.60");
		PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
				.getPaymentSetupPOSTRequestMockData();
		paymentSetupPOSTRequestMockData.getData().getInitiation().getCreditorAgent()
				.setIdentification("ABCDEFGHIJKLMNOPQRSTUVWXY");
		HttpEntity<PaymentSetupPOSTRequest> requestEntity = new HttpEntity<PaymentSetupPOSTRequest>(
				paymentSetupPOSTRequestMockData, headers);
		ResponseEntity<PaymentSetupPOSTResponse> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
				PaymentSetupPOSTResponse.class);
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		assertNotNull(response.getBody().getData().getPaymentId());
	}

	// ------------------Creditor Agent Validations end ------------
	// ------------------ debtor Accounts Validations start----------------

	// remove Identification in DebtorAccounts
	@Test(expected = PSD2Exception.class)
	public void removeIdentificationinDebtorAccountsInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getData().getInitiation().getDebtorAccount().setIdentification(null);
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("identification : may not be null;", e.getErrorInfo().getDetailErrorMessage());
			throw e;
		}

	}

	// remove Debtor Account
	@Test(expected = PSD2Exception.class)
	public void removeDebtorAccountInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getData().getInitiation().setDebtorAccount(null);
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Debtor agent details are found but field 'DebtorAccount' details are missing.",
					e.getErrorInfo().getErrorMessage());
			throw e;
		}

	}

	// set identification in debtor account with more than 34 characters
	@Test(expected = PSD2Exception.class)
	public void setIdentificationinDebtorAccountsWithMoreThan34CharactersInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getData().getInitiation().getDebtorAccount()
					.setIdentification("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("IBAN Identification value of Creditor/Debtor Account is not valid.",
					e.getErrorInfo().getErrorMessage());
			throw e;
		}

	}

	// set blank identification in debtor account
	@Test(expected = PSD2Exception.class)
	public void setIdentificationinDebtorAccountsWithBlankInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getData().getInitiation().getDebtorAccount().setIdentification("");
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}

	}

	// set identification in debtor account with 1- 34 characters
	@Test
	public void setIdentificationinDebtorAccountsWith34CharactersInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.77");
		PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
				.getPaymentSetupPOSTRequestMockData();
		paymentSetupPOSTRequestMockData.getData().getInitiation().getDebtorAccount()
				.setIdentification("GB29NWBK60161331926819");
		HttpEntity<PaymentSetupPOSTRequest> requestEntity = new HttpEntity<PaymentSetupPOSTRequest>(
				paymentSetupPOSTRequestMockData, headers);

		ResponseEntity<PaymentSetupPOSTResponse> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
				PaymentSetupPOSTResponse.class);
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		assertNotNull(response.getBody().getData().getPaymentId());
	}

	// set name in debtor account with more than 70 characters
	@Test(expected = PSD2Exception.class)
	public void setNameinDebtorAccountsWithMoreThan70CharactersInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getData().getInitiation().getDebtorAccount()
					.setName("ABCDEFGHIJKLMNOPQRSTUVWXYZARTYUIOPLKJHGFDSAZXCVBNMQWERTYUIOPLKJH1234csfdv");
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}

	}

	// set blank name in debtor account
	@Test(expected = PSD2Exception.class)
	public void setNameinDebtorAccountsWithBlankInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getData().getInitiation().getDebtorAccount().setName("");
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}

	}

	// set name in debtor account with 70 characters
	@Test
	public void setNameinDebtorAccountsWith70CharactersInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.59");

		PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
				.getPaymentSetupPOSTRequestMockData();
		paymentSetupPOSTRequestMockData.getData().getInitiation().getDebtorAccount()
				.setName("ABCDEFGHIJKLMNOPQRSTUVWXYZREFDIKLPGHTRDVBJKLHMNERTSDFGHBCOPL");
		HttpEntity<PaymentSetupPOSTRequest> requestEntity = new HttpEntity<PaymentSetupPOSTRequest>(
				paymentSetupPOSTRequestMockData, headers);
		ResponseEntity<PaymentSetupPOSTResponse> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
				PaymentSetupPOSTResponse.class);
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		assertNotNull(response.getBody().getData().getPaymentId());

	}

	// SecondaryIdentification in DebtorAccounts With More Than 34Characters
	@Test(expected = PSD2Exception.class)
	public void setSecondaryIdentificationinDebtorAccountsWithMoreThan34CharactersInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getData().getInitiation().getDebtorAccount()
					.setSecondaryIdentification("ABCDEFGHIJKLMNOPQRSTUVWXYZ123456feghijl");
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("secondaryIdentification : size must be between 1 and 34;",
					e.getErrorInfo().getDetailErrorMessage());
			throw e;
		}

	}

	// set SecondaryIdentification in DebtorAccounts With Blank
	@Test(expected = PSD2Exception.class)
	public void setSecondaryIdentificationinDebtorAccountsWithblankInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getData().getInitiation().getDebtorAccount().setSecondaryIdentification("");
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("secondaryIdentification : size must be between 1 and 34;",
					e.getErrorInfo().getDetailErrorMessage());
			throw e;
		}

	}

	// set SecondaryIdentification in DebtorAccounts With 1- 34 characters
	@Test()
	public void setSecondaryIdentificationinDebtorAccountsWith34CharactersInPaymentSetupPOSTRequestTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.58");
		PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
				.getPaymentSetupPOSTRequestMockData();
		paymentSetupPOSTRequestMockData.getData().getInitiation().getDebtorAccount()
				.setSecondaryIdentification("ABCDEFGHIJKLMNOPQRSTUVWX");
		HttpEntity<PaymentSetupPOSTRequest> requestEntity = new HttpEntity<PaymentSetupPOSTRequest>(
				paymentSetupPOSTRequestMockData, headers);
		ResponseEntity<PaymentSetupPOSTResponse> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
				PaymentSetupPOSTResponse.class);
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
	}

	@Test(expected = PSD2Exception.class)
	public void setTownNameMoreThan35CharInPaymentSetupPOSTRequestPayloadTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getRisk().getDeliveryAddress()
					.setTownName("AJWHDVCSBNSKDLDUOSDJLSMDKSDGYUAGDdskfhj");
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void setTownNameBlankInPaymentSetupPOSTRequestPayloadTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getRisk().getDeliveryAddress().setTownName("");
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test()
	public void setTownNameBetween1To35CharInPaymentSetupPOSTRequestPayloadTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.81");
		PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
				.getPaymentSetupPOSTRequestMockData();
		paymentSetupPOSTRequestMockData.getRisk().getDeliveryAddress().setTownName("dsjcique");
		HttpEntity<PaymentSetupPOSTRequest> requestEntity = new HttpEntity<PaymentSetupPOSTRequest>(
				paymentSetupPOSTRequestMockData, headers);

		ResponseEntity<PaymentSetupPOSTResponse> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
				PaymentSetupPOSTResponse.class);
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		assertNotNull(response.getBody().getData().getPaymentId());
	}

	@Test(expected = PSD2Exception.class)
	public void setStreetNameMoreThan70CharInPaymentSetupPOSTRequestPayloadTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getRisk().getDeliveryAddress()
					.setStreetName("AJWHDVCSBNSKDLDUOSDJLSMDKSDGYUAGDdskfhjjvbjhdiknskcbiwyehbidsnchDEVCINCBydgec");
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void setStreetNameBlankInPaymentSetupPOSTRequestPayloadTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getRisk().getDeliveryAddress().setStreetName("");
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test()
	public void setStreetNameBetween1To70CharInPaymentSetupPOSTRequestPayloadTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.82");
		PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
				.getPaymentSetupPOSTRequestMockData();
		paymentSetupPOSTRequestMockData.getRisk().getDeliveryAddress().setStreetName("Street 51");
		HttpEntity<PaymentSetupPOSTRequest> requestEntity = new HttpEntity<PaymentSetupPOSTRequest>(
				paymentSetupPOSTRequestMockData, headers);

		ResponseEntity<PaymentSetupPOSTResponse> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
				PaymentSetupPOSTResponse.class);
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		assertNotNull(response.getBody().getData().getPaymentId());
	}

	@Test(expected = PSD2Exception.class)
	public void setMerchantCategoryCodeLessThan3CharInPaymentSetupPOSTRequestPayloadTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getRisk().setMerchantCategoryCode("CA");
			;
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("merchantCategoryCode : size must be between 3 and 4;",
					e.getErrorInfo().getDetailErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void setMerchantCategoryCodeMoreThan4CharInPaymentSetupPOSTRequestPayloadTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getRisk().setMerchantCategoryCode("CATEG");
			;
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("merchantCategoryCode : size must be between 3 and 4;",
					e.getErrorInfo().getDetailErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void setMerchantCustomeridentificationMoreThan70CharInPaymentSetupPOSTRequestPayloadTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
					.getPaymentSetupPOSTRequestMockData();
			paymentSetupPOSTRequestMockData.getRisk().setMerchantCustomerIdentification(
					"AJWHDVCSBNSKDLDUOSDJLSMDKSDGYUAGDdskfhjjvbjhdiknskcbiwyehbidsnchDEVCINCBydgec");
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void setMerchantCustomeridentificationBlankInPaymentSetupPOSTRequestPayloadTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.83");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
				.getPaymentSetupPOSTRequestMockData();
		paymentSetupPOSTRequestMockData.getRisk().setMerchantCustomerIdentification("");
		try {
			restClient.callForPost(requestInfo, paymentSetupPOSTRequestMockData, PaymentSetupPOSTResponse.class,
					headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test
	public void setMerchantCustomeridentificationBetween1to70CharInPaymentSetupPOSTRequestPayloadTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.83");
		PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData = PaymentSetUpITMockData
				.getPaymentSetupPOSTRequestMockData();
		paymentSetupPOSTRequestMockData.getRisk().setMerchantCustomerIdentification("MerchantCategory");
		HttpEntity<PaymentSetupPOSTRequest> requestEntity = new HttpEntity<PaymentSetupPOSTRequest>(
				paymentSetupPOSTRequestMockData, headers);

		ResponseEntity<PaymentSetupPOSTResponse> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
				PaymentSetupPOSTResponse.class);
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		assertNotNull(response.getBody().getData().getPaymentId());

	}
	// --------------- Debtor Accounts End -----------------------

	// ------------ Single PaymentId Payment GET request IT -----------
	@Test
	public void successRetrievePaymentSetupResourceTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments/" + paymentId;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		HttpEntity<?> requestEntity = new HttpEntity<>(headers);
		ResponseEntity<PaymentSetupPOSTResponse> response = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
				PaymentSetupPOSTResponse.class);
		assertEquals(HttpStatus.OK,response.getStatusCode());
		PaymentSetupPOSTResponse paymentSetupPOSTResponse = response.getBody();
		assertEquals(paymentId,paymentSetupPOSTResponse.getData().getPaymentId());
		assertEquals("FS_PMPSV_002", paymentSetupPOSTResponse.getData().getInitiation().getEndToEndIdentification());
		assertEquals("053598653254", paymentSetupPOSTResponse.getRisk().getMerchantCustomerIdentification());
	}
	
	@Test(expected=PSD2Exception.class)
	public void paymentIdNotInDatabaseFailureTest(){
		String url = edgeserverURL +paymentSetUpURL + "/payments/" + notInDatabasePaymentId;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			restClient.callForGet(requestInfo, PaymentSetupPOSTResponse.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Payment setup id is not found in platform.", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected=PSD2Exception.class)
	public void responseTppDifferentThanCurrentTppFailureTest(){
		String url = edgeserverURL +paymentSetUpURL + "/payments/" + diffTppPaymentId;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			restClient.callForGet(requestInfo, PaymentSetupPOSTResponse.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(403, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("No Payment setup resource found against given TPP.", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}
	
	@Test(expected = PSD2Exception.class)
	public void invalidPaymentIdFailureTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments/" + invalidPaymentId;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			restClient.callForGet(requestInfo, PaymentSetupPOSTResponse.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	/** Authorization header is null or blank and sent in the request */
	@Test(expected = HttpClientErrorException.class)
	public void noAuthTokenGivenGetApiTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments/" + paymentId;
		HttpHeaders headers = new HttpHeaders();
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			HttpEntity<?> requestEntity = new HttpEntity<>(headers);
			restTemplate.exchange(url, HttpMethod.GET, requestEntity, Object.class);
		} catch (HttpClientErrorException e) {
			assertEquals(HttpStatus.UNAUTHORIZED, e.getStatusCode());
			// assertEquals("Full authentication is required to access this
			// resource", e.getResponseBodyAsString());
			throw e;
		}
	}

	/** Expired Authorization code is sent in the request */
	@Test(expected = PSD2Exception.class)
	public void expiredAuthorizationTokenGetApiTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments/" + paymentId;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", expiredToken);
		headers.add("x-fapi-financial-id", "1234");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			restClient.callForGet(requestInfo, AccountGETResponse.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(HttpStatus.UNAUTHORIZED.toString(), e.getErrorInfo().getStatusCode());
			throw e;
		}
	}

	/** Value of x-fapifinancial-id is missing in the request header. */
	@Test(expected = PSD2Exception.class)
	public void fapiMandatoryHeaderMissingGetApiTest() {
		String url = edgeserverURL +paymentSetUpURL + "/payments/" + paymentId;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			restClient.callForPost(requestInfo, PaymentSetUpITMockData.getPaymentSetupPOSTRequestMockData(),
					PaymentSetupPOSTResponse.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(HttpStatus.BAD_REQUEST.value(), Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("x-fapi-financial-id is missing in headers.", e.getErrorInfo().getDetailErrorMessage());
			throw e;
		}
	}
}