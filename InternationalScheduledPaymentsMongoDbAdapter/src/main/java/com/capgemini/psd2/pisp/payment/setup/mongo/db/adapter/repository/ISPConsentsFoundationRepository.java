package com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTResponse;

public interface ISPConsentsFoundationRepository extends MongoRepository<CustomISPConsentsPOSTResponse, String> {

	public CustomISPConsentsPOSTResponse findOneByDataConsentId(String consentId);
}
