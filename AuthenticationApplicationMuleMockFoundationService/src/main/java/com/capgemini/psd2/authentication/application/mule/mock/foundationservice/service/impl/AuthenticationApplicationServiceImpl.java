package com.capgemini.psd2.authentication.application.mule.mock.foundationservice.service.impl;

import java.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.capgemini.psd2.authentication.application.mule.mock.foundationservice.domain.AuthenticationRequest;
import com.capgemini.psd2.authentication.application.mule.mock.foundationservice.domain.CustomLoginResponse;
import com.capgemini.psd2.authentication.application.mule.mock.foundationservice.domain.DigitalUser;
import com.capgemini.psd2.authentication.application.mule.mock.foundationservice.domain.DigitalUser13;
import com.capgemini.psd2.authentication.application.mule.mock.foundationservice.domain.DigitalUserSession;
import com.capgemini.psd2.authentication.application.mule.mock.foundationservice.domain.Login;
import com.capgemini.psd2.authentication.application.mule.mock.foundationservice.domain.LoginResponse;
import com.capgemini.psd2.authentication.application.mule.mock.foundationservice.domain.PersonBasicInformation;
import com.capgemini.psd2.authentication.application.mule.mock.foundationservice.repository.AuthenticationApplicationRepository;
import com.capgemini.psd2.authentication.application.mule.mock.foundationservice.repository.NewBOLAuthenticationRepository;
import com.capgemini.psd2.authentication.application.mule.mock.foundationservice.service.AuthenticationApplicationService;
import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.validator.ValidationUtility;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;


@Service
public class AuthenticationApplicationServiceImpl implements AuthenticationApplicationService {

	@Autowired
	private AuthenticationApplicationRepository repository;
	

	@Autowired
	private ValidationUtility validationUtility;
	
	@Autowired
	private NewBOLAuthenticationRepository repo;
	
	

	@Override
	public AuthenticationRequest retrieveB365UserCredentials(String userName, String password) {

		validationUtility.validateMockBusinessValidations(userName);
		AuthenticationRequest authenticationRequest = null;
		try{
			 authenticationRequest = repository.findByUserNameAndPassword(userName, password);
		}catch(Exception ex){
			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.INTERNAL_SERVER_ERROR);
		}

		if (authenticationRequest == null) {
			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.AUTHENTICATION_FAILURE_LOGIN_AUTH);
		}

		return authenticationRequest;
	}

	@Override
	public LoginResponse validatenewBolUser(Login login) {
		LocalDateTime currDate = null;
		LoginResponse loginresponse = new LoginResponse();

		CustomLoginResponse custom = repo.findByDigitalUserIdentifier(login.getDigitalUser().getDigitalUserIdentifier());

		if (custom == null) {
			DigitalUser digitaluser = new DigitalUser();
			digitaluser.setDigitalUserIdentifier(login.getDigitalUser().getDigitalUserIdentifier());
			DigitalUserSession digitalusersession = new DigitalUserSession();
			digitalusersession.setSessionInitiationFailureIndicator(true);
			digitalusersession.setSessionInitiationFailureReasonCode("Invalid OTP");
			loginresponse.setDigitalUser(digitaluser);
			loginresponse.setDigitalUserSession(digitalusersession);
			return loginresponse;
			
		} 
		else if (login.getDigitalUser().getCustomerAuthenticationSession().get(0).getAuthenticationMethodCode()
				.toString().equalsIgnoreCase("ONE_TIME_PASSWORD")) {
			DigitalUser13 digitalUser13= login.getDigitalUser();
			DigitalUser digitaluser = new DigitalUser();
			digitaluser.setAuthenticationProtocolCode(custom.getAuthenticationProtocolCode());
			digitaluser.setChannelCode(digitalUser13.getChannelCode());
			digitaluser.setDigitalUserIdentifier(digitalUser13.getDigitalUserIdentifier());
			digitaluser.setPersonInformation(custom.getPersonInformation());
			digitaluser.setSecureKeyAttemptsRemainingCount(custom.getSecureKeyAttemptsRemainingCount());
			digitaluser.setDigitalUserLockedOutIndicator(custom.getDigitalUserLockedOutIndicator());
			PersonBasicInformation personInformation = new PersonBasicInformation();
			personInformation.setBirthDate(custom.getPersonInformation().getBirthDate());
			personInformation.setFirstName(custom.getPersonInformation().getFirstName());
			personInformation.setMothersMaidenName(custom.getPersonInformation().getMothersMaidenName());
			personInformation.setSurname(custom.getPersonInformation().getSurname());
			personInformation.setTitleCode(custom.getPersonInformation().getTitleCode());
			digitaluser.setPersonInformation(personInformation);
			loginresponse.setDigitalUser(digitaluser);
			DigitalUserSession digitalusersession = new DigitalUserSession();
			
			//BOL Password Validation
			
			if (custom.getCustomerAuthenticationSession().get(0).getSecureAccessKeyUsed().toString().equalsIgnoreCase(login.getDigitalUser().getCustomerAuthenticationSession().get(0).getSecureAccessKeyUsed().toString())){
				digitalusersession.setSessionInitiationFailureIndicator(false);
				currDate = LocalDateTime.now();
				digitalusersession.setSessionStartDateTime(currDate.toString());
			
				
			} else {
				digitalusersession.setSessionInitiationFailureIndicator(true);
				digitalusersession.setSessionInitiationFailureReasonCode("Invalid Credential");
			}
			
			loginresponse.setDigitalUserSession(digitalusersession);
		}
	
	return loginresponse;
}
		

}
