package com.capgemini.psd2.authentication.adapter;

import java.util.Map;

public interface AuthenticationAdapter {
	public <T> T authenticate(T authenticaiton, Map<String, String> headers);
}
