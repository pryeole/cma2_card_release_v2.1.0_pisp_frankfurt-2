package com.capgemini.psd2.cisp.domain;

import java.util.Objects;
import com.capgemini.psd2.cisp.domain.OBActiveOrHistoricCurrencyAndAmount;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.joda.time.DateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * OBFundsConfirmationDataResponse1
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-11-05T15:52:27.305+05:30")

public class OBFundsConfirmationDataResponse1   {
	@JsonProperty("FundsConfirmationId")
	private String fundsConfirmationId = null;

	@JsonProperty("ConsentId")
	private String consentId = null;

	@JsonProperty("CreationDateTime")
	private String creationDateTime = null;

	@JsonProperty("FundsAvailable")
	private Boolean fundsAvailable = null;

	@JsonProperty("Reference")
	private String reference = null;

	@JsonProperty("InstructedAmount")
	private OBActiveOrHistoricCurrencyAndAmount instructedAmount = null;

	
	@JsonIgnore
	private String tenantId = null;

	@JsonIgnore
	private String cmaVersion;

	public OBFundsConfirmationDataResponse1 fundsConfirmationId(String fundsConfirmationId) {
		this.fundsConfirmationId = fundsConfirmationId;
		return this;
	}

	/**
	 * Unique identification as assigned by the ASPSP to uniquely identify the funds confirmation resource.
	 * @return fundsConfirmationId
	 **/
	@ApiModelProperty(required = true, value = "Unique identification as assigned by the ASPSP to uniquely identify the funds confirmation resource.")
	@NotNull

	@Size(min=1,max=40)
	public String getFundsConfirmationId() {
		return fundsConfirmationId;
	}

	public void setFundsConfirmationId(String fundsConfirmationId) {
		this.fundsConfirmationId = fundsConfirmationId;
	}

	public OBFundsConfirmationDataResponse1 consentId(String consentId) {
		this.consentId = consentId;
		return this;
	}

	/**
	 * Unique identification as assigned by the ASPSP to uniquely identify the funds confirmation consent resource.
	 * @return consentId
	 **/
	@ApiModelProperty(required = true, value = "Unique identification as assigned by the ASPSP to uniquely identify the funds confirmation consent resource.")
	@NotNull

	@Size(min=1,max=128)
	public String getConsentId() {
		return consentId;
	}

	public void setConsentId(String consentId) {
		this.consentId = consentId;
	}

	public OBFundsConfirmationDataResponse1 creationDateTime(String creationDateTime) {
		this.creationDateTime = creationDateTime;
		return this;
	}

	/**
	 * Date and time at which the resource was created. All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00
	 * @return creationDateTime
	 **/
	@ApiModelProperty(required = true, value = "Date and time at which the resource was created. All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00")
	@NotNull

	@Valid

	public String getCreationDateTime() {
		return creationDateTime;
	}

	public void setCreationDateTime(String string) {
		this.creationDateTime = string;
	}

	public OBFundsConfirmationDataResponse1 fundsAvailable(Boolean fundsAvailable) {
		this.fundsAvailable = fundsAvailable;
		return this;
	}

	/**
	 * Flag to indicate the result of a confirmation of funds check.
	 * @return fundsAvailable
	 **/
	@ApiModelProperty(required = true, value = "Flag to indicate the result of a confirmation of funds check.")
	@NotNull


	public Boolean getFundsAvailable() {
		return fundsAvailable;
	}

	public void setFundsAvailable(Boolean fundsAvailable) {
		this.fundsAvailable = fundsAvailable;
	}

	public OBFundsConfirmationDataResponse1 reference(String reference) {
		this.reference = reference;
		return this;
	}

	/**
	 * Unique reference, as assigned by the CBPII, to unambiguously refer to the request related to the payment transaction.
	 * @return reference
	 **/
	@ApiModelProperty(required = true, value = "Unique reference, as assigned by the CBPII, to unambiguously refer to the request related to the payment transaction.")
	@NotNull

	@Size(min=1,max=35)
	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public OBFundsConfirmationDataResponse1 instructedAmount(OBActiveOrHistoricCurrencyAndAmount instructedAmount) {
		this.instructedAmount = instructedAmount;
		return this;
	}

	/**
	 * Get instructedAmount
	 * @return instructedAmount
	 **/
	@ApiModelProperty(required = true, value = "")
	@NotNull

	@Valid

	public OBActiveOrHistoricCurrencyAndAmount getInstructedAmount() {
		return instructedAmount;
	}

	public void setInstructedAmount(OBActiveOrHistoricCurrencyAndAmount instructedAmount) {
		this.instructedAmount = instructedAmount;
	}


	/**
	 * Get tenantId
	 * @return tenantId
	 **/
	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}



	public String getCmaVersion() {
		return cmaVersion;
	}

	public void setCmaVersion(String cmaVersion) {
		this.cmaVersion = cmaVersion;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		OBFundsConfirmationDataResponse1 obFundsConfirmationDataResponse1 = (OBFundsConfirmationDataResponse1) o;
		return Objects.equals(this.fundsConfirmationId, obFundsConfirmationDataResponse1.fundsConfirmationId) &&
				Objects.equals(this.consentId, obFundsConfirmationDataResponse1.consentId) &&
				Objects.equals(this.creationDateTime, obFundsConfirmationDataResponse1.creationDateTime) &&
				Objects.equals(this.fundsAvailable, obFundsConfirmationDataResponse1.fundsAvailable) &&
				Objects.equals(this.reference, obFundsConfirmationDataResponse1.reference) &&
				Objects.equals(this.instructedAmount, obFundsConfirmationDataResponse1.instructedAmount) &&
				Objects.equals(this.tenantId, obFundsConfirmationDataResponse1.tenantId);
	}

	@Override
	public int hashCode() {
		return Objects.hash(fundsConfirmationId, consentId, creationDateTime, fundsAvailable, reference, instructedAmount);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class OBFundsConfirmationDataResponse1 {\n");

		sb.append("    fundsConfirmationId: ").append(toIndentedString(fundsConfirmationId)).append("\n");
		sb.append("    consentId: ").append(toIndentedString(consentId)).append("\n");
		sb.append("    creationDateTime: ").append(toIndentedString(creationDateTime)).append("\n");
		sb.append("    fundsAvailable: ").append(toIndentedString(fundsAvailable)).append("\n");
		sb.append("    reference: ").append(toIndentedString(reference)).append("\n");
		sb.append("    instructedAmount: ").append(toIndentedString(instructedAmount)).append("\n");
		sb.append("    tenantId: ").append(toIndentedString(tenantId)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}

