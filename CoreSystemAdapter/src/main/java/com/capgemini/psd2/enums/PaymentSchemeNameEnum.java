package com.capgemini.psd2.enums;

import java.util.Arrays;
import java.util.Optional;

public enum PaymentSchemeNameEnum {
	IBAN("IBAN", "UK.OBIE.IBAN"),
	UK_OBIE_IBAN("UK.OBIE.IBAN", "UK.OBIE.IBAN"), 
	SORTCODEACCOUNTNUMBER("SortCodeAccountNumber", "UK.OBIE.SortCodeAccountNumber"), 
	UK_OBIE_SORTCODEACCOUNTNUMBER("UK.OBIE.SortCodeAccountNumber", "UK.OBIE.SortCodeAccountNumber"), 
	PAN("PAN", "UK.OBIE.PAN"), 
	UK_OBIE_PAN("UK.OBIE.PAN", "UK.OBIE.PAN"), 

	UK_OBIE_BBAN("UK.OBIE.BBAN", "UK.OBIE.BBAN"), 
	UK_OBIE_PAYM("UK.OBIE.Paym", "UK.OBIE.Paym");

	private String allowedSetupSchemeName;
	private String compatibleUpgradedSchemeName;

	PaymentSchemeNameEnum(String allowedSetupSchemeName, String compatibleUpgradedSchemeName) {
		this.allowedSetupSchemeName = allowedSetupSchemeName;
		this.compatibleUpgradedSchemeName = compatibleUpgradedSchemeName;
	}

	public String getAllowedSetupSchemeName() {
		return allowedSetupSchemeName;
	}

	public String getCompatibleUpgradedSchemeName() {
		return compatibleUpgradedSchemeName;
	}

	public static PaymentSchemeNameEnum locatePaymentSchemeNameEnum(String schemeName) {
		Optional<PaymentSchemeNameEnum> paymentSchemeNameEnums = Arrays.asList(PaymentSchemeNameEnum.values()).stream()
				.filter(e -> e.getAllowedSetupSchemeName().equalsIgnoreCase(schemeName)).findFirst();

		return paymentSchemeNameEnums.isPresent() ? paymentSchemeNameEnums.get() : null;
	}
}
