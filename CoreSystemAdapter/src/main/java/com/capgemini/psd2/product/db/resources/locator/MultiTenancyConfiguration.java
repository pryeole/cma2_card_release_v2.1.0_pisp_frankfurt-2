package com.capgemini.psd2.product.db.resources.locator;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.context.expression.BeanExpressionContextAccessor;
import org.springframework.context.expression.BeanFactoryResolver;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.expression.spel.support.StandardEvaluationContext;

@Configuration
@Order(Ordered.HIGHEST_PRECEDENCE)
public class MultiTenancyConfiguration implements BeanFactoryAware {

	private BeanFactory fc;

	@Bean(name = "proxyBean")
	@Primary
	@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
	public MultiTenancyRequestBean RequestHeaderAttributes() {
		MultiTenancyRequestBean proxy = new MultiTenancyRequestBean();
		proxy.setTenantId("Unused_Proxy_Field");
		return proxy;
	}

	@Bean("CollectionNameLocator")
	@Order(Ordered.HIGHEST_PRECEDENCE)
	public CollectionNameLocator getRequiredLocatorBean() {
		return new CollectionNameLocatorImpl();
	}

	@Bean
	public StandardEvaluationContext standardEvaluationContext() {
		StandardEvaluationContext s = new StandardEvaluationContext();
		s.setBeanResolver(new BeanFactoryResolver(this.fc));
		s.addPropertyAccessor(new BeanExpressionContextAccessor());
		return s;
	}

	@Override
	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
		this.fc = beanFactory;
	}
}
