package com.capgemini.psd2.product.db.resources.locator;

public interface CollectionNameLocator {

	String getAccountAccessSetupCollectionName();

	String getPaymentSetupCollectionName();

	String getPaymentSubmissionCollectionName();
	
	String getFundsSetupCollectionName();
	
	String getAispConsentCollectionName();

	String getPispConsentCollectionName();

	String getCispConsentCollectionName();
	
	String getCPNPCollectionName();
	
}
