package com.capgemini.psd2.aisp.platform.domain;

import java.util.Map;

import com.capgemini.psd2.aisp.domain.OBReadScheduledPayment1;

public class PlatformAccountSchedulePaymentsResponse {

	// CMA2Response
	private OBReadScheduledPayment1 obReadScheduledPayment1;

	// Additional Information
	private Map<String, String> additionalInformation;

	public OBReadScheduledPayment1 getObReadScheduledPayment1() {
		return obReadScheduledPayment1;
	}

	public void setObReadScheduledPayment1(OBReadScheduledPayment1 obReadScheduledPayment1) {
		this.obReadScheduledPayment1 = obReadScheduledPayment1;
	}

	public Map<String, String> getAdditionalInformation() {
		return additionalInformation;
	}

	public void setAdditionalInformation(Map<String, String> additionalInformation) {
		this.additionalInformation = additionalInformation;
	}

}
