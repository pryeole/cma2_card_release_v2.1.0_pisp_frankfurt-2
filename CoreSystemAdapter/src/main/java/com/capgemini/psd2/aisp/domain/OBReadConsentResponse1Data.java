package com.capgemini.psd2.aisp.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * OBReadConsentResponse1Data
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-11-01T11:43:19.895+05:30")
// @Document(collection="data1")
@Document(collection = "#{@CollectionNameLocator.getAccountAccessSetupCollectionName()}")
public class OBReadConsentResponse1Data {
	@JsonProperty("ConsentId")
	@Id
	private String consentId = null;

	@JsonProperty("CreationDateTime")
	private String creationDateTime = null;

	@JsonProperty("Status")
	private OBExternalRequestStatus1Code status = null;

	@JsonProperty("StatusUpdateDateTime")
	private String statusUpdateDateTime = null;

	@JsonProperty("Permissions")
	@Valid
	private List<OBExternalPermissions1Code> permissions = new ArrayList<OBExternalPermissions1Code>();

	@JsonProperty("ExpirationDateTime")
	private String expirationDateTime = null;

	@JsonProperty("TransactionFromDateTime")
	private String transactionFromDateTime = null;

	@JsonProperty("TransactionToDateTime")
	private String transactionToDateTime = null;

	@JsonIgnore
	private String tenantId = null;

	@JsonIgnore
	private String tppCID = null;

	@JsonIgnore
	private String cmaVersion;

	@JsonIgnore
	private String tppLegalEntityName;

	public OBReadConsentResponse1Data consentId(String consentId) {
		this.consentId = consentId;
		return this;
	}

	/**
	 * Unique identification as assigned to identify the account access consent
	 * resource.
	 * 
	 * @return consentId
	 **/
	@ApiModelProperty(required = true, value = "Unique identification as assigned to identify the account access consent resource.")
	@NotNull

	@Size(min = 1, max = 128)
	public String getConsentId() {
		return consentId;
	}

	public void setConsentId(String consentId) {
		this.consentId = consentId;
	}

	public OBReadConsentResponse1Data creationDateTime(String creationDateTime) {
		this.creationDateTime = creationDateTime;
		return this;
	}

	/**
	 * Date and time at which the resource was created. All dates in the JSON
	 * payloads are represented in ISO 8601 date-time format. All date-time
	 * fields in responses must include the timezone. An example is below:
	 * 2017-04-05T10:43:07+00:00
	 * 
	 * @return creationDateTime
	 **/
	@ApiModelProperty(required = true, value = "Date and time at which the resource was created. All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00")
	@NotNull

	@Valid

	public String getCreationDateTime() {
		return creationDateTime;
	}

	public void setCreationDateTime(String creationDateTime) {
		this.creationDateTime = creationDateTime;
	}

	public OBReadConsentResponse1Data status(OBExternalRequestStatus1Code status) {
		this.status = status;
		return this;
	}

	/**
	 * Get status
	 * 
	 * @return status
	 **/
	@ApiModelProperty(required = true, value = "")
	@NotNull

	@Valid

	public OBExternalRequestStatus1Code getStatus() {
		return status;
	}

	public void setStatus(OBExternalRequestStatus1Code status) {
		this.status = status;
	}

	public OBReadConsentResponse1Data statusUpdateDateTime(String statusUpdateDateTime) {
		this.statusUpdateDateTime = statusUpdateDateTime;
		return this;
	}

	/**
	 * Date and time at which the resource status was updated. All dates in the
	 * JSON payloads are represented in ISO 8601 date-time format. All date-time
	 * fields in responses must include the timezone. An example is below:
	 * 2017-04-05T10:43:07+00:00
	 * 
	 * @return statusUpdateDateTime
	 **/
	@ApiModelProperty(required = true, value = "Date and time at which the resource status was updated. All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00")
	@NotNull

	@Valid

	public String getStatusUpdateDateTime() {
		return statusUpdateDateTime;
	}

	public void setStatusUpdateDateTime(String statusUpdateDateTime) {
		this.statusUpdateDateTime = statusUpdateDateTime;
	}

	public OBReadConsentResponse1Data permissions(List<OBExternalPermissions1Code> permissions) {
		this.permissions = permissions;
		return this;
	}

	public OBReadConsentResponse1Data addPermissionsItem(OBExternalPermissions1Code permissionsItem) {
		this.permissions.add(permissionsItem);
		return this;
	}

	/**
	 * Specifies the Open Banking account access data types. This is a list of
	 * the data clusters being consented by the PSU, and requested for
	 * authorisation with the ASPSP.
	 * 
	 * @return permissions
	 **/
	@ApiModelProperty(required = true, value = "Specifies the Open Banking account access data types. This is a list of the data clusters being consented by the PSU, and requested for authorisation with the ASPSP.")
	@NotNull

	@Valid
	@Size(min = 1)
	public List<OBExternalPermissions1Code> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<OBExternalPermissions1Code> permissions) {
		this.permissions = permissions;
	}

	public OBReadConsentResponse1Data expirationDateTime(String expirationDateTime) {
		this.expirationDateTime = expirationDateTime;
		return this;
	}

	/**
	 * Specified date and time the permissions will expire. If this is not
	 * populated, the permissions will be open ended. All dates in the JSON
	 * payloads are represented in ISO 8601 date-time format. All date-time
	 * fields in responses must include the timezone. An example is below:
	 * 2017-04-05T10:43:07+00:00
	 * 
	 * @return expirationDateTime
	 **/
	@ApiModelProperty(value = "Specified date and time the permissions will expire. If this is not populated, the permissions will be open ended. All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00")

	@Valid

	public String getExpirationDateTime() {
		return expirationDateTime;
	}

	public void setExpirationDateTime(String expirationDateTime) {
		this.expirationDateTime = expirationDateTime;
	}

	public OBReadConsentResponse1Data transactionFromDateTime(String transactionFromDateTime) {
		this.transactionFromDateTime = transactionFromDateTime;
		return this;
	}

	/**
	 * Specified start date and time for the transaction query period. If this
	 * is not populated, the start date will be open ended, and data will be
	 * returned from the earliest available transaction. All dates in the JSON
	 * payloads are represented in ISO 8601 date-time format. All date-time
	 * fields in responses must include the timezone. An example is below:
	 * 2017-04-05T10:43:07+00:00
	 * 
	 * @return transactionFromDateTime
	 **/
	@ApiModelProperty(value = "Specified start date and time for the transaction query period. If this is not populated, the start date will be open ended, and data will be returned from the earliest available transaction. All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00")

	@Valid

	public String getTransactionFromDateTime() {
		return transactionFromDateTime;
	}

	public void setTransactionFromDateTime(String transactionFromDateTime) {
		this.transactionFromDateTime = transactionFromDateTime;
	}

	public OBReadConsentResponse1Data transactionToDateTime(String transactionToDateTime) {
		this.transactionToDateTime = transactionToDateTime;
		return this;
	}

	/**
	 * Specified end date and time for the transaction query period. If this is
	 * not populated, the end date will be open ended, and data will be returned
	 * to the latest available transaction. All dates in the JSON payloads are
	 * represented in ISO 8601 date-time format. All date-time fields in
	 * responses must include the timezone. An example is below:
	 * 2017-04-05T10:43:07+00:00
	 * 
	 * @return transactionToDateTime
	 **/
	@ApiModelProperty(value = "Specified end date and time for the transaction query period. If this is not populated, the end date will be open ended, and data will be returned to the latest available transaction. All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00")

	@Valid

	public String getTransactionToDateTime() {
		return transactionToDateTime;
	}

	public void setTransactionToDateTime(String transactionToDateTime) {
		this.transactionToDateTime = transactionToDateTime;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public String getTppCID() {
		return tppCID;
	}

	public void setTppCID(String tppCID) {
		this.tppCID = tppCID;
	}

	public String getCmaVersion() {
		return cmaVersion;
	}

	public void setCmaVersion(String cmaVersion) {
		this.cmaVersion = cmaVersion;
	}

	public String getTppLegalEntityName() {
		return tppLegalEntityName;
	}

	public void setTppLegalEntityName(String tppLegalEntityName) {
		this.tppLegalEntityName = tppLegalEntityName;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		OBReadConsentResponse1Data obReadConsentResponse1Data = (OBReadConsentResponse1Data) o;
		return Objects.equals(this.consentId, obReadConsentResponse1Data.consentId)
				&& Objects.equals(this.creationDateTime, obReadConsentResponse1Data.creationDateTime)
				&& Objects.equals(this.status, obReadConsentResponse1Data.status)
				&& Objects.equals(this.statusUpdateDateTime, obReadConsentResponse1Data.statusUpdateDateTime)
				&& Objects.equals(this.permissions, obReadConsentResponse1Data.permissions)
				&& Objects.equals(this.expirationDateTime, obReadConsentResponse1Data.expirationDateTime)
				&& Objects.equals(this.transactionFromDateTime, obReadConsentResponse1Data.transactionFromDateTime)
				&& Objects.equals(this.transactionToDateTime, obReadConsentResponse1Data.transactionToDateTime)
				&& Objects.equals(this.tenantId, obReadConsentResponse1Data.tenantId);
	}

	@Override
	public int hashCode() {
		return Objects.hash(consentId, creationDateTime, status, statusUpdateDateTime, permissions, expirationDateTime,
				transactionFromDateTime, transactionToDateTime, tenantId);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class OBReadConsentResponse1Data {\n");

		sb.append("    consentId: ").append(toIndentedString(consentId)).append("\n");
		sb.append("    creationDateTime: ").append(toIndentedString(creationDateTime)).append("\n");
		sb.append("    status: ").append(toIndentedString(status)).append("\n");
		sb.append("    statusUpdateDateTime: ").append(toIndentedString(statusUpdateDateTime)).append("\n");
		sb.append("    permissions: ").append(toIndentedString(permissions)).append("\n");
		sb.append("    expirationDateTime: ").append(toIndentedString(expirationDateTime)).append("\n");
		sb.append("    transactionFromDateTime: ").append(toIndentedString(transactionFromDateTime)).append("\n");
		sb.append("    transactionToDateTime: ").append(toIndentedString(transactionToDateTime)).append("\n");
		sb.append("    tenantId: ").append(toIndentedString(tenantId)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
