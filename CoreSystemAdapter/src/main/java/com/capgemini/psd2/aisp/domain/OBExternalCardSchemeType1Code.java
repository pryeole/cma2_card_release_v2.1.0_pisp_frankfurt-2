package com.capgemini.psd2.aisp.domain;

import java.util.Objects;
import io.swagger.annotations.ApiModel;
import com.fasterxml.jackson.annotation.JsonValue;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Name of the card scheme.
 */
public enum OBExternalCardSchemeType1Code {
  
  AMERICANEXPRESS("AmericanExpress"),
  
  DINERS("Diners"),
  
  DISCOVER("Discover"),
  
  MASTERCARD("MasterCard"),
  
  VISA("VISA");

  private String value;

  OBExternalCardSchemeType1Code(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static OBExternalCardSchemeType1Code fromValue(String text) {
    for (OBExternalCardSchemeType1Code b : OBExternalCardSchemeType1Code.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

