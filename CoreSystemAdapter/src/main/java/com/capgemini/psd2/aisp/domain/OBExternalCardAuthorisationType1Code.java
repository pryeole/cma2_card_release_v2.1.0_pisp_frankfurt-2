package com.capgemini.psd2.aisp.domain;

import java.util.Objects;
import io.swagger.annotations.ApiModel;
import com.fasterxml.jackson.annotation.JsonValue;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * The card authorisation type.
 */
public enum OBExternalCardAuthorisationType1Code {
  
  CONSUMERDEVICE("ConsumerDevice"),
  
  CONTACTLESS("Contactless"),
  
  NONE("None"),
  
  PIN("PIN");

  private String value;

  OBExternalCardAuthorisationType1Code(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static OBExternalCardAuthorisationType1Code fromValue(String text) {
    for (OBExternalCardAuthorisationType1Code b : OBExternalCardAuthorisationType1Code.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

