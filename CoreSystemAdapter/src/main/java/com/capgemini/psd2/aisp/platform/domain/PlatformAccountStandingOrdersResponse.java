package com.capgemini.psd2.aisp.platform.domain;

import java.util.Map;

import com.capgemini.psd2.aisp.domain.OBReadStandingOrder3;

public class PlatformAccountStandingOrdersResponse {

	// CMA2 response
	private OBReadStandingOrder3 oBReadStandingOrder3;

	// Additional Information
	private Map<String, String> additionalInformation;

	public OBReadStandingOrder3 getoBReadStandingOrder3() {
		return oBReadStandingOrder3;
	}

	public void setoBReadStandingOrder3(OBReadStandingOrder3 oBReadStandingOrder3) {
		this.oBReadStandingOrder3 = oBReadStandingOrder3;
	}

	public Map<String, String> getAdditionalInformation() {
		return additionalInformation;
	}

	public void setAdditionalInformation(Map<String, String> additionalInformation) {
		this.additionalInformation = additionalInformation;
	}

}
