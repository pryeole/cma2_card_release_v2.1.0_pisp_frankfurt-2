package com.capgemini.psd2.pisp.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.capgemini.psd2.pisp.enums.ProcessConsentStatusEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Document(collection = "filePaymentConsentsFoundationResources")
public class CustomFilePaymentConsentsPOSTResponse extends OBWriteFileConsentResponse1{
	
	@Id
	private String id;
	private String currency;
	private Object fraudScore;
	private ProcessConsentStatusEnum consentProcessStatus = null;
	
	@JsonIgnore
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Object getFraudScore() {
		return fraudScore;
	}

	public void setFraudScore(Object fraudScore) {
		this.fraudScore = fraudScore;
	}

	@JsonIgnore
	public ProcessConsentStatusEnum getConsentProcessStatus() {
		return consentProcessStatus;
	}

	public void setConsentProcessStatus(ProcessConsentStatusEnum consentProcessStatus) {
		this.consentProcessStatus = consentProcessStatus;
	}

	@Override
	public String toString() {
		return "CustomFilePaymentConsentsPOSTResponse [id=" + id + ", currency=" + currency + ", fraudScore="
				+ fraudScore + ", consentProcessStatus=" + consentProcessStatus + "]";
	}

	
	
}
