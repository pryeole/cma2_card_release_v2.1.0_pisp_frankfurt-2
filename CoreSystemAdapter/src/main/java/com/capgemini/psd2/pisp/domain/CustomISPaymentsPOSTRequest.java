package com.capgemini.psd2.pisp.domain;

public class CustomISPaymentsPOSTRequest extends OBWriteInternationalScheduled1 {
	private String createdOn;

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	@Override
	public String toString() {
		return "CustomISPaymentsPOSTRequest [createdOn=" + createdOn + "]";
	}

}
