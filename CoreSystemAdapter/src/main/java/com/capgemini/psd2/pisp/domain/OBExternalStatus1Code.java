package com.capgemini.psd2.pisp.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Specifies the status of resource in code form.
 */
public enum OBExternalStatus1Code {
  
  INITIATIONCOMPLETED("InitiationCompleted"),
  
  INITIATIONFAILED("InitiationFailed"),
  
  INITIATIONPENDING("InitiationPending");

  private String value;

  OBExternalStatus1Code(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static OBExternalStatus1Code fromValue(String text) {
    for (OBExternalStatus1Code b : OBExternalStatus1Code.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

