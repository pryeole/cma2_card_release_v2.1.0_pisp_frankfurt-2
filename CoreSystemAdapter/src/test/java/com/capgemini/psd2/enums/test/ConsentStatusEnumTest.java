package com.capgemini.psd2.enums.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.capgemini.psd2.enums.ConsentStatusEnum;

public class ConsentStatusEnumTest {

	private ConsentStatusEnum consentStatusEnum;
	
	@Before
	public void setUp() throws Exception {
		consentStatusEnum = ConsentStatusEnum.AUTHORISED;
	}
	/**
	 * Test.
	 */
	@Test
	public void testGetter(){
		assertEquals(ConsentStatusEnum.AUTHORISED.getStatusCode(), consentStatusEnum.getStatusCode()); 
		assertEquals(ConsentStatusEnum.AUTHORISED.getStatusId(), consentStatusEnum.getStatusId()); 
	}
	/**
	 * Test.
	 */
	@Test
	public void testFindStatusFromEnum(){
		assertEquals(ConsentStatusEnum.AUTHORISED,ConsentStatusEnum.findStatusFromEnum(consentStatusEnum.getStatusId()));
	}
	/**
	 * Test.
	 */
	@Test
	public void testIsActive(){
		assertTrue(ConsentStatusEnum.isAuthorised(consentStatusEnum.getStatusId()));
	}
	
	@After
	public void tearDown(){
		consentStatusEnum = null;
	}
	
	
}
