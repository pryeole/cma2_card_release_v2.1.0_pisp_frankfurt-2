package com.capgemini.psd2.enums.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.capgemini.psd2.enums.TokenStatusEnum;

public class TokenStatusEnumTest {

private TokenStatusEnum tokenStatusEnum;
	
	@Before
	public void setUp() throws Exception {
		tokenStatusEnum = TokenStatusEnum.ACTIVE;
	}
	/**
	 * Test.
	 */
	@Test
	public void testGetter(){
		assertEquals(TokenStatusEnum.ACTIVE.getStatusCode(),tokenStatusEnum.getStatusCode()); 
		assertEquals(TokenStatusEnum.ACTIVE.getStatusId(), tokenStatusEnum.getStatusId()); 
	}
	/**
	 * Test.
	 */
	@Test
	public void testFindStatusFromEnum(){
		assertEquals(TokenStatusEnum.ACTIVE,TokenStatusEnum.findStatusFromEnum(tokenStatusEnum.getStatusId()));
	}
	/**
	 * Test.
	 */
	@Test
	public void testIsActive(){
		assertTrue(TokenStatusEnum.isActive(tokenStatusEnum.getStatusId()));
	}
	
	@After
	public void tearDown(){
		tokenStatusEnum = null;
	}
	
	
}
