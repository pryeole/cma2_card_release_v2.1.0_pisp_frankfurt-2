package com.capgemini.psd2.account.statements.constants;

public class AccountStatementConstants {

	public static final String REQUESTED_FROM_DATETIME = "fromStatementDateTime";

	public static final String REQUESTED_TO_DATETIME = "toStatementDateTime";

	public static final String REQUESTED_PAGE_NUMBER = "RequestedPageNumber";

	public static final String REQUESTED_PAGE_SIZE = "RequestedPageSize";

	public static final String REQUESTED_MIN_PAGE_SIZE = "RequestedMinPageSize";

	public static final String REQUESTED_MAX_PAGE_SIZE = "RequestedMaxPageSize";

	public static final String REQUESTED_TXN_FILTER = "RequestedTxnFilter";
	
	public static final String PAGE_NUMBER_PARAM_NAME = "page";
	
	public static final String TXN_KEY_PARAM_NAME = "TxnKeyParamName";
	
	public static final String PAGE_SIZE_PARAM_NAME = "PageSizeParamName";
	
	public static final String REQUESTED_TXN_KEY = "RequestedTxnKey";

	public static final String DEFAULT_PAGE = String.valueOf(1);
	
	public static final String REQUESTED_FROM_CONSENT_DATETIME = "RequestedFromConsentDateTime";
	
	public static final String REQUESTED_TO_CONSENT_DATETIME = "RequestedToConsentDateTime";
	
	public static final String CONSENT_EXPIRATION_DATETIME = "ConsentExpirationDateTime";
	
	public static final String STATEMENT_ID = "statementId";
	
	public static final String ACCOUNT_SCOPE_NAME = "accounts";
	
	public static final String PAGE_SIZE="linksPageSize";
	
	public static final String LINK_PAGE_FROM_DATE="linksFromDate";
	
	public static final String LINK_PAGE_TO_DATE="linksToDate";
	
	public static final String PAGE_LINKS_PAGE_NUM = "linksPageNumber";
	
	public enum FilterEnum {
		CREDIT, DEBIT, ALL
	}

}
