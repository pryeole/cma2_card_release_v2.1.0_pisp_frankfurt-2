package com.capgemini.tpp.muleapi.schema;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "access_token", "token_type", "redirectUrl" })
public class LoginResponse implements Serializable {

	@JsonProperty("access_token")
	private String accessToken;
	
	@JsonProperty("token_type")
	private String tokenType;
	
	@JsonProperty("redirectUrl")
	private String redirectUrl;

	private static final long serialVersionUID = 4750467371586489078L;

	@JsonProperty("access_token")
	public String getAccessToken() {
		return accessToken;
	}

	@JsonProperty("access_token")
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	@JsonProperty("token_type")
	public String getTokenType() {
		return tokenType;
	}

	@JsonProperty("token_type")
	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}

	@JsonProperty("redirectUrl")
	public String getRedirectUrl() {
		return redirectUrl;
	}

	@JsonProperty("redirectUrl")
	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LoginResponse [getAccessToken()=");
		builder.append(getAccessToken());
		builder.append(", getTokenType()=");
		builder.append(getTokenType());
		builder.append(", getRedirectUrl()=");
		builder.append(getRedirectUrl());
		builder.append("]");
		return builder.toString();
	}
	
	

}