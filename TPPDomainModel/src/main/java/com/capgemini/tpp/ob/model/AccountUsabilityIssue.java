package com.capgemini.tpp.ob.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Account usability issue
 */
@ApiModel(description = "Account usability issue")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-12-21T15:23:35.120+05:30")

public class AccountUsabilityIssue   {
  @JsonProperty("name")
  private String name = null;

  @JsonProperty("message")
  private String message = null;

  public AccountUsabilityIssue name(String name) {
    this.name = name;
    return this;
  }

   /**
   * The name for this account usability issue.
   * @return name
  **/
  @ApiModelProperty(value = "The name for this account usability issue.")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public AccountUsabilityIssue message(String message) {
    this.message = message;
    return this;
  }

   /**
   * The human-readable message that provides specific details about this account usability issue.
   * @return message
  **/
  @ApiModelProperty(value = "The human-readable message that provides specific details about this account usability issue.")


  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AccountUsabilityIssue accountUsabilityIssue = (AccountUsabilityIssue) o;
    return Objects.equals(this.name, accountUsabilityIssue.name) &&
        Objects.equals(this.message, accountUsabilityIssue.message);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, message);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AccountUsabilityIssue {\n");
    
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    message: ").append(toIndentedString(message)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

