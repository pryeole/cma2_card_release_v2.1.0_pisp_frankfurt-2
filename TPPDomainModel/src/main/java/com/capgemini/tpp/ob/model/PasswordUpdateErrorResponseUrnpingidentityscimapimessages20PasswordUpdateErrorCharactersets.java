package com.capgemini.tpp.ob.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorCharactersets
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-12-21T15:23:35.120+05:30")

public class PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorCharactersets   {
  @JsonProperty("characters")
  private String characters = null;

  @JsonProperty("min-count")
  private Integer minCount = null;

  public PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorCharactersets characters(String characters) {
    this.characters = characters;
    return this;
  }

   /**
   * The characters included in a character set
   * @return characters
  **/
  @ApiModelProperty(value = "The characters included in a character set")


  public String getCharacters() {
    return characters;
  }

  public void setCharacters(String characters) {
    this.characters = characters;
  }

  public PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorCharactersets minCount(Integer minCount) {
    this.minCount = minCount;
    return this;
  }

   /**
   * The minimum number of characters from that set required to be present.
   * @return minCount
  **/
  @ApiModelProperty(value = "The minimum number of characters from that set required to be present.")


  public Integer getMinCount() {
    return minCount;
  }

  public void setMinCount(Integer minCount) {
    this.minCount = minCount;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorCharactersets passwordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorCharactersets = (PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorCharactersets) o;
    return Objects.equals(this.characters, passwordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorCharactersets.characters) &&
        Objects.equals(this.minCount, passwordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorCharactersets.minCount);
  }

  @Override
  public int hashCode() {
    return Objects.hash(characters, minCount);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PasswordUpdateErrorResponseUrnpingidentityscimapimessages20PasswordUpdateErrorCharactersets {\n");
    
    sb.append("    characters: ").append(toIndentedString(characters)).append("\n");
    sb.append("    minCount: ").append(toIndentedString(minCount)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

