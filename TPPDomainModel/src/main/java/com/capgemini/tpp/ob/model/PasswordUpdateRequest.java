package com.capgemini.tpp.ob.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Request object for updating user passwords
 */
@ApiModel(description = "Request object for updating user passwords")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-12-21T15:23:35.120+05:30")

public class PasswordUpdateRequest   {
  /**
   * Gets or Sets schemas
   */
  public enum SchemasEnum {
    PASSWORDUPDATEREQUEST("urn:pingidentity:scim:api:messages:2.0:PasswordUpdateRequest");

    private String value;

    SchemasEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static SchemasEnum fromValue(String text) {
      for (SchemasEnum b : SchemasEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("schemas")
  private List<SchemasEnum> schemas = new ArrayList<SchemasEnum>();

  @JsonProperty("currentPassword")
  private String currentPassword = null;

  @JsonProperty("newPassword")
  private String newPassword = null;

  public PasswordUpdateRequest schemas(List<SchemasEnum> schemas) {
    this.schemas = schemas;
    return this;
  }

  public PasswordUpdateRequest addSchemasItem(SchemasEnum schemasItem) {
    this.schemas.add(schemasItem);
    return this;
  }

   /**
   * Contains a list of one or more URIs that indicate included SCIM schemas that are used to indicate the attributes contained within a resource.
   * @return schemas
  **/
  @ApiModelProperty(required = true, value = "Contains a list of one or more URIs that indicate included SCIM schemas that are used to indicate the attributes contained within a resource.")
  @NotNull


  public List<SchemasEnum> getSchemas() {
    return schemas;
  }

  public void setSchemas(List<SchemasEnum> schemas) {
    this.schemas = schemas;
  }

  public PasswordUpdateRequest currentPassword(String currentPassword) {
    this.currentPassword = currentPassword;
    return this;
  }

   /**
   * The user's current password.
   * @return currentPassword
  **/
  @ApiModelProperty(value = "The user's current password.")


  public String getCurrentPassword() {
    return currentPassword;
  }

  public void setCurrentPassword(String currentPassword) {
    this.currentPassword = currentPassword;
  }

  public PasswordUpdateRequest newPassword(String newPassword) {
    this.newPassword = newPassword;
    return this;
  }

   /**
   * The requested new password for the user.
   * @return newPassword
  **/
  @ApiModelProperty(value = "The requested new password for the user.")


  public String getNewPassword() {
    return newPassword;
  }

  public void setNewPassword(String newPassword) {
    this.newPassword = newPassword;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PasswordUpdateRequest passwordUpdateRequest = (PasswordUpdateRequest) o;
    return Objects.equals(this.schemas, passwordUpdateRequest.schemas) &&
        Objects.equals(this.currentPassword, passwordUpdateRequest.currentPassword) &&
        Objects.equals(this.newPassword, passwordUpdateRequest.newPassword);
  }

  @Override
  public int hashCode() {
    return Objects.hash(schemas, currentPassword, newPassword);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PasswordUpdateRequest {\n");
    
    sb.append("    schemas: ").append(toIndentedString(schemas)).append("\n");
    sb.append("    currentPassword: ").append(toIndentedString(currentPassword)).append("\n");
    sb.append("    newPassword: ").append(toIndentedString(newPassword)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

