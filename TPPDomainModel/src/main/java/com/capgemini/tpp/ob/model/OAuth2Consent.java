package com.capgemini.tpp.ob.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * A user&#39;s consent for an OAuth2 client
 */
@ApiModel(description = "A user's consent for an OAuth2 client")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-12-21T15:23:35.120+05:30")

public class OAuth2Consent   {
  /**
   * Gets or Sets schemas
   */
  public enum SchemasEnum {
    CONSENT("urn:pingidentity:scim:api:messages:2.0:consent");

    private String value;

    SchemasEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static SchemasEnum fromValue(String text) {
      for (SchemasEnum b : SchemasEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("schemas")
  private List<SchemasEnum> schemas = new ArrayList<SchemasEnum>();

  @JsonProperty("client")
  private OAuth2Client client = null;

  @JsonProperty("scopes")
  private List<OAuth2ConsentScopes> scopes = null;

  public OAuth2Consent schemas(List<SchemasEnum> schemas) {
    this.schemas = schemas;
    return this;
  }

  public OAuth2Consent addSchemasItem(SchemasEnum schemasItem) {
    this.schemas.add(schemasItem);
    return this;
  }

   /**
   * Contains a list of one or more URIs that indicate included SCIM schemas that are used to indicate the attributes contained within a resource.
   * @return schemas
  **/
  @ApiModelProperty(required = true, value = "Contains a list of one or more URIs that indicate included SCIM schemas that are used to indicate the attributes contained within a resource.")
  @NotNull


  public List<SchemasEnum> getSchemas() {
    return schemas;
  }

  public void setSchemas(List<SchemasEnum> schemas) {
    this.schemas = schemas;
  }

  public OAuth2Consent client(OAuth2Client client) {
    this.client = client;
    return this;
  }

   /**
   * The target OAuth2 client.
   * @return client
  **/
  @ApiModelProperty(value = "The target OAuth2 client.")

  @Valid

  public OAuth2Client getClient() {
    return client;
  }

  public void setClient(OAuth2Client client) {
    this.client = client;
  }

  public OAuth2Consent scopes(List<OAuth2ConsentScopes> scopes) {
    this.scopes = scopes;
    return this;
  }

  public OAuth2Consent addScopesItem(OAuth2ConsentScopes scopesItem) {
    if (this.scopes == null) {
      this.scopes = new ArrayList<OAuth2ConsentScopes>();
    }
    this.scopes.add(scopesItem);
    return this;
  }

   /**
   * The consent decision for OAuth2 scopes.
   * @return scopes
  **/
  @ApiModelProperty(value = "The consent decision for OAuth2 scopes.")

  @Valid

  public List<OAuth2ConsentScopes> getScopes() {
    return scopes;
  }

  public void setScopes(List<OAuth2ConsentScopes> scopes) {
    this.scopes = scopes;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OAuth2Consent oauth2Consent = (OAuth2Consent) o;
    return Objects.equals(this.schemas, oauth2Consent.schemas) &&
        Objects.equals(this.client, oauth2Consent.client) &&
        Objects.equals(this.scopes, oauth2Consent.scopes);
  }

  @Override
  public int hashCode() {
    return Objects.hash(schemas, client, scopes);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OAuth2Consent {\n");
    
    sb.append("    schemas: ").append(toIndentedString(schemas)).append("\n");
    sb.append("    client: ").append(toIndentedString(client)).append("\n");
    sb.append("    scopes: ").append(toIndentedString(scopes)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

