package com.capgemini.psd2.security.saas.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import com.capgemini.psd2.adapter.security.custom.domain.CustomAuthenticationServiceGetResponse;
import com.capgemini.psd2.adapter.security.custom.domain.CustomAuthenticationServicePostRequest;
import com.capgemini.psd2.adapter.security.custom.domain.CustomAuthenticationServicePostResponse;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.scaconsenthelper.config.PFConfig;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.security.constants.PSD2SecurityConstants;
import com.capgemini.psd2.security.saas.constants.SaaSAppConstants;
import com.capgemini.psd2.security.saas.service.SaaSAppService;
import com.capgemini.psd2.utilities.DateUtilites;
import com.capgemini.psd2.utilities.JSONUtilities;

@RestController
public class SaaSAppController {
	
	@Autowired
	private SaaSAppService saaSAppService;
	
	@Autowired
	private RequestHeaderAttributes requestHeaderAttribute;
	
	@Autowired
	private PFConfig pfConfig;

	@RequestMapping(value = "/authenticate-dob", method=RequestMethod.POST, consumes= "application/json")
	public String authenticateDOB(@RequestBody CustomAuthenticationServicePostRequest customAuthenticationServicePostRequest) {
		ModelAndView model = new ModelAndView();
		CustomAuthenticationServiceGetResponse customAuthenticationServiceGetResponse = saaSAppService.authenticateDOB(customAuthenticationServicePostRequest);
		
		if(!customAuthenticationServiceGetResponse.getSessionInitiationFailureIndicator()){
			model.addObject(SaaSAppConstants.CUSTOMER_AUTHENTICATION_SESSIONS, customAuthenticationServiceGetResponse.getAuthenticationParameters().getCustomerAuthenticationSessions());
			model.addObject(SaaSAppConstants.ELECTRONIC_DEVICES, customAuthenticationServiceGetResponse.getAuthenticationParameters().getElectronicDevices());
		}
		model.addObject(SaaSAppConstants.DIGITAL_USER, customAuthenticationServiceGetResponse.getAuthenticationParameters().getDigitalUser());	
		model.addObject(SaaSAppConstants.SESSION_INITIATION_FAILURE_INDICATOR, customAuthenticationServiceGetResponse.getSessionInitiationFailureIndicator());
		return JSONUtilities.getJSONOutPutFromObject(model);			
	}
	
	
	@RequestMapping(value = "/authenticate-pin-listOfDevices", method=RequestMethod.POST, consumes= "application/json")
	public String submitPINDetailsandListOfDevices(@RequestBody CustomAuthenticationServicePostRequest postRequest, @RequestParam String resumePath){
		ModelAndView model = new ModelAndView();
		CustomAuthenticationServicePostResponse customAuthenticationServicePostResponse =  saaSAppService.submitPINDetailsandSelectedDevices(postRequest);
		model.addObject(SaaSAppConstants.DIGITAL_USER, customAuthenticationServicePostResponse.getLoginResponse().getDigitalUser());
		model.addObject(SaaSAppConstants.DIGITAL_USER_SESSION, customAuthenticationServicePostResponse.getLoginResponse().getDigitalUserSession());
		model.addObject(SaaSAppConstants.SESSION_INITIATION_FAILURE_INDICATOR, customAuthenticationServicePostResponse.getLoginResponse().getDigitalUserSession().isSessionInitiationFailureIndicator());
		
		resumePath = pfConfig.getTenantSpecificResumePathBaseUrl(requestHeaderAttribute.getTenantId()).concat(resumePath);
		
		String redirectURI = UriComponentsBuilder.fromHttpUrl(resumePath).queryParam(PFConstants.REF, "abcd").toUriString();
		
		model.addObject(PSD2SecurityConstants.REDIRECT_URI_MODEL_ATTRIBUTE, redirectURI);
		return JSONUtilities.getJSONOutPutFromObject(model);
	}
	
	@RequestMapping(value = "/consent-redirect", method=RequestMethod.POST, consumes= "application/json")
	public void handleConsentRedirection(){
		
	}
}
