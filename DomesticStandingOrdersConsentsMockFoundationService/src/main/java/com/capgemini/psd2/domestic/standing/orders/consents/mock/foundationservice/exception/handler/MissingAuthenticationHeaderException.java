package com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.exception.handler;

public class MissingAuthenticationHeaderException extends Exception{

	
	private static final long serialVersionUID = 1L;

	public MissingAuthenticationHeaderException(String m){
		super(m);
	}
	
}