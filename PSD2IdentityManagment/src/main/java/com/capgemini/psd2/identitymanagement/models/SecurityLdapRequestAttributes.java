package com.capgemini.psd2.identitymanagement.models;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SecurityLdapRequestAttributes {
	
	private CustomClientDetails customClientDetails;

	public CustomClientDetails getCustomClientDetails() {
		return customClientDetails;
	}

	public void setCustomClientDetails(CustomClientDetails customClientDetails) {
		this.customClientDetails = customClientDetails;
	}
	

}
