package com.capgemini.psd2.idmanagement.handlers;

import org.springframework.http.HttpStatus;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.provider.token.store.jwk.JwkException;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;

import com.capgemini.psd2.rest.client.exception.handler.ExceptionHandler;

public class IdmanagementExceptionHandlerImpl implements ExceptionHandler{

	/* (non-Javadoc)
	 * @see com.capgemini.psd2.rest.client.exception.handler.ExceptionHandler#handleException(org.springframework.web.client.HttpServerErrorException)
	 */
	@Override
	public void handleException(HttpServerErrorException e) {
		throw new JwkException(e.getMessage());
	}

	/* (non-Javadoc)
	 * @see com.capgemini.psd2.rest.client.exception.handler.ExceptionHandler#handleException(org.springframework.web.client.HttpClientErrorException)
	 */
	@Override
	public void handleException(HttpClientErrorException e) {
		String responseBody  = e.getResponseBodyAsString();
		if(e.getStatusCode().equals(HttpStatus.NOT_FOUND) && responseBody.equalsIgnoreCase("Client not found")){
			throw OAuth2Exception.create(OAuth2Exception.INVALID_CLIENT, "Invalid client id provided.");
		}
		throw new JwkException(e.getMessage());
	}

	/* (non-Javadoc)
	 * @see com.capgemini.psd2.rest.client.exception.handler.ExceptionHandler#handleException(org.springframework.web.client.ResourceAccessException)
	 */
	@Override
	public void handleException(ResourceAccessException e) {
		throw new JwkException(e.getMessage());
	}
}
