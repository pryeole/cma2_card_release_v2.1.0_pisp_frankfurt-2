package com.capgemini.psd2.identitymanagement.test.service.impl;

import static org.hamcrest.CoreMatchers.any;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.identitymanagement.constants.IdentityManagementConstants;
import com.capgemini.psd2.identitymanagement.models.CustomClientDetails;
import com.capgemini.psd2.identitymanagement.models.LoginCredentials;
import com.capgemini.psd2.identitymanagement.services.impl.TppInfoMgmtServiceMuleImpl;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;

@RunWith(SpringJUnit4ClassRunner.class)
public class TppInfoMgmtServiceMuleImplTest 
{
	@Mock
	private RestClientSync restClient;
	
	
	private String username="abcd";
	private String password="1234";
	private String clientId="1234";
	private String clientDetailsUrl;
	
	@Mock
	LoginCredentials loginCredentials;
	
	@InjectMocks
	private TppInfoMgmtServiceMuleImpl tppInfoMgmtServiceMuleImpl;
	
	
	
	@Before
	public void setUp() throws Exception {

		Set<String> registeredRedirectUri=new HashSet<>();
		registeredRedirectUri.add("/test");
		Collection<GrantedAuthority> authorities= new HashSet<>();
		GrantedAuthority grantedAuthority = new GrantedAuthority() {
			
			@Override
			public String getAuthority() {
				// TODO Auto-generated method stub
				return null;
			}
		};
		authorities.add(grantedAuthority);
		CustomClientDetails customClientDetails = new CustomClientDetails("12345", "123456", registeredRedirectUri, authorities, true, "abcd", "1234567");
		
	}
	
	/*@Test(expected =OAuth2Exception.class)
	public void testFindTppInfoFailure(){
		tppInfoMgmtServiceMuleImpl.findTppInfo("Moneywise_Wealth");
		}*/
	
	
	
	@Test
	public void testTppDetail(){
		RequestInfo requestInfo = new RequestInfo();
		//loginCredentials = new LoginCredentials(username , password);
		Map<String,String> map = new HashMap<>();
		map.put(IdentityManagementConstants.RES_MULE_ACCESS_TOKEN, "token");
		Map<String,Object> clientDetails = new HashMap<>();
		clientDetails.put(IdentityManagementConstants.RES_MULE_CLIENT_ID, "clientId");
		clientDetails.put(IdentityManagementConstants.RES_MULE_CLIENT_SECRET, "secret");
		List<String> redirectUri = new ArrayList<>();
		redirectUri.add("/redirectUri");
		clientDetails.put(IdentityManagementConstants.RES_MULE_REDIRECT_URI, redirectUri);
		ReflectionTestUtils.setField(tppInfoMgmtServiceMuleImpl, "username", "John");
		ReflectionTestUtils.setField(tppInfoMgmtServiceMuleImpl, "password", "John");
		List<String> authorities = new ArrayList<>();
		authorities.add("authority");
		ReflectionTestUtils.setField(tppInfoMgmtServiceMuleImpl, "authorities", authorities );
		when(restClient.callForPost(anyObject(),anyObject(),any(),anyObject())).thenReturn(map);
		when(restClient.callForGet(anyObject(),any(), anyObject())).thenReturn(clientDetails);
		assertNotNull(tppInfoMgmtServiceMuleImpl.findTppInfo("1234"));
	}
	@Test
	public void testForGetPassword()
	{
		tppInfoMgmtServiceMuleImpl.setPassword(password);
		assertEquals(password, tppInfoMgmtServiceMuleImpl.getPassword());
	} 

	@Test
	public void testForGetUsername()
	{
		tppInfoMgmtServiceMuleImpl.setUsername(username);
		assertEquals(username, tppInfoMgmtServiceMuleImpl.getUsername());
	} 
	@Test
	public void testForClientDetailsUrl()
	{
		tppInfoMgmtServiceMuleImpl.setClientDetailsUrl(username);
		assertEquals(username, tppInfoMgmtServiceMuleImpl.getClientDetailsUrl());
	} 
		
	}
	


