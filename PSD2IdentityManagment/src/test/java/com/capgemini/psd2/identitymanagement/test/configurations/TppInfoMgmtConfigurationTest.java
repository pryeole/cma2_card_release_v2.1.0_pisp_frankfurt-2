package com.capgemini.psd2.identitymanagement.test.configurations;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.capgemini.psd2.identitymanagement.configurations.TppInfoMgmtConfiguration;

public class TppInfoMgmtConfigurationTest {

	private TppInfoMgmtConfiguration tppInfoMgmtConfiguration;
	
	@Before
	public void setUp() throws Exception {
		tppInfoMgmtConfiguration = new TppInfoMgmtConfiguration();
		tppInfoMgmtConfiguration.setLdapPassword("password");
		tppInfoMgmtConfiguration.setUrl("/testURL");
		tppInfoMgmtConfiguration.setUserDn("userDn");
	}
	
	@Test
	public void test(){
		assertEquals("password",tppInfoMgmtConfiguration.getLdapPassword());
		assertEquals("/testURL", tppInfoMgmtConfiguration.getUrl());
		assertEquals("userDn", tppInfoMgmtConfiguration.getUserDn());
		assertNotNull(tppInfoMgmtConfiguration.contextSource());
		assertNotNull(tppInfoMgmtConfiguration.ldapTemplate());
	}
}
