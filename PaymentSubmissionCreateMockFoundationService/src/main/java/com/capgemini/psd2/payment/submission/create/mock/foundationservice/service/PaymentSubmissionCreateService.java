package com.capgemini.psd2.payment.submission.create.mock.foundationservice.service;

import com.capgemini.psd2.foundationservice.validator.ValidationPassed;
import com.capgemini.psd2.payment.submission.create.mock.foundationservice.domain.PaymentInstruction;

public interface PaymentSubmissionCreateService {
	
	public ValidationPassed validatePaymentInstruction(PaymentInstruction paymentInstruction);

}
