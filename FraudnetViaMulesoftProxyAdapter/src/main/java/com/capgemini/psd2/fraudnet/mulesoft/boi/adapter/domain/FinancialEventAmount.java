package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.SerializedName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * The financial value of the transaction
 */
@ApiModel(description = "The financial value of the transaction")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-03-19T13:42:54.062+05:30")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FinancialEventAmount {
	@SerializedName("groupReportingCurrency")
	private Double groupReportingCurrency;

	@SerializedName("localReportingCurrency")
	private Double localReportingCurrency;

	@SerializedName("transactionCurrency")
	private Double transactionCurrency;

	public FinancialEventAmount groupReportingCurrency(Double groupReportingCurrency) {
		this.groupReportingCurrency = groupReportingCurrency;
		return this;
	}

	/**
	 * The value in group reporting currency
	 * 
	 * @return groupReportingCurrency
	 **/
	@ApiModelProperty(value = "The value in group reporting currency")
	public Double getGroupReportingCurrency() {
		return groupReportingCurrency;
	}

	public void setGroupReportingCurrency(Double groupReportingCurrency) {
		this.groupReportingCurrency = groupReportingCurrency;
	}

	public FinancialEventAmount localReportingCurrency(Double localReportingCurrency) {
		this.localReportingCurrency = localReportingCurrency;
		return this;
	}

	/**
	 * The value in Local reporting currency
	 * 
	 * @return localReportingCurrency
	 **/
	@ApiModelProperty(value = "The value in Local reporting currency")
	public Double getLocalReportingCurrency() {
		return localReportingCurrency;
	}

	public void setLocalReportingCurrency(Double localReportingCurrency) {
		this.localReportingCurrency = localReportingCurrency;
	}

	public FinancialEventAmount transactionCurrency(Double transactionCurrency) {
		this.transactionCurrency = transactionCurrency;
		return this;
	}

	/**
	 * The actual value in the transaction currency
	 * 
	 * @return transactionCurrency
	 **/
	@ApiModelProperty(required = true, value = "The actual value in the transaction currency")
	public Double getTransactionCurrency() {
		return transactionCurrency;
	}

	public void setTransactionCurrency(Double transactionCurrency) {
		this.transactionCurrency = transactionCurrency;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		FinancialEventAmount financialEventAmount = (FinancialEventAmount) o;
		return Objects.equals(this.groupReportingCurrency, financialEventAmount.groupReportingCurrency)
				&& Objects.equals(this.localReportingCurrency, financialEventAmount.localReportingCurrency)
				&& Objects.equals(this.transactionCurrency, financialEventAmount.transactionCurrency);
	}

	@Override
	public int hashCode() {
		return Objects.hash(groupReportingCurrency, localReportingCurrency, transactionCurrency);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class FinancialEventAmount {\n");

		sb.append("    groupReportingCurrency: ").append(toIndentedString(groupReportingCurrency)).append("\n");
		sb.append("    localReportingCurrency: ").append(toIndentedString(localReportingCurrency)).append("\n");
		sb.append("    transactionCurrency: ").append(toIndentedString(transactionCurrency)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

}
