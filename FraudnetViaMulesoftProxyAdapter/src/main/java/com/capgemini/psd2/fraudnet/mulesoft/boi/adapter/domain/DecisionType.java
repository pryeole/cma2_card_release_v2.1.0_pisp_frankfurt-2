package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * The decision type obtained after immediate decision defined by the risk engine results and model threshold settings and eventual risk assessment.
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public enum DecisionType {
  
  APPROVE("APPROVE"),
  
  INVESTIGATE_APPROVE("INVESTIGATE - APPROVE"),
  
  INVESTIGATE_CANCEL("INVESTIGATE - CANCEL"),
  
  INVESTIGATE_WAIT("INVESTIGATE - WAIT"),
  
  REJECT("REJECT");

  private String value;

  DecisionType(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static DecisionType fromValue(String text) {
    for (DecisionType b : DecisionType.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }

}

