package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.SerializedName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * The party entity on whose behalf the event was orignated
 */
@ApiModel(description = "The party entity on whose behalf the event was orignated")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-03-19T13:42:54.062+05:30")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Party {
	@SerializedName("partyName")
	private String partyName = null;

	@SerializedName("gcisCustomertype")
	private GcisCustomertype gcisCustomertype = null;

	@SerializedName("taxIdNumber")
	private String taxIdNumber = null;

	@SerializedName("contactInformation")
	private ContactInformation contactInformation = null;

	@SerializedName("address")
	private Address address = null;

	@SerializedName("person")
	private Person person = null;

	public Party partyName(String partyName) {
		this.partyName = partyName;
		return this;
	}

	/**
	 * This is the plain text representation of the name of the party (e.g. John
	 * Brooks, Vauxhall, Dublin University Hospital etc.).
	 * 
	 * @return partyName
	 **/
	@ApiModelProperty(required = true, value = "This is the plain text representation of the name of the party (e.g. John Brooks, Vauxhall, Dublin University Hospital etc.).")
	public String getPartyName() {
		return partyName;
	}

	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}

	public Party gcisCustomertype(GcisCustomertype gcisCustomertype) {
		this.gcisCustomertype = gcisCustomertype;
		return this;
	}

	/**
	 * Get gcisCustomertype
	 * 
	 * @return gcisCustomertype
	 **/
	@ApiModelProperty(value = "")
	public GcisCustomertype getGcisCustomertype() {
		return gcisCustomertype;
	}

	public void setGcisCustomertype(GcisCustomertype gcisCustomertype) {
		this.gcisCustomertype = gcisCustomertype;
	}

	public Party taxIdNumber(String taxIdNumber) {
		this.taxIdNumber = taxIdNumber;
		return this;
	}

	/**
	 * This is the tax-number a party may have either for individual or
	 * organisation taxation purpose
	 * 
	 * @return taxIdNumber
	 **/
	@ApiModelProperty(value = "This is the tax-number a party may have either for individual or organisation taxation purpose")
	public String getTaxIdNumber() {
		return taxIdNumber;
	}

	public void setTaxIdNumber(String taxIdNumber) {
		this.taxIdNumber = taxIdNumber;
	}

	public Party contactInformation(ContactInformation contactInformation) {
		this.contactInformation = contactInformation;
		return this;
	}

	/**
	 * Get contactInformation
	 * 
	 * @return contactInformation
	 **/
	@ApiModelProperty(required = true, value = "")
	public ContactInformation getContactInformation() {
		return contactInformation;
	}

	public void setContactInformation(ContactInformation contactInformation) {
		this.contactInformation = contactInformation;
	}

	public Party address(Address address) {
		this.address = address;
		return this;
	}

	/**
	 * Get address
	 * 
	 * @return address
	 **/
	@ApiModelProperty(value = "")
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Party person(Person person) {
		this.person = person;
		return this;
	}

	/**
	 * Get person
	 * 
	 * @return person
	 **/
	@ApiModelProperty(value = "")
	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Party party = (Party) o;
		return Objects.equals(this.partyName, party.partyName)
				&& Objects.equals(this.gcisCustomertype, party.gcisCustomertype)
				&& Objects.equals(this.taxIdNumber, party.taxIdNumber)
				&& Objects.equals(this.contactInformation, party.contactInformation)
				&& Objects.equals(this.address, party.address) && Objects.equals(this.person, party.person);
	}

	@Override
	public int hashCode() {
		return Objects.hash(partyName, gcisCustomertype, taxIdNumber, contactInformation, address, person);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Party {\n");

		sb.append("    partyName: ").append(toIndentedString(partyName)).append("\n");
		sb.append("    gcisCustomertype: ").append(toIndentedString(gcisCustomertype)).append("\n");
		sb.append("    taxIdNumber: ").append(toIndentedString(taxIdNumber)).append("\n");
		sb.append("    contactInformation: ").append(toIndentedString(contactInformation)).append("\n");
		sb.append("    address: ").append(toIndentedString(address)).append("\n");
		sb.append("    person: ").append(toIndentedString(person)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

}
