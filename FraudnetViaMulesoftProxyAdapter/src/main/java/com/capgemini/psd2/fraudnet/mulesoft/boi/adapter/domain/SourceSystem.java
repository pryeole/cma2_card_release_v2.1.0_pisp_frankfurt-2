package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * The source system on which the event originated
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public enum SourceSystem {

	API_PLATFORM("API Platform"),

	BUSINESS_ONLINE("Business Online");

	private String value;

	SourceSystem(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	@Override
	@JsonValue
	public String toString() {
		return String.valueOf(value);
	}

	@JsonCreator
	public static SourceSystem fromValue(String text) {
		for (SourceSystem b : SourceSystem.values()) {
			if (String.valueOf(b.value).equals(text)) {
				return b;
			}
		}
		return null;
	}

}
