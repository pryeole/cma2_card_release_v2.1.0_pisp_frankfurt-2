package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.SerializedName;

import io.swagger.annotations.ApiModelProperty;

/**
 * EventData1
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-03-19T13:42:54.062+05:30")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EventData1 {
  @SerializedName("electronicDeviceSession")
  private ElectronicDeviceSession electronicDeviceSession = null;

  @SerializedName("onlineUserSession")
  private OnlineUserSession onlineUserSession = null;

  @SerializedName("onlineUser")
  private OnlineUser onlineUser = null;

  @SerializedName("party")
  private Party party = null;

  @SerializedName("account")
  private List<Account> account = null;

  @SerializedName("transaction")
  private Transaction transaction = null;

  public EventData1 electronicDeviceSession(ElectronicDeviceSession electronicDeviceSession) {
    this.electronicDeviceSession = electronicDeviceSession;
    return this;
  }

   /**
   * Get electronicDeviceSession
   * @return electronicDeviceSession
  **/
  @ApiModelProperty(required = true, value = "")
  public ElectronicDeviceSession getElectronicDeviceSession() {
    return electronicDeviceSession;
  }

  public void setElectronicDeviceSession(ElectronicDeviceSession electronicDeviceSession) {
    this.electronicDeviceSession = electronicDeviceSession;
  }

  public EventData1 onlineUserSession(OnlineUserSession onlineUserSession) {
    this.onlineUserSession = onlineUserSession;
    return this;
  }

   /**
   * Get onlineUserSession
   * @return onlineUserSession
  **/
  @ApiModelProperty(required = true, value = "")
  public OnlineUserSession getOnlineUserSession() {
    return onlineUserSession;
  }

  public void setOnlineUserSession(OnlineUserSession onlineUserSession) {
    this.onlineUserSession = onlineUserSession;
  }

  public EventData1 onlineUser(OnlineUser onlineUser) {
    this.onlineUser = onlineUser;
    return this;
  }

   /**
   * Get onlineUser
   * @return onlineUser
  **/
  @ApiModelProperty(required = true, value = "")
  public OnlineUser getOnlineUser() {
    return onlineUser;
  }

  public void setOnlineUser(OnlineUser onlineUser) {
    this.onlineUser = onlineUser;
  }

  public EventData1 party(Party party) {
    this.party = party;
    return this;
  }

   /**
   * Get party
   * @return party
  **/
  @ApiModelProperty(value = "")
  public Party getParty() {
    return party;
  }

  public void setParty(Party party) {
    this.party = party;
  }

  public EventData1 account(List<Account> account) {
    this.account = account;
    return this;
  }

  public EventData1 addAccountItem(Account accountItem) {
    if (this.account == null) {
      this.account = new ArrayList<Account>();
    }
    this.account.add(accountItem);
    return this;
  }

   /**
   * The account information of the end user used in the originating event. Multiple account information for Account - Authorize\&quot;
   * @return account
  **/
  @ApiModelProperty(value = "The account information of the end user used in the originating event. Multiple account information for Account - Authorize\"")
  public List<Account> getAccount() {
    return account;
  }

  public void setAccount(List<Account> account) {
    this.account = account;
  }

  public EventData1 transaction(Transaction transaction) {
    this.transaction = transaction;
    return this;
  }

   /**
   * Get transaction
   * @return transaction
  **/
  @ApiModelProperty(value = "")
  public Transaction getTransaction() {
    return transaction;
  }

  public void setTransaction(Transaction transaction) {
    this.transaction = transaction;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    EventData1 eventData1 = (EventData1) o;
    return Objects.equals(this.electronicDeviceSession, eventData1.electronicDeviceSession) &&
        Objects.equals(this.onlineUserSession, eventData1.onlineUserSession) &&
        Objects.equals(this.onlineUser, eventData1.onlineUser) &&
        Objects.equals(this.party, eventData1.party) &&
        Objects.equals(this.account, eventData1.account) &&
        Objects.equals(this.transaction, eventData1.transaction);
  }

  @Override
  public int hashCode() {
    return Objects.hash(electronicDeviceSession, onlineUserSession, onlineUser, party, account, transaction);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class EventData1 {\n");
    
    sb.append("    electronicDeviceSession: ").append(toIndentedString(electronicDeviceSession)).append("\n");
    sb.append("    onlineUserSession: ").append(toIndentedString(onlineUserSession)).append("\n");
    sb.append("    onlineUser: ").append(toIndentedString(onlineUser)).append("\n");
    sb.append("    party: ").append(toIndentedString(party)).append("\n");
    sb.append("    account: ").append(toIndentedString(account)).append("\n");
    sb.append("    transaction: ").append(toIndentedString(transaction)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

