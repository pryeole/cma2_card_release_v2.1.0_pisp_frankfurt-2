package com.capgemini.psd2.pisp.payment.setup.test.mock.data;
// Mock Data
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTRequest;
import com.fasterxml.jackson.databind.ObjectMapper;

public class InternationalPaymentConsentsPOSTRequestResponseMockData {
	
	public static CustomIPaymentConsentsPOSTRequest getPaymentSubmissionPOSTRequest() {
		return new CustomIPaymentConsentsPOSTRequest();

	}

	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
