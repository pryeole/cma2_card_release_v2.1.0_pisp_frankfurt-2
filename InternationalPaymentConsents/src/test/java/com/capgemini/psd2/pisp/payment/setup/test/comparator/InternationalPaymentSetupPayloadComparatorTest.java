package com.capgemini.psd2.pisp.payment.setup.test.comparator;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBBranchAndFinancialInstitutionIdentification3;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBDomestic1;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBExternalAuthorisation1Code;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.OBExternalPaymentContext1Code;
import com.capgemini.psd2.pisp.domain.OBInternational1;
import com.capgemini.psd2.pisp.domain.OBPartyIdentification43;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticConsentResponse1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalConsentResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseLinks;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseMeta;
import com.capgemini.psd2.pisp.payment.setup.comparator.InternationalPaymentSetupPayloadComparator;
import com.capgemini.psd2.pisp.utilities.PispUtilities;

@RunWith(SpringJUnit4ClassRunner.class)
public class InternationalPaymentSetupPayloadComparatorTest {

	@InjectMocks
	InternationalPaymentSetupPayloadComparator comparator;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void compareTest() {

		// For Equality

		CustomIPaymentConsentsPOSTResponse response = new CustomIPaymentConsentsPOSTResponse();
		OBWriteDataInternationalConsentResponse1 data = new OBWriteDataInternationalConsentResponse1();
		response.setData(data);
		data.setConsentId("12345");
		data.setCreationDateTime(PispUtilities.getCurrentDateInISOFormat());
		data.setStatus(OBExternalConsentStatus1Code.AWAITINGAUTHORISATION);

		OBRisk1 risk = new OBRisk1();
		response.setRisk(risk);

		OBInternational1 initiation = new OBInternational1();
		data.setInitiation(initiation);
		initiation.setInstructionIdentification("ABDDC");
		initiation.setEndToEndIdentification("FRESCO.21302.GFX.28");

		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("777777777.00");
		instructedAmount.setCurrency("EUR");
		initiation.setInstructedAmount(instructedAmount);

		OBBranchAndFinancialInstitutionIdentification3 creditorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		creditorAgent.setSchemeName("BOI");
		creditorAgent.setIdentification("SC080801");
		initiation.setCreditorAgent(creditorAgent);

		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSchemeName("testBOI");
		creditorAccount.setIdentification("NWBK60161331926819");
		creditorAccount.setName("Test user");
		creditorAccount.setSecondaryIdentification("0002");
		initiation.setCreditorAccount(creditorAccount);

		OBBranchAndFinancialInstitutionIdentification3 debtorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		debtorAgent.setSchemeName("debtorAgentBOI");
		debtorAgent.setIdentification("SC112800");

		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setSchemeName("Scheme");
		debtorAccount.setIdentification("GB29NWBK60161331926819");
		debtorAccount.setName("Andrea Smith");
		debtorAccount.setIdentification("0002");
		initiation.setDebtorAccount(debtorAccount);

		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		remittanceInformation.setReference("FRESCO-101");
		remittanceInformation.setUnstructured("Internal ops code 5120101");
		initiation.setRemittanceInformation(remittanceInformation);

		risk.setMerchantCategoryCode("5967");
		risk.setMerchantCustomerIdentification("053598653254");
		risk.paymentContextCode(OBExternalPaymentContext1Code.ECOMMERCESERVICES);

		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);

		List<String> addressLine = new ArrayList<>();
		addressLine.add("Flat 7");
		addressLine.add("Acacia Lodge");
		deliveryAddress.setAddressLine(addressLine);

		deliveryAddress.setBuildingNumber("27");
		deliveryAddress.setCountry("UK");

		deliveryAddress.setCountrySubDivision("DEFG");

		deliveryAddress.setPostCode("GU31 2ZZ");
		deliveryAddress.setStreetName("AcaciaAvenue");
		deliveryAddress.setTownName("Sparsholt");

		response.setLinks(new PaymentSetupPOSTResponseLinks());
		response.setMeta(new PaymentSetupPOSTResponseMeta());

		CustomIPaymentConsentsPOSTResponse response1 = new CustomIPaymentConsentsPOSTResponse();
		response1.setData(data);
		response1.setRisk(risk);

		response1.setLinks(new PaymentSetupPOSTResponseLinks());
		response1.setMeta(new PaymentSetupPOSTResponseMeta());

		int result = comparator.compare(response, response1);
		assertEquals(0, result);

		// For Non-Equality

		CustomIPaymentConsentsPOSTResponse response2 = new CustomIPaymentConsentsPOSTResponse();
		OBWriteDataInternationalConsentResponse1 data1 = new OBWriteDataInternationalConsentResponse1();
		response2.setData(data1);
		data1.setConsentId("12345");
		data1.setCreationDateTime(PispUtilities.getCurrentDateInISOFormat());
		data1.setStatus(OBExternalConsentStatus1Code.AUTHORISED);

		OBRisk1 risk1 = new OBRisk1();
		response1.setRisk(risk1);

		OBInternational1 initiation1 = new OBInternational1();
		data.setInitiation(initiation1);

		initiation1.setInstructionIdentification("ABDDC");
		initiation1.setEndToEndIdentification("FRESCO.21302.GFX.28");

		OBDomestic1InstructedAmount instructedAmount1 = new OBDomestic1InstructedAmount();
		instructedAmount1.setAmount("777777777.00");
		instructedAmount1.setCurrency("EUR");
		initiation1.setInstructedAmount(instructedAmount1);

		OBBranchAndFinancialInstitutionIdentification3 creditorAgent1 = new OBBranchAndFinancialInstitutionIdentification3();
		creditorAgent1.setSchemeName("BOI");
		creditorAgent1.setIdentification("SC080801");
		initiation1.setCreditorAgent(creditorAgent1);

		OBCashAccountCreditor2 creditorAccount1 = new OBCashAccountCreditor2();
		creditorAccount1.setSchemeName("testBOI");
		creditorAccount1.setIdentification("NWBK60161331926819");
		creditorAccount1.setName("Test user");
		creditorAccount1.setSecondaryIdentification("0002");
		initiation1.setCreditorAccount(creditorAccount1);

		OBBranchAndFinancialInstitutionIdentification3 debtorAgent1 = new OBBranchAndFinancialInstitutionIdentification3();
		debtorAgent1.setSchemeName("debtorAgent2BOI");
		debtorAgent1.setIdentification("SC112800");

		OBCashAccountDebtor3 debtorAccount1 = new OBCashAccountDebtor3();
		debtorAccount1.setSchemeName("Scheme1");
		debtorAccount1.setIdentification("GB29NWBK60161331926819");
		debtorAccount1.setName("Andrea Smith");
		debtorAccount1.setSecondaryIdentification("0002");
		initiation1.setDebtorAccount(debtorAccount1);

		OBRemittanceInformation1 remittanceInformation1 = new OBRemittanceInformation1();
		remittanceInformation1.setReference("FRESCO-101");
		remittanceInformation1.setUnstructured("Internal ops code 5120101");
		initiation1.setRemittanceInformation(remittanceInformation1);

		risk1.setMerchantCategoryCode("5967");
		risk1.setMerchantCustomerIdentification("053598653254");
		risk1.setPaymentContextCode(OBExternalPaymentContext1Code.BILLPAYMENT);

		OBRisk1DeliveryAddress deliveryAddress1 = new OBRisk1DeliveryAddress();
		risk1.setDeliveryAddress(deliveryAddress1);

		List<String> addressLine1 = new ArrayList<>();
		addressLine1.add("Flat 7");
		addressLine1.add("Acacia Lodge");
		deliveryAddress1.setAddressLine(addressLine1);

		deliveryAddress1.setBuildingNumber("27");
		deliveryAddress1.setCountry("UK");

		deliveryAddress1.setCountrySubDivision("DEFG");

		deliveryAddress1.setPostCode("GU31 2ZZ");
		deliveryAddress1.setStreetName("AcaciaAvenue");
		deliveryAddress1.setTownName("Sparsholt");

		response1.setLinks(new PaymentSetupPOSTResponseLinks());
		response1.setMeta(new PaymentSetupPOSTResponseMeta());

		result = comparator.compare(response, response1);
		assertEquals(1, result);
	}

	@Test
	public void comparePaymentDetailsTest() {

		CustomIPaymentConsentsPOSTRequest request = new CustomIPaymentConsentsPOSTRequest();
		OBWriteDataInternationalConsent1 data = new OBWriteDataInternationalConsent1();

		request.setData(data);
		OBRisk1 risk = new OBRisk1();
		request.setRisk(risk);

		OBInternational1 initiation = new OBInternational1();
		data.setInitiation(initiation);

		initiation.setInstructionIdentification("ABDDC");
		initiation.setEndToEndIdentification("FRESCO.21302.GFX.28");

		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("777777777.00");
		instructedAmount.setCurrency("EUR");
		initiation.setInstructedAmount(instructedAmount);
		
		OBBranchAndFinancialInstitutionIdentification3 creditorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		creditorAgent.setSchemeName("creditorScheme");
		creditorAgent.setIdentification("SC080801");
		initiation.setCreditorAgent(creditorAgent);

		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSchemeName("BOI");
		creditorAccount.setIdentification("NWBK60161331926819");
		creditorAccount.setName("Test user");
		creditorAccount.setSecondaryIdentification("0002");
		initiation.setCreditorAccount(creditorAccount);
		initiation.setCreditor(new OBPartyIdentification43());
		initiation.getCreditor().setPostalAddress(new OBPostalAddress6());
		initiation.getCreditor().getPostalAddress().setCountry("ABC");
		initiation.getCreditor().getPostalAddress().setAddressLine(null);


		OBBranchAndFinancialInstitutionIdentification3 debtorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		debtorAgent.setSchemeName("debtorScheme");
		debtorAgent.setIdentification("SC112800");

		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setSchemeName("debtorAccountScheme");
		debtorAccount.setIdentification("GB29NWBK60161331926819");
		debtorAccount.setName("Andrea Smith");
		debtorAccount.setSecondaryIdentification("0002");
		initiation.setDebtorAccount(debtorAccount);

		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		remittanceInformation.setReference(null);
		remittanceInformation.setUnstructured(null);
		initiation.setRemittanceInformation(remittanceInformation);

		risk.setMerchantCategoryCode("5967");
		risk.setMerchantCustomerIdentification("053598653254");
		risk.setPaymentContextCode(OBExternalPaymentContext1Code.BILLPAYMENT);

		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);

		List<String> addressLine = new ArrayList<>();
		addressLine.add("Flat 7");
		addressLine.add("Acacia Lodge");
		deliveryAddress.setAddressLine(addressLine);

		deliveryAddress.setBuildingNumber("27");
		deliveryAddress.setCountry("UK");

		deliveryAddress.setCountrySubDivision("DEFG");

		deliveryAddress.setPostCode("GU31 2ZZ");
		deliveryAddress.setStreetName("AcaciaAvenue");
		deliveryAddress.setTownName("Sparsholt");

		CustomIPaymentConsentsPOSTResponse response1 = new CustomIPaymentConsentsPOSTResponse();

		OBWriteDataInternationalConsentResponse1 data1 = new OBWriteDataInternationalConsentResponse1();
		data1.setConsentId("12345");
		data1.setCreationDateTime(PispUtilities.getCurrentDateInISOFormat());
		data1.setStatus(OBExternalConsentStatus1Code.AWAITINGAUTHORISATION);

		OBRisk1 risk1 = new OBRisk1();
		response1.setRisk(risk1);

		OBInternational1 initiation1 = new OBInternational1();
		response1.setData(new OBWriteDataInternationalConsentResponse1());

		initiation1.setInstructionIdentification("ABDDC");
		initiation1.setEndToEndIdentification("FRESCO.21302.GFX.28");
		initiation1.setRemittanceInformation(null);
		
		OBDomestic1InstructedAmount instructedAmount1 = new OBDomestic1InstructedAmount();
		instructedAmount1.setAmount("777777777.00");
		instructedAmount1.setCurrency("EUR");
		initiation1.setInstructedAmount(instructedAmount1);

		OBBranchAndFinancialInstitutionIdentification3 creditorAgent1 = new OBBranchAndFinancialInstitutionIdentification3();
		creditorAgent1.setSchemeName("creditorScheme");
		creditorAgent1.setIdentification("SC080801");
		initiation1.setCreditorAgent(creditorAgent1);
		OBCashAccountCreditor2 creditorAccount1 = new OBCashAccountCreditor2();
		creditorAccount1.setSchemeName("");
		creditorAccount1.setIdentification("NWBK60161331926819");
		creditorAccount1.setName("Test user");
		creditorAccount1.setIdentification("0002");
		initiation1.setCreditorAccount(creditorAccount1);

		OBBranchAndFinancialInstitutionIdentification3 debtorAgent1 = new OBBranchAndFinancialInstitutionIdentification3();
		debtorAgent1.setSchemeName("debtorAgent2BOI");
		debtorAgent1.setIdentification("SC112800");

		OBCashAccountDebtor3 debtorAccount1 = new OBCashAccountDebtor3();
		debtorAccount1.setSchemeName("Scheme1");
		debtorAccount1.setIdentification("GB29NWBK60161331926819");
		debtorAccount1.setName("Andrea Smith");
		debtorAccount1.setSecondaryIdentification("0002");
		initiation1.setDebtorAccount(debtorAccount1);
		
		response1.getData().setInitiation(initiation1);
		response1.getData().getInitiation().setDebtorAccount(debtorAccount1);

		risk1.setMerchantCategoryCode("5967");
		risk1.setMerchantCustomerIdentification("053598653254");
		risk1.setPaymentContextCode(OBExternalPaymentContext1Code.BILLPAYMENT);

		OBRisk1DeliveryAddress deliveryAddress1 = new OBRisk1DeliveryAddress();
		risk1.setDeliveryAddress(deliveryAddress1);

		List<String> addressLine1 = new ArrayList<>();
		addressLine1.add("Flat 7");
		addressLine1.add("Acacia Lodge");
		deliveryAddress1.setAddressLine(addressLine1);

		deliveryAddress1.setBuildingNumber("27");
		deliveryAddress1.setCountry("UK");

		deliveryAddress1.setCountrySubDivision("DEFG");

		deliveryAddress1.setPostCode("GU31 2ZZ");
		deliveryAddress1.setStreetName("AcaciaAvenue");
		deliveryAddress1.setTownName("Sparsholt");

		response1.setLinks(new PaymentSetupPOSTResponseLinks());
		response1.setMeta(new PaymentSetupPOSTResponseMeta());
		
		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();
		resource.setTppDebtorDetails("true");
		resource.setTppDebtorNameDetails("true");
		int result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);
		resource.setTppDebtorDetails("false");
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);

		// DebtorAccount null
		initiation.setCreditorAgent(null);
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);

		// DebtorAccount null
		initiation.setCreditorAgent(null);
		initiation1.setDebtorAccount(null);
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);

		// TppDebtorNameDetails is false
		resource.setTppDebtorDetails("true");
		resource.setTppDebtorNameDetails("false");
		response1.getData().getInitiation().setDebtorAccount(debtorAccount1);
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);

		// request debtorAccount is null
		response1.getData().getInitiation().setDebtorAccount(debtorAccount1);
		resource.setTppDebtorDetails("false");
		request.getData().getInitiation().setDebtorAccount(null);
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);

		resource.setTppDebtorDetails("false");
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);
	}

	@Test
	public void comparePaymentDetailsTest2() {

		CustomIPaymentConsentsPOSTRequest request = new CustomIPaymentConsentsPOSTRequest();
		OBWriteDataInternationalConsent1 data = new OBWriteDataInternationalConsent1();

		request.setData(data);
		OBRisk1 risk = new OBRisk1();
		request.setRisk(risk);

		OBInternational1 initiation = new OBInternational1();
		data.setInitiation(initiation);

		initiation.setInstructionIdentification("ABDDC");
		initiation.setEndToEndIdentification("FRESCO.21302.GFX.28");

		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("777777777.00");
		instructedAmount.setCurrency("EUR");
		initiation.setInstructedAmount(instructedAmount);
		initiation.setCreditor(new OBPartyIdentification43());
		initiation.getCreditor().setPostalAddress(new OBPostalAddress6());
		initiation.getCreditor().getPostalAddress().setCountry("ABC");
		initiation.getCreditor().getPostalAddress().setAddressLine(null);


		OBBranchAndFinancialInstitutionIdentification3 creditorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		creditorAgent.setSchemeName("creditorScheme");
		creditorAgent.setIdentification("SC080801");
		initiation.setCreditorAgent(creditorAgent);

		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSchemeName("BOI");
		creditorAccount.setIdentification("NWBK60161331926819");
		creditorAccount.setName("Test user");
		creditorAccount.setSecondaryIdentification("0002");
		initiation.setCreditorAccount(creditorAccount);

		OBBranchAndFinancialInstitutionIdentification3 debtorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		debtorAgent.setSchemeName("debtorScheme");
		debtorAgent.setIdentification("SC112800");

		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setSchemeName("debtorAccountScheme");
		debtorAccount.setIdentification("GB29NWBK60161331926819");
		debtorAccount.setName("Andrea Smith");
		debtorAccount.setSecondaryIdentification("0002");
		initiation.setDebtorAccount(debtorAccount);

		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		remittanceInformation.setReference("FRESCO-101");
		remittanceInformation.setUnstructured("Internal ops code 5120101");
		initiation.setRemittanceInformation(remittanceInformation);

		risk.setMerchantCategoryCode("5967");
		risk.setMerchantCustomerIdentification("053598653254");
		risk.setPaymentContextCode(OBExternalPaymentContext1Code.BILLPAYMENT);

		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);

		List<String> addressLine = new ArrayList<>();
		addressLine.add("Flat 7");
		addressLine.add("Acacia Lodge");
		deliveryAddress.setAddressLine(addressLine);

		deliveryAddress.setBuildingNumber("27");
		deliveryAddress.setCountry("UK");

		deliveryAddress.setCountrySubDivision("DEFG");

		deliveryAddress.setPostCode("GU31 2ZZ");
		deliveryAddress.setStreetName("AcaciaAvenue");
		deliveryAddress.setTownName("Sparsholt");

		CustomIPaymentConsentsPOSTResponse response1 = new CustomIPaymentConsentsPOSTResponse();

		OBWriteDataInternationalConsentResponse1 data1 = new OBWriteDataInternationalConsentResponse1();
		response1.setData(data1);
		data1.setConsentId("12345");
		data1.setCreationDateTime(PispUtilities.getCurrentDateInISOFormat());
		data1.setStatus(OBExternalConsentStatus1Code.AWAITINGAUTHORISATION);

		OBRisk1 risk1 = new OBRisk1();
		response1.setRisk(risk1);

		OBInternational1 initiation1 = new OBInternational1();
		data1.setInitiation(initiation1);

		initiation1.setInstructionIdentification("ABDDC");
		initiation1.setEndToEndIdentification("FRESCO.21302.GFX.28");

		OBDomestic1InstructedAmount instructedAmount1 = new OBDomestic1InstructedAmount();
		instructedAmount1.setAmount("777777777.00");
		instructedAmount1.setCurrency("EUR");
		initiation1.setInstructedAmount(instructedAmount1);

		OBBranchAndFinancialInstitutionIdentification3 creditorAgent1 = new OBBranchAndFinancialInstitutionIdentification3();
		creditorAgent1.setSchemeName("creditorScheme");
		creditorAgent1.setIdentification("SC080801");
		initiation1.setCreditorAgent(creditorAgent1);
		OBCashAccountCreditor2 creditorAccount1 = new OBCashAccountCreditor2();
		creditorAccount1.setSchemeName("");
		creditorAccount1.setIdentification("NWBK60161331926819");
		creditorAccount1.setName("Test user");
		creditorAccount1.setIdentification("0002");
		initiation1.setCreditorAccount(creditorAccount1);

		OBBranchAndFinancialInstitutionIdentification3 debtorAgent1 = new OBBranchAndFinancialInstitutionIdentification3();
		debtorAgent1.setSchemeName("debtorAgent2BOI");
		debtorAgent1.setIdentification("SC112800");

		OBCashAccountDebtor3 debtorAccount1 = new OBCashAccountDebtor3();
		debtorAccount1.setSchemeName("Scheme1");
		debtorAccount1.setIdentification("GB29NWBK60161331926819");
		debtorAccount1.setName("Andrea Smith");
		debtorAccount1.setSecondaryIdentification("0002");
		initiation1.setDebtorAccount(debtorAccount1);

		OBRemittanceInformation1 remittanceInformation1 = new OBRemittanceInformation1();
		remittanceInformation1.setReference("FRESCO-101");
		remittanceInformation1.setUnstructured("Internal ops code 5120101");
		initiation1.setRemittanceInformation(remittanceInformation1);

		risk1.setMerchantCategoryCode("5967");
		risk1.setMerchantCustomerIdentification("053598653254");
		risk1.setPaymentContextCode(OBExternalPaymentContext1Code.BILLPAYMENT);

		OBRisk1DeliveryAddress deliveryAddress1 = new OBRisk1DeliveryAddress();
		risk1.setDeliveryAddress(deliveryAddress1);

		List<String> addressLine1 = new ArrayList<>();
		addressLine1.add("Flat 7");
		addressLine1.add("Acacia Lodge");
		deliveryAddress1.setAddressLine(addressLine1);

		deliveryAddress1.setBuildingNumber("27");
		deliveryAddress1.setCountry("UK");

		deliveryAddress1.setCountrySubDivision("DEFG");

		deliveryAddress1.setPostCode("GU31 2ZZ");
		deliveryAddress1.setStreetName("AcaciaAvenue");
		deliveryAddress1.setTownName("Sparsholt");

		response1.setLinks(new PaymentSetupPOSTResponseLinks());
		response1.setMeta(new PaymentSetupPOSTResponseMeta());

		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();
		resource.setTppDebtorDetails("true");
		resource.setTppDebtorNameDetails("true");
		int result = comparator.comparePaymentDetails(response1, request, resource);
		// assertEquals(1, result);
		resource.setTppDebtorDetails("false");
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);

		// DebtorAccount null
		initiation.setCreditorAgent(null);
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);

		// DebtorAccount null
		initiation.setCreditorAgent(null);
		initiation1.setDebtorAccount(null);
		result = comparator.comparePaymentDetails(response1, request, resource);
		// assertEquals(1, result);

		// TppDebtorNameDetails is false
		resource.setTppDebtorDetails("true");
		resource.setTppDebtorNameDetails("false");
		response1.getData().getInitiation().setDebtorAccount(debtorAccount1);
		result = comparator.comparePaymentDetails(response1, request, resource);

		response1.getData().getInitiation().setDebtorAccount(debtorAccount1);
		resource.setTppDebtorDetails("false");
		request.getData().getInitiation().setDebtorAccount(null);
		result = comparator.comparePaymentDetails(response1, request, resource);

		resource.setTppDebtorDetails("false");
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);
	}

	@After
	public void tearDown() throws Exception {
		comparator = null;
	}

	@Test
	public void comparePaymentDetailsDeliveryAddressNullTest() {

		CustomIPaymentConsentsPOSTRequest request = new CustomIPaymentConsentsPOSTRequest();
		OBWriteDataInternationalConsent1 data = new OBWriteDataInternationalConsent1();

		request.setData(data);
		OBRisk1 risk = new OBRisk1();
		request.setRisk(risk);

		OBInternational1 initiation = new OBInternational1();
		data.setInitiation(initiation);

		initiation.setInstructionIdentification("ABDDC");
		initiation.setEndToEndIdentification("FRESCO.21302.GFX.28");

		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("777777777.00");
		instructedAmount.setCurrency("EUR");
		initiation.setInstructedAmount(instructedAmount);
		initiation.setCreditor(new OBPartyIdentification43());
		initiation.getCreditor().setPostalAddress(new OBPostalAddress6());
		initiation.getCreditor().getPostalAddress().setCountry("ABC");
		initiation.getCreditor().getPostalAddress().setAddressLine(null);


		OBBranchAndFinancialInstitutionIdentification3 creditorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		creditorAgent.setSchemeName("creditorScheme");
		creditorAgent.setIdentification("SC080801");
		initiation.setCreditorAgent(creditorAgent);

		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSchemeName("BOI");
		creditorAccount.setIdentification("NWBK60161331926819");
		creditorAccount.setName("Test user");
		creditorAccount.setSecondaryIdentification("0002");
		initiation.setCreditorAccount(creditorAccount);

		OBBranchAndFinancialInstitutionIdentification3 debtorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		debtorAgent.setSchemeName("debtorScheme");
		debtorAgent.setIdentification("SC112800");

		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setSchemeName("debtorAccountScheme");
		debtorAccount.setIdentification("GB29NWBK60161331926819");
		debtorAccount.setName("Andrea Smith");
		debtorAccount.setSecondaryIdentification("0002");
		initiation.setDebtorAccount(debtorAccount);

		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		remittanceInformation.setReference("FRESCO-101");
		remittanceInformation.setUnstructured("Internal ops code 5120101");
		initiation.setRemittanceInformation(remittanceInformation);

		risk.setMerchantCategoryCode("5967");
		risk.setMerchantCustomerIdentification("053598653254");
		risk.setPaymentContextCode(OBExternalPaymentContext1Code.BILLPAYMENT);

		CustomIPaymentConsentsPOSTResponse response1 = new CustomIPaymentConsentsPOSTResponse();

		OBWriteDataInternationalConsentResponse1 data1 = new OBWriteDataInternationalConsentResponse1();
		response1.setData(data1);
		data1.setConsentId("12345");
		data1.setCreationDateTime(PispUtilities.getCurrentDateInISOFormat());
		data1.setStatus(OBExternalConsentStatus1Code.AWAITINGAUTHORISATION);

		OBRisk1 risk1 = new OBRisk1();
		response1.setRisk(risk1);

		OBInternational1 initiation1 = new OBInternational1();
		data1.setInitiation(initiation1);

		initiation1.setInstructionIdentification("ABDDC");
		initiation1.setEndToEndIdentification("FRESCO.21302.GFX.28");

		OBDomestic1InstructedAmount instructedAmount1 = new OBDomestic1InstructedAmount();
		instructedAmount1.setAmount("777777777.00");
		instructedAmount1.setCurrency("EUR");
		initiation1.setInstructedAmount(instructedAmount1);

		OBBranchAndFinancialInstitutionIdentification3 creditorAgent1 = new OBBranchAndFinancialInstitutionIdentification3();
		creditorAgent1.setSchemeName("creditorScheme");
		creditorAgent1.setIdentification("SC080801");
		initiation1.setCreditorAgent(creditorAgent1);
		OBCashAccountCreditor2 creditorAccount1 = new OBCashAccountCreditor2();
		creditorAccount1.setSchemeName("");
		creditorAccount1.setIdentification("NWBK60161331926819");
		creditorAccount1.setName("Test user");
		creditorAccount1.setIdentification("0002");
		initiation1.setCreditorAccount(creditorAccount1);

		OBBranchAndFinancialInstitutionIdentification3 debtorAgent1 = new OBBranchAndFinancialInstitutionIdentification3();
		debtorAgent1.setSchemeName("debtorAgent2BOI");
		debtorAgent1.setIdentification("SC112800");

		OBCashAccountDebtor3 debtorAccount1 = new OBCashAccountDebtor3();
		debtorAccount1.setSchemeName("Scheme1");
		debtorAccount1.setIdentification("GB29NWBK60161331926819");
		debtorAccount1.setName("Andrea Smith");
		debtorAccount1.setSecondaryIdentification("0002");
		initiation1.setDebtorAccount(debtorAccount1);

		OBRemittanceInformation1 remittanceInformation1 = new OBRemittanceInformation1();
		remittanceInformation1.setReference("FRESCO-101");
		remittanceInformation1.setUnstructured("Internal ops code 5120101");
		initiation1.setRemittanceInformation(remittanceInformation1);

		risk1.setMerchantCategoryCode("5967");
		risk1.setMerchantCustomerIdentification("053598653254");
		risk1.setPaymentContextCode(OBExternalPaymentContext1Code.BILLPAYMENT);

		OBRisk1DeliveryAddress deliveryAddress1 = new OBRisk1DeliveryAddress();
		risk1.setDeliveryAddress(deliveryAddress1);

		List<String> addressLine1 = new ArrayList<>();
		addressLine1.add("");
		deliveryAddress1.setAddressLine(addressLine1);

		deliveryAddress1.setBuildingNumber("27");
		deliveryAddress1.setCountry("UK");

		deliveryAddress1.setCountrySubDivision("DEFG");

		deliveryAddress1.setPostCode("GU31 2ZZ");
		deliveryAddress1.setStreetName("AcaciaAvenue");
		deliveryAddress1.setTownName("Sparsholt");

		response1.setLinks(new PaymentSetupPOSTResponseLinks());
		response1.setMeta(new PaymentSetupPOSTResponseMeta());

		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();
		resource.setTppDebtorDetails("true");
		resource.setTppDebtorNameDetails("true");
		int result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);
		resource.setTppDebtorDetails("false");
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);

		// DebtorAccount null
		initiation.setCreditorAgent(null);
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);

		// DebtorAccount null
		initiation.setCreditorAgent(null);
		initiation1.setDebtorAccount(null);
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);

		// TppDebtorNameDetails is false
		resource.setTppDebtorDetails("true");
		resource.setTppDebtorNameDetails("false");
		response1.getData().getInitiation().setDebtorAccount(debtorAccount1);
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);

		// request debtorAccount is null
		response1.getData().getInitiation().setDebtorAccount(debtorAccount1);
		resource.setTppDebtorDetails("false");
		request.getData().getInitiation().setDebtorAccount(null);
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);

		resource.setTppDebtorDetails("false");
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);
	}

	@Test
	public void comparePaymentDetailsRemittanceNullTest() {

		CustomIPaymentConsentsPOSTRequest request = new CustomIPaymentConsentsPOSTRequest();
		OBWriteDataInternationalConsent1 data = new OBWriteDataInternationalConsent1();

		request.setData(data);
		OBRisk1 risk = new OBRisk1();
		request.setRisk(risk);

		OBInternational1 initiation = new OBInternational1();
		data.setInitiation(initiation);

		initiation.setInstructionIdentification("ABDDC");
		initiation.setEndToEndIdentification("FRESCO.21302.GFX.28");

		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("777777777.00");
		instructedAmount.setCurrency("EUR");
		initiation.setInstructedAmount(instructedAmount);
		initiation.setCreditor(new OBPartyIdentification43());
		initiation.getCreditor().setPostalAddress(new OBPostalAddress6());
		initiation.getCreditor().getPostalAddress().setCountry("ABC");
		initiation.getCreditor().getPostalAddress().setAddressLine(null);


		OBBranchAndFinancialInstitutionIdentification3 creditorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		creditorAgent.setSchemeName("creditorScheme");
		creditorAgent.setIdentification("SC080801");
		initiation.setCreditorAgent(creditorAgent);

		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSchemeName("BOI");
		creditorAccount.setIdentification("NWBK60161331926819");
		creditorAccount.setName("Test user");
		creditorAccount.setSecondaryIdentification("0002");
		initiation.setCreditorAccount(creditorAccount);

		OBBranchAndFinancialInstitutionIdentification3 debtorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		debtorAgent.setSchemeName("debtorScheme");
		debtorAgent.setIdentification("SC112800");

		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setSchemeName("debtorAccountScheme");
		debtorAccount.setIdentification("GB29NWBK60161331926819");
		debtorAccount.setName("Andrea Smith");
		debtorAccount.setSecondaryIdentification("0002");
		initiation.setDebtorAccount(debtorAccount);

		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		remittanceInformation.setReference("FRESCO-101");
		remittanceInformation.setUnstructured("Internal ops code 5120101");
		initiation.setRemittanceInformation(remittanceInformation);

		risk.setMerchantCategoryCode("5967");
		risk.setMerchantCustomerIdentification("053598653254");
		risk.setPaymentContextCode(OBExternalPaymentContext1Code.BILLPAYMENT);

		CustomIPaymentConsentsPOSTResponse response1 = new CustomIPaymentConsentsPOSTResponse();

		OBWriteDataInternationalConsentResponse1 data1 = new OBWriteDataInternationalConsentResponse1();
		response1.setData(data1);
		data1.setConsentId("12345");
		data1.setCreationDateTime(PispUtilities.getCurrentDateInISOFormat());
		data1.setStatus(OBExternalConsentStatus1Code.AWAITINGAUTHORISATION);

		OBRisk1 risk1 = new OBRisk1();
		response1.setRisk(risk1);

		OBInternational1 initiation1 = new OBInternational1();
		data1.setInitiation(initiation1);

		initiation1.setInstructionIdentification("ABDDC");
		initiation1.setEndToEndIdentification("FRESCO.21302.GFX.28");

		OBDomestic1InstructedAmount instructedAmount1 = new OBDomestic1InstructedAmount();
		instructedAmount1.setAmount("777777777.00");
		instructedAmount1.setCurrency("EUR");
		initiation1.setInstructedAmount(instructedAmount1);

		OBBranchAndFinancialInstitutionIdentification3 creditorAgent1 = new OBBranchAndFinancialInstitutionIdentification3();
		creditorAgent1.setSchemeName("creditorScheme");
		creditorAgent1.setIdentification("SC080801");
		initiation1.setCreditorAgent(creditorAgent1);
		OBCashAccountCreditor2 creditorAccount1 = new OBCashAccountCreditor2();
		creditorAccount1.setSchemeName("");
		creditorAccount1.setIdentification("NWBK60161331926819");
		creditorAccount1.setName("Test user");
		creditorAccount1.setIdentification("0002");
		initiation1.setCreditorAccount(creditorAccount1);

		OBBranchAndFinancialInstitutionIdentification3 debtorAgent1 = new OBBranchAndFinancialInstitutionIdentification3();
		debtorAgent1.setSchemeName("debtorAgent2BOI");
		debtorAgent1.setIdentification("SC112800");

		OBCashAccountDebtor3 debtorAccount1 = new OBCashAccountDebtor3();
		debtorAccount1.setSchemeName("Scheme1");
		debtorAccount1.setIdentification("GB29NWBK60161331926819");
		debtorAccount1.setName("Andrea Smith");
		debtorAccount1.setSecondaryIdentification("0002");
		initiation1.setDebtorAccount(debtorAccount1);

		OBRemittanceInformation1 remittanceInformation1 = new OBRemittanceInformation1();
		remittanceInformation1.setReference("FRESCO-101");
		remittanceInformation1.setUnstructured("Internal ops code 5120101");
		initiation1.setRemittanceInformation(remittanceInformation1);

		risk1.setMerchantCategoryCode("5967");
		risk1.setMerchantCustomerIdentification("053598653254");
		risk1.setPaymentContextCode(OBExternalPaymentContext1Code.BILLPAYMENT);

		OBRisk1DeliveryAddress deliveryAddress1 = new OBRisk1DeliveryAddress();
		risk1.setDeliveryAddress(deliveryAddress1);

		List<String> addressLine1 = new ArrayList<>();
		addressLine1.add("Flat 7");
		addressLine1.add("Acacia Lodge");
		deliveryAddress1.setAddressLine(addressLine1);

		deliveryAddress1.setBuildingNumber("27");
		deliveryAddress1.setCountry("UK");

		deliveryAddress1.setCountrySubDivision("DEFG");

		deliveryAddress1.setPostCode("GU31 2ZZ");
		deliveryAddress1.setStreetName("AcaciaAvenue");
		deliveryAddress1.setTownName("Sparsholt");

		response1.setLinks(new PaymentSetupPOSTResponseLinks());
		response1.setMeta(new PaymentSetupPOSTResponseMeta());

		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();
		resource.setTppDebtorDetails("true");
		resource.setTppDebtorNameDetails("true");
		int result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);
		resource.setTppDebtorDetails("false");
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);

		// DebtorAccount null
		initiation.setCreditorAgent(null);
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);

		// DebtorAccount null
		initiation.setCreditorAgent(null);
		initiation1.setDebtorAccount(null);
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);

		// TppDebtorNameDetails is false
		resource.setTppDebtorDetails("true");
		resource.setTppDebtorNameDetails("false");
		response1.getData().getInitiation().setDebtorAccount(debtorAccount1);
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);

		// request debtorAccount is null
		response1.getData().getInitiation().setDebtorAccount(debtorAccount1);
		resource.setTppDebtorDetails("false");
		request.getData().getInitiation().setDebtorAccount(null);
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);

		resource.setTppDebtorDetails("false");
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);
	}

	@Test
	public void comparePaymentDetailsAddressLineNullTest() {

		CustomIPaymentConsentsPOSTRequest request = new CustomIPaymentConsentsPOSTRequest();
		OBWriteDataInternationalConsent1 data = new OBWriteDataInternationalConsent1();

		request.setData(data);
		OBRisk1 risk = new OBRisk1();
		request.setRisk(risk);

		OBInternational1 initiation = new OBInternational1();
		data.setInitiation(initiation);

		initiation.setInstructionIdentification("ABDDC");
		initiation.setEndToEndIdentification("FRESCO.21302.GFX.28");

		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("777777777.00");
		instructedAmount.setCurrency("EUR");
		initiation.setInstructedAmount(instructedAmount);
		initiation.setCreditor(new OBPartyIdentification43());
		initiation.getCreditor().setPostalAddress(new OBPostalAddress6());
		initiation.getCreditor().getPostalAddress().setCountry("ABC");
		
		List<String> addressLine = new ArrayList<>();
		addressLine.add("Flat 7");
		addressLine.add("Acacia Lodge");
		
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);

		deliveryAddress.setAddressLine(addressLine);

		deliveryAddress.setBuildingNumber("27");
		deliveryAddress.setCountry("UK");

		deliveryAddress.setCountrySubDivision("DEFG");

		deliveryAddress.setPostCode("GU31 2ZZ");
		deliveryAddress.setStreetName("AcaciaAvenue");
		deliveryAddress.setTownName("Sparsholt");

		initiation.getCreditor().getPostalAddress().setAddressLine(addressLine);

		OBBranchAndFinancialInstitutionIdentification3 creditorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		creditorAgent.setSchemeName("creditorScheme");
		creditorAgent.setIdentification("SC080801");
		initiation.setCreditorAgent(creditorAgent);

		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSchemeName("BOI");
		creditorAccount.setIdentification("NWBK60161331926819");
		creditorAccount.setName("Test user");
		creditorAccount.setSecondaryIdentification("0002");
		initiation.setCreditorAccount(creditorAccount);

		OBBranchAndFinancialInstitutionIdentification3 debtorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		debtorAgent.setSchemeName("debtorScheme");
		debtorAgent.setIdentification("SC112800");

		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setSchemeName("debtorAccountScheme");
		debtorAccount.setIdentification("GB29NWBK60161331926819");
		debtorAccount.setName("Andrea Smith");
		debtorAccount.setSecondaryIdentification("0002");
		initiation.setDebtorAccount(debtorAccount);

		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		remittanceInformation.setReference("FRESCO-101");
		remittanceInformation.setUnstructured("Internal ops code 5120101");
		initiation.setRemittanceInformation(remittanceInformation);

		risk.setMerchantCategoryCode("5967");
		risk.setMerchantCustomerIdentification("053598653254");
		risk.setPaymentContextCode(OBExternalPaymentContext1Code.BILLPAYMENT);

		CustomIPaymentConsentsPOSTResponse response1 = new CustomIPaymentConsentsPOSTResponse();

		OBWriteDataInternationalConsentResponse1 data1 = new OBWriteDataInternationalConsentResponse1();
		response1.setData(data1);
		data1.setConsentId("12345");
		data1.setCreationDateTime(PispUtilities.getCurrentDateInISOFormat());
		data1.setStatus(OBExternalConsentStatus1Code.AWAITINGAUTHORISATION);

		OBRisk1 risk1 = new OBRisk1();
		response1.setRisk(risk1);

		OBInternational1 initiation1 = new OBInternational1();
		data1.setInitiation(initiation1);

		initiation1.setInstructionIdentification("ABDDC");
		initiation1.setEndToEndIdentification("FRESCO.21302.GFX.28");

		OBDomestic1InstructedAmount instructedAmount1 = new OBDomestic1InstructedAmount();
		instructedAmount1.setAmount("777777777.00");
		instructedAmount1.setCurrency("EUR");
		initiation1.setInstructedAmount(instructedAmount1);
		initiation1.setCreditor(new OBPartyIdentification43());
		initiation1.getCreditor().setPostalAddress(new OBPostalAddress6());
		initiation1.getCreditor().getPostalAddress().setCountry("ABCD");
		initiation1.getCreditor().getPostalAddress().setAddressLine(null);
		
		OBCashAccountDebtor3 debtorAccount1 = new OBCashAccountDebtor3();
		debtorAccount1.setSchemeName("Scheme1");
		debtorAccount1.setIdentification("GB29NWBK60161331926819");
		debtorAccount1.setName("Andrea Smith");
		debtorAccount1.setSecondaryIdentification("0002");
		initiation1.setDebtorAccount(debtorAccount1);

		OBRemittanceInformation1 remittanceInformation1 = new OBRemittanceInformation1();
		remittanceInformation1.setReference("FRESCO-101");
		remittanceInformation1.setUnstructured("Internal ops code 5120101");
		initiation1.setRemittanceInformation(remittanceInformation1);

		risk1.setMerchantCategoryCode("5967");
		risk1.setMerchantCustomerIdentification("053598653254");
		risk1.setPaymentContextCode(OBExternalPaymentContext1Code.BILLPAYMENT);

		response1.setLinks(new PaymentSetupPOSTResponseLinks());
		response1.setMeta(new PaymentSetupPOSTResponseMeta());

		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();
		resource.setTppDebtorDetails("true");
		resource.setTppDebtorNameDetails("true");
		int result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);
		resource.setTppDebtorDetails("false");
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);

		// DebtorAccount null
		initiation.setCreditorAgent(null);
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);

		// DebtorAccount null
		initiation.setCreditorAgent(null);
		initiation1.setDebtorAccount(null);
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);

		// TppDebtorNameDetails is false
		resource.setTppDebtorDetails("true");
		resource.setTppDebtorNameDetails("false");
		response1.getData().getInitiation().setDebtorAccount(debtorAccount1);
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);

		// request debtorAccount is null
		response1.getData().getInitiation().setDebtorAccount(debtorAccount1);
		resource.setTppDebtorDetails("false");
		request.getData().getInitiation().setDebtorAccount(null);
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);

		resource.setTppDebtorDetails("false");
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);
	}

	@Test
	public void comparePaymentDetailsAmountTest() {

		CustomIPaymentConsentsPOSTRequest request = new CustomIPaymentConsentsPOSTRequest();
		OBWriteDataInternationalConsent1 data = new OBWriteDataInternationalConsent1();

		request.setData(data);
		OBRisk1 risk = new OBRisk1();
		request.setRisk(risk);

		OBInternational1 initiation = new OBInternational1();
		data.setInitiation(initiation);

		initiation.setInstructionIdentification("ABDDC");
		initiation.setEndToEndIdentification("FRESCO.21302.GFX.28");

		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("0");
		instructedAmount.setCurrency("EUR");
		initiation.setInstructedAmount(instructedAmount);
		initiation.setCreditor(new OBPartyIdentification43());
		initiation.getCreditor().setPostalAddress(new OBPostalAddress6());
		initiation.getCreditor().getPostalAddress().setCountry("ABC");
		
		List<String> addressLine = new ArrayList<>();
		addressLine.add("Flat 7");
		addressLine.add("Acacia Lodge");
		
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);

		deliveryAddress.setAddressLine(addressLine);

		deliveryAddress.setBuildingNumber("27");
		deliveryAddress.setCountry("UK");

		deliveryAddress.setCountrySubDivision("DEFG");

		deliveryAddress.setPostCode("GU31 2ZZ");
		deliveryAddress.setStreetName("AcaciaAvenue");
		deliveryAddress.setTownName("Sparsholt");

		initiation.getCreditor().getPostalAddress().setAddressLine(addressLine);

		OBBranchAndFinancialInstitutionIdentification3 creditorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		creditorAgent.setSchemeName("creditorScheme");
		creditorAgent.setIdentification("SC080801");
		initiation.setCreditorAgent(creditorAgent);

		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSchemeName("BOI");
		creditorAccount.setIdentification("NWBK60161331926819");
		creditorAccount.setName("Test user");
		creditorAccount.setSecondaryIdentification("0002");
		initiation.setCreditorAccount(creditorAccount);

		OBBranchAndFinancialInstitutionIdentification3 debtorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		debtorAgent.setSchemeName("debtorScheme");
		debtorAgent.setIdentification("SC112800");

		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setSchemeName("debtorAccountScheme");
		debtorAccount.setIdentification("GB29NWBK60161331926819");
		debtorAccount.setName("Andrea Smith");
		debtorAccount.setSecondaryIdentification("0002");
		initiation.setDebtorAccount(debtorAccount);

		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		remittanceInformation.setReference("FRESCO-101");
		remittanceInformation.setUnstructured("Internal ops code 5120101");
		initiation.setRemittanceInformation(remittanceInformation);

		risk.setMerchantCategoryCode("5967");
		risk.setMerchantCustomerIdentification("053598653254");
		risk.setPaymentContextCode(OBExternalPaymentContext1Code.BILLPAYMENT);

		CustomIPaymentConsentsPOSTResponse response1 = new CustomIPaymentConsentsPOSTResponse();

		OBWriteDataInternationalConsentResponse1 data1 = new OBWriteDataInternationalConsentResponse1();
		response1.setData(data1);
		data1.setConsentId("12345");
		data1.setCreationDateTime(PispUtilities.getCurrentDateInISOFormat());
		data1.setStatus(OBExternalConsentStatus1Code.AWAITINGAUTHORISATION);

		OBRisk1 risk1 = new OBRisk1();
		response1.setRisk(risk1);

		OBInternational1 initiation1 = new OBInternational1();
		data1.setInitiation(initiation1);

		initiation1.setInstructionIdentification("ABDDC");
		initiation1.setEndToEndIdentification("FRESCO.21302.GFX.28");

		OBDomestic1InstructedAmount instructedAmount1 = new OBDomestic1InstructedAmount();
		instructedAmount1.setAmount("0");
		instructedAmount1.setCurrency("EUR");
		initiation1.setInstructedAmount(instructedAmount1);

		OBBranchAndFinancialInstitutionIdentification3 creditorAgent1 = new OBBranchAndFinancialInstitutionIdentification3();
		creditorAgent1.setSchemeName("creditorScheme");
		creditorAgent1.setIdentification("SC080801");
		initiation1.setCreditorAgent(creditorAgent1);
		OBCashAccountCreditor2 creditorAccount1 = new OBCashAccountCreditor2();
		creditorAccount1.setSchemeName("");
		creditorAccount1.setIdentification("NWBK60161331926819");
		creditorAccount1.setName("Test user");
		creditorAccount1.setIdentification("0002");
		initiation1.setCreditorAccount(creditorAccount1);

		OBBranchAndFinancialInstitutionIdentification3 debtorAgent1 = new OBBranchAndFinancialInstitutionIdentification3();
		debtorAgent1.setSchemeName("debtorAgent2BOI");
		debtorAgent1.setIdentification("SC112800");

		OBCashAccountDebtor3 debtorAccount1 = new OBCashAccountDebtor3();
		debtorAccount1.setSchemeName("Scheme1");
		debtorAccount1.setIdentification("GB29NWBK60161331926819");
		debtorAccount1.setName("Andrea Smith");
		debtorAccount1.setSecondaryIdentification("0002");
		initiation1.setDebtorAccount(debtorAccount1);

		OBRemittanceInformation1 remittanceInformation1 = new OBRemittanceInformation1();
		remittanceInformation1.setReference("FRESCO-101");
		remittanceInformation1.setUnstructured("Internal ops code 5120101");
		initiation1.setRemittanceInformation(remittanceInformation1);

		risk1.setMerchantCategoryCode("5967");
		risk1.setMerchantCustomerIdentification("053598653254");
		risk1.setPaymentContextCode(OBExternalPaymentContext1Code.BILLPAYMENT);

		OBRisk1DeliveryAddress deliveryAddress1 = new OBRisk1DeliveryAddress();
		risk1.setDeliveryAddress(deliveryAddress1);

		List<String> addressLine1 = new ArrayList<>();
		addressLine1.add("Flat 7");
		addressLine1.add("Acacia Lodge");
		deliveryAddress1.setAddressLine(addressLine1);

		deliveryAddress1.setBuildingNumber("27");
		deliveryAddress1.setCountry("UK");

		deliveryAddress1.setCountrySubDivision("DEFG");

		deliveryAddress1.setPostCode("GU31 2ZZ");
		deliveryAddress1.setStreetName("AcaciaAvenue");
		deliveryAddress1.setTownName("Sparsholt");

		response1.setLinks(new PaymentSetupPOSTResponseLinks());
		response1.setMeta(new PaymentSetupPOSTResponseMeta());

		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();
		resource.setTppDebtorDetails("true");
		resource.setTppDebtorNameDetails("true");
		int result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);
		resource.setTppDebtorDetails("false");
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);

		// DebtorAccount null
		initiation.setCreditorAgent(null);
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);

		// DebtorAccount null
		initiation.setCreditorAgent(null);
		initiation1.setDebtorAccount(null);
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);

		// TppDebtorNameDetails is false
		resource.setTppDebtorDetails("true");
		resource.setTppDebtorNameDetails("false");
		response1.getData().getInitiation().setDebtorAccount(debtorAccount1);
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);

		// request debtorAccount is null
		response1.getData().getInitiation().setDebtorAccount(debtorAccount1);
		resource.setTppDebtorDetails("false");
		request.getData().getInitiation().setDebtorAccount(null);
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);

		resource.setTppDebtorDetails("false");
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);
	}

	@Test
	public void compareTest_Auth() {

		// For Equality

		CustomIPaymentConsentsPOSTResponse response = new CustomIPaymentConsentsPOSTResponse();
		OBWriteDataInternationalConsentResponse1 data = new OBWriteDataInternationalConsentResponse1();
		response.setData(data);
		data.setConsentId("12345");
		data.setCreationDateTime(PispUtilities.getCurrentDateInISOFormat());
		data.setStatus(OBExternalConsentStatus1Code.AWAITINGAUTHORISATION);

		OBRisk1 risk = new OBRisk1();
		response.setRisk(risk);

		OBInternational1 initiation = new OBInternational1();
		data.setInitiation(initiation);
		initiation.setInstructionIdentification("ABDDC");
		initiation.setEndToEndIdentification("FRESCO.21302.GFX.28");

		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("777777777.00");
		instructedAmount.setCurrency("EUR");
		initiation.setInstructedAmount(instructedAmount);

		OBBranchAndFinancialInstitutionIdentification3 creditorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		creditorAgent.setSchemeName("BOI");
		creditorAgent.setIdentification("SC080801");
		initiation.setCreditorAgent(creditorAgent);

		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSchemeName("testBOI");
		creditorAccount.setIdentification("NWBK60161331926819");
		creditorAccount.setName("Test user");
		creditorAccount.setSecondaryIdentification("0002");
		initiation.setCreditorAccount(creditorAccount);

		OBBranchAndFinancialInstitutionIdentification3 debtorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		debtorAgent.setSchemeName("debtorAgentBOI");
		debtorAgent.setIdentification("SC112800");

		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setSchemeName("Scheme");
		debtorAccount.setIdentification("GB29NWBK60161331926819");
		debtorAccount.setName("Andrea Smith");
		debtorAccount.setIdentification("0002");
		initiation.setDebtorAccount(debtorAccount);

		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		remittanceInformation.setReference("FRESCO-101");
		remittanceInformation.setUnstructured("Internal ops code 5120101");
		initiation.setRemittanceInformation(remittanceInformation);

		risk.setMerchantCategoryCode("5967");
		risk.setMerchantCustomerIdentification("053598653254");
		risk.paymentContextCode(OBExternalPaymentContext1Code.ECOMMERCESERVICES);

		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);

		List<String> addressLine = new ArrayList<>();
		addressLine.add("Flat 7");
		addressLine.add("Acacia Lodge");
		deliveryAddress.setAddressLine(addressLine);

		deliveryAddress.setBuildingNumber("27");
		deliveryAddress.setCountry("UK");

		deliveryAddress.setCountrySubDivision("DEFG");

		deliveryAddress.setPostCode("GU31 2ZZ");
		deliveryAddress.setStreetName("AcaciaAvenue");
		deliveryAddress.setTownName("Sparsholt");

		response.setLinks(new PaymentSetupPOSTResponseLinks());
		response.setMeta(new PaymentSetupPOSTResponseMeta());

		CustomIPaymentConsentsPOSTResponse response1 = new CustomIPaymentConsentsPOSTResponse();
		response1.setData(data);
		response1.setRisk(risk);

		response1.setLinks(new PaymentSetupPOSTResponseLinks());
		response1.setMeta(new PaymentSetupPOSTResponseMeta());

		response1.setLinks(new PaymentSetupPOSTResponseLinks());
		response1.setMeta(new PaymentSetupPOSTResponseMeta());
		
		OBAuthorisation1 authorisation = new OBAuthorisation1();
		authorisation.setAuthorisationType(OBExternalAuthorisation1Code.ANY);
		response1.getData().setAuthorisation(authorisation );
		
		authorisation.setAuthorisationType(OBExternalAuthorisation1Code.ANY);
		response.getData().setAuthorisation(authorisation );
		
		int result = comparator.compare(response, response1);
		assertEquals(0, result);

		
	}
	
	@Test
	public void compareTest_AuthNotNull_NonEqual() {

		// For Non-Equality

		CustomIPaymentConsentsPOSTResponse response = new CustomIPaymentConsentsPOSTResponse();
		OBWriteDataInternationalConsentResponse1 data = new OBWriteDataInternationalConsentResponse1();
		response.setData(data);
		data.setConsentId("12345");
		data.setCreationDateTime(PispUtilities.getCurrentDateInISOFormat());
		data.setStatus(OBExternalConsentStatus1Code.AWAITINGAUTHORISATION);

		OBRisk1 risk = new OBRisk1();
		response.setRisk(risk);

		OBInternational1 initiation = new OBInternational1();
		data.setInitiation(initiation);
		initiation.setInstructionIdentification("ABDDC");
		initiation.setEndToEndIdentification("FRESCO.21302.GFX.28");

		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("777777777.00");
		instructedAmount.setCurrency("EUR");
		initiation.setInstructedAmount(instructedAmount);
		initiation.setCreditor(new OBPartyIdentification43());
		initiation.getCreditor().setPostalAddress(new OBPostalAddress6());
		initiation.getCreditor().getPostalAddress().setCountry("ABC");
		
		List<String> addressLine = new ArrayList<>();
		addressLine.add("Flat 7");
		addressLine.add("Acacia Lodge");
		
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);

		deliveryAddress.setAddressLine(addressLine);

		deliveryAddress.setBuildingNumber("27");
		deliveryAddress.setCountry("UK");

		deliveryAddress.setCountrySubDivision("DEFG");

		deliveryAddress.setPostCode("GU31 2ZZ");
		deliveryAddress.setStreetName("AcaciaAvenue");
		deliveryAddress.setTownName("Sparsholt");

		initiation.getCreditor().getPostalAddress().setAddressLine(addressLine);


		OBBranchAndFinancialInstitutionIdentification3 creditorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		creditorAgent.setSchemeName("BOI");
		creditorAgent.setIdentification("SC080801");
		initiation.setCreditorAgent(creditorAgent);

		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSchemeName("testBOI");
		creditorAccount.setIdentification("NWBK60161331926819");
		creditorAccount.setName("Test user");
		creditorAccount.setSecondaryIdentification("0002");
		initiation.setCreditorAccount(creditorAccount);

		OBBranchAndFinancialInstitutionIdentification3 debtorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		debtorAgent.setSchemeName("debtorAgentBOI");
		debtorAgent.setIdentification("SC112800");

		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setSchemeName("Scheme");
		debtorAccount.setIdentification("GB29NWBK60161331926819");
		debtorAccount.setName("Andrea Smith");
		debtorAccount.setIdentification("0002");
		initiation.setDebtorAccount(debtorAccount);

		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		remittanceInformation.setReference("FRESCO-101");
		remittanceInformation.setUnstructured("Internal ops code 5120101");
		initiation.setRemittanceInformation(remittanceInformation);

		risk.setMerchantCategoryCode("5967");
		risk.setMerchantCustomerIdentification("053598653254");
		risk.paymentContextCode(OBExternalPaymentContext1Code.ECOMMERCESERVICES);

		response.setLinks(new PaymentSetupPOSTResponseLinks());
		response.setMeta(new PaymentSetupPOSTResponseMeta());

		OBAuthorisation1 authorisation = new OBAuthorisation1();
		authorisation.setAuthorisationType(OBExternalAuthorisation1Code.ANY);
		response.getData().setAuthorisation(authorisation);
		
		
		CustomIPaymentConsentsPOSTResponse response1 = new CustomIPaymentConsentsPOSTResponse();
		OBWriteDataInternationalConsentResponse1 data1 = new OBWriteDataInternationalConsentResponse1();
		response1.setData(data1);
		response1.getData().setInitiation(new OBInternational1());
		response1.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		response1.getData().getInitiation().getInstructedAmount().setAmount("777777777.00");
		response1.setRisk(risk);

		response1.setLinks(new PaymentSetupPOSTResponseLinks());
		response1.setMeta(new PaymentSetupPOSTResponseMeta());

		OBAuthorisation1 authorisation1 = new OBAuthorisation1();
		authorisation1.setAuthorisationType(OBExternalAuthorisation1Code.SINGLE);
		response1.getData().setAuthorisation(authorisation1);
		
		int result = comparator.compare(response, response1);
		assertEquals(1, result);
	}
	
	@Test
	public void comparePaymentDetailsTest_Auth() {

		CustomIPaymentConsentsPOSTRequest request = new CustomIPaymentConsentsPOSTRequest();
		OBWriteDataInternationalConsent1 data = new OBWriteDataInternationalConsent1();

		request.setData(data);
		OBRisk1 risk = new OBRisk1();
		request.setRisk(risk);

		OBInternational1 initiation = new OBInternational1();
		data.setInitiation(initiation);

		initiation.setInstructionIdentification("ABDDC");
		initiation.setEndToEndIdentification("FRESCO.21302.GFX.28");
		
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("777777777.00");
		instructedAmount.setCurrency("EUR");
		initiation.setInstructedAmount(instructedAmount);

		OBBranchAndFinancialInstitutionIdentification3 creditorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		creditorAgent.setSchemeName("creditorScheme");
		creditorAgent.setIdentification("SC080801");
		initiation.setCreditorAgent(creditorAgent);

		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSchemeName("BOI");
		creditorAccount.setIdentification("NWBK60161331926819");
		creditorAccount.setName("Test user");
		creditorAccount.setSecondaryIdentification("0002");
		initiation.setCreditorAccount(creditorAccount);

		OBBranchAndFinancialInstitutionIdentification3 debtorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		debtorAgent.setSchemeName("debtorScheme");
		debtorAgent.setIdentification("SC112800");

		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setSchemeName("debtorAccountScheme");
		debtorAccount.setIdentification("GB29NWBK60161331926819");
		debtorAccount.setName("Andrea Smith");
		debtorAccount.setSecondaryIdentification("0002");
		initiation.setDebtorAccount(debtorAccount);

		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		remittanceInformation.setReference("FRESCO-101");
		remittanceInformation.setUnstructured("Internal ops code 5120101");
		initiation.setRemittanceInformation(remittanceInformation);

		risk.setMerchantCategoryCode("5967");
		risk.setMerchantCustomerIdentification("053598653254");
		risk.setPaymentContextCode(OBExternalPaymentContext1Code.BILLPAYMENT);

		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);

		List<String> addressLine = new ArrayList<>();
		addressLine.add("Flat 7");
		addressLine.add("Acacia Lodge");
		deliveryAddress.setAddressLine(addressLine);

		deliveryAddress.setBuildingNumber("27");
		deliveryAddress.setCountry("UK");

		deliveryAddress.setCountrySubDivision("DEFG");

		deliveryAddress.setPostCode("GU31 2ZZ");
		deliveryAddress.setStreetName("AcaciaAvenue");
		deliveryAddress.setTownName("Sparsholt");

		CustomIPaymentConsentsPOSTResponse response1 = new CustomIPaymentConsentsPOSTResponse();

		OBWriteDataInternationalConsentResponse1 data1 = new OBWriteDataInternationalConsentResponse1();
		response1.setData(data1);
		data1.setConsentId("12345");
		data1.setCreationDateTime(PispUtilities.getCurrentDateInISOFormat());
		data1.setStatus(OBExternalConsentStatus1Code.AWAITINGAUTHORISATION);

		OBRisk1 risk1 = new OBRisk1();
		response1.setRisk(risk1);

		OBInternational1 initiation1 = new OBInternational1();
		data1.setInitiation(initiation1);

		initiation1.setInstructionIdentification("ABDDC");
		initiation1.setEndToEndIdentification("FRESCO.21302.GFX.28");

		OBDomestic1InstructedAmount instructedAmount1 = new OBDomestic1InstructedAmount();
		instructedAmount1.setAmount("777777777.00");
		instructedAmount1.setCurrency("EUR");
		initiation1.setInstructedAmount(instructedAmount1);

		OBBranchAndFinancialInstitutionIdentification3 creditorAgent1 = new OBBranchAndFinancialInstitutionIdentification3();
		creditorAgent1.setSchemeName("creditorScheme");
		creditorAgent1.setIdentification("SC080801");
		initiation1.setCreditorAgent(creditorAgent1);
		OBCashAccountCreditor2 creditorAccount1 = new OBCashAccountCreditor2();
		creditorAccount1.setSchemeName("");
		creditorAccount1.setIdentification("NWBK60161331926819");
		creditorAccount1.setName("Test user");
		creditorAccount1.setIdentification("0002");
		initiation1.setCreditorAccount(creditorAccount1);

		OBBranchAndFinancialInstitutionIdentification3 debtorAgent1 = new OBBranchAndFinancialInstitutionIdentification3();
		debtorAgent1.setSchemeName("debtorAgent2BOI");
		debtorAgent1.setIdentification("SC112800");

		OBCashAccountDebtor3 debtorAccount1 = new OBCashAccountDebtor3();
		debtorAccount1.setSchemeName("Scheme1");
		debtorAccount1.setIdentification("GB29NWBK60161331926819");
		debtorAccount1.setName("Andrea Smith");
		debtorAccount1.setSecondaryIdentification("0002");
		initiation1.setDebtorAccount(debtorAccount1);

		OBRemittanceInformation1 remittanceInformation1 = new OBRemittanceInformation1();
		remittanceInformation1.setReference("FRESCO-101");
		remittanceInformation1.setUnstructured("Internal ops code 5120101");
		initiation1.setRemittanceInformation(remittanceInformation1);

		risk1.setMerchantCategoryCode("5967");
		risk1.setMerchantCustomerIdentification("053598653254");
		risk1.setPaymentContextCode(OBExternalPaymentContext1Code.BILLPAYMENT);

		OBRisk1DeliveryAddress deliveryAddress1 = new OBRisk1DeliveryAddress();
		risk1.setDeliveryAddress(deliveryAddress1);

		List<String> addressLine1 = new ArrayList<>();
		addressLine1.add("Flat 7");
		addressLine1.add("Acacia Lodge");
		deliveryAddress1.setAddressLine(addressLine1);

		deliveryAddress1.setBuildingNumber("27");
		deliveryAddress1.setCountry("UK");

		deliveryAddress1.setCountrySubDivision("DEFG");

		deliveryAddress1.setPostCode("GU31 2ZZ");
		deliveryAddress1.setStreetName("AcaciaAvenue");
		deliveryAddress1.setTownName("Sparsholt");

		response1.setLinks(new PaymentSetupPOSTResponseLinks());
		response1.setMeta(new PaymentSetupPOSTResponseMeta());

		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();
		resource.setTppDebtorDetails("true");
		resource.setTppDebtorNameDetails("true");
		int result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);
		resource.setTppDebtorDetails("false");
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);

		//response Auth Not Null and request not null
		OBAuthorisation1 reqAuth = new OBAuthorisation1();
		reqAuth.setAuthorisationType(OBExternalAuthorisation1Code.SINGLE);
		request.getData().setAuthorisation(reqAuth);
		
		OBAuthorisation1 authorisation1 = new OBAuthorisation1();
		authorisation1.setAuthorisationType(OBExternalAuthorisation1Code.SINGLE);
		response1.getData().setAuthorisation(authorisation1);
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);
		
		//response Auth Null
		response1.getData().setAuthorisation(null);
		request.getData().setAuthorisation(reqAuth);
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);
		
		//reqAuth Null
		response1.getData().setAuthorisation(authorisation1);
		request.getData().setAuthorisation(null);
		response1.getData().setAuthorisation(authorisation1);
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);
	}
	
	@Test
	public void comparePaymentDetailsTest_Remittance() {

		CustomIPaymentConsentsPOSTRequest request = new CustomIPaymentConsentsPOSTRequest();
		OBWriteDataInternationalConsent1 data = new OBWriteDataInternationalConsent1();

		request.setData(data);
		OBRisk1 risk = new OBRisk1();
		request.setRisk(risk);

		OBInternational1 initiation = new OBInternational1();
		data.setInitiation(initiation);

		initiation.setInstructionIdentification("ABDDC");
		initiation.setEndToEndIdentification("FRESCO.21302.GFX.28");
		
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("777777777.00");
		instructedAmount.setCurrency("EUR");
		initiation.setInstructedAmount(instructedAmount);
		initiation.setCreditor(new OBPartyIdentification43());
		initiation.getCreditor().setPostalAddress(new OBPostalAddress6());
		initiation.getCreditor().getPostalAddress().setCountry("ABC");
		initiation.getCreditor().getPostalAddress().setAddressLine(null);


		OBBranchAndFinancialInstitutionIdentification3 creditorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		creditorAgent.setSchemeName("creditorScheme");
		creditorAgent.setIdentification("SC080801");
		initiation.setCreditorAgent(creditorAgent);

		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSchemeName("BOI");
		creditorAccount.setIdentification("NWBK60161331926819");
		creditorAccount.setName("Test user");
		creditorAccount.setSecondaryIdentification("0002");
		initiation.setCreditorAccount(creditorAccount);

		OBBranchAndFinancialInstitutionIdentification3 debtorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		debtorAgent.setSchemeName("debtorScheme");
		debtorAgent.setIdentification("SC112800");

		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setSchemeName("debtorAccountScheme");
		debtorAccount.setIdentification("GB29NWBK60161331926819");
		debtorAccount.setName("Andrea Smith");
		debtorAccount.setSecondaryIdentification("0002");
		//initiation.setDebtorAccount(debtorAccount);

		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		//remittanceInformation.setReference("FRESCO-101");
		//remittanceInformation.setUnstructured("Internal ops code 5120101");
		initiation.setRemittanceInformation(remittanceInformation);

		risk.setMerchantCategoryCode("5967");
		risk.setMerchantCustomerIdentification("053598653254");
		risk.setPaymentContextCode(OBExternalPaymentContext1Code.BILLPAYMENT);

		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);

		List<String> addressLine = new ArrayList<>();
		addressLine.add("Flat 7");
		addressLine.add("Acacia Lodge");
		deliveryAddress.setAddressLine(addressLine);

		deliveryAddress.setBuildingNumber("27");
		deliveryAddress.setCountry("UK");

		deliveryAddress.setCountrySubDivision("DEFG");

		deliveryAddress.setPostCode("GU31 2ZZ");
		deliveryAddress.setStreetName("AcaciaAvenue");
		deliveryAddress.setTownName("Sparsholt");

		CustomIPaymentConsentsPOSTResponse response1 = new CustomIPaymentConsentsPOSTResponse();

		OBWriteDataInternationalConsentResponse1 data1 = new OBWriteDataInternationalConsentResponse1();
		response1.setData(data1);
		data1.setConsentId("12345");
		data1.setCreationDateTime(PispUtilities.getCurrentDateInISOFormat());
		data1.setStatus(OBExternalConsentStatus1Code.AWAITINGAUTHORISATION);

		OBRisk1 risk1 = new OBRisk1();
		response1.setRisk(risk1);

		OBInternational1 initiation1 = new OBInternational1();
		data1.setInitiation(initiation1);

		initiation1.setInstructionIdentification("ABDDC");
		initiation1.setEndToEndIdentification("FRESCO.21302.GFX.28");

		OBDomestic1InstructedAmount instructedAmount1 = new OBDomestic1InstructedAmount();
		instructedAmount1.setAmount("777777777.00");
		instructedAmount1.setCurrency("EUR");
		initiation1.setInstructedAmount(instructedAmount1);

		OBBranchAndFinancialInstitutionIdentification3 creditorAgent1 = new OBBranchAndFinancialInstitutionIdentification3();
		creditorAgent1.setSchemeName("creditorScheme");
		creditorAgent1.setIdentification("SC080801");
		initiation1.setCreditorAgent(creditorAgent1);
		OBCashAccountCreditor2 creditorAccount1 = new OBCashAccountCreditor2();
		creditorAccount1.setSchemeName("");
		creditorAccount1.setIdentification("NWBK60161331926819");
		creditorAccount1.setName("Test user");
		creditorAccount1.setIdentification("0002");
		initiation1.setCreditorAccount(creditorAccount1);

		OBBranchAndFinancialInstitutionIdentification3 debtorAgent1 = new OBBranchAndFinancialInstitutionIdentification3();
		debtorAgent1.setSchemeName("debtorAgent2BOI");
		debtorAgent1.setIdentification("SC112800");

		OBCashAccountDebtor3 debtorAccount1 = new OBCashAccountDebtor3();
		debtorAccount1.setSchemeName("Scheme1");
		debtorAccount1.setIdentification("GB29NWBK60161331926819");
		debtorAccount1.setName("Andrea Smith");
		debtorAccount1.setSecondaryIdentification("0002");
		//initiation1.setDebtorAccount(debtorAccount1);

		response1.getData().getInitiation().setDebtorAccount(debtorAccount1);
		
		OBRemittanceInformation1 remittanceInformation1 = new OBRemittanceInformation1();
		remittanceInformation1.setReference("FRESCO-101");
		remittanceInformation1.setUnstructured("Internal ops code 5120101");
		initiation1.setRemittanceInformation(remittanceInformation1);

		risk1.setMerchantCategoryCode("5967");
		risk1.setMerchantCustomerIdentification("053598653254");
		risk1.setPaymentContextCode(OBExternalPaymentContext1Code.BILLPAYMENT);

		OBRisk1DeliveryAddress deliveryAddress1 = new OBRisk1DeliveryAddress();
		risk1.setDeliveryAddress(deliveryAddress1);

		List<String> addressLine1 = new ArrayList<>();
		addressLine1.add("Flat 7");
		addressLine1.add("Acacia Lodge");
		deliveryAddress1.setAddressLine(addressLine1);

		deliveryAddress1.setBuildingNumber("27");
		deliveryAddress1.setCountry("UK");

		deliveryAddress1.setCountrySubDivision("DEFG");

		deliveryAddress1.setPostCode("GU31 2ZZ");
		deliveryAddress1.setStreetName("AcaciaAvenue");
		deliveryAddress1.setTownName("Sparsholt");

		response1.setLinks(new PaymentSetupPOSTResponseLinks());
		response1.setMeta(new PaymentSetupPOSTResponseMeta());

		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();
		resource.setTppDebtorDetails("false");
		resource.setTppDebtorNameDetails("false");
		int result = comparator.comparePaymentDetails(response1, request, resource);

		assertEquals(1, result);
		resource.setTppDebtorDetails("false");
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);

		
		//response Auth Not Null and request not null
		OBAuthorisation1 reqAuth = new OBAuthorisation1();
		reqAuth.setAuthorisationType(OBExternalAuthorisation1Code.SINGLE);
		request.getData().setAuthorisation(reqAuth);
			
		OBAuthorisation1 authorisation1 = new OBAuthorisation1();
		authorisation1.setAuthorisationType(OBExternalAuthorisation1Code.SINGLE);
		response1.getData().setAuthorisation(authorisation1);
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);
				
		//response Auth Null
		response1.getData().setAuthorisation(null);
		request.getData().setAuthorisation(reqAuth);
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);
				
		//reqAuth Null
		response1.getData().setAuthorisation(authorisation1);
		request.getData().setAuthorisation(null);
		response1.getData().setAuthorisation(authorisation1);
		result = comparator.comparePaymentDetails(response1, request, resource);
		assertEquals(1, result);
	}
}