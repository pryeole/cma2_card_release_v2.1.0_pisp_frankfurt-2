package com.capgemini.psd2.scaconsenthelper.constants;

public interface PFConstants {

	public final static String RESUME_PATH	=	"resumePath";
	public final static String PING_INSTANCE_ID = "ping.instanceId";
	public final static String REF = "REF";
	public final static String DATE_FORMAT ="yyyy-MM-dd HH:mm:ssZ";
}
