package com.capgemini.psd2.scaconsenthelper.config.helpers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.capgemini.psd2.aisp.domain.OBAccount2;
import com.capgemini.psd2.aisp.domain.OBReadAccount2;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.utilities.NullCheckUtils;

public abstract class ConsentAuthorizationHelper {

	public List<OBAccount2> matchAccountFromConsentAccount(OBReadAccount2 accountGETResponse,
			AispConsent aispConsent) {
		List<OBAccount2> consentAccounts = null;
		Map<String, OBAccount2> mapOfAccounts = null;
		OBAccount2 account = null;
		if (accountGETResponse == null || accountGETResponse.getData().getAccount().isEmpty()) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.SELECTED_ACCOUNTS_NOT_OWNED_BY_CUSTOMER);
		}
		if (aispConsent.getAccountDetails() == null || aispConsent.getAccountDetails().isEmpty()) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_ACCOUNTS_SELECTED_BY_CUSTOMER);
		}
		mapOfAccounts = new HashMap<String, OBAccount2>();
		consentAccounts = new ArrayList<OBAccount2>();
		for (OBAccount2 currAccount : accountGETResponse.getData().getAccount()) {
			mapOfAccounts.put(((PSD2Account) currAccount).getHashedValue(), currAccount);
		}
		for (AccountDetails accountDetail : aispConsent.getAccountDetails()) {
			account = mapOfAccounts.get(accountDetail.getHashValue());
			consentAccounts.add(account);
		}
		return consentAccounts;
	}

	public OBAccount2 matchAccountFromList(OBReadAccount2 accountGETResponse, PSD2Account selectedAccount) {
		PSD2Account customerAccount = null;
		if (accountGETResponse == null || accountGETResponse.getData().getAccount().isEmpty()) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.SELECTED_ACCOUNTS_NOT_OWNED_BY_CUSTOMER);
		}
		if (selectedAccount != null) {
			for (OBAccount2 account : accountGETResponse.getData().getAccount()) {
				customerAccount = matchHash(selectedAccount, (PSD2Account) account);
				if (customerAccount != null) {
					break;
				}
			}
			if (customerAccount == null) {
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.SELECTED_ACCOUNTS_NOT_OWNED_BY_CUSTOMER);
			}
		} else {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_ACCOUNTS_SELECTED_BY_CUSTOMER);
		}
		customerAccount = populateAccountwithUnmaskedValues(selectedAccount, (PSD2Account) customerAccount);
		return customerAccount;
	}

	private PSD2Account matchHash(PSD2Account submittedAccount, PSD2Account customerAccount) {

		String acctHashedValue = submittedAccount.getHashedValue();
		if (NullCheckUtils.isNullOrEmpty(acctHashedValue)) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.SELECTED_ACCOUNTS_NOT_OWNED_BY_CUSTOMER);
		}

		boolean accountMatch = acctHashedValue.equals(customerAccount.getHashedValue());
		if (accountMatch) {
			return customerAccount;
		}
		return null;
	}

	public abstract PSD2Account populateAccountwithUnmaskedValues(PSD2Account accountwithMask,
			PSD2Account accountwithoutMask);
	
	public abstract <T> List<OBAccount2> populateAccountListFromAccountDetails(T input);

}
