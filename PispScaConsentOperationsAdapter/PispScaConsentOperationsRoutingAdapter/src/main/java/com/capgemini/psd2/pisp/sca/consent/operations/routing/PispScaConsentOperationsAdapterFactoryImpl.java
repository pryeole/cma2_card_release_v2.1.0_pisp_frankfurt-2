package com.capgemini.psd2.pisp.sca.consent.operations.routing;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.pisp.sca.consent.operations.adapter.PispScaConsentOperationsAdapter;

@Component
public class PispScaConsentOperationsAdapterFactoryImpl implements ApplicationContextAware, PispScaConsentOperationsAdapterFactory {

	private ApplicationContext applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}

	@Override
	public PispScaConsentOperationsAdapter getAdapterInstance(String coreSystemName) {
		return (PispScaConsentOperationsAdapter) applicationContext.getBean(coreSystemName);
	}
}