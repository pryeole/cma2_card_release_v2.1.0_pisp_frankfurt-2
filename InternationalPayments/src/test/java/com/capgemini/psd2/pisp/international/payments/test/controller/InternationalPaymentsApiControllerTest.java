package com.capgemini.psd2.pisp.international.payments.test.controller;

import static org.mockito.Matchers.anyString;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.CustomIPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentInternationalSubmitPOST201Response;
import com.capgemini.psd2.pisp.international.payments.controller.InternationalPaymentsApiController;
import com.capgemini.psd2.pisp.international.payments.service.InternationalPaymentsService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.ser.DefaultSerializerProvider;
import com.fasterxml.jackson.databind.ser.SerializerFactory;

@RunWith(SpringJUnit4ClassRunner.class)
public class InternationalPaymentsApiControllerTest {

	@Mock
	private InternationalPaymentsService service;

	@Mock
	private OBPSD2ExceptionUtility util;

	@Mock
	ObjectMapper objectMapper;

	@InjectMocks
	private InternationalPaymentsApiController controller;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		Map<String, String> map = new HashMap<>();

		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");

		Map<String, String> specificErrorMessageMap = new HashMap<>();

		specificErrorMessageMap.put("signature_invalid_content",
				"Invalid content prsent in joseheader of x-jws-signature header");

		specificErrorMessageMap.put("signature_missing", "signature header missing in request");

		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);

		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);

	}

	@Test
	public void contextLoads() {

	}

	@Test
	public void testObjectMapper() {
		controller.getObjectMapper();
	}

	@Test
	public void testRequest() {
		controller.getRequest();
	}

	@Test
	public void testCreateInternationalPayments() {
		CustomIPaymentsPOSTRequest request = new CustomIPaymentsPOSTRequest();

		DefaultSerializerProvider p = new DefaultSerializerProvider() {

			@Override
			public DefaultSerializerProvider createInstance(SerializationConfig config, SerializerFactory jsf) {
				// TODO Auto-generated method stub
				return null;
			}
		};
		objectMapper.setSerializerProvider(p);
		/*
		 * request.setCreatedOn("11.302"); Optional<ObjectMapper> objectMapper =
		 * null; objectMapper.equals(request);
		 * controller.getObjectMapper().isPresent();
		 */
		// controller.getRequest().isPresent();

		String xFapiFinancialId = "1";
		String authorization = "2";
		String xIdempotencyKey = "3";
		String xJwsSignature = "4";

		PaymentInternationalSubmitPOST201Response response = new PaymentInternationalSubmitPOST201Response();
		Mockito.when(service.createInternationalPaymentsResource(request)).thenReturn(response);
		controller.createInternationalPayments(request, xFapiFinancialId, authorization, xIdempotencyKey, xJwsSignature,
				null, null, null, null);

	}

	@Test(expected = PSD2Exception.class)
	public void testCreateInternationalPaymentsException() {

		CustomIPaymentsPOSTRequest request = new CustomIPaymentsPOSTRequest();
		request.setCreatedOn("11/30");
		PaymentInternationalSubmitPOST201Response response = new PaymentInternationalSubmitPOST201Response();

		String xFapiFinancialId = "1";
		String authorization = "2";
		String xIdempotencyKey = "3";
		String xJwsSignature = "4";

		Mockito.when(service.createInternationalPaymentsResource(request)).thenThrow(PSD2Exception.class);
		CustomIPaymentsPOSTRequest postRequest = new CustomIPaymentsPOSTRequest();
		controller.createInternationalPayments(postRequest, xFapiFinancialId, authorization, xIdempotencyKey,
				xJwsSignature, null, null, null, null);

	}

	@Test
	public void testGetInternationalPaymentsInternationalPaymentId() {
		PaymentInternationalSubmitPOST201Response response = new PaymentInternationalSubmitPOST201Response();
		DefaultSerializerProvider p = new DefaultSerializerProvider() {

			@Override
			public DefaultSerializerProvider createInstance(SerializationConfig config, SerializerFactory jsf) {
				// TODO Auto-generated method stub
				return null;
			}
		};
		objectMapper.setSerializerProvider(p);

		String xFapiFinancialId = "1";
		String authorization = "2";
		String internationalPaymentId = "123456";

		Mockito.when(service.retrieveInternationalPaymentsResource(anyString())).thenReturn(response);
		controller.getInternationalPaymentsInternationalPaymentId(internationalPaymentId, xFapiFinancialId,
				authorization, null, null, null, null);
	}

	@Test(expected = PSD2Exception.class)
	public void testGetInternationalPaymentsInternationalPaymentIdException() {
		String xFapiFinancialId = "1";
		String authorization = "2";
		String internationalPaymentId = "123456";
		Mockito.when(service.retrieveInternationalPaymentsResource(anyString())).thenThrow(PSD2Exception.class);
		controller.getInternationalPaymentsInternationalPaymentId(internationalPaymentId, xFapiFinancialId,
				authorization, null, null, null, null);
	}

	@After
	public void tearDown() throws Exception {
		controller = null;
		service = null;
	}
}
