package com.capgemini.psd2.response.test.validator;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import com.capgemini.psd2.response.validator.ResponseValidator;
import com.capgemini.psd2.validator.PSD2Validator;

public class ResponseValidatorTest {

	@Mock
	private PSD2Validator cmaValidator;
	
	@InjectMocks
	private ResponseValidator responseValidator = new ResponseValidator();

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testValidateResponse() {
		doNothing().when(cmaValidator).validateWithError(anyObject(), anyString());
		responseValidator.validateResponse(new Object());
		
	}
}
