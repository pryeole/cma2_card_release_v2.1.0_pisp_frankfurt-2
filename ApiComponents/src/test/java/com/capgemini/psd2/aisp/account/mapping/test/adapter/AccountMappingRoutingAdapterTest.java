package com.capgemini.psd2.aisp.account.mapping.test.adapter;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.aisp.account.identfication.mapping.adapter.domain.AccountIdentificationMapping;
import com.capgemini.psd2.aisp.account.mapping.adapter.AccountMappingRoutingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountIdentificationMappingAdapter;

public class AccountMappingRoutingAdapterTest {
	
	@Mock
	private ApplicationContext context;
	
	@InjectMocks
	AccountMappingRoutingAdapter obj=new AccountMappingRoutingAdapter();

	@Before
	public void setUp() {
	   MockitoAnnotations.initMocks(this);	
	}
	
	@Test
	public void retrieveAccountIdentificationMappingTest() {
		ReflectionTestUtils.setField(obj, "adapterName", "abcd");
		List<String> identifierList=new ArrayList<>();
		when(context.getBean(anyString())).thenReturn(new AccountIdentificationMappingAdapter() {

			@Override
			public AccountIdentificationMapping retrieveAccountIdentificationMapping(
					List<String> mappingIdentifierList) {
				// TODO Auto-generated method stub
				return null;
			}
			
		});
		obj.retrieveAccountIdentificationMapping(identifierList);
	}
}
