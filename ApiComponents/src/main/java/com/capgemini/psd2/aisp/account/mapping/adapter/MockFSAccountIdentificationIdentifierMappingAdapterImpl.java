/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.aisp.account.mapping.adapter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.account.identfication.mapping.adapter.domain.AccountIdentificationMapping;
import com.capgemini.psd2.aisp.account.identfication.mapping.adapter.domain.IdntificationMapping;
import com.capgemini.psd2.aisp.adapter.AccountIdentificationMappingAdapter;

/**
 * The Class MockFSAccountIdentificationIdentifierMappingAdapterImpl.
 */
@Component
public class MockFSAccountIdentificationIdentifierMappingAdapterImpl implements AccountIdentificationMappingAdapter {

	@Autowired
	private MockFSAccountMappingRepository mockFSAccountMappingRepository;

	@Override
	public AccountIdentificationMapping retrieveAccountIdentificationMapping(List<String> mappingIdentifierList) {
		String identifier = mappingIdentifierList.get(0);
		IdntificationMapping idntificationMapping = mockFSAccountMappingRepository.findByIdentifier(identifier);
		AccountIdentificationMapping accountIdentificationMapping = new AccountIdentificationMapping();
		List<IdntificationMapping> identificationMappingList = new ArrayList<>();
		identificationMappingList.add(idntificationMapping);
		accountIdentificationMapping.setIdentificationMappingList(identificationMappingList);
		return accountIdentificationMapping;
	}
}
