package com.capgemini.psd2.payment.presubmissionvalidation.mock.foundationservice.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.foundationservice.validator.SuccesCodeEnum;
import com.capgemini.psd2.foundationservice.validator.ValidationPassed;
import com.capgemini.psd2.foundationservice.validator.ValidationUtility;
import com.capgemini.psd2.payment.presubmissionvalidation.mock.foundationservice.domain.PaymentInstruction;
import com.capgemini.psd2.payment.presubmissionvalidation.mock.foundationservice.service.PaymentPreSubmissionValidationService;

@Service
public class PaymentPreSubmissionValidationServiceImpl implements PaymentPreSubmissionValidationService{

	@Autowired
	private ValidationUtility validationUtility;
	
	@Override
	public ValidationPassed validatePaymentInstruction(PaymentInstruction paymentInstruction) {
	
		String endToEndIdentification = paymentInstruction.getEndToEndIdentification();
		
		return validationUtility.validateMockBusinessValidations(endToEndIdentification, SuccesCodeEnum.SUCCESS_VALIDATE_PAYMENT.getSuccessCode(), SuccesCodeEnum.SUCCESS_PRESTAGE_VALIDATE_PAYMENT.getSuccessMessage());
	}

}
