package com.capgemini.psd2.payment.presubmissionvalidation.mock.foundationservice.domain;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlAdapter;


/**
 * Adapter to marshal and unmarshall data entered in xs:date field, as defined by XSD, in to java.util.Date and vice versa.
 *
 * @author A970560
 *
 */
public class XMLFormatDateTimeAdapter extends XmlAdapter<String, Date>{

	private static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

	//@Loggable(LogLevel.INFO)
	public Date parseDate(String s) throws ParseException {
		Date date = new SimpleDateFormat(DATE_TIME_FORMAT).parse(s);
		return  date;
	}

	//@Loggable(LogLevel.INFO)
	public String printDate(Date dt) {
		SimpleDateFormat format = new SimpleDateFormat(DATE_TIME_FORMAT);
		return format.format(dt);
	}


	@Override
	public String marshal(Date v) throws Exception {
		return printDate(v);
	}

	@Override
	public Date unmarshal(String v) throws Exception {
		return parseDate(v);
	}
}
