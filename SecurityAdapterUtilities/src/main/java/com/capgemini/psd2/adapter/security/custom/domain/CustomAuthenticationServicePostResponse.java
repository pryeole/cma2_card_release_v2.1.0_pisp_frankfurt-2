package com.capgemini.psd2.adapter.security.custom.domain;

import com.capgemini.psd2.adapter.security.domain.LoginResponse;

public class CustomAuthenticationServicePostResponse {
	
	private String dropOffRef;
	
	public String getDropOffRef() {
		return dropOffRef;
	}

	public void setDropOffRef(String dropOffRef) {
		this.dropOffRef = dropOffRef;
	}

	private LoginResponse loginResponse;

	public LoginResponse getLoginResponse() {
		return loginResponse;
	}

	public void setLoginResponse(LoginResponse loginResponse) {
		this.loginResponse = loginResponse;
	}
}
