/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.adapter.security.test;

import static org.junit.Assert.assertEquals;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;

import com.capgemini.psd2.adapter.exceptions.security.SecurityAdapterException;
import com.capgemini.psd2.rest.client.exception.handler.SecurityExceptionHandlerImpl;

/**
 * The Class AdapterExceptionHandlerImplTest.
 */
public class SecurityExceptionHandlerImplTest {
    
    /** The adapter exception handler impl. */
    @InjectMocks
	private SecurityExceptionHandlerImpl securityExceptionHandlerImpl;
	
	/** The http server error exception. */
	@Mock
	private HttpServerErrorException httpServerErrorException;
	
	/** The http client error exception. */
	@Mock 
	private HttpClientErrorException httpClientErrorException;
	
	/** The resource access exception. */
	@Mock
	private ResourceAccessException resourceAccessException;
	
	
	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		securityExceptionHandlerImpl = new SecurityExceptionHandlerImpl();
	}
	
	/**
	 * Test handle exception with server error.
	 */
	@Test(expected=SecurityAdapterException.class)
	public void testHandleExceptionWithServerError(){
		httpServerErrorException = new HttpServerErrorException(HttpStatus.BAD_REQUEST);
		securityExceptionHandlerImpl.handleException(httpServerErrorException);
	}
	
	/**
	 * Test handle exception with clent error.
	 */
	@Test(expected=SecurityAdapterException.class)
	public void testHandleExceptionWithClentError(){
		httpClientErrorException=new HttpClientErrorException(HttpStatus.NOT_FOUND);
		securityExceptionHandlerImpl.handleException(httpClientErrorException);
	}
	
	/*
	 * Test error map.
	 */
	@Test
	public void testErrorMap(){
		Map<String, String> errormap = new HashMap<>();
		errormap.put("errorCode", "400");
		securityExceptionHandlerImpl.setErrormap(errormap);
		assertEquals("400", securityExceptionHandlerImpl.getErrormap().get("errorCode"));
	}
	
	/*
	 * Test resource access exception.
	 */
	@Test(expected=SecurityAdapterException.class)
	public void testResourceAccessException(){
		resourceAccessException=new ResourceAccessException("Connetion Time Out");
		securityExceptionHandlerImpl.handleException(resourceAccessException);
	}
	
	/**
	 * Test handle exception with server exception.
	 */
	@Test(expected=SecurityAdapterException.class)
	public void testHandleExceptionWithServerException1(){
		String errorStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><validationViolations xmlns=\"http://bankofireland.com/channels/errors/\"><validationViolation><errorCode>FS_ABTB_004</errorCode><errorText>Technical Error. Please try again later</errorText><errorField>Technical Error. Please try again later</errorField><errorValue>Server Error</errorValue></validationViolation></validationViolations>";
		byte[] responseBody =errorStr.getBytes();
		Map<String, String> errormap = new HashMap<>();
		errormap.put("FS_ABTB_004", "BAD_REQUEST");
		securityExceptionHandlerImpl.setErrormap(errormap);
		httpServerErrorException = new HttpServerErrorException(HttpStatus.BAD_REQUEST,"Bad Request",responseBody,Charset.forName("UTF-8"));
		securityExceptionHandlerImpl.handleException(httpServerErrorException);
	}
	
	@Test(expected=SecurityAdapterException.class)
	public void testHandleExceptionWithServerException2(){
		String errorStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><validationViolations xmlns=\"http://bankofireland.com/channels/erro/\"><validationViolation><errorCode>FS_ABTB_004</errorCode><errorText>Technical Error. Please try again later</errorText><errorField>Technical Error. Please try again later</errorField><errorValue>Server Error</errorValue></validationViolation></validationViolations>";
		byte[] responseBody =errorStr.getBytes();
		Map<String, String> errormap = new HashMap<>();
		errormap.put("FS_ABTB_004", "BAD_REQUEST");
		securityExceptionHandlerImpl.setErrormap(errormap);
		httpServerErrorException = new HttpServerErrorException(HttpStatus.BAD_REQUEST,"Bad Request",responseBody,Charset.forName("UTF-8"));
		securityExceptionHandlerImpl.handleException(httpServerErrorException);
	}
	
	@Test(expected=SecurityAdapterException.class)
	public void testHandleExceptionWithServerExceptiontrycatch1(){
		String errorStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><authenticationResponse xmlns=\"http://bankofireland.com/channels/auth/\"><responseCode>FS_AUTH_001</responseCode><responseMessage>Authentication failed.</responseMessage><remainingAttempts>1</remainingAttempts></authenticationResponse>";
		
		byte[] responseBody =errorStr.getBytes();
		Map<String, String> errormap = new HashMap<>();
		errormap.put("200", "Success Response");
		errormap.put("FS_AUTH_001", "BAD_REQUEST");
		securityExceptionHandlerImpl.setErrormap(errormap);
		httpServerErrorException = new HttpServerErrorException(HttpStatus.OK,"Success Response",responseBody,Charset.forName("UTF-8"));
		securityExceptionHandlerImpl.handleException(httpServerErrorException);

	}
	
	@Test(expected=SecurityAdapterException.class)
	public void testHandleExceptionWithServerExceptiontrycatch2(){
		String errorStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><authenticationResponse xmlns=\"http://bankofireland.com/channels/auth/\"><responseCode>FS_AUTH_001</responseCode><responseMessage>Authentication failed.</responseMessage><remainingAttempts>0</remainingAttempts></authenticationResponse>";
		
		byte[] responseBody =errorStr.getBytes();
		Map<String, String> errormap = new HashMap<>();
		errormap.put("200", "Success Response");
		errormap.put("FS_AUTH_001", "BAD_REQUEST");
		securityExceptionHandlerImpl.setErrormap(errormap);
		httpServerErrorException = new HttpServerErrorException(HttpStatus.OK,"Success Response",responseBody,Charset.forName("UTF-8"));
		securityExceptionHandlerImpl.handleException(httpServerErrorException);

	}
	
	
	@Test(expected=SecurityAdapterException.class)
	public void testHandleExceptionWithclientExceptiontrycatch1(){
		String errorStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><validationViolations xmlns=\"http://bankofireland.com/channels/errors/\"><validationViolation><responseCode>200</responseCode><responseMessage>Success Response</responseMessage><errorField>Technical Error. Please try again later</errorField><errorValue>Server Error</errorValue></validationViolation></validationViolations>";
		
		byte[] responseBody = errorStr.getBytes();
		Map<String, String> errormap = new HashMap<>();
		errormap.put("200", "Success Response");
		securityExceptionHandlerImpl.setErrormap(errormap);
		httpClientErrorException = new HttpClientErrorException(HttpStatus.OK,"Success Response",responseBody,Charset.forName("UTF-8"));
		securityExceptionHandlerImpl.handleException(httpClientErrorException);

	}
	
	@Test(expected=SecurityAdapterException.class)
	public void testHandleExceptionWithclientExceptiontrycatch2(){
		String errorStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><authenticationResponse xmlns=\"http://bankofireland.com/channels/auth/\"><responseCode>FS_AUTH_001</responseCode><responseMessage>Authentication failed.</responseMessage><remainingAttempts>0</remainingAttempts></authenticationResponse>";
		
		byte[] responseBody =errorStr.getBytes();
		Map<String, String> errormap = new HashMap<>();
		errormap.put("200", "Success Response");
		securityExceptionHandlerImpl.setErrormap(errormap);
		httpClientErrorException = new HttpClientErrorException(HttpStatus.OK,"Success Response",responseBody,Charset.forName("UTF-8"));
		securityExceptionHandlerImpl.handleException(httpClientErrorException);

	}
	
	@Test(expected=SecurityAdapterException.class)
	public void testHandleExceptionWithclientExceptiontrycatch3(){
		String errorStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><authenticationResponse xmlns=\"http://bankofireland.com/channels/auth/\"><responseCode>FS_AUTH_001</responseCode><responseMessage>Authentication failed.</responseMessage><remainingAttempts>1</remainingAttempts></authenticationResponse>";
		
		byte[] responseBody =errorStr.getBytes();
		Map<String, String> errormap = new HashMap<>();
		errormap.put("200", "Success Response");
		errormap.put("FS_AUTH_001", "TECHNICAL_ERROR");
		securityExceptionHandlerImpl.setErrormap(errormap);
		httpClientErrorException = new HttpClientErrorException(HttpStatus.OK,"Success Response",responseBody,Charset.forName("UTF-8"));
		securityExceptionHandlerImpl.handleException(httpClientErrorException);

	}
	
	@Test(expected=SecurityAdapterException.class)
	public void testHandleExceptionWithclientExceptiontrycatch4(){
		String errorStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><authenticationResponse xmlns=\"http://bankofireland.com/channels/auth/\"><responseCod>FS_AUTH_001</responseCod><responseMessage>Authentication failed.</responseMessage><remainingAttempts>0</remainingAttempts></authenticationResponse>";
		
		byte[] responseBody =errorStr.getBytes();
		Map<String, String> errormap = new HashMap<>();
		errormap.put("200", "Success Response");
		securityExceptionHandlerImpl.setErrormap(errormap);
		httpClientErrorException = new HttpClientErrorException(HttpStatus.OK,"Success Response",responseBody,Charset.forName("UTF-8"));
		securityExceptionHandlerImpl.handleException(httpClientErrorException);

	}
	
	@Test(expected=SecurityAdapterException.class)
	public void testHandleExceptionWithclientExceptiontrycatch5(){
		String errorStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><authenticationResponse xmlns=\"http://bankofireland.com/channels/auth/\"><responseCodeFS_AUTH_001</responseCode><responseMessage>Authentication failed.</responseMessage><remainingAttempts>0</remainingAttempts></authenticationResponse>";
		
		byte[] responseBody =errorStr.getBytes();
		Map<String, String> errormap = new HashMap<>();
		errormap.put("200", "Success Response");
		securityExceptionHandlerImpl.setErrormap(errormap);
		httpServerErrorException = new HttpServerErrorException(HttpStatus.OK,"Success Response",responseBody,Charset.forName("UTF-8"));
		securityExceptionHandlerImpl.handleException(httpServerErrorException);

	}
	
	/**
	 * Test handle exception with clent exception.
	 */
	@Test(expected=SecurityAdapterException.class)
	public void testHandleExceptionWithClentException1(){
		String errorStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><validationViolations xmlns=\"http://bankofireland.com/channels/errors/\"><validationViolation><errorCode>FS_ABTB_004</errorCode><errorText>Technical Error. Please try again later</errorText><errorField>Technical Error. Please try again later</errorField><errorValue>Server Error</errorValue></validationViolation></validationViolations>";
		byte[] responseBody =errorStr.getBytes();
		Map<String, String> errormap = new HashMap<>();
		errormap.put("FS_ABTB_004", "BAD_REQUEST");
		securityExceptionHandlerImpl.setErrormap(errormap);
		httpClientErrorException = new HttpClientErrorException(HttpStatus.BAD_REQUEST,"Bad Request",responseBody,Charset.forName("UTF-8"));
		securityExceptionHandlerImpl.handleException(httpClientErrorException);
	}
	
	@Test(expected=SecurityAdapterException.class)
	public void testHandleExceptionWithClentException2(){
		String errorStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><validationViolations xmlns=\"http://bankofireland.com/channels/erro/\"><validationViolation><errorCode>FS_ABTB_004</errorCode><errorText>Technical Error. Please try again later</errorText><errorField>Technical Error. Please try again later</errorField><errorValue>Server Error</errorValue></validationViolation></validationViolations>";
		byte[] responseBody =errorStr.getBytes();
		Map<String, String> errormap = new HashMap<>();
		errormap.put("FS_ABTB_004", "BAD_REQUEST");
		securityExceptionHandlerImpl.setErrormap(errormap);
		httpClientErrorException = new HttpClientErrorException(HttpStatus.BAD_REQUEST,"Bad Request",responseBody,Charset.forName("UTF-8"));
		securityExceptionHandlerImpl.handleException(httpClientErrorException);
	}
	
}
