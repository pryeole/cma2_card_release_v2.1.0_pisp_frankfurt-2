package com.capgemini.psd2.pisp.payment.submission.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.capgemini.pd2.pisp.payment.submission.mock.data.PaymentSubmissionITMockData;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.PaymentSubmissionPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentSubmitPOST201Response;
import com.capgemini.psd2.pisp.payment.submission.config.PaymentSubmissionITConfig;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = PaymentSubmissionITConfig.class)
public class PaymentSubmissionIT {

	@Autowired
	protected RestClientSyncImpl restClient;

	@Autowired
	protected RestTemplate restTemplate;

	@Value("${app.edgeserverURL}")
	private String edgeserverURL;
	@Value("${app.paymentSubmissionURL}")
	private String paymentSubmissionURL;

	@Value("${app.refToken}")
	private String refToken;
	@Value("${app.expiredToken}")
	private String expiredToken;
	@Value("${app.invalidToken}")
	private String invalidToken;
	@Value("${app.refTokenForSuccessFlow}")
	private String refTokenForSuccessFlow;
	@Value("${app.refTokenForConsentStatusCheck}")
	private String refTokenForConsentStatusCheck;

	// success case for payment Submission API
	@Test
	public void successFlowForPaymentSubmissionTest() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refTokenForSuccessFlow);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);

		PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
				.getPaymentSubmissionPOSTRequestMockData();
		paymentSubmissionPOSTRequestMockData.getData().setPaymentId("e5bcd9f127144ca99e07bbf0335b920b");
		PaymentSubmitPOST201Response paymentSubmitPOST201Response = restClient.callForPost(requestInfo,
				paymentSubmissionPOSTRequestMockData, PaymentSubmitPOST201Response.class, headers);

		assertNotNull(paymentSubmitPOST201Response.getData().getPaymentSubmissionId());
		assertTrue(paymentSubmitPOST201Response.getData().getPaymentSubmissionId().length()<=40);
		assertNotNull(paymentSubmitPOST201Response.getData().getPaymentId());
		assertNotNull(paymentSubmitPOST201Response.getData().getCreationDateTime());
		assertNotNull(paymentSubmitPOST201Response.getLinks().getSelf());
		
	}
/*
	@Test
	public void paymentStatusPendingInvalidTest() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization",refTokenForConsentStatusCheck);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			HttpEntity<?> requestEntity = new HttpEntity<>(headers);
			PaymentSubmissionPOSTRequest paymentSubmission = PaymentSubmissionITMockData.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmission.getData().setPaymentId("4bdcbb46b41446aca6738fb97f3b5416");
			restClient.callForPost(requestInfo,paymentSubmission,
					PaymentSubmitPOST201Response.class, headers);
		} catch (HttpClientErrorException e) {
			assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
			throw e;
		}
	}*/

	/** Authorization header is null or blank and sent in the request */
	@Test(expected = HttpClientErrorException.class)
	public void noAuthTokenGivenTest() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			HttpEntity<?> requestEntity = new HttpEntity<>(headers);
			restTemplate.exchange(url, HttpMethod.GET, requestEntity, Object.class);
		} catch (HttpClientErrorException e) {
			assertEquals(HttpStatus.UNAUTHORIZED, e.getStatusCode());
			throw e;
		}
	}

	/** Expired Authorization code is sent in the request */
	@Test(expected = PSD2Exception.class)
	public void expiredAuthorizationTokenTest() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", expiredToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESsssCOsa.2s1302d.GFX.513");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			restClient.callForPost(requestInfo, PaymentSubmissionITMockData.getPaymentSubmissionPOSTRequestMockData(),
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(HttpStatus.UNAUTHORIZED.toString(), e.getErrorInfo().getStatusCode());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void testInvalidToken() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", invalidToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			restClient.callForPost(requestInfo, PaymentSubmissionITMockData.getPaymentSubmissionPOSTRequestMockData(),
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertNotNull(e.getErrorInfo().getErrorMessage());
			assertEquals(HttpStatus.UNAUTHORIZED.toString(), e.getErrorInfo().getStatusCode());
			throw e;
		}
	}

	/** Value of x-fapifinancial-id is missing in the request header. */
	@Test(expected = PSD2Exception.class)
	public void fapiMandatoryHeaderMissingTest() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			restClient.callForPost(requestInfo, PaymentSubmissionITMockData.getPaymentSubmissionPOSTRequestMockData(),
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(HttpStatus.BAD_REQUEST.value(), Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("x-fapi-financial-id is missing in headers.", e.getErrorInfo().getDetailErrorMessage());
			throw e;
		}
	}

	/** mandatory header x-idempotency-key header length > 40 */
	@Test(expected = PSD2Exception.class)
	public void invalidIdempotencyKeyTest() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCOABCDEFGH.213021234.GFXLOBIJ.5712345");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			restClient.callForPost(requestInfo, PaymentSubmissionITMockData.getPaymentSubmissionPOSTRequestMockData(),
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(HttpStatus.BAD_REQUEST.value(), Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Length of header 'x-idempotency-key' can not be more than defined size.",
					e.getErrorInfo().getErrorMessage());
			throw e;
		}
	
	}

	@Test(expected = RestClientException.class)
	public void contentTypeInvalidTest() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		headers.add("Content-Type","application/pdf");
		PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
				.getPaymentSubmissionPOSTRequestMockData();
		HttpEntity<PaymentSubmissionPOSTRequest> requestEntity = new HttpEntity<PaymentSubmissionPOSTRequest>(
				paymentSubmissionPOSTRequestMockData, headers);
		try {
			restTemplate.exchange(url, HttpMethod.POST, requestEntity, PaymentSubmitPOST201Response.class);
		} catch (RestClientException e) {
			assertNotNull(e.getLocalizedMessage());
			throw e;
		}
	}
	
	@Test(expected = HttpClientErrorException.class)
	public void acceptTypeInvalidTest() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		headers.add("Accept","application/pdf");
		
		PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
				.getPaymentSubmissionPOSTRequestMockData();
		HttpEntity<PaymentSubmissionPOSTRequest> requestEntity = new HttpEntity<PaymentSubmissionPOSTRequest>(
				paymentSubmissionPOSTRequestMockData, headers);
		try {
			
			restTemplate.exchange(url, HttpMethod.POST, requestEntity, PaymentSubmitPOST201Response.class);
		} catch (HttpClientErrorException e) {
			assertEquals(HttpStatus.NOT_ACCEPTABLE, e.getStatusCode());
			throw e;
		}
	}
	
	@Test(expected = HttpClientErrorException.class)
	public void otherThanPOSTRequestTest() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.78");
		PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
				.getPaymentSubmissionPOSTRequestMockData();
		HttpEntity<PaymentSubmissionPOSTRequest> requestEntity = new HttpEntity<PaymentSubmissionPOSTRequest>(
				paymentSubmissionPOSTRequestMockData, headers);
		try {
			restTemplate.exchange(url, HttpMethod.PUT, requestEntity, PaymentSubmitPOST201Response.class);
		} catch (HttpClientErrorException e) {
			assertEquals(HttpStatus.METHOD_NOT_ALLOWED, e.getStatusCode());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void nullRiskInPaymentSubmissionPOSTRequestTest() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
					.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmissionPOSTRequestMockData.setRisk(null);
			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Field 'Risk' is missing from request payload", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void moreThan16CharacterForPostCodeInPaymentSubmissionPOSTRequestTest() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
					.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmissionPOSTRequestMockData.getRisk().getDeliveryAddress().setPostCode("7412589630123654789");
			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void blankValueForPostCodeInPaymentSubmissionPOSTRequestTest() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
					.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmissionPOSTRequestMockData.getRisk().getDeliveryAddress().setPostCode("");
			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void nullCountryNameInPaymentSubmissionPOSTRequestTest() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
					.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmissionPOSTRequestMockData.getRisk().getDeliveryAddress().setCountry(null);
			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void moreThanTwoCharactersForCountryNameInPaymentSubmissionPOSTRequestTest() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
					.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmissionPOSTRequestMockData.getRisk().getDeliveryAddress().setCountry("INDI");
			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void lessThanTwoCharactersForCountryNameInPaymentSubmissionPOSTRequestTest() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
					.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmissionPOSTRequestMockData.getRisk().getDeliveryAddress().setCountry("I");
			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void countryNameNotAsPerIsoInPaymentSubmissionPOSTRequestTest() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
					.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmissionPOSTRequestMockData.getRisk().getDeliveryAddress().setCountry("INDIA");
			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void setMoreThan35CharEndToEndIdentificationInPaymentSetupInPaymentSetUpPayload() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
					.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmissionPOSTRequestMockData.getData().getInitiation()
					.setEndToEndIdentification("FS_PMPSV_002YDSHKZONRDRDZJABNJL:AYYCSLSLA");

			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void blankEndToEndIdentificationInPaymentSetupInPaymentSetUpPayload() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
					.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmissionPOSTRequestMockData.getData().getInitiation().setEndToEndIdentification("");
			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void nullCurrencyInInstructedAmountInPaymentSetupInPaymentSetUpITMockData() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
					.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmissionPOSTRequestMockData.getData().getInitiation().getInstructedAmount().setCurrency(null);

			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("currency : may not be null;", e.getErrorInfo().getDetailErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void blankAmountInInstructedAmountInPaymentSetupInPaymentSetUpITPayload() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
					.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmissionPOSTRequestMockData.getData().getInitiation().getInstructedAmount().setAmount("");
			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void setNegativeAmountInInstructedAmountInPaymentSetupInPaymentSetUpITPayload() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
					.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmissionPOSTRequestMockData.getData().getInitiation().getInstructedAmount().setAmount("-7777");

			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void setFiveDigitAmountInInstructedAmountInPaymentSetupInPaymentSetUpITMockData() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
					.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmissionPOSTRequestMockData.getData().getInitiation().getInstructedAmount().setAmount(".888881");

			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void setThirteenDigitAmountInInstructedAmountInPaymentSetupInPaymentSetUpPayload() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
					.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmissionPOSTRequestMockData.getData().getInitiation().getInstructedAmount()
					.setAmount("8888876548976");

			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void setThirteenDigitPrecisionMoreThanFiveAmountInInstructedAmountInPaymentSetupPayload() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
					.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmissionPOSTRequestMockData.getData().getInitiation().getInstructedAmount()
					.setAmount("8888876548976.2345567");

			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void setCurrencyValueMoreThanThreeInInstructedAmountInPaymentSetupInPaymentSetUpITMockData() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
					.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmissionPOSTRequestMockData.getData().getInitiation().getInstructedAmount().setCurrency("GBPR");

			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void setCurrencyValueLessThanThreeInInstructedAmountInPaymentSetupInPaymentSetUpITMockData() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
					.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmissionPOSTRequestMockData.getData().getInitiation().getInstructedAmount().setCurrency("12");

			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void BlankInstructionIdentificationInPaymentSetupInPaymentSetUpITMockData() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
					.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmissionPOSTRequestMockData.getData().getInitiation().setInstructionIdentification("");
			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void setReferenceMoreThanThirtyFiveCharInRemittanceInformationInPaymentSetupInPaymentSetUpITMockData() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
					.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmissionPOSTRequestMockData.getData().getInitiation().getRemittanceInformation()
					.setReference("AJWHDVCSBNSKDLDUOSDJ.LSMDKSDGYUAGDUYKWND");
			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void blankReferenceInRemittanceInformationInPaymentSetupInPaymentSetUpITMockData() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
					.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmissionPOSTRequestMockData.getData().getInitiation().getRemittanceInformation().setReference("");

			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void setReferenceMoreThanOneFortyCharInRemittanceInformationInPaymentSetupInPaymentSetUpITMockData() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
					.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmissionPOSTRequestMockData.getData().getInitiation().getRemittanceInformation().setReference(
					"java.lang.AssertionError:Expectedexceptioncom.capgemini.psd2.exceptions.PSD2Exceptionatorg.junit.internal.runners.statements.ExpectExcept");

			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void blankUnstructuredInRemittanceInformationInPaymentSetupInPaymentSetUpITMockData() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
					.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmissionPOSTRequestMockData.getData().getInitiation().getRemittanceInformation()
					.setUnstructured("");

			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	// remove initiation
	@Test(expected = PSD2Exception.class)
	public void removeInitiationWithNullPaymentSubmissionPOSTRequestTest() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
					.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmissionPOSTRequestMockData.getData().setInitiation(null);
			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Field 'Initiation' is missing from submission request payload.",
					e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	// ---------------- Creditor Account Validations test -------------

	// remove creditor account
	@Test(expected = PSD2Exception.class)
	public void removeCreditorAccountWithNullInPaymentSubmissionPOSTRequestTest() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
					.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmissionPOSTRequestMockData.getData().getInitiation().setCreditorAccount(null);
			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Field 'CreditorAccount' is missing from request payload", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	// remove Identification in CreditorAccount
	@Test(expected = PSD2Exception.class)
	public void removeIdentificationinCreditorAccountWithNullInPaymentSubmissionPOSTRequestTest() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
					.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmissionPOSTRequestMockData.getData().getInitiation().getCreditorAccount().setIdentification(null);
			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	// set Identification in CreditorAccount with more than 34 characters
	@Test(expected = PSD2Exception.class)
	public void setIdentificationinCreditorAccountWithMoreThan34CharactersInPaymentSubmissionPOSTRequestTest() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
					.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmissionPOSTRequestMockData.getData().getInitiation().getCreditorAccount()
					.setIdentification("RRTSPXYZABCILOPGEMIBDVHUBHREYHIKPWZRS");
			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	// blank value for Identification in CreditorAccount
	@Test(expected = PSD2Exception.class)
	public void setIdentificationinCreditorAccounttWithBlankInPaymentSubmissionPOSTRequestTest() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
					.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmissionPOSTRequestMockData.getData().getInitiation().getCreditorAccount().setIdentification("");
			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	// more than 70 characters name in CreditorAccount
	@Test(expected = PSD2Exception.class)
	public void setNameinCreditorAccountWithMoreThan70CharactersInPaymentSubmissionPOSTRequestTest() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
					.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmissionPOSTRequestMockData.getData().getInitiation().getCreditorAccount()
					.setName("RRTSPXYZABCILOPGEMIBDVHUBHREYHIYURHDGJGDGGDGGEDGGDJGDGHGDGGGHBVHDOIOPLOJBDVYFYF");
			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}

	}

	// blank name in CreditorAccount
	@Test(expected = PSD2Exception.class)
	public void setNameinCreditorAccountWithBlankInPaymentSubmissionPOSTRequestTest() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
					.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmissionPOSTRequestMockData.getData().getInitiation().getCreditorAccount().setName("");
			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}

	}

	// set SecondaryIdentification in CreditorAccount With >34 Characters
	@Test(expected = PSD2Exception.class)
	public void setSecondaryIdentificationinCreditorAccountsWithMoreThan34CharactersInPaymentSubmissionPOSTRequestTest() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
					.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmissionPOSTRequestMockData.getData().getInitiation().getCreditorAccount()
					.setSecondaryIdentification("ABCDEFGHIJKLMNOPQRSTUVWXYZvmsbvjkhdsvh");
			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}

	}

	// set Secondary Identification in CreditorAccount With Blank
	@Test(expected = PSD2Exception.class)
	public void setSecondaryIdentificationinCreditorAccountWithBlankInPaymentSubmissionPOSTRequestTest() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
					.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmissionPOSTRequestMockData.getData().getInitiation().getCreditorAccount()
					.setSecondaryIdentification("");
			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}

	}

	// not Enter Some Mandatory Fields Of Creditor Agent
	@Test(expected = PSD2Exception.class)
	public void notEnterSomeMandatoryFieldsOfCreditorAgentInPaymentSubmissionPOSTRequestTest() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
					.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmissionPOSTRequestMockData.getData().getInitiation().getCreditorAgent().setIdentification(null);
			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}

	}

	// set Identification of CreditorAgent With More Than 35 Characters
	@Test(expected = PSD2Exception.class)
	public void setIdentificationOfCreditorAgentWithMoreThan35CharactersInPaymentSubmissionPOSTRequestTest() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
					.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmissionPOSTRequestMockData.getData().getInitiation().getCreditorAgent()
					.setIdentification("ABCDEFGHIJKLMNOPQRSTUVWXYZABHC123456");
			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	// set Identification of CreditorAgent With Blank
	@Test(expected = PSD2Exception.class)
	public void setIdentificationOfCreditorAgentWithNullInPaymentSubmissionPOSTRequestTest() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
					.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmissionPOSTRequestMockData.getData().getInitiation().getCreditorAgent().setIdentification("");
			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}

	}

	// ------------------Creditor Agent Validations end ------------

	@Test(expected = PSD2Exception.class)
	public void setTownNameMoreThan35CharInPaymentSubmissionPOSTRequestPayloadTest() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
					.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmissionPOSTRequestMockData.getRisk().getDeliveryAddress()
					.setTownName("AJWHDVCSBNSKDLDUOSDJLSMDKSDGYUAGDdskfhj");
			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void setTownNameBlankInPaymentSubmissionPOSTRequestPayloadTest() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
					.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmissionPOSTRequestMockData.getRisk().getDeliveryAddress().setTownName("");
			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void setStreetNameMoreThan70CharInPaymentSubmissionPOSTRequestPayloadTest() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
					.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmissionPOSTRequestMockData.getRisk().getDeliveryAddress()
					.setStreetName("AJWHDVCSBNSKDLDUOSDJLSMDKSDGYUAGDdskfhjjvbjhdiknskcbiwyehbidsnchDEVCINCBydgec");
			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void setStreetNameBlankInPaymentSubmissionPOSTRequestPayloadTest() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
					.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmissionPOSTRequestMockData.getRisk().getDeliveryAddress().setStreetName("");
			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void setMerchantCategoryCodeLessThan3CharInPaymentSubmissionPOSTRequestPayloadTest() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
					.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmissionPOSTRequestMockData.getRisk().setMerchantCategoryCode("CA");
			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("merchantCategoryCode : size must be between 3 and 4;",
					e.getErrorInfo().getDetailErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void setMerchantCategoryCodeMoreThan4CharInPaymentSubmissionPOSTRequestPayloadTest() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
					.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmissionPOSTRequestMockData.getRisk().setMerchantCategoryCode("CATEG");
			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("merchantCategoryCode : size must be between 3 and 4;",
					e.getErrorInfo().getDetailErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void setMerchantCustomeridentificationMoreThan70CharInPaymentSubmissionPOSTRequestPayloadTest() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.57");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
					.getPaymentSubmissionPOSTRequestMockData();
			paymentSubmissionPOSTRequestMockData.getRisk().setMerchantCustomerIdentification(
					"AJWHDVCSBNSKDLDUOSDJLSMDKSDGYUAGDdskfhjjvbjhdiknskcbiwyehbidsnchDEVCINCBydgec");
			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void setMerchantCustomeridentificationBlankInPaymentSubmissionPOSTRequestPayloadTest() {
		String url = edgeserverURL  +paymentSubmissionURL  + "/payment-submissions";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", refToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("x-idempotency-key", "FRESCO.21302.GFX.83");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		PaymentSubmissionPOSTRequest paymentSubmissionPOSTRequestMockData = PaymentSubmissionITMockData
				.getPaymentSubmissionPOSTRequestMockData();
		paymentSubmissionPOSTRequestMockData.getRisk().setMerchantCustomerIdentification("");
		try {
			restClient.callForPost(requestInfo, paymentSubmissionPOSTRequestMockData,
					PaymentSubmitPOST201Response.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(400, Integer.parseInt(e.getErrorInfo().getStatusCode()));
			assertEquals("Validation error found with the provided input", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}
}
