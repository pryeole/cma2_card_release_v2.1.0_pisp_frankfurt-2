package com.capgemini.psd2.account.product.routing.adapter.routing;

import com.capgemini.psd2.aisp.adapter.AccountProductsAdapter;

@FunctionalInterface
public interface AccountProductAdapterFactory {
	/**
	 * Gets the adapter instance.
	 *
	 * @param coreSystemName
	 *            the core system name
	 * @return the adapter instance
	 */
	public AccountProductsAdapter getAdapterInstance(String coreSystemName);
}
