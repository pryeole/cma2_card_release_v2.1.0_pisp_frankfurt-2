package com.capgemini.psd2.account.product.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.account.product.service.AccountProductService;
import com.capgemini.psd2.aisp.domain.OBReadProduct2;


@RestController
public class AccountProductController 
{
	/** The service. */
	@Autowired
	private AccountProductService service;
 
	/**
	 * Retrieve account product.
	 *
	 * @param accountId the account id
	 * @return the product GET response
	 */
	@RequestMapping(value ="/accounts/{accountId}/product", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public OBReadProduct2 retrieveAccountProduct(@PathVariable("accountId") String accountId) {

		return service.retrieveAccountProduct(accountId);
	}
}
