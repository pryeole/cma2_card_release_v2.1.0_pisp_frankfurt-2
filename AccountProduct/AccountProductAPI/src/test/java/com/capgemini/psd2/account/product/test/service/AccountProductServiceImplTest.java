package com.capgemini.psd2.account.product.test.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.account.product.service.impl.AccountProductServiceImpl;
import com.capgemini.psd2.account.product.test.mock.data.AccountProductMockData;
import com.capgemini.psd2.aisp.account.mapping.adapter.AccountMappingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountProductsAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.OBReadProduct2;
import com.capgemini.psd2.aisp.validation.adapter.impl.AccountProductsValidatorImpl;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.response.validator.AccountProductResponseValidator;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountProductServiceImplTest {
	/** The adapter. */
	@Mock
	private AccountProductsAdapter adapter;

	/** The mapping adapter */
	@Mock
	private AccountMappingAdapter accountMappingAdapter;

	/** The consent mapping. */
	@Mock
	private AispConsentAdapter aispConsentAdapter;

	/** The header atttributes. */
	@Mock
	private RequestHeaderAttributes headerAtttributes;

	@Mock
	private LoggerUtils loggerUtils;

	@Mock
	private AccountProductsValidatorImpl accountsCustomValidator;

	/** The service. */
	@InjectMocks
	private AccountProductServiceImpl service = new AccountProductServiceImpl();

	/**
	 * Sets the up.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(headerAtttributes.getToken()).thenReturn(AccountProductMockData.getToken());
		when(aispConsentAdapter.retrieveAccountMapping(anyString()))
				.thenReturn(AccountProductMockData.getMockAccountMapping());
		Map<String, String> map=new HashMap<>();
		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");
		Map<String, String> specificErrorMessageMap=new HashMap<>();
		specificErrorMessageMap.put("signature_invalid_content", "Invalid content prsent in joseheader of x-jws-signature header");
		specificErrorMessageMap.put("signature_missing", "signature header missing in request");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);

	}

	/**
	 * Retrieve success response
	 */

	@Test
	public void retrieveAccountProductSuccessWithLinksMetaTest() {
		
		accountsCustomValidator.validateUniqueId(anyObject());
		
		Mockito.when(aispConsentAdapter.validateAndRetrieveConsentByAccountId(anyString(), anyString()))
				.thenReturn(AccountProductMockData.getMockAispConsent());

		Mockito.when(accountMappingAdapter.retrieveAccountMappingDetails(anyString(), anyObject()))
				.thenReturn(AccountProductMockData.getMockAccountMapping());

		Mockito.when(adapter.retrieveAccountProducts(anyObject(), anyObject()))
				.thenReturn(AccountProductMockData.getMockPlatformResponseWithLinksMeta());

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountProductMockData.getMockLoggerData());

		accountsCustomValidator.validateResponseParams(anyObject());
		//verify(responseValidator).validateResponse(anyObject());

		OBReadProduct2 actualResponse = service.retrieveAccountProduct("f4483fda-81be-4873-b4a6-20375b7f55c1");

		assertEquals(AccountProductMockData.getMockExpectedResponseWithLinksMeta(), actualResponse);

	}

	@Test
	public void retrieveAccountProductSuccessWithoutLinkMetaTest() {
		Mockito.when(aispConsentAdapter.validateAndRetrieveConsentByAccountId(anyString(), anyString()))
				.thenReturn(AccountProductMockData.getMockAispConsent());

		Mockito.when(accountMappingAdapter.retrieveAccountMappingDetails(anyString(), anyObject()))
				.thenReturn(AccountProductMockData.getMockAccountMapping());

		Mockito.when(adapter.retrieveAccountProducts(anyObject(), anyObject()))
				.thenReturn(AccountProductMockData.getMockPlatformResponseWithoutLinksMeta());

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountProductMockData.getMockLoggerData());

		accountsCustomValidator.validateResponseParams(anyObject());
		//verify(responseValidator).validateResponse(AccountProductMockData.getMockExpectedResponseWithoutLinkMeta());

		OBReadProduct2 actualResponse = service.retrieveAccountProduct("123");

		assertEquals(AccountProductMockData.getMockExpectedResponseWithoutLinkMeta(), actualResponse);

	}

	/**
	 * Tear down.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@After
	public void tearDown() throws Exception {
		service = null;
	}

}
