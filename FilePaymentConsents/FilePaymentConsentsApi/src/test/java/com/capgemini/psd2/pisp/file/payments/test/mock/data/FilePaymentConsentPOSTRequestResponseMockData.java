package com.capgemini.psd2.pisp.file.payments.test.mock.data;

import com.capgemini.psd2.pisp.domain.CustomFilePaymentSetupPOSTRequest;
import com.fasterxml.jackson.databind.ObjectMapper;

public class FilePaymentConsentPOSTRequestResponseMockData {

	public static CustomFilePaymentSetupPOSTRequest getPaymentSubmissionPOSTRequest() {
		return new CustomFilePaymentSetupPOSTRequest();
		
	}
	
	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
