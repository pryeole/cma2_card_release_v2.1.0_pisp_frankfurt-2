package com.capgemini.psd2.pisp.file.payment.setup.service;

import org.springframework.web.multipart.MultipartFile;

import com.capgemini.psd2.pisp.domain.CustomFilePaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPlatformDetails;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.domain.PlatformFilePaymentsFileResponse;

public interface FilePaymentsConsentsService {

	public CustomFilePaymentConsentsPOSTResponse createFilePaymentConsentsResource(
			CustomFilePaymentSetupPOSTRequest paymentRequest, boolean isPaymentValidated);

	public CustomPaymentSetupPlatformDetails populateFilePlatformDetails(
			CustomFilePaymentSetupPOSTRequest paymentConsentsRequest);

	public CustomFilePaymentConsentsPOSTResponse retrieveFilePaymentConsentsResource(
			PaymentRetrieveGetRequest paymentRetrieveRequest);

	public void uploadPaymentConsentFile(MultipartFile file, String consentId);

	public PlatformFilePaymentsFileResponse downloadTransactionFileByConsentId(String consentId);

}
