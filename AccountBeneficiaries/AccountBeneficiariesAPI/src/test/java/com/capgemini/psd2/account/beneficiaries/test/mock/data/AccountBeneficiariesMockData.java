package com.capgemini.psd2.account.beneficiaries.test.mock.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBBeneficiary2;
import com.capgemini.psd2.aisp.domain.OBReadBeneficiary2;
import com.capgemini.psd2.aisp.domain.OBReadBeneficiary2Data;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountBeneficiariesResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.logger.LoggerAttribute;
import com.capgemini.psd2.token.ConsentTokenData;
import com.capgemini.psd2.token.Token;

public class AccountBeneficiariesMockData {
	/** The mock token. */
	public static Token mockToken;

	/**
	 * Gets the token.
	 *
	 * @return the token
	 */
	public static Token getToken() {
		mockToken = new Token();
		ConsentTokenData consentTokenData = new ConsentTokenData();
		consentTokenData.setConsentExpiry("1509348259877L");
		consentTokenData.setConsentId("12345");
		mockToken.setConsentTokenData(consentTokenData);
		Map<String,String> map=new HashMap<>();
		mockToken.setSeviceParams(map);
		return mockToken;
	}

	/**
	 * Gets the mock account mapping.
	 *
	 * @return the mock account mapping
	 */
	public static AccountMapping getMockAccountMapping() {
		AccountMapping mapping = new AccountMapping();
		mapping.setTppCID("tpp123");
		mapping.setPsuId("user123");
		List<AccountDetails> selectedAccounts = new ArrayList<>();
		AccountDetails accountRequest = new AccountDetails();
		accountRequest.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		accountRequest.setAccountNSC("SC802001");
		accountRequest.setAccountNumber("10203345");
		selectedAccounts.add(accountRequest);
		mapping.setAccountDetails(selectedAccounts);
		return mapping;
	}

	public static AispConsent getMockAispConsent() {
		AispConsent aispConsent = new AispConsent();
		aispConsent.setTppCId("6443e15975554bce8099e35b88b40465");
		aispConsent.setPsuId("88888888");
		aispConsent.setConsentId("39032481");
		aispConsent.setAccountRequestId("ae8c4441-f783-4e80-8810-254241bed98c");
		aispConsent.setChannelId("B365");
		aispConsent.setStartDate("2017-08-09T06:44:31.250Z");
		aispConsent.setEndDate("2018-09-02T00:00:00.875");
		aispConsent.setTransactionFromDateTime("2015-05-03T00:00:00.800");
		aispConsent.setTransactionToDateTime("2018-12-03T00:00:00.345");
		aispConsent.setStatus(ConsentStatusEnum.AUTHORISED);

		List<AccountDetails> selectedAispConsentAccountDetails = new ArrayList<>();
		AccountDetails accountRequest = new AccountDetails();

		accountRequest.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accountRequest.setAccountNSC("903779");
		accountRequest.setAccountNumber("76528776");

		selectedAispConsentAccountDetails.add(accountRequest);
		aispConsent.setAccountDetails(selectedAispConsentAccountDetails);

		return aispConsent;
	}

	public static PlatformAccountBeneficiariesResponse getMockPlatformReponseWithLinksMeta() {
		PlatformAccountBeneficiariesResponse platformAccountBeneficiariesResponse = new PlatformAccountBeneficiariesResponse();
		OBReadBeneficiary2Data obReadBeneficiary2Data = new OBReadBeneficiary2Data();
		List<OBBeneficiary2> obBeneficiaryList = new ArrayList<>();
		OBBeneficiary2 obBeneficiary2 = new OBBeneficiary2();
		obBeneficiary2.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		obBeneficiary2.setBeneficiaryId("TestBeneficiaryId");
		obBeneficiaryList.add(obBeneficiary2);
		obReadBeneficiary2Data.setBeneficiary(obBeneficiaryList);

		OBReadBeneficiary2 obReadBeneficiary2 = new OBReadBeneficiary2();
		obReadBeneficiary2.setData(obReadBeneficiary2Data);

		Links links = new Links();
		obReadBeneficiary2.setLinks(links);
		Meta meta = new Meta();
		obReadBeneficiary2.setMeta(meta);
		platformAccountBeneficiariesResponse.setoBReadBeneficiary2(obReadBeneficiary2);
		return platformAccountBeneficiariesResponse;

	}

	public static Object getMockExpectedAccountBeneficiaryResponseWithLinksMeta() {
		OBReadBeneficiary2 obReadBeneficiary2 = new OBReadBeneficiary2();
		OBReadBeneficiary2Data obReadBeneficiary2Data = new OBReadBeneficiary2Data();
		List<OBBeneficiary2> obBeneficiaryList = new ArrayList<>();
		OBBeneficiary2 obBeneficiary2 = new OBBeneficiary2();
		obBeneficiary2.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		obBeneficiary2.setBeneficiaryId("TestBeneficiaryId");
		obBeneficiaryList.add(obBeneficiary2);
		obReadBeneficiary2Data.setBeneficiary(obBeneficiaryList);

		obReadBeneficiary2.setData(obReadBeneficiary2Data);

		Links links = new Links();
		obReadBeneficiary2.setLinks(links);
		Meta meta = new Meta();
		meta.setTotalPages(1);
		obReadBeneficiary2.setMeta(meta);

		return obReadBeneficiary2;

	}
	
	
	public static LoggerAttribute getMockLoggerData() {
		LoggerAttribute x = new LoggerAttribute();
		x.setApiId("testApiID");
		return x;
	}
	
	
	public static PlatformAccountBeneficiariesResponse getMockPlatformReponseWithoutLinksMeta() {
		PlatformAccountBeneficiariesResponse platformAccountBeneficiariesResponse = new PlatformAccountBeneficiariesResponse();
		OBReadBeneficiary2Data obReadBeneficiary2Data = new OBReadBeneficiary2Data();
		List<OBBeneficiary2> obBeneficiaryList = new ArrayList<>();
		OBBeneficiary2 obBeneficiary2 = new OBBeneficiary2();
		obBeneficiary2.setAccountId("123");
		obBeneficiary2.setBeneficiaryId("TestBeneficiaryId");
		obBeneficiaryList.add(obBeneficiary2);
		obReadBeneficiary2Data.setBeneficiary(obBeneficiaryList);

		OBReadBeneficiary2 obReadBeneficiary2 = new OBReadBeneficiary2();
		obReadBeneficiary2.setData(obReadBeneficiary2Data);
		
		platformAccountBeneficiariesResponse.setoBReadBeneficiary2(obReadBeneficiary2);
		return platformAccountBeneficiariesResponse;

	}
	
	public static Object getMockExpectedAccountBeneficiaryResponseWithoutLinksMeta() {
		OBReadBeneficiary2 obReadBeneficiary2 = new OBReadBeneficiary2();
		OBReadBeneficiary2Data obReadBeneficiary2Data = new OBReadBeneficiary2Data();
		List<OBBeneficiary2> obBeneficiaryList = new ArrayList<>();
		OBBeneficiary2 obBeneficiary2 = new OBBeneficiary2();
		obBeneficiary2.setAccountId("123");
		obBeneficiary2.setBeneficiaryId("TestBeneficiaryId");
		obBeneficiaryList.add(obBeneficiary2);
		obReadBeneficiary2Data.setBeneficiary(obBeneficiaryList);

		obReadBeneficiary2.setData(obReadBeneficiary2Data);

		Links links = new Links();
		obReadBeneficiary2.setLinks(links);
		Meta meta = new Meta();
		meta.setTotalPages(1);
		obReadBeneficiary2.setMeta(meta);

		return obReadBeneficiary2;
		
	}



}