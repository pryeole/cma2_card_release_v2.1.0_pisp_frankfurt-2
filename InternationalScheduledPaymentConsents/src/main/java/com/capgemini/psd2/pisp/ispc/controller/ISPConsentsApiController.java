package com.capgemini.psd2.pisp.ispc.controller;

import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.OBPSD2Exception;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBWriteInternationalScheduledConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteInternationalScheduledConsentResponse1;
import com.capgemini.psd2.pisp.ispc.service.ISPConsentsService;
import com.fasterxml.jackson.databind.ObjectMapper;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-12-03T16:58:05.464+05:30")

@Controller
public class ISPConsentsApiController implements ISPConsentsApi {

	@Autowired
	private ISPConsentsService service;

	@Value("${app.responseErrorMessageEnabled}")
	private String responseErrorMessageEnabled;
	private final ObjectMapper objectMapper;

	private final HttpServletRequest request;

	@org.springframework.beans.factory.annotation.Autowired
	public ISPConsentsApiController(ObjectMapper objectMapper, HttpServletRequest request) {
		this.objectMapper = objectMapper;
		this.request = request;
	}

	@Override
	public Optional<ObjectMapper> getObjectMapper() {
		return Optional.ofNullable(objectMapper);
	}

	@Override
	public Optional<HttpServletRequest> getRequest() {
		return Optional.ofNullable(request);
	}

	@Override
	public ResponseEntity<OBWriteInternationalScheduledConsentResponse1> createInternationalScheduledPaymentConsents(
			@RequestBody OBWriteInternationalScheduledConsent1 obWriteInternationalScheduledConsent1Param,
			String xFapiFinancialId, String authorization, String xIdempotencyKey, String xJwsSignature,
			String xFapiCustomerLastLoggedTime, String xFapiCustomerIpAddress, String xFapiInteractionId,
			String xCustomerUserAgent) {
		if (getObjectMapper().isPresent()) {
			CustomISPConsentsPOSTRequest customRequest = new CustomISPConsentsPOSTRequest();
			customRequest.setData(obWriteInternationalScheduledConsent1Param.getData());
			customRequest.setRisk(obWriteInternationalScheduledConsent1Param.getRisk());
			CustomISPConsentsPOSTResponse customResponse = service
					.createInternationalScheduledPaymentConsentsResource(customRequest);
			OBWriteInternationalScheduledConsentResponse1 tppResponse = new OBWriteInternationalScheduledConsentResponse1();
			tppResponse.setData(customResponse.getData());
			tppResponse.setLinks(customResponse.getLinks());
			tppResponse.setMeta(customResponse.getMeta());
			tppResponse.setRisk(customResponse.getRisk());
			return new ResponseEntity<>(tppResponse, HttpStatus.CREATED);
		}

		throw OBPSD2Exception.populateOBException(OBErrorCodeEnum.UK_OBIE_RESOURCE_INVALIDFORMAT,
				ErrorMapKeys.RESOURCE_INVALIDFORMAT);
	}

	@Override
	public ResponseEntity<OBWriteInternationalScheduledConsentResponse1> getInternationalScheduledPaymentConsentsConsentId(
			@PathVariable("ConsentId") String consentId, String xFapiFinancialId, String authorization,
			String xFapiCustomerLastLoggedTime, String xFapiCustomerIpAddress, String xFapiInteractionId,
			String xCustomerUserAgent) {
		if (getObjectMapper().isPresent()) {

			CustomISPConsentsPOSTResponse customResponse = service
					.retrieveInternationalScheduledPaymentConsentsResource(consentId);

			OBWriteInternationalScheduledConsentResponse1 tppResponse = new OBWriteInternationalScheduledConsentResponse1();
			tppResponse.setData(customResponse.getData());
			tppResponse.setLinks(customResponse.getLinks());
			tppResponse.setMeta(customResponse.getMeta());
			tppResponse.setRisk(customResponse.getRisk());

			return new ResponseEntity<>(tppResponse, HttpStatus.OK);
		}
		throw PSD2Exception.populatePSD2Exception(
				new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_INVALIDFORMAT, ErrorMapKeys.RESOURCE_INVALIDFORMAT));
	}

}
