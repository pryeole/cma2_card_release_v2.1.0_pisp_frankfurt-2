package com.capgemini.psd2.pisp.international.standing.order.service;

import com.capgemini.psd2.pisp.domain.CustomIStandingOrderPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIStandingOrderPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBWriteInternationalStandingOrderResponse1;

public interface InternationalStandingOrdersService {

	CustomIStandingOrderPOSTResponse createInternationalStandingOrderResource(CustomIStandingOrderPOSTRequest customRequest);

	OBWriteInternationalStandingOrderResponse1 retrieveInternationalStandingOrderResource(String submissionId);

}
