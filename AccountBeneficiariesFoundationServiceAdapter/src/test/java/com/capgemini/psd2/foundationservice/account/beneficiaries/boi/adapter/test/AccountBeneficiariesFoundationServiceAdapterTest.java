package com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.OBExternalAccountSubType1Code;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountBeneficiariesResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.AccountBeneficiariesFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.client.AccountBeneficiariesFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.delegate.AccountBeneficiariesFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.Address;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.Beneficiary;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.BeneficiaryAccount;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.Country;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.ExternalPaymentBeneficiaryIndicator;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.OperationalOrganisationUnit;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.PartiesPaymentBeneficiariesresponse;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.PaymentBeneficiaryLocaleTypeCode;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.CustomerAccountsFilterFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.AdapterFilterUtility;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Account;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountEntitlements;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Currency;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.CurrentAccount;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUser;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUserProfile;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PartyBasicInformation;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.RetailNonRetailCode;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.SourceSystemAccountType;


@RunWith(SpringJUnit4ClassRunner.class)
public class AccountBeneficiariesFoundationServiceAdapterTest {

	@InjectMocks
	private AccountBeneficiariesFoundationServiceAdapter accountBeneficiariesFoundationServiceAdapter;

	@Mock
	private AccountBeneficiariesFoundationServiceDelegate accountBeneficiaryFoundationServiceDelegate;

	@Mock
	private AccountBeneficiariesFoundationServiceClient accountBeneficiaryFoundationServiceClient;
	
	@Mock
	private CustomerAccountsFilterFoundationServiceAdapter commonFilterUtility;
	
	@Mock
	private AdapterFilterUtility adapterFilterUtility;

	@Test
	public void testAccountBeneficiaryFS1() {
		PartiesPaymentBeneficiariesresponse partiesPaymentBeneficiariesresponse = new PartiesPaymentBeneficiariesresponse();
		List<Beneficiary> paymentBeneficiaryList = new ArrayList<Beneficiary>();
		Beneficiary paymentBeneficiary = new Beneficiary();
		BeneficiaryAccount beneficiaryAccount = new BeneficiaryAccount();
		OperationalOrganisationUnit operationalOrganisationUnit = new OperationalOrganisationUnit();
		Address addressReference = new Address();
		Country addressCountry = new Country();

		addressReference.setGeoCodeBuildingName("New Century House");
		addressReference.setFirstAddressLine("Mayor Street Lower");
		addressReference.setSecondAddressLine("International Financial Services Centre");
		addressReference.setThirdAddressLine("Dublin 1");

		addressCountry.setIsoCountryAlphaTwoCode("IE");
		addressCountry.setCountryName("Republic of Ireland");
		addressReference.setAddressCountry(addressCountry);

		operationalOrganisationUnit.setAddressReference(addressReference);

		beneficiaryAccount.setAccountName("ABC Corporation");
		beneficiaryAccount.setAccountNumber("24512786");
		beneficiaryAccount.setParentNationalSortCodeNSCNumber("902127");
		beneficiaryAccount.setInternationalBankAccountNumberIBAN("GB58BOFI90212724512786");
		beneficiaryAccount.setSwiftBankIdentifierCodeBIC("BOFIGB2BXXX");
		beneficiaryAccount.setOperationalOrganisationUnit(operationalOrganisationUnit);

		paymentBeneficiary.setPaymentBeneficiaryIdentifier("1234");
		paymentBeneficiary.setPaymentBeneficiaryLabel("ACME Co. Invoices");
		paymentBeneficiary.setPaymentBeneficiaryNarrativeText("October Invoices");
		paymentBeneficiary.setPaymentBeneficiaryBankName("Bank of Ireland");
		paymentBeneficiary.setPaymentBeneficiaryLocaleTypeCode(PaymentBeneficiaryLocaleTypeCode.SEPA);
		paymentBeneficiary.setExternalPaymentBeneficiaryIndicator(ExternalPaymentBeneficiaryIndicator.N);
		paymentBeneficiary.setBeneficiaryAccount(beneficiaryAccount);

		paymentBeneficiaryList.add(paymentBeneficiary);

		partiesPaymentBeneficiariesresponse.setPaymentBeneficiaryList(paymentBeneficiaryList);
		partiesPaymentBeneficiariesresponse.setTotal(1d);

		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setCorrelationId("test");
		accountMapping.setPsuId("test");
		
		DigitalUser du = new DigitalUser();
		du.setDigitalUserLockedOutIndicator(false);
		DigitalUserProfile dup = new DigitalUserProfile();
		dup.setDigitalUser(du);
		PartyBasicInformation pi = new PartyBasicInformation();
		pi.setPartyActiveIndicator(true);
		dup.setPartyInformation(pi);
		List<AccountEntitlements> accountEntitlements = new ArrayList<AccountEntitlements>();
		AccountEntitlements ea = new AccountEntitlements();
		List<String> ent = new ArrayList<>();
		ent.add("Display Only Account");
		Account account = new Account();
		RetailNonRetailCode retail = RetailNonRetailCode.fromValue("Retail");
		SourceSystemAccountType sourceSystemAccountTypeCurrent = SourceSystemAccountType.fromValue("Current Account");
		account.setAccountNumber("24512786");
		Currency currency = new Currency();
		CurrentAccount cuacinfo = new CurrentAccount();
		currency.setIsoAlphaCode("GBP");
		account.setAccountCurrency(currency);
		account.setRetailNonRetailCode(retail);
		account.setSourceSystemAccountType(sourceSystemAccountTypeCurrent);
		cuacinfo.setParentNationalSortCodeNSCNumber("902127");
		cuacinfo.setInternationalBankAccountNumberIBAN("GB58BOFI90212724512786");
		cuacinfo.setSwiftBankIdentifierCodeBIC("BOFIGB2BXXX");
		account.setCurrentAccountInformation(cuacinfo);
		ea.setAccount(account);
		ea.setEntitlements(ent);
		accountEntitlements.add(ea);
		dup.setAccountEntitlements(accountEntitlements);
		
		Mockito.when(accountBeneficiaryFoundationServiceDelegate.getFoundationServiceURL(any(), any(), any()))
				.thenReturn("http://10.102.19.131:8081/psd2-abt-service/services/abt/accounts/nsc1234/acct1234");
		Mockito.when(accountBeneficiaryFoundationServiceClient.restTransportForAccountBeneficiaryFS(any(), any(), any(),any())).thenReturn(partiesPaymentBeneficiariesresponse);
		Mockito.when(accountBeneficiaryFoundationServiceDelegate.transform((any()), any()))
				.thenReturn(new PlatformAccountBeneficiariesResponse());
		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(any(), any())).thenReturn(dup);
		Map<String, String> params = new HashMap<String, String>();
		params.put("channelId","B365");
		Mockito.when(adapterFilterUtility.prevalidateAccounts(dup, accountMapping)).thenReturn(accountMapping);
		accountBeneficiariesFoundationServiceAdapter.retrieveAccountBeneficiaries(accountMapping, params);
	}

	@Test
	public void testAccountBeneficiaryFS2() {
		PartiesPaymentBeneficiariesresponse partiesPaymentBeneficiariesresponse = new PartiesPaymentBeneficiariesresponse();
		List<Beneficiary> paymentBeneficiaryList = new ArrayList<Beneficiary>();
		Beneficiary paymentBeneficiary = new Beneficiary();
		BeneficiaryAccount beneficiaryAccount = new BeneficiaryAccount();
		OperationalOrganisationUnit operationalOrganisationUnit = new OperationalOrganisationUnit();
		Address addressReference = new Address();
		Country addressCountry = new Country();

		addressReference.setGeoCodeBuildingName("New Century House");
		addressReference.setFirstAddressLine("Mayor Street Lower");
		addressReference.setSecondAddressLine("International Financial Services Centre");
		addressReference.setThirdAddressLine("Dublin 1");

		addressCountry.setIsoCountryAlphaTwoCode("IE");
		addressCountry.setCountryName("Republic of Ireland");
		addressReference.setAddressCountry(addressCountry);

		operationalOrganisationUnit.setAddressReference(addressReference);

		beneficiaryAccount.setAccountName("ABC Corporation");
		beneficiaryAccount.setAccountNumber("24512786");
		beneficiaryAccount.setParentNationalSortCodeNSCNumber("902127");
		beneficiaryAccount.setInternationalBankAccountNumberIBAN("GB58BOFI90212724512786");
		beneficiaryAccount.setSwiftBankIdentifierCodeBIC("BOFIGB2BXXX");
		beneficiaryAccount.setOperationalOrganisationUnit(operationalOrganisationUnit);

		paymentBeneficiary.setPaymentBeneficiaryIdentifier("1234");
		paymentBeneficiary.setPaymentBeneficiaryLabel("ACME Co. Invoices");
		paymentBeneficiary.setPaymentBeneficiaryNarrativeText("October Invoices");
		paymentBeneficiary.setPaymentBeneficiaryBankName("Bank of Ireland");
		paymentBeneficiary.setPaymentBeneficiaryLocaleTypeCode(PaymentBeneficiaryLocaleTypeCode.SEPA);
		paymentBeneficiary.setExternalPaymentBeneficiaryIndicator(ExternalPaymentBeneficiaryIndicator.N);
		paymentBeneficiary.setBeneficiaryAccount(beneficiaryAccount);

		paymentBeneficiaryList.add(paymentBeneficiary);

		partiesPaymentBeneficiariesresponse.setPaymentBeneficiaryList(paymentBeneficiaryList);
		partiesPaymentBeneficiariesresponse.setTotal(1d);

		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountSubType(OBExternalAccountSubType1Code.SAVINGS);
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setCorrelationId("test");
		accountMapping.setPsuId("test");

		DigitalUser du = new DigitalUser();
		du.setDigitalUserLockedOutIndicator(false);
		DigitalUserProfile dup = new DigitalUserProfile();
		dup.setDigitalUser(du);
		PartyBasicInformation pi = new PartyBasicInformation();
		pi.setPartyActiveIndicator(true);
		dup.setPartyInformation(pi);
		List<AccountEntitlements> accountEntitlements = new ArrayList<AccountEntitlements>();
		AccountEntitlements ea = new AccountEntitlements();
		List<String> ent = new ArrayList<>();
		ent.add("Display Only Account");
		Account account = new Account();
		RetailNonRetailCode retail = RetailNonRetailCode.fromValue("Retail");
		SourceSystemAccountType sourceSystemAccountTypeCurrent = SourceSystemAccountType.fromValue("Current Account");
		account.setAccountNumber("24512786");
		Currency currency = new Currency();
		CurrentAccount cuacinfo = new CurrentAccount();
		currency.setIsoAlphaCode("GBP");
		account.setAccountCurrency(currency);
		account.setRetailNonRetailCode(retail);
		account.setSourceSystemAccountType(sourceSystemAccountTypeCurrent);
		cuacinfo.setParentNationalSortCodeNSCNumber("902127");
		cuacinfo.setInternationalBankAccountNumberIBAN("GB58BOFI90212724512786");
		cuacinfo.setSwiftBankIdentifierCodeBIC("BOFIGB2BXXX");
		account.setCurrentAccountInformation(cuacinfo);
		ea.setAccount(account);
		ea.setEntitlements(ent);
		accountEntitlements.add(ea);
		dup.setAccountEntitlements(accountEntitlements);

		Mockito.when(accountBeneficiaryFoundationServiceDelegate.getFoundationServiceURL(any(), any(), any()))
				.thenReturn("http://10.102.19.131:8081/psd2-abt-service/services/abt/accounts/nsc1234/acct1234");
		Mockito.when(accountBeneficiaryFoundationServiceClient.restTransportForAccountBeneficiaryFS(any(), any(), any(),any())).thenReturn(partiesPaymentBeneficiariesresponse);
		Mockito.when(accountBeneficiaryFoundationServiceDelegate.transform((any()), any()))
				.thenReturn(new PlatformAccountBeneficiariesResponse());
		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(any(), any())).thenReturn(dup);
		Map<String, String> params = new HashMap<String, String>();
		params.put("channelId","B365");
		Mockito.when(adapterFilterUtility.prevalidateAccounts(dup, accountMapping)).thenReturn(accountMapping);
		accountBeneficiariesFoundationServiceAdapter.retrieveAccountBeneficiaries(accountMapping, params);
	}

	@Test
	public void testAccountBeneficiaryFS3() {
		PartiesPaymentBeneficiariesresponse partiesPaymentBeneficiariesresponse = new PartiesPaymentBeneficiariesresponse();
		List<Beneficiary> paymentBeneficiaryList = new ArrayList<Beneficiary>();
		Beneficiary paymentBeneficiary = new Beneficiary();
		BeneficiaryAccount beneficiaryAccount = new BeneficiaryAccount();
		OperationalOrganisationUnit operationalOrganisationUnit = new OperationalOrganisationUnit();
		Address addressReference = new Address();
		Country addressCountry = new Country();

		addressReference.setGeoCodeBuildingName("New Century House");
		addressReference.setFirstAddressLine("Mayor Street Lower");
		addressReference.setSecondAddressLine("International Financial Services Centre");
		addressReference.setThirdAddressLine("Dublin 1");

		addressCountry.setIsoCountryAlphaTwoCode("IE");
		addressCountry.setCountryName("Republic of Ireland");
		addressReference.setAddressCountry(addressCountry);

		operationalOrganisationUnit.setAddressReference(addressReference);

		beneficiaryAccount.setAccountName("ABC Corporation");
		beneficiaryAccount.setAccountNumber("24512786");
		beneficiaryAccount.setParentNationalSortCodeNSCNumber("902127");
		beneficiaryAccount.setInternationalBankAccountNumberIBAN("GB58BOFI90212724512786");
		beneficiaryAccount.setSwiftBankIdentifierCodeBIC("BOFIGB2BXXX");
		beneficiaryAccount.setOperationalOrganisationUnit(operationalOrganisationUnit);

		paymentBeneficiary.setPaymentBeneficiaryIdentifier("1234");
		paymentBeneficiary.setPaymentBeneficiaryLabel("ACME Co. Invoices");
		paymentBeneficiary.setPaymentBeneficiaryNarrativeText("October Invoices");
		paymentBeneficiary.setPaymentBeneficiaryBankName("Bank of Ireland");
		paymentBeneficiary.setPaymentBeneficiaryLocaleTypeCode(PaymentBeneficiaryLocaleTypeCode.SEPA);
		paymentBeneficiary.setExternalPaymentBeneficiaryIndicator(ExternalPaymentBeneficiaryIndicator.N);
		paymentBeneficiary.setBeneficiaryAccount(beneficiaryAccount);

		paymentBeneficiaryList.add(paymentBeneficiary);

		partiesPaymentBeneficiariesresponse.setPaymentBeneficiaryList(paymentBeneficiaryList);
		partiesPaymentBeneficiariesresponse.setTotal(1d);

		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CREDITCARD);
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setCorrelationId("test");
		accountMapping.setPsuId("test");

		DigitalUser du = new DigitalUser();
		du.setDigitalUserLockedOutIndicator(false);
		DigitalUserProfile dup = new DigitalUserProfile();
		dup.setDigitalUser(du);
		PartyBasicInformation pi = new PartyBasicInformation();
		pi.setPartyActiveIndicator(true);
		dup.setPartyInformation(pi);
		List<AccountEntitlements> accountEntitlements = new ArrayList<AccountEntitlements>();
		AccountEntitlements ea = new AccountEntitlements();
		List<String> ent = new ArrayList<>();
		ent.add("Display Only Account");
		Account account = new Account();
		RetailNonRetailCode retail = RetailNonRetailCode.fromValue("Retail");
		SourceSystemAccountType sourceSystemAccountTypeCurrent = SourceSystemAccountType.fromValue("Current Account");
		account.setAccountNumber("24512786");
		Currency currency = new Currency();
		CurrentAccount cuacinfo = new CurrentAccount();
		currency.setIsoAlphaCode("GBP");
		account.setAccountCurrency(currency);
		account.setRetailNonRetailCode(retail);
		account.setSourceSystemAccountType(sourceSystemAccountTypeCurrent);
		cuacinfo.setParentNationalSortCodeNSCNumber("902127");
		cuacinfo.setInternationalBankAccountNumberIBAN("GB58BOFI90212724512786");
		cuacinfo.setSwiftBankIdentifierCodeBIC("BOFIGB2BXXX");
		account.setCurrentAccountInformation(cuacinfo);
		ea.setAccount(account);
		ea.setEntitlements(ent);
		accountEntitlements.add(ea);
		dup.setAccountEntitlements(accountEntitlements);

		Mockito.when(accountBeneficiaryFoundationServiceDelegate.getFoundationServiceURL(any(), any(), any()))
				.thenReturn("http://10.102.19.131:8081/psd2-abt-service/services/abt/accounts/nsc1234/acct1234");
		Mockito.when(accountBeneficiaryFoundationServiceClient.restTransportForAccountBeneficiaryFS(any(), any(), any(),any())).thenReturn(partiesPaymentBeneficiariesresponse);
		Mockito.when(accountBeneficiaryFoundationServiceDelegate.transform((any()), any()))
				.thenReturn(new PlatformAccountBeneficiariesResponse());
		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(any(), any())).thenReturn(dup);
		Map<String, String> params = new HashMap<String, String>();
		params.put("channelId","B365");
		Mockito.when(adapterFilterUtility.prevalidateAccounts(dup, accountMapping)).thenReturn(accountMapping);
		accountBeneficiariesFoundationServiceAdapter.retrieveAccountBeneficiaries(accountMapping, params);
	}

	@Test(expected = AdapterException.class)
	public void testExceptionNullCheckUtil1() {
		AccountMapping accountMapping = new AccountMapping();
		Map<String, String> params = new HashMap<>();
		accountMapping.setPsuId("test");
		accountMapping.setCorrelationId("test");
		Mockito.when(accountBeneficiaryFoundationServiceClient.restTransportForAccountBeneficiaryFS(any(), any(), any(),any())).thenReturn(null);
		accountBeneficiariesFoundationServiceAdapter.retrieveAccountBeneficiaries(accountMapping, params);
	}

	@Test(expected = AdapterException.class)
	public void testExceptionNullParams() {
		AccountMapping accountMapping = new AccountMapping();
		Map<String, String> params = null;
		accountMapping.setPsuId("test");
		accountMapping.setCorrelationId("test");
		Mockito.when(accountBeneficiaryFoundationServiceClient.restTransportForAccountBeneficiaryFS(any(), any(),any(),any())).thenReturn(null);
		accountBeneficiariesFoundationServiceAdapter.retrieveAccountBeneficiaries(accountMapping, params);
	}

	@Test(expected = AdapterException.class)
	public void testExceptionNullCheckUtil2() {
		AccountMapping accountMapping = new AccountMapping();
		Map<String, String> params = new HashMap<>();
		accountMapping.setPsuId(null);
		Mockito.when(accountBeneficiaryFoundationServiceClient.restTransportForAccountBeneficiaryFS(any(), any(), any(),any())).thenReturn(null);
		accountBeneficiariesFoundationServiceAdapter.retrieveAccountBeneficiaries(accountMapping, params);
	}

	@Test(expected = AdapterException.class)
	public void testExceptionNullAccountMappingPsu() {
		AccountMapping accountMapping = null;
		Map<String, String> params = new HashMap<>();
		Mockito.when(accountBeneficiaryFoundationServiceClient.restTransportForAccountBeneficiaryFS(any(), any(), any(),any())).thenReturn(null);
		accountBeneficiariesFoundationServiceAdapter.retrieveAccountBeneficiaries(accountMapping, params);
	}

	@Test(expected = AdapterException.class)
	public void testExceptionNullCheckUtil3() {
		AccountMapping accountMapping = new AccountMapping();
		Map<String, String> params = new HashMap<>();
		accountMapping.setPsuId(null);
		Mockito.when(
				accountBeneficiaryFoundationServiceClient.restTransportForAccountBeneficiaryFS(null, null, null,null))
				.thenReturn(null);
		accountBeneficiariesFoundationServiceAdapter.retrieveAccountBeneficiaries(accountMapping, params);
	}
	

	@Test(expected = Exception.class)
	public void testRestTransportForBeneficiary() {
		PlatformAccountBeneficiariesResponse partiesPaymentBeneficiariesresponse = null;
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("test");
		Mockito.when(accountBeneficiaryFoundationServiceClient.restTransportForAccountBeneficiaryFS(anyObject(), anyObject(), anyObject(),anyObject()))
				.thenThrow(PSD2Exception.populatePSD2Exception("", ErrorCodeEnum.TECHNICAL_ERROR));
		Mockito.when(accountBeneficiaryFoundationServiceDelegate.transform((any()), any()))
				.thenReturn(partiesPaymentBeneficiariesresponse);
		accountBeneficiariesFoundationServiceAdapter.retrieveAccountBeneficiaries(accountMapping,
				new HashMap<String, String>());
	}

}