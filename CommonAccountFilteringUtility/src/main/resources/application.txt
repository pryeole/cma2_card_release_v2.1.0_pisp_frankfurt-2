server:
  port: 8086
spring:
  application:
    name: adapter
foundationService:
  apiId: CustomerAccountProfile
  foundationUrl:    
    BOL: http://localhost:9093/fs-customeraccountprofile-service/services/customerAccountProfile/channel/business/account
    B365: http://localhost:9093/fs-customeraccountprofile-service/services/customerAccountProfile/channel/personal/account
    PO: http://localhost:9093/fs-customeraccountprofile-service/services/customerAccountProfile/channel/personal/account
    AA: http://localhost:9093/fs-customeraccountprofile-service/services/customerAccountProfile/channel/personal/account
  foundationMultiAcUrl:
    BOL: http://localhost:9090/fs-customeraccountprofile-service/services/customerAccountProfile/channel/business/profile
    B365: http://localhost:9090/fs-customeraccountprofile-service/services/customerAccountProfile/channel/personal/profile
    PO: http://localhost:9090/fs-customeraccountprofile-service/services/customerAccountProfile/channel/personal/profile
    AA: http://localhost:9090/fs-customeraccountprofile-service/services/customerAccountProfile/channel/personal/profile
  userInReqHeader: X-BOI-USER
  channelInReqHeader: X-BOI-CHANNEL
  platformInReqHeader: X-BOI-PLATFORM
  correlationReqHeader: X-CORRELATION-ID
  accountFiltering: 
   permission: 
      AISP:
       - V
       - A
      CISP: 
       - V
       - A
      PISP:
       - X
       - A
   accountType:
      AISP:
       - Current Account
      CISP: 
       - Current Account
      PISP:
       - Current Account
   jurisdiction:
      AISP:
        - NORTHERN_IRELAND.SchemeName=BBAN
        - NORTHERN_IRELAND.Identification=AccountNumber
        - NORTHERN_IRELAND.ServicerSchemeName=UKSORTCODE
        - NORTHERN_IRELAND.ServicerIdentification=AccountNSCNumber
        - GREAT_BRITAIN.SchemeName=BBAN
        - GREAT_BRITAIN.Identification=AccountNumber
        - GREAT_BRITAIN.ServicerSchemeName=UKSORTCODE
        - GREAT_BRITAIN.ServicerIdentification=AccountNSCNumber
      CISP: 
        - NORTHERN_IRELAND.SchemeName=BBAN
        - NORTHERN_IRELAND.Identification=AccountNumber
        - NORTHERN_IRELAND.ServicerSchemeName=UKSORTCODE
        - NORTHERN_IRELAND.ServicerIdentification=AccountNSCNumber
        - GREAT_BRITAIN.SchemeName=BBAN
        - GREAT_BRITAIN.Identification=AccountNumber
        - GREAT_BRITAIN.ServicerSchemeName=UKSORTCODE
        - GREAT_BRITAIN.ServicerIdentification=AccountNSCNumber
      PISP: 
        - NORTHERN_IRELAND.SchemeName=BBAN
        - NORTHERN_IRELAND.Identification=AccountNumber
        - NORTHERN_IRELAND.ServicerSchemeName=UKSORTCODE
        - NORTHERN_IRELAND.ServicerIdentification=AccountNSCNumber
        - GREAT_BRITAIN.SchemeName=BBAN
        - GREAT_BRITAIN.Identification=AccountNumber
        - GREAT_BRITAIN.ServicerSchemeName=UKSORTCODE
        - GREAT_BRITAIN.ServicerIdentification=AccountNSCNumber
   
app:
  platform: platform