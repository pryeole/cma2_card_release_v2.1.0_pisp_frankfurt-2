/*
 * Party Process API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain;

import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

/**
 * GCIS customer classification separating Industrial and Consumer
 */
@JsonAdapter(GcisCustomertype.Adapter.class)
public enum GcisCustomertype {
  
  INDIVIDUAL("Individual"),
  
  INDUSTRIAL("Industrial");

  private String value;

  GcisCustomertype(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static GcisCustomertype fromValue(String text) {
    for (GcisCustomertype b : GcisCustomertype.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }

  public static class Adapter extends TypeAdapter<GcisCustomertype> {
    @Override
    public void write(final JsonWriter jsonWriter, final GcisCustomertype enumeration) throws IOException {
      jsonWriter.value(enumeration.getValue());
    }

    @Override
    public GcisCustomertype read(final JsonReader jsonReader) throws IOException {
      String value = jsonReader.nextString();
      return GcisCustomertype.fromValue(String.valueOf(value));
    }
  }
}

