package com.capgemini.psd2.customer.account.profile.mock.foundationservice.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.customer.account.profile.mock.foundationservice.domain.ChannelProfile;
import com.capgemini.psd2.customer.account.profile.mock.foundationservice.exception.handler.InvalidParameterRequestException;
import com.capgemini.psd2.customer.account.profile.mock.foundationservice.exception.handler.MissingAuthenticationHeaderException;
import com.capgemini.psd2.customer.account.profile.mock.foundationservice.exception.handler.UserBlockedRecordNotFoundException;
import com.capgemini.psd2.customer.account.profile.mock.foundationservice.service.CustomerAccountInformationService;

@RestController
@RequestMapping("/fs-customeraccountprofile-service/services/customerAccountProfilev2/channel")
@ConfigurationProperties(prefix = "mockFoundationService")
@EnableAutoConfiguration
public class CustomerAccountProfileController {
	@Autowired
	CustomerAccountInformationService customerAccountInformationService;
	private List<String> blockedUserList=new ArrayList<String>();
	
	@RequestMapping(value = "/business/profile/{USER-ID}/accounts", method = RequestMethod.GET, produces = "application/xml")
	public ChannelProfile channelAReteriveCustomerAccountInformation(		
			@PathVariable("USER-ID") String userid,
			@RequestHeader(required = false,value="X-BOI-USER") String boiUser,
			@RequestHeader(required = false,value="X-BOI-CHANNEL") String boiChannel,
			@RequestHeader(required = false,value="X-BOI-PLATFORM") String boiPlatform,
			@RequestHeader(required = false,value="X-CORRELATION-ID") String correlationID) throws Exception
	{
		if(boiUser==null || boiChannel==null || boiPlatform==null || correlationID==null ){
			throw new MissingAuthenticationHeaderException("Header Missing");
		}		
		if(userid==null)
		{
			throw new InvalidParameterRequestException("Bad request");
		}
		if(blockedUserList.contains(userid))
		{
			throw new UserBlockedRecordNotFoundException("The Customer Account Profile is blocked");
		}
		return customerAccountInformationService.retrieveCustomerAccountInformation(userid);
		
	}
	
	@RequestMapping(value = "/personal/profile/{USER-ID}/accounts", method = RequestMethod.GET, produces = "application/xml")
	public ChannelProfile channelBReteriveCustomerAccountInformation(		
			@PathVariable("USER-ID") String userid,
			@RequestHeader(required = false,value="X-BOI-USER") String boiUser,
			@RequestHeader(required = false,value="X-BOI-CHANNEL") String boiChannel,
			@RequestHeader(required = false,value="X-BOI-PLATFORM") String boiPlatform,
			@RequestHeader(required = false,value="X-CORRELATION-ID") String correlationID) throws Exception
	{		
		if(boiUser==null || boiChannel==null || boiPlatform==null || correlationID==null ){
			throw new MissingAuthenticationHeaderException("Header Missing");
		}		
		if(userid==null)
		{
			throw new InvalidParameterRequestException("Bad request");
		}
		if(blockedUserList.contains(userid))
		{
			throw new UserBlockedRecordNotFoundException("The Customer Account Profile is blocked");
		}
		return customerAccountInformationService.retrieveCustomerAccountInformation(userid);
	}

	public List<String> getBlockedUserList() {
		return blockedUserList;
	}

	public void setBlockedUserList(List<String> blockedUserList) {
		this.blockedUserList = blockedUserList;
	}

}
