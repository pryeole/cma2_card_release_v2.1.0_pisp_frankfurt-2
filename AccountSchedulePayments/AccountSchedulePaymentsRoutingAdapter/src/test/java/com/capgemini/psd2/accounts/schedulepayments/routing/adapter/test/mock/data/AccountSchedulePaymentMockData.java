package com.capgemini.psd2.accounts.schedulepayments.routing.adapter.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.OBReadScheduledPayment1;
import com.capgemini.psd2.aisp.domain.OBReadScheduledPayment1Data;
import com.capgemini.psd2.aisp.domain.OBScheduledPayment1;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountSchedulePaymentsResponse;

public class AccountSchedulePaymentMockData {

	public static PlatformAccountSchedulePaymentsResponse getMockSchedulePaymentGETResponse() {
		PlatformAccountSchedulePaymentsResponse platformAccountSchedulePaymentResponse = new PlatformAccountSchedulePaymentsResponse();
		OBReadScheduledPayment1 data = new OBReadScheduledPayment1();
		OBReadScheduledPayment1Data data1 = new OBReadScheduledPayment1Data();
		List<OBScheduledPayment1> payment = new ArrayList<>();
		data1.setScheduledPayment(payment);
		data.setData(data1);
		platformAccountSchedulePaymentResponse.setObReadScheduledPayment1(data);
		return platformAccountSchedulePaymentResponse;
	}
}
