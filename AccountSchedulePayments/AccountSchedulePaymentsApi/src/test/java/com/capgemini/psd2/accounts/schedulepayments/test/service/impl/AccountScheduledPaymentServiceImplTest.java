package com.capgemini.psd2.accounts.schedulepayments.test.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.accounts.schedulepayments.service.impl.SchedulePaymentServiceImpl;
import com.capgemini.psd2.accounts.schedulepayments.test.mock.data.AccountSchedulePaymentMockData;
import com.capgemini.psd2.aisp.account.mapping.adapter.AccountMappingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountSchedulePaymentsAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.OBReadScheduledPayment1;
import com.capgemini.psd2.aisp.validation.adapter.impl.AccountSchedulePaymentsValidatorImpl;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.RequestHeaderAttributes;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountScheduledPaymentServiceImplTest {

	/** The adapter. */
	@Mock
	private AccountSchedulePaymentsAdapter adapter;

	@Mock
	private AccountMappingAdapter accountMappingAdapter;

	/** The consent mapping. */
	@Mock
	private AispConsentAdapter aispConsentAdapter;

	/** The header atttributes. */
	@Mock
	private RequestHeaderAttributes headerAtttributes;

	@Mock
	private LoggerUtils loggerUtils;

	/** The service. */
	@InjectMocks
	private SchedulePaymentServiceImpl service = new SchedulePaymentServiceImpl();
	
	@Mock
	private AccountSchedulePaymentsValidatorImpl accountSchedulePaymentsValidatorImpl;

	/**
	 * Sets the up.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(headerAtttributes.getToken()).thenReturn(AccountSchedulePaymentMockData.getToken());
		when(aispConsentAdapter.retrieveAccountMapping(anyString()))
				.thenReturn(AccountSchedulePaymentMockData.getMockAccountMapping());
		Map<String, String> map=new HashMap<>();
		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");
		Map<String, String> specificErrorMessageMap=new HashMap<>();
		specificErrorMessageMap.put("signature_invalid_content", "Invalid content prsent in joseheader of x-jws-signature header");
		specificErrorMessageMap.put("signature_missing", "signature header missing in request");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);

	}

	/**
	 * Retrieve account schedule payment test (empty list).
	 */
	@Test
	public void retrieveAccountSchedulePaymentSuccessWithLinksMeta() {
		accountSchedulePaymentsValidatorImpl.validateUniqueId(anyObject());
		
		Mockito.when(aispConsentAdapter.validateAndRetrieveConsentByAccountId(anyString(), anyString()))
				.thenReturn(AccountSchedulePaymentMockData.getMockAispConsent());

		Mockito.when(accountMappingAdapter.retrieveAccountMappingDetails(anyString(), anyObject()))
				.thenReturn(AccountSchedulePaymentMockData.getMockAccountMapping());

		Mockito.when(adapter.retrieveAccountSchedulePayments(anyObject(), anyObject()))
				.thenReturn(AccountSchedulePaymentMockData.getMockPlatformReponseWithLinksMeta());

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
		.thenReturn(AccountSchedulePaymentMockData.getMockLoggerData());
		
		accountSchedulePaymentsValidatorImpl.validateResponseParams(anyObject());
		
		OBReadScheduledPayment1 actualResponse = service.retrieveSchedulePayments("f4483fda-81be-4873-b4a6-20375b7f55c1");
		assertEquals(AccountSchedulePaymentMockData.getMockExpectedAccountSchedulePaymentResponseWithLinksMeta(),actualResponse);

	}
	
	
	@Test
	public void retrieveAccountSchedulePaymentsSuccessWithoutLinksMeta() {
		accountSchedulePaymentsValidatorImpl.validateUniqueId(anyObject());
		
		Mockito.when(aispConsentAdapter.validateAndRetrieveConsentByAccountId(anyString(), anyString()))
				.thenReturn(AccountSchedulePaymentMockData.getMockAispConsent());

		Mockito.when(accountMappingAdapter.retrieveAccountMappingDetails(anyString(), anyObject()))
				.thenReturn(AccountSchedulePaymentMockData.getMockAccountMapping());

		Mockito.when(adapter.retrieveAccountSchedulePayments(anyObject(), anyObject()))
				.thenReturn(AccountSchedulePaymentMockData.getMockPlatformReponseWithoutLinksMeta());

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
		.thenReturn(AccountSchedulePaymentMockData.getMockLoggerData());

		accountSchedulePaymentsValidatorImpl.validateResponseParams(anyObject());
		
		OBReadScheduledPayment1 actualResponse = service.retrieveSchedulePayments("123");
		assertEquals(AccountSchedulePaymentMockData.getMockExpectedAccountSchedulePaymentsResponseWithoutLinksMeta(),actualResponse);

	}

	/**
	 * Tear down.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@After
	public void tearDown() throws Exception {
		service = null;
	}
}
