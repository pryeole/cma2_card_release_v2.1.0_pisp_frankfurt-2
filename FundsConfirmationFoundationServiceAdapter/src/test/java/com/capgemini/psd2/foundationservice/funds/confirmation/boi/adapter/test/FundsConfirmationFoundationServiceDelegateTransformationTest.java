package com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.test;
/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************//*
package com.capgemini.psd2.foundationservice.account.balance.boi.adapter.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.delegate.FundsConfirmationFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.transformer.FundsConfirmationFoundationServiceTransformer;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;
import com.capgemini.psd2.validator.PSD2Validator;

*//**
 * The Class AccountBalanceFoundationServiceDelegateTransformationTest.
 *//*
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountBalanceFoundationServiceDelegateTransformationTest {

	@InjectMocks
	private FundsConfirmationFoundationServiceDelegate delegate;

	*//** The account balance FS transformer. *//*
	@InjectMocks
	private FundsConfirmationFoundationServiceTransformer accountBalanceFSTransformer = new FundsConfirmationFoundationServiceTransformer();

	*//** The rest client. *//*
	@Mock
	private RestClientSyncImpl restClient;

	*//** The psd 2 validator. *//*
	@Mock
	private PSD2Validator psd2Validator;

	@Mock
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;

	*//**
	 * Sets the up.
	 *//*
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	*//**
	 * Context loads.
	 *//*
	@Test
	public void contextLoads() {
	}

	*//**
	 * Test transform response from FD to API.
	 *//*
	
	 * @Test public void testTransformResponseFromFDToAPI() { OBReadBalance1
	 * obReadBalance1= new OBReadBalance1(); OBReadBalance1Data
	 * obReadBalance1Data= new OBReadBalance1Data(); List<OBCashBalance1>
	 * balanceList = new ArrayList<>();
	 * obReadBalance1Data.setBalance(balanceList); Map<String, String> params =
	 * new HashMap<>(); params.put("accountId", "12345"); Balance bal = new
	 * Balance(); bal.setAvailableBalance(new BigDecimal(5000.00d));
	 * bal.setCurrency("GBP"); bal.setPostedBalance(new BigDecimal(5000.00d));
	 * Accounts accounts = new Accounts(); Accnt accnt = new Accnt();
	 * accnt.setBalance(bal); accounts.getAccount().add(accnt);
	 * 
	 * PlatformAccountBalanceResponse finalGBResponseObj = new
	 * PlatformAccountBalanceResponse();
	 * obReadBalance1.setData(obReadBalance1Data);
	 * finalGBResponseObj.setoBReadBalance1(obReadBalance1);
	 * OBCashBalance1Amount amount = new OBCashBalance1Amount();
	 * amount.setAmount(new BigDecimal(5000.00d).toString());
	 * amount.setCurrency("GBP"); OBCashBalance1 responseDataObj = new
	 * OBCashBalance1(); responseDataObj.setAccountId(params.get("accountId"));
	 * responseDataObj.type(TypeEnum.CLOSINGBOOKED);
	 * responseDataObj.setAmount(amount);
	 * finalGBResponseObj.getoBReadBalance1().getData().getBalance().add(
	 * responseDataObj); doNothing().when(psd2Validator).validate(anyObject());
	 * params.put(AccountBalanceFoundationServiceConstants.ACCOUNT_SUBTYPE,
	 * "CurrentAccount"); PlatformAccountBalanceResponse res =
	 * accountBalanceFSTransformer.transformAccountBalance(accounts, params);
	 * OBCashBalance1Amount
	 * ans=res.getoBReadBalance1().getData().getBalance().get(0).getAmount();
	 * String exp="5000.0"; assertEquals(exp, ans.getAmount());
	 * 
	 * }
	 

	@Test
	public void testcreateRequestHeaders() {

		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("userId");
		accountMapping.setCorrelationId("correlationId");
		HttpHeaders httpHeaders = new HttpHeaders();
		ReflectionTestUtils.setField(delegate, "transactioReqHeader", "X-BOI-USER");
		ReflectionTestUtils.setField(delegate, "correlationMuleReqHeader", "X-BOI-CHANNEL");
		ReflectionTestUtils.setField(delegate, "sourcesystem", "X-BOI-PLATFORM");
		ReflectionTestUtils.setField(delegate, "sourceUserReqHeader", "X-CORRELATION-ID");
		ReflectionTestUtils.setField(delegate, "apiChannelCode", "X-API-SOURCE-SYSTEM");
		ReflectionTestUtils.setField(delegate, "partySourceNumber", "X-API-PARTY-SOURCE-ID-NUMBER");
		Map<String, String> params = new HashMap<>();
		params.put("channelId", "channel123");
		httpHeaders = delegate.createAccountRequestHeaders(new RequestInfo(), accountMapping, params);

		assertNotNull(httpHeaders);
	}

	
	 * @Test public void testGetFoundationServiceURL() { String version="12";
	 * String accountNSC = "number"; String accountNumber = "accountNumber";
	 * String baseURL =
	 * "http://localhost:8082/fs-customeraccountprofile-service/services/customerAccountProfile/channel/business/account";
	 * String testURL = delegate.getFoundationServiceURL(accountNSC,
	 * accountNumber,baseURL,version); String url=
	 * "http://localhost:8082/fs-customeraccountprofile-service/services/customerAccountProfile/channel/business/account/number/accountNumber";
	 * assertEquals(url, testURL); //assertNotNull(testURL); }
	 
	//
	// @Test(expected=AdapterException.class)
	// public void testGetFoundationServiceURLForNullNSC() {
	// String version="12";
	//
	// String accountNSC = "";
	// String accountNumber = "accountNumber";
	// String baseURL =
	// "http://localhost:8082/fs-customeraccountprofile-service/services/customerAccountProfile/channel/business/account";
	// String testURL = delegate.getFoundationServiceURL(accountNSC,
	// accountNumber,baseURL,version);
	// String
	// url="http://localhost:8082/fs-customeraccountprofile-service/services/customerAccountProfile/channel/business/account/number/accountNumber";
	// assertEquals(url, testURL);
	// //assertNotNull(testURL);
	// }

	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceURLForNullAccNumber() {
		String version = "12";

		String accountNSC = "number";
		String accountNumber = null;
		String baseURL = "http://localhost:8082/fs-customeraccountprofile-service/services/customerAccountProfile/channel/business/account";
		String testURL = delegate.getFoundationServiceURL(accountNSC, accountNumber, baseURL, version);
		String url = "http://localhost:8082/fs-customeraccountprofile-service/services/customerAccountProfile/channel/business/account/number/accountNumber";
		assertEquals(url, testURL);
		// assertNotNull(testURL);
	}

	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceURLForNullbaseUrl() {
		String version = "12";

		String accountNSC = "number";
		String accountNumber = "accountNumber";
		String baseURL = null;
		String testURL = delegate.getFoundationServiceURL(accountNSC, accountNumber, baseURL, version);
		String url = "http://localhost:8082/fs-customeraccountprofile-service/services/customerAccountProfile/channel/business/account/number/accountNumber";
		assertEquals(url, testURL);
		// assertNotNull(testURL);
	}

	
	 * @Test public void testgetCreditCardFoundationServiceURL() { String
	 * version="12";
	 * 
	 * String creditCardNumber="4747"; String plApplId="68"; String
	 * baseURL="http://localhost:8082/fs-customeraccountprofile-service" ;
	 * String endUrl="/success"; String
	 * testUrl=delegate.getCreditCardFoundationServiceURL(creditCardNumber,
	 * plApplId, baseURL,version); String url=
	 * "http://localhost:8082/fs-customeraccountprofile-service/4747/68/balance";
	 * assertEquals(url, testUrl);
	 * 
	 * }
	 
	
	 * @Test public void
	 * testgetCreditCardFoundationServiceURLForMoreThanLengthFour() { String
	 * version="12"; String creditCardNumber="354747"; String plApplId="68";
	 * String baseURL="http://localhost:8082/fs-customeraccountprofile-service"
	 * ; String
	 * testUrl=delegate.getCreditCardFoundationServiceURL(creditCardNumber,
	 * plApplId, baseURL,version ); String url=
	 * "http://localhost:8082/fs-customeraccountprofile-service/4747/68/balances";
	 * assertEquals(url, testUrl);
	 * 
	 * }
	 
	@Test
	public void testgetCreditCardFoundationServiceURLFormOreThanLengthFour() {
		String version = "12";

		String creditCardNumber = "345747";
		String plApplId = "68";
		String baseURL = "http://localhost:8082/fs-customeraccountprofile-service";
		String testUrl = delegate.getCreditCardFoundationServiceURL(creditCardNumber, plApplId, baseURL, version);
		String url = "http://localhost:8082/fs-customeraccountprofile-service/4747/68/balance";
		assertNotEquals(url, testUrl);

	}

	@Test
	public void testgetCreditCardFoundationServiceURLForNullCreditCard() {
		String version = "12";

		String creditCardNumber = null;
		String plApplId = "68";
		String baseURL = "http://localhost:8082/fs-customeraccountprofile-service";
		delegate.getCreditCardFoundationServiceURL(creditCardNumber, plApplId, baseURL, version);

	}

	@Test(expected = AdapterException.class)
	public void testgetCreditCardFoundationServiceURLForNullplApplId() {
		String version = "12";

		String creditCardNumber = "5678";
		String plApplId = null;
		String baseURL = "http://localhost:8082/fs-customeraccountprofile-service";
		delegate.getCreditCardFoundationServiceURL(creditCardNumber, plApplId, baseURL, version);

	}

	@Test(expected = AdapterException.class)
	public void testgetCreditCardFoundationServiceURLForNullbaseURL() {
		String version = "12";

		String creditCardNumber = "5678";
		String plApplId = "68";
		String baseURL = null;
		delegate.getCreditCardFoundationServiceURL(creditCardNumber, plApplId, baseURL, version);

	}

}
*/