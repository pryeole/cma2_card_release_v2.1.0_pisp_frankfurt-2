/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.party.test.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.account.party.service.impl.AccountPartyServiceImpl;
import com.capgemini.psd2.account.party.test.mock.data.AccountPartyMockData;
import com.capgemini.psd2.aisp.account.mapping.adapter.AccountMappingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountPartyAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.cma2.aisp.domain.OBReadParty1;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.response.validator.ResponseValidator;

/**
 * The Class AccountDirectDebitsServiceImplTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountPartyServiceImplTest {

	@Mock
	private LoggerUtils loggerUtils;

	/** The adapter. */
	@Mock
	private AccountPartyAdapter adapter;

	/** The consent mapping. */
	@Mock
	private AispConsentAdapter aispConsentAdapter;

	/** The header atttributes. */
	@Mock
	private RequestHeaderAttributes headerAtttributes;

	@Mock
	AccountMappingAdapter accountMappingAdapter;

	@Mock
	private ResponseValidator responsevalidator;

	/** The service. */
	@InjectMocks
	private AccountPartyServiceImpl service = new AccountPartyServiceImpl();

	/**
	 * Sets the up.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(headerAtttributes.getToken()).thenReturn(AccountPartyMockData.getToken());
		when(aispConsentAdapter.retrieveAccountMapping(anyString()))
				.thenReturn(AccountPartyMockData.getMockAccountMapping());

	}

	/**
	 * Retrieve success response
	 */

	@Test
	public void retrieveAccountPartySuccessWithLinksMeta() {
		Mockito.when(aispConsentAdapter.validateAndRetrieveConsentByAccountId(anyString(), anyString()))
				.thenReturn(AccountPartyMockData.getMockAispConsent());

		Mockito.when(accountMappingAdapter.retrieveAccountMappingDetails(anyString(), anyObject()))
				.thenReturn(AccountPartyMockData.getMockAccountMapping());

		Mockito.when(adapter.retrieveAccountParty(anyObject(), anyObject()))
				.thenReturn(AccountPartyMockData.getMockPlatformResponseWithLinksMeta());
		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountPartyMockData.getMockLoggerData());

		responsevalidator.validateResponse(anyObject());

		OBReadParty1 actualresponse = service.retrieveAccountParty("f4483fda-81be-4873-b4a6-20375b7f55c1");

		assertEquals(AccountPartyMockData.getMockExpectedResponseWithLinksMeta(),
				actualresponse);

	}

	@Test
	public void retrieveAccountPartySuccessWithoutLinksMeta() {
		
		Mockito.when(aispConsentAdapter.validateAndRetrieveConsentByAccountId(anyString(), anyString()))
				.thenReturn(AccountPartyMockData.getMockAispConsent());

		Mockito.when(accountMappingAdapter.retrieveAccountMappingDetails(anyString(), anyObject()))
				.thenReturn(AccountPartyMockData.getMockAccountMapping());

		Mockito.when(adapter.retrieveAccountParty(anyObject(), anyObject()))
				.thenReturn(AccountPartyMockData.getMockPlatformResponseWithoutLinksMeta());
		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountPartyMockData.getMockLoggerData());

		responsevalidator.validateResponse(anyObject());

		OBReadParty1 actualresponse = service.retrieveAccountParty("123");

		assertEquals(AccountPartyMockData.getMockPlatformResponseWithoutLinksMeta().getobReadParty1().getData(),
				actualresponse.getData());

	}

	/**
	 * Retrieve account direct debits test (empty list).
	 */
	/*
	 * @Test public void retrieveAccountDirectDebitsTest() {
	 * when(adapter.retrieveAccountDirectDebits(anyObject(), anyObject()))
	 * .thenReturn(AccountDirectDebitsMockData.getMockBlankResponse());
	 * AccountGETResponse1 accountGETResponse1 =
	 * service.retrieveAccountDirectDebits( "123");
	 * assertTrue(accountGETResponse1.getData().getDirectDebit().isEmpty()); }
	 */

	/**
	 * Tear down.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@After
	public void tearDown() throws Exception {
		service = null;
	}
}
