package com.capgemini.psd2.fraudsystem.request.handler.impl;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.StringTokenizer;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.capgemini.psd2.aisp.adapter.CustomerAccountListAdapter;
import com.capgemini.psd2.aisp.domain.OBReadAccount2;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.fraudsystem.constants.FraudSystemConstants;
import com.capgemini.psd2.fraudsystem.domain.PSD2FinancialAccount;
import com.capgemini.psd2.fraudsystem.request.handler.FraudSystemRequestAttributes;
import com.capgemini.psd2.fraudsystem.request.handler.FraudSystemRequestHandler;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.utilities.DateUtilites;
import com.capgemini.psd2.utilities.GenerateUniqueIdUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.utilities.StringUtils;

@Component
@ConfigurationProperties(prefix = "app")
@Configuration
@EnableAutoConfiguration
public class FraudSystemRequestHandlerImpl implements FraudSystemRequestHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(FraudSystemRequestHandlerImpl.class);

	@Autowired
	private FraudSystemRequestAttributes fraudSystemRequestAttributes;

	@Autowired
	@Qualifier("CustomerAccountListFraudSystemAdapter")
	private CustomerAccountListAdapter customerAccountListAdapter;

	@Autowired
	private HttpServletRequest request;

	/** The fraudSystemMap request values */
	private Map<String, Map<String, String>> fraudSystemMap = new HashMap<>();

	/** The fraudSystem request values */
	private Map<String, String> fraudSystem = new HashMap<>();

	@Override
	public void captureDeviceEventInfo(Map<String, Object> params) {
		try {
			Map<String, Map<String, Object>> fraudSystemRequestMaps = new HashMap<>();
			Map<String, Object> fraudSystemEventMap = new HashMap<>();

			// EventInfo
			fraudSystemEventMap.put(FraudSystemRequestMapping.EVENT_ORG_CODE, fraudSystem.get("orgCode"));
			fraudSystemEventMap.put(FraudSystemRequestMapping.EVENT_MODEL_CODE,
					fraudSystemMap.get("modelCode")
							.get(params.get(FraudSystemConstants.FLOWTYPE_KEY).toString().toLowerCase()
									+ params.get(FraudSystemConstants.PAGETYPE_KEY)));
			
			String timestamp = request.getParameter(FraudSystemConstants.EVENT_TIME_TIMESTAMP_SAAS) != null
					? request.getParameter(FraudSystemConstants.EVENT_TIME_TIMESTAMP_SAAS)
					: request.getHeader(FraudSystemConstants.EVENT_TIME_TIMESTAMP_CONSENT);
					
			String sessionDuration = request.getParameter(FraudSystemConstants.SESSION_DURATION) != null
							? request.getParameter(FraudSystemConstants.SESSION_DURATION)
							: request.getHeader(FraudSystemConstants.TRANS_TIME_PARAMETER);		
					
			String corrId = request.getParameter(FraudSystemConstants.EVENT_ID_PARAMETER) != null
					? request.getParameter(FraudSystemConstants.EVENT_ID_PARAMETER)
					: String.valueOf(params.get(FraudSystemConstants.CORRELATION_ID));	
			//String corrId = generateRandom(11);
			fraudSystemEventMap.put(FraudSystemRequestMapping.EVENT_ID, corrId);
			
			fraudSystemEventMap.put(FraudSystemRequestMapping.EVENT_TIME, timestamp);
			
			fraudSystemEventMap.put(FraudSystemRequestMapping.SESSION_DURATION, sessionDuration);
			
			
			fraudSystemEventMap.put(FraudSystemRequestMapping.EVENT_SOURCE, fraudSystem.get("source"));
			fraudSystemEventMap.put(FraudSystemRequestMapping.EVENT_SOURCE_SYSTEM, fraudSystem.get("sourceSystem"));
			fraudSystemEventMap.put(FraudSystemRequestMapping.EVENT_SOURCE_SUBSYSTEM,
					params.get(FraudSystemConstants.FLOWTYPE_KEY).toString().toUpperCase());
			fraudSystemEventMap.put(FraudSystemRequestMapping.EVENT_TYPE,
					params.get(FraudSystemConstants.PAGETYPE_KEY));
			
			fraudSystemEventMap.put(FraudSystemRequestMapping.EVENT_ACTION, params.get(FraudSystemConstants.ACTION_KEY));
			fraudSystemEventMap.put(FraudSystemRequestMapping.EVENT_OUTCOME, params.get(FraudSystemConstants.OUTCOME_KEY));
			
			
			fraudSystemEventMap.put(FraudSystemRequestMapping.EVENT_CHANNEL, fraudSystem.get("channelCode"));
			Map<String, Object> fraudSystemDeviceMap = new HashMap<>();

			// DeviceInfo
			String xForwardedForHeader = request.getHeader(FraudSystemRequestMapping.USER_IP_ADDRESS);
			if (xForwardedForHeader == null)
				fraudSystemDeviceMap.put(FraudSystemRequestMapping.DEVICE_IP, request.getRemoteHost());
			else
				fraudSystemDeviceMap.put(FraudSystemRequestMapping.DEVICE_IP,
						new StringTokenizer(xForwardedForHeader, ",").nextToken().trim());

			fraudSystemDeviceMap.put(FraudSystemRequestMapping.DEVICE_JSC,
					(request.getParameter(FraudSystemConstants.DEVICE_JSC_PARAMETER) != null)
							? request.getParameter(FraudSystemConstants.DEVICE_JSC_PARAMETER)
							: request.getHeader(FraudSystemConstants.TRANS_JSC_PARAMETER));
			fraudSystemDeviceMap.put(FraudSystemRequestMapping.DEVICE_PAYLOAD,
					(request.getParameter(FraudSystemConstants.DEVICE_PAYLOAD_PARAMETER) != null)
							? request.getParameter(FraudSystemConstants.DEVICE_PAYLOAD_PARAMETER)
							: request.getHeader(FraudSystemConstants.TRANS_HDIM_PARAMETER));
			
			//Cookies
			fraudSystemDeviceMap.put(FraudSystemRequestMapping.DEVICE_COOKIES, request.getCookies());

			// SessionInfo
			fraudSystemDeviceMap.put(FraudSystemRequestMapping.SESSION_ID,
					getCookieValue(request, FraudSystemConstants.SESSION_ID_PARAMETER));
			
			
			/*fraudSystemDeviceMap.put(FraudSystemRequestMapping.SESSION_PAGE_CODE,
					fraudSystemMap.get("pageCode")
							.get(params.get(FraudSystemConstants.FLOWTYPE_KEY).toString().toLowerCase()
									+ params.get(FraudSystemConstants.PAGETYPE_KEY)));
			fraudSystemDeviceMap.put(FraudSystemRequestMapping.SESSION_LOGGEDIN,
					params.get("authenticationStatus").equals(FraudSystemConstants.LOGIN_SUCCESS) ? Boolean.TRUE
							: Boolean.FALSE);
			fraudSystemDeviceMap.put(FraudSystemRequestMapping.SESSION_MULTIFACTORED,
					fraudSystem.get("multiFactorAuthenticated"));*/
			
			fraudSystemDeviceMap.put(FraudSystemRequestMapping.SESSION_LOGGEDIN,
					params.get("authenticationStatus").equals(FraudSystemConstants.LOGIN_SUCCESS) ? Boolean.TRUE
							: Boolean.FALSE);

			/*if (request.getParameter(FraudSystemRequestMapping.FS_HEADERS) != null) {
				fraudSystemDeviceMap.put(FraudSystemRequestMapping.DEVICE_HEADERS,
						request.getParameter(FraudSystemRequestMapping.FS_HEADERS));
			} else if (!NullCheckUtils.isNullOrEmpty((String) (params.get(FraudSystemRequestMapping.DEVICE_HEADERS)))) {
				fraudSystemDeviceMap.put(FraudSystemRequestMapping.DEVICE_HEADERS,
						(String) (params.get(FraudSystemRequestMapping.DEVICE_HEADERS)));
			}*/
			
			if (!NullCheckUtils.isNullOrEmpty((String) (params.get(FraudSystemRequestMapping.DEVICE_HEADERS)))) {
				fraudSystemDeviceMap.put(FraudSystemRequestMapping.DEVICE_HEADERS,
						(String) (params.get(FraudSystemRequestMapping.DEVICE_HEADERS)));
			}

			fraudSystemRequestMaps.put(FraudSystemRequestMapping.FRAUDREQUEST_EVENT_MAP, fraudSystemEventMap);
			fraudSystemRequestMaps.put(FraudSystemRequestMapping.FRAUDREQUEST_DEVICE_MAP, fraudSystemDeviceMap);
			fraudSystemRequestAttributes.setFraudSystemRequest(fraudSystemRequestMaps);
		} catch (Exception e) {
			LOGGER.info("{\"Exit\":\"{}\",\"timestamp\":\"{}\",\"correlationId\": \"{}\"}",
					"com.capgemini.psd2.fraudsystem.request.handler.impl.FraudSystemRequestHandlerImpl.captureDeviceEventInfo(Map<String, Object> params)",
					DateUtilites.generateCurrentTimeStamp(), e.getMessage());

		}
	}

	private String getCookieValue(HttpServletRequest req, String cookieName) {
		return Arrays.stream(req.getCookies()).filter(c -> c.getName().equals(cookieName)).findFirst()
				.map(Cookie::getValue).orElse(null);
	}

	@Override
	public OBReadAccount2 captureContactInfo(Map<String, Object> params) {
		try {
			HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
					.getRequest();
			Map<String, String> serviceParams = new HashMap<>();
			serviceParams.put(FraudSystemConstants.FLOW_TYPE, params.get(FraudSystemConstants.FLOWTYPE_KEY).toString());
			serviceParams.put(FraudSystemConstants.USERID, params.get(FraudSystemConstants.PSUID_KEY).toString());
			serviceParams.put(FraudSystemConstants.CORRELATION_ID,
					(request.getParameter("correlationId") != null) ? request.getParameter("correlationId")
							: params.get(FraudSystemConstants.CORRELATION_ID).toString());
			serviceParams.put(FraudSystemConstants.CHANNEL_ID, (request.getParameter("channelId") != null)
					? request.getParameter("channelId") : params.get(FraudSystemConstants.CHANNEL_ID).toString());
			
			/*PSD2CustomerInfo custInfo = customerAccountListAdapter
					.retrieveCustomerInfo(params.get(FraudSystemConstants.PSUID_KEY).toString(), serviceParams);*/
			OBReadAccount2 custAccountInfo= null;
			if(params.get("pageType").equals(FraudSystemConstants.CONSENT_PAGE)){
				custAccountInfo = customerAccountListAdapter.retrieveCustomerAccountList(params.get(FraudSystemConstants.PSUID_KEY).toString(), serviceParams);
				LOGGER.debug("custAccountInfo" + custAccountInfo);
			}
			
			Map<String, Map<String, Object>> fraudSystemRequestMaps = fraudSystemRequestAttributes
					.getFraudSystemRequest();

			Map<String, Object> fraudSystemCustomerMap = new HashMap<>();
			
			
			//fraudSystemCustomerMap.put(FraudSystemRequestMapping.CONTACT_DETAILS, custInfo);
			
			
			fraudSystemCustomerMap.put(FraudSystemRequestMapping.ACCOUNT_ID,
					params.get(FraudSystemConstants.PSUID_KEY));
			
			
			/*fraudSystemCustomerMap.put(FraudSystemRequestMapping.ACCOUNT_HOLDER,
					(custInfo.getId() != null) ? custInfo.getId() : FraudSystemConstants.CONTACT_ID);*/

			fraudSystemRequestMaps.put(FraudSystemRequestMapping.FRAUDREQUEST_CUSTOMER_MAP, fraudSystemCustomerMap);
			fraudSystemRequestAttributes.setFraudSystemRequest(fraudSystemRequestMaps);
			return custAccountInfo;
		} catch (Exception e) {
			LOGGER.info("{\"Exit\":\"{}\",\"timestamp\":\"{}\",\"correlationId\": \"{}\"}",
					"com.capgemini.psd2.fraudsystem.request.handler.impl.FraudSystemRequestHandlerImpl.captureContactInfo(Map<String, Object> params)",
					DateUtilites.generateCurrentTimeStamp(), e.getMessage());
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void captureFinanacialAccountsInfo(OBReadAccount2 custAccountInfo, Map<String, Object> params) {
		try {
			List<PSD2FinancialAccount> finAccntList = new ArrayList<>();
			List<String> hashedAccNums = new ArrayList<>();

			hashedAccNums.addAll((List<String>) params.get("hashedIdentificationList"));
			
			
			custAccountInfo.getData().getAccount().forEach(account -> {
				String accountIdentification = account.getAccount().get(0).getIdentification();
				if (hashedAccNums.contains(convertStringToHash(accountIdentification))) {
					PSD2FinancialAccount finAccnt = new PSD2FinancialAccount();
					finAccnt.setId(GenerateUniqueIdUtilities.generateRandomUniqueID());
					finAccnt.setType(fraudSystem.get("finAccountsType"));
					finAccnt.setSubType(((PSD2Account) (account)).getAccountSubType().toString());
					finAccnt.setName(((PSD2Account) (account)).getNickname());
					// Hashed
					finAccnt.setHashedAccountNumber(convertStringToHash(accountIdentification));
					
					
					/*finAccnt.setAccountHolder(
							(custInfo.getId() != null) ? custInfo.getId() : FraudSystemConstants.CONTACT_ID);*/
					
					
					finAccnt.setRoutingNumber(
							((PSD2Account) (account)).getAdditionalInformation().get(PSD2Constants.ACCOUNT_NSC));
					finAccnt.setCurrency(account.getCurrency());
					finAccntList.add(finAccnt);
				}
			});
			
			
			
			Map<String, Map<String, Object>> fraudSystemRequestMaps = fraudSystemRequestAttributes
					.getFraudSystemRequest();
			Map<String, Object> fraudSystemEventMap = fraudSystemRequestMaps
					.get(FraudSystemRequestMapping.FRAUDREQUEST_EVENT_MAP);
			fraudSystemEventMap.put(FraudSystemRequestMapping.FIN_ACCNTS, finAccntList);
			fraudSystemRequestMaps.put(FraudSystemRequestMapping.FRAUDREQUEST_EVENT_MAP, fraudSystemEventMap);
			fraudSystemRequestAttributes.setFraudSystemRequest(fraudSystemRequestMaps);
		} catch (Exception e) {
			LOGGER.info("{\"Exit\":\"{}\",\"timestamp\":\"{}\",\"correlationId\": \"{}\"}",
					"com.capgemini.psd2.fraudsystem.request.handler.impl.FraudSystemRequestHandlerImpl.captureFinanacialAccountsInfo(PSD2CustomerInfo custInfo, Map<String, Object> params)",
					DateUtilites.generateCurrentTimeStamp(), e.getMessage());
		}
	}

	@Override
	public void captureTransferInfo(Map<String, Object> params) {
		try {
			HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
					.getRequest();
			request.getHeaderNames();
			Map<String, Map<String, Object>> fraudSystemRequestMaps = fraudSystemRequestAttributes
					.getFraudSystemRequest();
			Map<String, Object> fraudSystemEventMap = fraudSystemRequestMaps
					.get(FraudSystemRequestMapping.FRAUDREQUEST_EVENT_MAP);
			fraudSystemEventMap.put(FraudSystemRequestMapping.TRANSFER_ID, params.get(FraudSystemConstants.PAYMENT_ID));
			OffsetDateTime transDateTime = OffsetDateTime.parse(
					params.get(FraudSystemRequestMapping.TRANSFER_TIME).toString(), DateTimeFormatter.ISO_DATE_TIME);
			fraudSystemEventMap.put(FraudSystemRequestMapping.TRANSFER_TIME,
					transDateTime.plusNanos(1000000).toString().replace("1Z", "0Z"));
			fraudSystemEventMap.put(FraudSystemRequestMapping.TRANSFER_CURRENCY,
					params.get(FraudSystemRequestMapping.TRANSFER_CURRENCY).toString());
			fraudSystemEventMap.put(FraudSystemRequestMapping.TRANSFER_AMNT,
					params.get(FraudSystemRequestMapping.TRANSFER_AMNT).toString());
			fraudSystemEventMap.put(FraudSystemRequestMapping.TRANSFER_MEMO,
					params.get(FraudSystemRequestMapping.TRANSFER_MEMO));
			
			
			fraudSystemEventMap.put(FraudSystemRequestMapping.TRANSFER_ACCOUNT_NUMBER, params.get(FraudSystemRequestMapping.TRANSFER_ACCOUNT_NUMBER));
			
			fraudSystemRequestMaps.put(FraudSystemRequestMapping.FRAUDREQUEST_EVENT_MAP, fraudSystemEventMap);
			fraudSystemRequestAttributes.setFraudSystemRequest(fraudSystemRequestMaps);
		} catch (Exception e) {
			LOGGER.info("{\"Exit\":\"{}\",\"timestamp\":\"{}\",\"correlationId\": \"{}\"}",
					"com.capgemini.psd2.fraudsystem.request.handler.impl.FraudSystemRequestHandlerImpl.captureTransferInfo(Map<String, Object> params)\r\n"
							+ "",
					DateUtilites.generateCurrentTimeStamp(), e.getMessage());
		}
	}
	
	private String generateRandom(int prefix) {
        Random rand = new Random();

        long x = (long)(rand.nextDouble()*100000000000000L);

        String s = String.valueOf(prefix) + String.format("%014d", x);
        return s;
    }

	private String convertStringToHash(String input) {
		return StringUtils.generateHashedValue(input);
	}

	public Map<String, Map<String, String>> getFraudSystemMap() {
		return fraudSystemMap;
	}

	public void setFraudSystemMap(Map<String, Map<String, String>> fraudSystemMap) {
		this.fraudSystemMap = fraudSystemMap;
	}

	public Map<String, String> getFraudSystem() {
		return fraudSystem;
	}

	public void setFraudSystem(Map<String, String> fraudSystem) {
		this.fraudSystem = fraudSystem;
	}
}
