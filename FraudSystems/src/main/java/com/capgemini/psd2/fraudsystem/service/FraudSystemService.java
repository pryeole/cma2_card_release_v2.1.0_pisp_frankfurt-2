package com.capgemini.psd2.fraudsystem.service;

import java.util.Map;

@FunctionalInterface
public interface FraudSystemService {
	public <T> T callFraudSystemService(Map<String, Object> fraudSystemParams);
}
