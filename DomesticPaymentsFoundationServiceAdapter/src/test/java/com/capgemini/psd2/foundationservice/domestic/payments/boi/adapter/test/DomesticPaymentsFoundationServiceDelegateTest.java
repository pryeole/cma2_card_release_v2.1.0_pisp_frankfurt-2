package com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.test;

import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.delegate.DomesticPaymentsFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.PaymentInstruction1;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.PaymentInstructionProposalComposite;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.transformer.DomesticPaymentsFoundationServiceTransformer;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.CustomDPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.rest.client.model.RequestInfo;

@RunWith(SpringRunner.class)
public class DomesticPaymentsFoundationServiceDelegateTest {
	@InjectMocks
	 DomesticPaymentsFoundationServiceDelegate delegate;
	
	@Mock
	private DomesticPaymentsFoundationServiceTransformer transformer;
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**//**
	 * Context loads.
	 *//*
*/	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testcreatePaymentRequestHeaders(){
		ReflectionTestUtils.setField(delegate,"transactioReqHeader","transactioReqHeader");
		ReflectionTestUtils.setField(delegate,"correlationMuleReqHeader","correlation");
		ReflectionTestUtils.setField(delegate,"sourceUserReqHeader","sorceuser");
		ReflectionTestUtils.setField(delegate,"sourcesystem","source");
		ReflectionTestUtils.setField(delegate,"apiChannelCode","channel");
		ReflectionTestUtils.setField(delegate,"partySourceId","partySourceId");
		ReflectionTestUtils.setField(delegate,"domesticPaymentSubmissionSetupVersion","version");
		ReflectionTestUtils.setField(delegate,"systemApiVersion","version");
		ReflectionTestUtils.setField(delegate,"channelBrand","brand");
		
		Map<String, String> params=new HashMap<String, String>();
		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		customStageIdentifiers.setPaymentSetupVersion("abc");
		params.put("systemApiVersion", "3.0");
		params.put(PSD2Constants.USER_IN_REQ_HEADER,"dcfs");
		params.put(PSD2Constants.CORRELATION_REQ_HEADER,"dcfs");
		params.put(PSD2Constants.CHANNEL_IN_REQ_HEADER,"dcfs");
		params.put(PSD2Constants.PARTY_IDENTIFIER,"dcfs");
		params.put(PSD2Constants.TENANT_ID,"BOIUK");
		HttpHeaders header=delegate.createPaymentRequestHeaders(customStageIdentifiers,params);
		assertNotNull(header);
			
	}
	
	@Test
	public void testcreatePaymentRequestHeadersPost(){
		ReflectionTestUtils.setField(delegate,"transactioReqHeader","transactioReqHeader");
		ReflectionTestUtils.setField(delegate,"correlationMuleReqHeader","correlation");
		ReflectionTestUtils.setField(delegate,"sourceUserReqHeader","sorceuser");
		ReflectionTestUtils.setField(delegate,"sourcesystem","source");
		ReflectionTestUtils.setField(delegate,"apiChannelCode","channel");
		ReflectionTestUtils.setField(delegate,"partySourceId","partySourceId");
		ReflectionTestUtils.setField(delegate,"domesticPaymentSubmissionSetupVersion","version");
		ReflectionTestUtils.setField(delegate,"systemApiVersion","version");
		RequestInfo requestInfo = new RequestInfo();
		Map<String, String> params=new HashMap<String, String>();
		params.put("transactioReqHeader", "boi");
		HttpHeaders header=delegate.createPaymentRequestHeadersPost( params);
		assertNotNull(header);
			
	}	
	
	@Test
	public void testtransformDomesticSubmissionResponseFromAPIToFDForInsert()
	{
		
		CustomDPaymentsPOSTRequest domesticPaymentsRequest =new CustomDPaymentsPOSTRequest();
		 Map<String, String> params = new HashMap<String, String>();
		// transformer.transformDomesticConsentResponseFromFDToAPIForInsert(domesticPaymentsRequest, params);
		
		
	delegate.transformDomesticSubmissionResponseFromAPIToFDForInsert(domesticPaymentsRequest, params);
		
	}
	
	@Test
	public void testtransformDomesticConsentResponseFromFDToAPIForInsert()
	{
		
		PaymentInstructionProposalComposite domesticPaymentsRequest =new PaymentInstructionProposalComposite();
		 Map<String, String> params = new HashMap<String, String>();
		 PaymentInstruction1 paymentInstruction2 = new PaymentInstruction1();
		
		domesticPaymentsRequest.setPaymentInstruction(paymentInstruction2);
	 delegate.transformDomesticConsentResponseFromFDToAPIForInsert(domesticPaymentsRequest);
		
	}
	/*@Test
	public void testgetPaymentFoundationServiceURL()
	{
		String version="1.0";
		String DomesticPaymentId="1007";
		String  domesticPaymentBaseURL ="domestic/payment-instructions";
		
		delegate.getPaymentFoundationServiceURL( domesticPaymentBaseURL, DomesticPaymentId,param);
		assertNotNull(delegate);
		
		
		
	}*/
	
	@Test
	public void testpostPaymentFoundationServiceURL() {
		
		
		
		
		String domesticPaymentSubmissionBaseURL ="domestic/payment-instructions";
		
		
		
		delegate.postPaymentFoundationServiceURL(domesticPaymentSubmissionBaseURL);
		
		
		
	}

}
