package com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.test;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;

import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.Address;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.Amount;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.AuthorisationType;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.AuthorisingPartyAccount;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.BrandCode3;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.Channel;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.ChannelCode;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.ChargeBearer;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.ContactInformation;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.Country;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.Currency;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.FinancialEventAmount;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.FraudSystemResponse;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.Organisation;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.Organisation2;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.Party2;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.PartyAddress3;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.PartyBasicInformation;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.PaymentInstruction;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.PaymentInstruction1;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.PaymentInstructionBasic;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.PaymentInstructionCharge;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.PaymentInstructionPostalAddress;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.PaymentInstructionProposal;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.PaymentInstructionProposalComposite;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.PaymentInstructionStatusCode;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.PaymentInstrumentRiskFactor;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.PaymentType;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.Person;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.PersonBasicInformation;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.ProposalStatus;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.ProposingPartyAccount;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.transformer.DomesticPaymentsFoundationServiceTransformer;
import com.capgemini.psd2.pisp.domain.CustomDPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBAddressTypeCode;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBDomestic1;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBExternalPaymentContext1Code;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomestic1;
import com.capgemini.psd2.pisp.domain.OBWriteDomestic1;
import com.capgemini.psd2.pisp.domain.PaymentDomesticSubmitPOST201Response;

@RunWith(SpringRunner.class)
public class DomesticPaymentsFoundationServiceTransformerTest {
	

	@InjectMocks
	DomesticPaymentsFoundationServiceTransformer transformer;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	
	  //Context loads.
	
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testtransformDomesticPaymentResponse() {
		
		Map<String, String> params = new HashMap<String, String>();
		PaymentInstructionProposalComposite inputBalanceObj = new PaymentInstructionProposalComposite();
		PaymentInstructionProposal paymentInstructionProposal = new PaymentInstructionProposal();
		
		PaymentInstruction1 paymentInstruction2 = new PaymentInstruction1();
		
		paymentInstructionProposal.setPaymentInstructionProposalId("123");	
		inputBalanceObj.setPaymentInstruction(paymentInstruction2);
		paymentInstruction2.setPaymentInstructionNumber("1234");
		
		List<PaymentInstructionCharge> charges = new ArrayList<PaymentInstructionCharge>();
		PaymentInstructionCharge e = new PaymentInstructionCharge();
		e.setChargeBearer(ChargeBearer.BorneByCreditor);
		e.setType("credit");
		Amount amount=new Amount();
		amount.setTransactionCurrency(23.00);
		e.setAmount(amount);
		Currency currency = new Currency();
		currency.setIsoAlphaCode("EUR");
		currency.setCurrencyName("123");
		e.setCurrency(currency);
		charges.add(e);
		paymentInstructionProposal.setCharges(charges);
		
		paymentInstructionProposal.setInstructionReference("asdf");
		paymentInstructionProposal.setInstructionEndToEndReference("ghbh");
		paymentInstructionProposal.setInstructionLocalInstrument("se45r");
		Currency transactionCurrency = new Currency();
		transactionCurrency.setIsoAlphaCode("EUR");
		paymentInstructionProposal.setTransactionCurrency(transactionCurrency);
		
		AuthorisingPartyAccount authorisingPartyAccount = new AuthorisingPartyAccount();
		authorisingPartyAccount.setSchemeName("sfd");
		authorisingPartyAccount.setAccountName("boi");
		authorisingPartyAccount.setAccountNumber("1334");
		authorisingPartyAccount.setSecondaryIdentification("dsd");
		authorisingPartyAccount.setAccountIdentification("asddf");
		paymentInstructionProposal.setAuthorisingPartyAccount(authorisingPartyAccount);
		
	
		ProposingPartyAccount proposingPartyAccount = new ProposingPartyAccount();
		proposingPartyAccount.setSchemeName("asdas");
		proposingPartyAccount.setAccountIdentification("rthyrh");
		proposingPartyAccount.setAccountName("vvxv");
		proposingPartyAccount.setSecondaryIdentification("dgghdgh");
		paymentInstructionProposal.setProposingPartyAccount(proposingPartyAccount);
		
		paymentInstructionProposal.setAuthorisingPartyReference("capg");
		paymentInstructionProposal.setAuthorisingPartyUnstructuredReference("fdfg");
		FinancialEventAmount financialEventAmount = new FinancialEventAmount();
		financialEventAmount.setTransactionCurrency(56.00);
		paymentInstructionProposal.setFinancialEventAmount(financialEventAmount);
		PaymentInstructionPostalAddress  proposingPartyPostalAddress = new PaymentInstructionPostalAddress();
		proposingPartyPostalAddress.setDepartment("savings");
		proposingPartyPostalAddress.setTownName("pune");
		proposingPartyPostalAddress.setAddressType("work");
		proposingPartyPostalAddress.setSubDepartment("accounts");
		proposingPartyPostalAddress.setGeoCodeBuildingName("kalyani");
		proposingPartyPostalAddress.setGeoCodeBuildingNumber("2-5-78");
		proposingPartyPostalAddress.setCountrySubDivision("Maharashtra");
		proposingPartyPostalAddress.setPostCodeNumber("500072");
		
		Country addressCountry = new Country();
		addressCountry.setIsoCountryAlphaTwoCode("IND");
		proposingPartyPostalAddress.setAddressCountry(addressCountry);
		List<String> addressLine = new ArrayList<String>();
		addressLine.add("devi indrayani");
		proposingPartyPostalAddress.setAddressLine(addressLine);
		paymentInstructionProposal.setProposingPartyPostalAddress(proposingPartyPostalAddress);
		inputBalanceObj.setPaymentInstructionProposal(paymentInstructionProposal);
		
		Date d= new Date();
		//paymentInstruction.setPaymentInstruction(paymentinstructionbasic);
		paymentInstruction2.setInstructionIssueDate("20-10-1993");
		paymentInstruction2.setInstructionStatusUpdateDateTime("20-10-1993");
		inputBalanceObj.setPaymentInstruction(paymentInstruction2);
		
		paymentInstruction2.setPaymentInstructionStatusCode(PaymentInstructionStatusCode.AcceptedSettlementCompleted);
		
		inputBalanceObj.setPaymentInstructionProposal(paymentInstructionProposal);
		
	
		PaymentDomesticSubmitPOST201Response response = transformer.transformDomesticPaymentResponse(inputBalanceObj);
		assertNotNull(response);
	}
	
	@Test
	public void testtransformDomesticSubmissionResponseFromAPIToFDForInsert()
	
	
	{
		CustomDPaymentsPOSTRequest domesticPaymentsRequest = new CustomDPaymentsPOSTRequest();
	
		
		Map<String, String> params = new HashMap<String, String>();
		
		
		 OBWriteDataDomestic1  obwriteDataDomestic1= new  OBWriteDataDomestic1 ();
		 OBDomestic1  obDomestc1= new  OBDomestic1();
		 OBDomestic1InstructedAmount obDomestic1InstructedAmount =new OBDomestic1InstructedAmount ();
		 OBCashAccountDebtor3  obCashAccountDebtor3= new  OBCashAccountDebtor3();
		 OBCashAccountCreditor2  obCashAccountCreditor2 = new  OBCashAccountCreditor2();
		 OBPostalAddress6 obPostalAddress6 = new OBPostalAddress6();
		 OBRemittanceInformation1  obRemittanceInformation1= new OBRemittanceInformation1 (); 
		 
		 OBRisk1 obRisk1 = new OBRisk1();
		 Channel channel = new Channel();
		 channel.setChannelName("Boi");
		channel.setBrandCode(BrandCode3.NIGB);
		channel.setChannelCode(ChannelCode.B365);
		obwriteDataDomestic1.setConsentId("2");
		obDomestc1.setInstructionIdentification("eet");
		obDomestc1.setEndToEndIdentification("fhj");
		obDomestc1.setLocalInstrument("jkklj");
		obDomestic1InstructedAmount.setAmount("11");
		obDomestic1InstructedAmount.setCurrency("123");
		
		obCashAccountDebtor3.setSchemeName("riri");
		obCashAccountDebtor3.setName("ruru");
		obCashAccountDebtor3.setIdentification("1233");
		obCashAccountDebtor3.setSecondaryIdentification("jgh");
		
		obCashAccountCreditor2.setIdentification("jfj");
		obCashAccountCreditor2.setName("dfg");
		obCashAccountCreditor2.setSchemeName("nju");
		obCashAccountCreditor2.setSecondaryIdentification("wqe");
		 
		List<String> addressLine =new ArrayList<String>();
		addressLine.add("jgjghjhj");
		addressLine.add("tfff");
		obPostalAddress6.setBuildingNumber("123");
		obPostalAddress6.setAddressLine(addressLine);
		obPostalAddress6.setCountry("usa");
		obPostalAddress6.setCountrySubDivision("fjjg");
		obPostalAddress6.setDepartment("chem");
		
		obPostalAddress6.setPostCode("wrtt");
		obPostalAddress6.setStreetName("qwer");
		obPostalAddress6.setSubDepartment("dkkf");
		obPostalAddress6.setTownName("iriti");
		obPostalAddress6.setAddressType(OBAddressTypeCode.BUSINESS);
		
		 obRemittanceInformation1.setReference("fkfk");
		 obRemittanceInformation1.setUnstructured("addf");
		 
		 obRisk1.setMerchantCategoryCode("dkd");
		 obRisk1.setMerchantCustomerIdentification("dkdk");
		 OBRisk1DeliveryAddress deliveryAddress=new OBRisk1DeliveryAddress();
		 deliveryAddress.setAddressLine(addressLine);
		 deliveryAddress.setBuildingNumber("123");
		 deliveryAddress.setCountry("india");
		 deliveryAddress.setCountrySubDivision("234");
		 deliveryAddress.setPostCode("kdkdk");
		 deliveryAddress.setStreetName("kdk");
		 deliveryAddress.setTownName("pune");
		 obRisk1.setDeliveryAddress(deliveryAddress);
		 obRisk1.setPaymentContextCode(OBExternalPaymentContext1Code.BILLPAYMENT);
		
		obDomestc1.setRemittanceInformation(obRemittanceInformation1);
		
		obDomestc1.setDebtorAccount(obCashAccountDebtor3);
		
		obDomestc1.setCreditorAccount(obCashAccountCreditor2);
		obDomestc1.setCreditorPostalAddress(obPostalAddress6);
		
		obDomestc1.setInstructedAmount(obDomestic1InstructedAmount);
		
		
		obwriteDataDomestic1.setInitiation(obDomestc1);
		
		domesticPaymentsRequest.setRisk(obRisk1);
		domesticPaymentsRequest.setFraudSystemResponse("sksk");
		domesticPaymentsRequest.setData(obwriteDataDomestic1);
		domesticPaymentsRequest.setCreatedOn("dkd");
		 PaymentInstructionProposalComposite response = transformer.transformDomesticSubmissionResponseFromAPIToFDForInsert(domesticPaymentsRequest, params);
		 assertNotNull(response);
		
	}
	
	@Test
	public void testtransformDomesticSubmissionResponseFromAPIToFDForInsert1()
	
	
	{
		CustomDPaymentsPOSTRequest domesticPaymentsRequest = new CustomDPaymentsPOSTRequest();
	
		
		Map<String, String> params = new HashMap<String, String>();
		
		params.put("X-BOI-CHANNEL", "BOL");
		 OBWriteDataDomestic1  obwriteDataDomestic1= new  OBWriteDataDomestic1 ();
		 OBDomestic1  obDomestc1= new  OBDomestic1();
		 OBDomestic1InstructedAmount obDomestic1InstructedAmount =new OBDomestic1InstructedAmount ();
		 OBCashAccountDebtor3  obCashAccountDebtor3= new  OBCashAccountDebtor3();
		 OBCashAccountCreditor2  obCashAccountCreditor2 = new  OBCashAccountCreditor2();
		 OBPostalAddress6 obPostalAddress6 = new OBPostalAddress6();
		 OBRemittanceInformation1  obRemittanceInformation1= new OBRemittanceInformation1 (); 
		 
		 OBRisk1 obRisk1 = new OBRisk1();
		 Channel channel = new Channel();
		 channel.setChannelName("Boi");
		channel.setBrandCode(BrandCode3.NIGB);
		channel.setChannelCode(ChannelCode.B365);
		obwriteDataDomestic1.setConsentId("2");
		obDomestc1.setInstructionIdentification("eet");
		obDomestc1.setEndToEndIdentification("fhj");
		obDomestc1.setLocalInstrument("jkklj");
		obDomestic1InstructedAmount.setAmount("11");
		obDomestic1InstructedAmount.setCurrency("123");
		
		obCashAccountDebtor3.setSchemeName("riri");
		obCashAccountDebtor3.setName("ruru");
		obCashAccountDebtor3.setIdentification("1233");
		obCashAccountDebtor3.setSecondaryIdentification("jgh");
		
		obCashAccountCreditor2.setIdentification("jfj");
		obCashAccountCreditor2.setName("dfg");
		obCashAccountCreditor2.setSchemeName("nju");
		obCashAccountCreditor2.setSecondaryIdentification("wqe");
		 
		List<String> addressLine =new ArrayList<String>();
		addressLine.add("jgjghjhj");
		addressLine.add("tfff");
		obPostalAddress6.setBuildingNumber("123");
		obPostalAddress6.setAddressLine(addressLine);
		obPostalAddress6.setCountry("usa");
		obPostalAddress6.setCountrySubDivision("fjjg");
		obPostalAddress6.setDepartment("chem");
		
		obPostalAddress6.setPostCode("wrtt");
		obPostalAddress6.setStreetName("qwer");
		obPostalAddress6.setSubDepartment("dkkf");
		obPostalAddress6.setTownName("iriti");
		obPostalAddress6.setAddressType(OBAddressTypeCode.BUSINESS);
		
		 obRemittanceInformation1.setReference("fkfk");
		 obRemittanceInformation1.setUnstructured("addf");
		 
		 obRisk1.setMerchantCategoryCode("dkd");
		 obRisk1.setMerchantCustomerIdentification("dkdk");
		 OBRisk1DeliveryAddress deliveryAddress=new OBRisk1DeliveryAddress();
		 deliveryAddress.setAddressLine(addressLine);
		 deliveryAddress.setBuildingNumber("123");
		 deliveryAddress.setCountry("india");
		 deliveryAddress.setCountrySubDivision("234");
		 deliveryAddress.setPostCode("kdkdk");
		 deliveryAddress.setStreetName("kdk");
		 deliveryAddress.setTownName("pune");
		 obRisk1.setDeliveryAddress(deliveryAddress);
		 obRisk1.setPaymentContextCode(OBExternalPaymentContext1Code.BILLPAYMENT);
		
		obDomestc1.setRemittanceInformation(obRemittanceInformation1);
		
		obDomestc1.setDebtorAccount(obCashAccountDebtor3);
		
		obDomestc1.setCreditorAccount(obCashAccountCreditor2);
		obDomestc1.setCreditorPostalAddress(obPostalAddress6);
		
		obDomestc1.setInstructedAmount(obDomestic1InstructedAmount);
		
		
		obwriteDataDomestic1.setInitiation(obDomestc1);
		
		domesticPaymentsRequest.setRisk(obRisk1);
		domesticPaymentsRequest.setFraudSystemResponse("sksk");
		domesticPaymentsRequest.setData(obwriteDataDomestic1);
		domesticPaymentsRequest.setCreatedOn("dkd");
		
		 PaymentInstructionProposalComposite response = transformer.transformDomesticSubmissionResponseFromAPIToFDForInsert(domesticPaymentsRequest, params);
		 assertNotNull(response);
		
	}
	
	//@SuppressWarnings("deprecation")
	@Test
	
	public void testtransformDomesticConsentResponseFromFDToAPIForInsert2() {
		
		
		Map<String, String> params = new HashMap<String, String>();
		PaymentInstructionProposalComposite paymentInstructionProposal =new PaymentInstructionProposalComposite();
		
		 PaymentInstruction1  paymentInstruction2 = new  PaymentInstruction1();
		 PaymentInstructionProposal paymentInstructionProposal1 =new PaymentInstructionProposal();
		 
			
		Party2 instructingParty =new Party2();
		 instructingParty.setMerchantCategoryCode("wiwi");
		 instructingParty.setMerchantDescription("wee");
		 instructingParty.setMerchantName("nuty");
		 instructingParty.setTaxIdNumber("123");
		 
		 
		 Organisation2 organisation = new Organisation2();
		 organisation.setContactName("Niean");
		 Organisation organisationInformation =new Organisation();
		 organisationInformation.setTradingName("dhdhd");
		 
		 organisation.setOrganisationInformation(organisationInformation);
		 
		 instructingParty.setOrganisation(organisation);
		 
		 ContactInformation contactInformation = new ContactInformation();
		 contactInformation.setEmailAddress("djdj@gmail.com");
		 contactInformation.setFaxNumber("1234");
		 
		 contactInformation.setHomePhoneNumber("123467");
		 contactInformation.setMobilePhoneNumber("7543");
		 
		
		 
		 contactInformation.setWorkPhoneNumber("12354");
		
		 
		 instructingParty.setContactInformation(contactInformation);
		 PartyAddress3 address =new PartyAddress3();
		 
		 List<PartyAddress3> partyAddress = new ArrayList<PartyAddress3>();
		 partyAddress.add(address);
		 instructingParty.setPartyAddress(partyAddress);
		 
		 PartyBasicInformation partyInformation = new PartyBasicInformation();
		
		 Organisation organisation2 =new Organisation();
		 partyInformation.setOrganisation(organisation2);
		 partyInformation.setPartyActiveIndicator( true);
		 partyInformation.setPartyName("sjsjs");
		 partyInformation.setPartySourceIdNumber("2929");
		 PersonBasicInformation person =new PersonBasicInformation();
		 partyInformation.setPerson(person);
		
		
		 
		 instructingParty.setPartyInformation(partyInformation);
		 Person person4= new Person();
		 person4.setEmployersName("ssks");
		PersonBasicInformation personInformation= new PersonBasicInformation();
		 Date birthDate= new Date();
		 birthDate.setYear(122);
		 birthDate.setTime(0);
		 personInformation.setFirstName("djdjd");
		 personInformation.setMothersMaidenName("djdj");
		 personInformation.setSurname("cncn");
		 personInformation.setTitleCode("dkdk");
		 person4.setPersonInformation(personInformation);
		 instructingParty.setPerson(person4);
		Date instructionStatusUpdateDateTime =new Date();
		 
		 instructionStatusUpdateDateTime.setTime(12);
		 instructionStatusUpdateDateTime.setMinutes(12);
		 instructionStatusUpdateDateTime.setYear(2018);
		
		 
		Date instructionIssueDate =new Date();
		
		 instructionIssueDate.setTime(12);
		 instructionIssueDate.setMinutes(15);
		 instructionIssueDate.setSeconds(12);
		 Date authorisationDatetime =new Date();
		 
		 paymentInstruction2.setPaymentInstructionNumber("asdf");
		 paymentInstruction2.setInstructionIssueDate("20-10-1993");
		 paymentInstruction2.setInstructingParty(instructingParty);
		 paymentInstruction2.setPaymentInstructionStatusCode(PaymentInstructionStatusCode.AcceptedSettlementCompleted);
		 paymentInstruction2.setInstructionStatusUpdateDateTime("20-10-1993");
		 
		 
		 
		 
		 authorisationDatetime.setMonth(2);
		 authorisationDatetime.setHours(5);
		 paymentInstructionProposal1.setAuthorisationDatetime("20-10-1993");
		 
		 
		 paymentInstructionProposal1.setAuthorisationType(AuthorisationType.Any);
		 
		 Party2 authorisingParty= new Party2();
		 
		 authorisingParty.setContactInformation(contactInformation);
		 authorisingParty.setMerchantCategoryCode("23");
		 authorisingParty.setMerchantDescription("kdkdkdkdkddkkk");
		 authorisingParty.setMerchantName("sjsj");
		 authorisingParty.setOrganisation(organisation);
		 authorisingParty.setPartyAddress(partyAddress);
		 authorisingParty.setPartyInformation(partyInformation);
		 authorisingParty.setPerson(person4);
		 authorisingParty.setTaxIdNumber("dkdkdk");
		 
		 paymentInstructionProposal1.setAuthorisingParty(authorisingParty);
		 OBWriteDataDomestic1   obWriteDataDomestic1 =new  OBWriteDataDomestic1  ();
		 OBWriteDomestic1 obWritedomestic1 = new OBWriteDomestic1();
		 OBRemittanceInformation1 remittanceInformation =new OBRemittanceInformation1();
		 OBDomestic1 obDomestic1= new OBDomestic1();
		 obDomestic1.setRemittanceInformation(remittanceInformation);
		 obWriteDataDomestic1 .setInitiation(obDomestic1);
		 CustomDPaymentsPOSTRequest customDPaymentsPOSTRequest = new CustomDPaymentsPOSTRequest();
		
		 remittanceInformation.setReference("weiei");
		 remittanceInformation.setUnstructured("dkdk");
		 
		 obWriteDataDomestic1.setInitiation(obDomestic1);
		 obWritedomestic1.setData(obWriteDataDomestic1);
		 customDPaymentsPOSTRequest.setData(obWriteDataDomestic1);
		 AuthorisingPartyAccount authorisingPartyAccount= new AuthorisingPartyAccount();
		 authorisingPartyAccount.setAccountName("sksk");
		 authorisingPartyAccount.setAccountNumber("2229");
		 authorisingPartyAccount.setSchemeName("jsjsj");
		 authorisingPartyAccount.setSecondaryIdentification(null);
		 
		 authorisingPartyAccount.setSecondaryIdentification("sksks");
		 paymentInstructionProposal1.setAuthorisingPartyAccount(authorisingPartyAccount);
		 paymentInstructionProposal1.setAuthorisingPartyReference("wiwiiwi");
		 paymentInstructionProposal1.setAuthorisingPartyUnstructuredReference("fkfkfkf");
		 
		 PaymentInstructionCharge charge =new PaymentInstructionCharge();
		 Amount  amount= new Amount();
		 amount.setGroupReportingCurrency(44.0);
		 amount.setTransactionCurrency(55.0);
		 charge.setAmount(amount);
		 charge.setType("djdj");
		 charge.setChargeBearer(ChargeBearer.BorneByDebtor);
		
		 
		 Currency currency= new Currency();
		 currency.setCurrencyName("eiei");
		 currency.setIsoAlphaCode("229");
		  charge.setCurrency(currency);
		 
		 List<PaymentInstructionCharge> charges = new ArrayList<PaymentInstructionCharge>();
		 charges.add(charge);
		 
		 paymentInstructionProposal1.setCharges(charges);
		 
		 FinancialEventAmount financialEventAmount= new FinancialEventAmount();
		 financialEventAmount.setGroupReportingCurrency(800.0);
		 financialEventAmount.setLocalReportingCurrency(3.0);
		 financialEventAmount.setTransactionCurrency(34.0);
		 
		 paymentInstructionProposal1.setFinancialEventAmount(financialEventAmount);
		 paymentInstructionProposal1.setFraudSystemResponse(FraudSystemResponse.Approve);
		 paymentInstructionProposal1.setInstructionEndToEndReference("292");
		 paymentInstructionProposal1.setInstructionLocalInstrument("wwi");
		 paymentInstructionProposal1.setInstructionReference("slssl");
		 paymentInstructionProposal1.setPaymentInstructionProposalId("ajaj");
		PaymentInstrumentRiskFactor paymentInstructionRiskFactorReference= new PaymentInstrumentRiskFactor();
		Address counterPartyAddress =new Address();
		Country addressCountry =new Country();
		 addressCountry.setCountryName("eiei");
		 addressCountry.setIsoCountryAlphaTwoCode("202");
		 counterPartyAddress.setAddressCountry(addressCountry);
		 counterPartyAddress.setFifthAddressLine("qiqiqi");
		 counterPartyAddress.setFirstAddressLine("ieiei");
		 counterPartyAddress.setFourthAddressLine("sjsj");
		 counterPartyAddress.setGeoCodeBuildingName("eueu");
		 counterPartyAddress.setGeoCodeBuildingNumber("12344");
		 counterPartyAddress.setSecondAddressLine("wiwi");
		 counterPartyAddress.setThirdAddressLine("1919");
		 paymentInstructionRiskFactorReference.setCounterPartyAddress(counterPartyAddress);
		 paymentInstructionProposal1.setPaymentInstructionRiskFactorReference(paymentInstructionRiskFactorReference);
		 paymentInstructionProposal1.setPaymentType(PaymentType.International);
		 Date proposalCreationDatetime = new Date();
		 proposalCreationDatetime.setHours(12);
		 
		 
		 paymentInstructionProposal1.setProposalCreationDatetime("20-10-1993");
		 paymentInstructionProposal1.setProposalStatus(ProposalStatus.Authorised);
		 paymentInstructionProposal1.setProposalStatusUpdateDatetime("20-10-1993");
		 Party2 proposingParty = new Party2();
		 
		 proposingParty.setMerchantDescription("sjsjs");
		 proposingParty.setContactInformation(contactInformation);
		 proposingParty.setMerchantCategoryCode("slsl");
		 proposingParty.setMerchantName("sksksk");
		 proposingParty.setOrganisation(organisation);
		 proposingParty.setPartyAddress(partyAddress);
		 proposingParty.setPartyInformation(partyInformation);
		 proposingParty.setPerson(person4);
		 proposingParty.setTaxIdNumber("1244");
		 paymentInstructionProposal1.setProposingParty(proposingParty);
		 ProposingPartyAccount proposingPartyAccount= new ProposingPartyAccount();
		 proposingPartyAccount.setAccountName("sksk");
		 proposingPartyAccount.setAccountNumber("kdkgk");
		 proposingPartyAccount.setSchemeName("wowo");
		 proposingPartyAccount.setSecondaryIdentification("dkdk");
		 
		 paymentInstructionProposal1.setProposingPartyAccount(proposingPartyAccount);
		PaymentInstructionPostalAddress proposingPartyPostalAddress =new PaymentInstructionPostalAddress();
		 proposingPartyPostalAddress.setAddressCountry(addressCountry);
		 List<String> addressLine = new ArrayList<String>();
		 addressLine.add("djdjd");
		 proposingPartyPostalAddress.setAddressLine(addressLine );
		 proposingPartyPostalAddress.setAddressType("wiwi");
		 proposingPartyPostalAddress.setCountrySubDivision("sksk");
		 proposingPartyPostalAddress.setDepartment("dkdk");
		 proposingPartyPostalAddress.setGeoCodeBuildingName("124");
		 proposingPartyPostalAddress.setGeoCodeBuildingNumber("dldl");
		 proposingPartyPostalAddress.setPostCodeNumber("dldl");
		 proposingPartyPostalAddress.setSubDepartment("sksk");
		 proposingPartyPostalAddress.setTownName("alal");
		 
		 
		 paymentInstructionProposal1.setProposingPartyPostalAddress(proposingPartyPostalAddress);
		 
		Currency transactionCurrency =new Currency();
		 transactionCurrency.setCurrencyName("slsl");
		 transactionCurrency.setIsoAlphaCode("124");
		 
		 paymentInstructionProposal1.setTransactionCurrency(transactionCurrency);
		 
		 
		 paymentInstruction2.setPaymentInstructionStatusCode(PaymentInstructionStatusCode.AcceptedSettlementInProcess);
		
		
		 paymentInstructionProposal.setPaymentInstruction(paymentInstruction2);
		
		 paymentInstructionProposal .setPaymentInstructionProposal(paymentInstructionProposal1);
		
		 
		
		
		 PaymentDomesticSubmitPOST201Response resp = transformer.transformDomesticConsentResponseFromFDToAPIForInsert(paymentInstructionProposal);
		 assertNotNull(resp);
	
		
	}
	
	

	
    //@SuppressWarnings("deprecation")
	@Test
	
	public void testtransformDomesticConsentResponseFromFDToAPIForInsert() {
		
		
		Map<String, String> params = new HashMap<String, String>();
		PaymentInstructionProposalComposite paymentInstructionProposal =new PaymentInstructionProposalComposite();
		
		
		 PaymentInstruction  paymentInstruction3 = new  PaymentInstruction();
		 PaymentInstruction1  paymentInstruction2 = new  PaymentInstruction1();
		 
		 PaymentInstructionProposal paymentInstructionProposal1 =new PaymentInstructionProposal();
		 
			
		 Party2 instructingParty =new Party2();
		 instructingParty.setMerchantCategoryCode("wiwi");
		 instructingParty.setMerchantDescription("wee");
		 instructingParty.setMerchantName("nuty");
		 instructingParty.setTaxIdNumber("123");
		 
		 
		 Organisation2 organisation = new Organisation2();
		 organisation.setContactName("Niean");
		 Organisation organisationInformation =new Organisation();
		 organisationInformation.setTradingName("dhdhd");
		 
		 organisation.setOrganisationInformation(organisationInformation);
		 
		 instructingParty.setOrganisation(organisation);
		 
		 ContactInformation contactInformation = new ContactInformation();
		 contactInformation.setEmailAddress("djdj@gmail.com");
		 contactInformation.setFaxNumber("1234");
		 
		 contactInformation.setHomePhoneNumber("123467");
		 contactInformation.setMobilePhoneNumber("7543");
		 
		
		 
		 contactInformation.setWorkPhoneNumber("12354");
		
		 
		 instructingParty.setContactInformation(contactInformation);
		 PartyAddress3 address =new PartyAddress3();
		 
		 List<PartyAddress3> partyAddress = new ArrayList<PartyAddress3>();
		 partyAddress.add(address);
		 instructingParty.setPartyAddress(partyAddress);
		 
		 PartyBasicInformation partyInformation = new PartyBasicInformation();
		
		 Organisation organisation2 =new Organisation();
		 partyInformation.setOrganisation(organisation2);
		 partyInformation.setPartyActiveIndicator( true);
		 partyInformation.setPartyName("sjsjs");
		 partyInformation.setPartySourceIdNumber("2929");
		 PersonBasicInformation person =new PersonBasicInformation();
		 partyInformation.setPerson(person);
		
		
		 
		 instructingParty.setPartyInformation(partyInformation);
		 Person person4= new Person();
		 person4.setEmployersName("ssks");
		PersonBasicInformation personInformation= new PersonBasicInformation();
		 Date birthDate= new Date();
		 birthDate.setYear(122);
		 birthDate.setTime(0);
		 //personInformation.setBirthDate(birthDate);
		 personInformation.setFirstName("djdjd");
		 personInformation.setMothersMaidenName("djdj");
		 personInformation.setSurname("cncn");
		 personInformation.setTitleCode("dkdk");
		 person4.setPersonInformation(personInformation);
		 instructingParty.setPerson(person4);
		 Date instructionStatusUpdateDateTime =new Date();
		 //OffsetDateTime instructionStatusUpdateDateTime  = OffsetDateTime.now();
		 instructionStatusUpdateDateTime.setTime(12);
		 instructionStatusUpdateDateTime.setMinutes(12);
		 instructionStatusUpdateDateTime.setYear(2018);
		
		 
		 Date instructionIssueDate =new Date();
		// OffsetDateTime instructionIssueDate    = OffsetDateTime.now();
		 instructionIssueDate.setTime(12);
		 instructionIssueDate.setMinutes(15);
		 instructionIssueDate.setSeconds(12);
		 Date authorisationDatetime =new Date();
		 
		 paymentInstruction2.setInstructionStatusUpdateDateTime("20-10-1993");;
		 paymentInstruction2.setPaymentInstructionNumber("asdf");
		 paymentInstruction2.setInstructionIssueDate("20-10-1993");
		 paymentInstruction2.setInstructingParty(instructingParty);
		 paymentInstruction2.setPaymentInstructionStatusCode(PaymentInstructionStatusCode.AcceptedSettlementCompleted);
		 paymentInstruction2.setInstructionStatusUpdateDateTime("20-10-1993");
		
		 authorisationDatetime.setMonth(2);
		 authorisationDatetime.setHours(5);
		 paymentInstructionProposal1.setAuthorisationDatetime("20-10-1993");
		 
		 
		 paymentInstructionProposal1.setAuthorisationType(AuthorisationType.Any);
		 
		Party2 authorisingParty= new Party2();
		 
		 authorisingParty.setContactInformation(contactInformation);
		 authorisingParty.setMerchantCategoryCode("23");
		 authorisingParty.setMerchantDescription("kdkdkdkdkddkkk");
		 authorisingParty.setMerchantName("sjsj");
		 authorisingParty.setOrganisation(organisation);
		 authorisingParty.setPartyAddress(partyAddress);
		 authorisingParty.setPartyInformation(partyInformation);
		 authorisingParty.setPerson(person4);
		 authorisingParty.setTaxIdNumber("dkdkdk");
		 
		 paymentInstructionProposal1.setAuthorisingParty(authorisingParty);
		 OBWriteDataDomestic1   obWriteDataDomestic1 =new  OBWriteDataDomestic1  ();
		
		 OBDomestic1 obDomestic1= new OBDomestic1();
		 obWriteDataDomestic1 .setInitiation(obDomestic1);
		 
		 OBRemittanceInformation1 remittanceInformation =new OBRemittanceInformation1();
		 remittanceInformation.setReference("weiei");
		 remittanceInformation.setUnstructured("dkdk");
		 obDomestic1.setRemittanceInformation(remittanceInformation);
		 
		 AuthorisingPartyAccount authorisingPartyAccount= new AuthorisingPartyAccount();
		 authorisingPartyAccount.setAccountName("sksk");
		 authorisingPartyAccount.setAccountNumber("2229");
		 authorisingPartyAccount.setSchemeName("jsjsj");
		 authorisingPartyAccount.setSecondaryIdentification("sksks");
		 paymentInstructionProposal1.setAuthorisingPartyAccount(authorisingPartyAccount);
		 paymentInstructionProposal1.setAuthorisingPartyReference("wiwiiwi");
		 paymentInstructionProposal1.setAuthorisingPartyUnstructuredReference("fkfkfkf");
		PaymentInstructionPostalAddress proposingPartyPostalAddress = new PaymentInstructionPostalAddress();
		 proposingPartyPostalAddress.setGeoCodeBuildingNumber("12");
		 paymentInstructionProposal1.setProposingPartyPostalAddress(proposingPartyPostalAddress);
		 
		 PaymentInstructionCharge charge =new PaymentInstructionCharge();
		 Amount  amount= new Amount();
		 amount.setGroupReportingCurrency(44.0);
		 amount.setTransactionCurrency(55.0);
		 charge.setAmount(amount);
		 charge.setType("djdj");
		 charge.setChargeBearer(ChargeBearer.BorneByDebtor);
		
		 
		 Currency currency= new Currency();
		 currency.setCurrencyName("eiei");
		 currency.setIsoAlphaCode("229");
		  charge.setCurrency(currency);
		 
		 List<PaymentInstructionCharge> charges = new ArrayList<PaymentInstructionCharge>();
		 charges.add(charge);
		 
		 paymentInstructionProposal1.setCharges(charges);
		 
		 FinancialEventAmount financialEventAmount= new FinancialEventAmount();
		 financialEventAmount.setGroupReportingCurrency(800.0);
		 financialEventAmount.setLocalReportingCurrency(3.0);
		 financialEventAmount.setTransactionCurrency(34.0);
		 
		 paymentInstructionProposal1.setFinancialEventAmount(financialEventAmount);
		 paymentInstructionProposal1.setFraudSystemResponse(FraudSystemResponse.Approve);
		 paymentInstructionProposal1.setInstructionEndToEndReference("292");
		 paymentInstructionProposal1.setInstructionLocalInstrument("wwi");
		 paymentInstructionProposal1.setInstructionReference("slssl");
		 paymentInstructionProposal1.setPaymentInstructionProposalId("ajaj");
		 PaymentInstrumentRiskFactor paymentInstructionRiskFactorReference= new PaymentInstrumentRiskFactor();
		 Address counterPartyAddress =new Address();
		Country addressCountry =new Country();
		 addressCountry.setCountryName("eiei");
		 addressCountry.setIsoCountryAlphaTwoCode("202");
		 counterPartyAddress.setAddressCountry(addressCountry);
		 counterPartyAddress.setFifthAddressLine("qiqiqi");
		 counterPartyAddress.setFirstAddressLine("ieiei");
		 counterPartyAddress.setFourthAddressLine("sjsj");
		 counterPartyAddress.setGeoCodeBuildingName("eueu");
		 counterPartyAddress.setGeoCodeBuildingNumber("12344");
		 counterPartyAddress.setSecondAddressLine("wiwi");
		 counterPartyAddress.setThirdAddressLine("1919");
		 paymentInstructionRiskFactorReference.setCounterPartyAddress(counterPartyAddress);
		 paymentInstructionProposal1.setPaymentInstructionRiskFactorReference(paymentInstructionRiskFactorReference);
		 paymentInstructionProposal1.setPaymentType(PaymentType.International);
		 Date proposalCreationDatetime = new Date();
		 proposalCreationDatetime.setHours(12);
		 
		 
		 paymentInstructionProposal1.setProposalCreationDatetime("20-10-1993");
		 paymentInstructionProposal1.setProposalStatus(ProposalStatus.Authorised);
		 paymentInstructionProposal1.setProposalStatusUpdateDatetime("20-10-1993");
		 Party2 proposingParty = new Party2();
		 
		 proposingParty.setMerchantDescription("sjsjs");
		 proposingParty.setContactInformation(contactInformation);
		 proposingParty.setMerchantCategoryCode("slsl");
		 proposingParty.setMerchantName("sksksk");
		 proposingParty.setOrganisation(organisation);
		 proposingParty.setPartyAddress(partyAddress);
		 proposingParty.setPartyInformation(partyInformation);
		 proposingParty.setPerson(person4);
		 proposingParty.setTaxIdNumber("1244");
		 paymentInstructionProposal1.setProposingParty(proposingParty);
		 ProposingPartyAccount proposingPartyAccount= new ProposingPartyAccount();
		 proposingPartyAccount.setAccountName("sksk");
		 proposingPartyAccount.setAccountNumber("kdkgk");
		 proposingPartyAccount.setSchemeName("wowo");
		 proposingPartyAccount.setSecondaryIdentification("dkdk");
		 
		 paymentInstructionProposal1.setProposingPartyAccount(proposingPartyAccount);
		
		 proposingPartyPostalAddress.setAddressCountry(addressCountry);
		 List<String> addressLine = new ArrayList<String>();
		 addressLine.add("djdjd");
		 proposingPartyPostalAddress.setAddressLine(addressLine );
		 proposingPartyPostalAddress.setAddressType("wiwi");
		 proposingPartyPostalAddress.setCountrySubDivision("sksk");
		 proposingPartyPostalAddress.setDepartment("dkdk");
		 proposingPartyPostalAddress.setGeoCodeBuildingName("124");
		 proposingPartyPostalAddress.setGeoCodeBuildingNumber("dldl");
		 proposingPartyPostalAddress.setPostCodeNumber("dldl");
		 proposingPartyPostalAddress.setSubDepartment("sksk");
		 proposingPartyPostalAddress.setTownName("alal");
		 
		 
		 paymentInstructionProposal1.setProposingPartyPostalAddress(proposingPartyPostalAddress);
		 
		 Currency transactionCurrency =new Currency();
		 transactionCurrency.setCurrencyName("slsl");
		 transactionCurrency.setIsoAlphaCode("124");
		 
		 paymentInstructionProposal1.setTransactionCurrency(transactionCurrency);
		 
		 
		 paymentInstruction2.setPaymentInstructionStatusCode(PaymentInstructionStatusCode.Rejected);
		 paymentInstruction2.setPaymentInstructionNumber("12312");

		
		
		 paymentInstructionProposal .setPaymentInstruction(paymentInstruction2);
		 paymentInstructionProposal .setPaymentInstructionProposal(paymentInstructionProposal1);
		 PaymentDomesticSubmitPOST201Response resp = transformer.transformDomesticConsentResponseFromFDToAPIForInsert(paymentInstructionProposal);
		 assertNotNull(resp);
	}
		
		
		
    //@SuppressWarnings("deprecation")
	@Test
	public void testtransformDomesticPaymentResponsefornull () {
			
			Map<String, String> params = new HashMap<String, String>();
			PaymentInstructionProposalComposite inputBalanceObj = new PaymentInstructionProposalComposite();
			PaymentInstructionProposal paymentInstructionProposal = new PaymentInstructionProposal();
			paymentInstructionProposal.setInstructionLocalInstrument(null);
			paymentInstructionProposal.setAuthorisingPartyReference("djdj");
		
			paymentInstructionProposal.setPaymentInstructionProposalId("123");	
			List<PaymentInstructionCharge> charges = new ArrayList<PaymentInstructionCharge>();
			PaymentInstructionCharge e = new PaymentInstructionCharge();
			e.setChargeBearer(ChargeBearer.BorneByCreditor);
			e.setType("credit");
			Amount amount=new Amount();
			amount.setTransactionCurrency(23.00);
			e.setAmount(amount);
			Currency currency = new Currency();
			currency.setIsoAlphaCode("EUR");
			currency.setCurrencyName("123");
			e.setCurrency(currency);
			charges.add(e);
			
			paymentInstructionProposal.setCharges(charges);
			paymentInstructionProposal.setInstructionReference("asdf");
			paymentInstructionProposal.setInstructionEndToEndReference("ghbh");
			
			Currency transactionCurrency = new Currency();
			transactionCurrency.setIsoAlphaCode("EUR");
			paymentInstructionProposal.setTransactionCurrency(transactionCurrency);
			AuthorisingPartyAccount authorisingPartyAccount = new AuthorisingPartyAccount();
			authorisingPartyAccount.setSchemeName("sfd");
            authorisingPartyAccount.setAccountName(null);
			authorisingPartyAccount.setAccountNumber("1334");
			
			authorisingPartyAccount.setSecondaryIdentification(null);
			paymentInstructionProposal.setAuthorisingPartyAccount(authorisingPartyAccount);
			paymentInstructionProposal.setAuthorisingPartyReference("capg");
			paymentInstructionProposal.setAuthorisingPartyUnstructuredReference("fdfg");
			FinancialEventAmount financialEventAmount = new FinancialEventAmount();
			financialEventAmount.setTransactionCurrency(56.00);
			paymentInstructionProposal.setFinancialEventAmount(financialEventAmount);
			PaymentInstructionPostalAddress proposingPartyPostalAddress = new PaymentInstructionPostalAddress();
			
			proposingPartyPostalAddress.setDepartment(null);
			
			proposingPartyPostalAddress.setTownName(null);
			
			
			proposingPartyPostalAddress.setAddressType(null);
			
			proposingPartyPostalAddress.setSubDepartment(null);
			
			proposingPartyPostalAddress.setGeoCodeBuildingName(null);
			
			proposingPartyPostalAddress.setGeoCodeBuildingNumber(null);
			proposingPartyPostalAddress.setCountrySubDivision(null);
			
			
			proposingPartyPostalAddress.setPostCodeNumber(null);
			
			Country addressCountry = new Country();
			//addressCountry.setIsoCountryAlphaTwoCode("IND");
			addressCountry.setIsoCountryAlphaTwoCode(null);
			proposingPartyPostalAddress.setAddressCountry(addressCountry);
			List<String> addressLine = new ArrayList<String>();
			addressLine.add("devi indrayani");
			//proposingPartyPostalAddress.setAddressLine(addressLine);
			proposingPartyPostalAddress.setAddressLine(null);
			paymentInstructionProposal.setProposingPartyPostalAddress(proposingPartyPostalAddress);
			inputBalanceObj.setPaymentInstructionProposal(paymentInstructionProposal);
			PaymentInstruction paymentInstruction = new PaymentInstruction();
			PaymentInstructionBasic paymentinstructionbasic = new PaymentInstructionBasic();
			//paymentInstruction.setPaymentInstruction(paymentinstructionbasic);
			PaymentInstruction1 paymentInstruction2 = new PaymentInstruction1();
			paymentInstruction2.setPaymentInstructionStatusCode(PaymentInstructionStatusCode.AcceptedSettlementCompleted);
			inputBalanceObj.setPaymentInstruction(paymentInstruction2 );
			
			PaymentDomesticSubmitPOST201Response response2 = transformer.transformDomesticPaymentResponse(inputBalanceObj);
			assertNotNull(response2);
			
		}
		
		
			
		@Test
		public void testtransformDomesticSubmissionResponseFromAPIToFDForInsertfornull()
		
		
		{
			CustomDPaymentsPOSTRequest domesticPaymentsRequest = new CustomDPaymentsPOSTRequest();
		
			
			Map<String, String> params = new HashMap<String, String>();
			
			
			 OBWriteDataDomestic1  obwriteDataDomestic1= new  OBWriteDataDomestic1 ();
			 OBDomestic1  obDomestc1= new  OBDomestic1();
			 OBDomestic1InstructedAmount obDomestic1InstructedAmount =new OBDomestic1InstructedAmount ();
			 OBCashAccountDebtor3  obCashAccountDebtor3= new  OBCashAccountDebtor3();
			 OBCashAccountCreditor2  obCashAccountCreditor2 = new  OBCashAccountCreditor2();
			 OBPostalAddress6 obPostalAddress6 = new OBPostalAddress6();
			 OBRemittanceInformation1  obRemittanceInformation1= new OBRemittanceInformation1 (); 
			 
			 OBRisk1 obRisk1 = new OBRisk1();
			
			
			obwriteDataDomestic1.setConsentId(null);
			
			obDomestc1.setInstructionIdentification(null);

			obDomestc1.setEndToEndIdentification(null);
			obDomestc1.setLocalInstrument(null);
			obDomestic1InstructedAmount.setAmount(null);
			obDomestic1InstructedAmount.setCurrency(null);
			
			obCashAccountDebtor3.setSchemeName(null);
			obCashAccountDebtor3.setName(null);
			obCashAccountDebtor3.setIdentification(null);
			obCashAccountDebtor3.setSecondaryIdentification(null);
			
			obCashAccountCreditor2.setIdentification(null);
			obCashAccountCreditor2.setName(null);
			obCashAccountCreditor2.setSchemeName(null);
			obCashAccountCreditor2.setSecondaryIdentification(null);
			 
			List<String> addressLine =new ArrayList<String>();
			addressLine.add("jgjghjhj");
			addressLine.add("tfff");
			obPostalAddress6.setBuildingNumber(null);
			obPostalAddress6.setAddressLine(addressLine);
			obPostalAddress6.setCountry(null);
			obPostalAddress6.setCountrySubDivision(null);
			obPostalAddress6.setDepartment(null);
			
			obPostalAddress6.setPostCode(null);
			obPostalAddress6.setStreetName(null);
			obPostalAddress6.setSubDepartment(null);
			obPostalAddress6.setTownName(null);
			obPostalAddress6.setAddressType(OBAddressTypeCode.BUSINESS);
			
			 obRemittanceInformation1.setReference("fkfk");
			 
			 obRisk1.setMerchantCategoryCode("dkd");
			 obRisk1.setMerchantCustomerIdentification("dkdk");
			 OBRisk1DeliveryAddress deliveryAddress=new OBRisk1DeliveryAddress();
			 deliveryAddress.setAddressLine(addressLine);
			 deliveryAddress.setBuildingNumber("123");
			 deliveryAddress.setCountry("india");
			 deliveryAddress.setCountrySubDivision("234");
			 deliveryAddress.setPostCode("kdkdk");
			 deliveryAddress.setStreetName("kdk");
			 deliveryAddress.setTownName("pune");
			 obRisk1.setDeliveryAddress(deliveryAddress);
			 obRisk1.setPaymentContextCode(OBExternalPaymentContext1Code.BILLPAYMENT);
			
			obDomestc1.setRemittanceInformation(obRemittanceInformation1);
			
			obDomestc1.setDebtorAccount(obCashAccountDebtor3);
			
			obDomestc1.setCreditorAccount(obCashAccountCreditor2);
			obDomestc1.setCreditorPostalAddress(obPostalAddress6);
			
			obDomestc1.setInstructedAmount(null);
			
			
			obwriteDataDomestic1.setInitiation(obDomestc1);
			
			domesticPaymentsRequest.setRisk(obRisk1);
			domesticPaymentsRequest.setFraudSystemResponse("sksk");
			domesticPaymentsRequest.setData(obwriteDataDomestic1);
			domesticPaymentsRequest.setCreatedOn("dkd");
			 PaymentInstructionProposalComposite response = transformer.transformDomesticSubmissionResponseFromAPIToFDForInsert(domesticPaymentsRequest, params);
			 assertNotNull(response);
			
		}
		
		
		
		
		
	
}
