package com.capgemini.psd2.payment.retrieval.mock.foundationservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;
import com.capgemini.psd2.foundationservice.utilities.NullCheckUtils;
import com.capgemini.psd2.payment.retrieval.mock.foundationservice.domain.PaymentInstruction;
import com.capgemini.psd2.payment.retrieval.mock.foundationservice.service.PaymentRetrievalServiceInterface;

@RestController
@RequestMapping("/fs-payment-web-service/services/retrievePayment")

public class PaymentRetrievalController {
	public static final String CONNECTION_REFUSED = "Connection refused";
	@Autowired
	private PaymentRetrievalServiceInterface retrivepaymentservice;
	
	@RequestMapping(value = "/{Payment-Id}/payments", method = RequestMethod.GET, produces="application/xml")
	public PaymentInstruction reterivepayment(
			
			@PathVariable("Payment-Id") String paymentId,
			@RequestHeader(required = false, value = "X-BOI-USER") String boiUser,
			@RequestHeader(required = false, value = "X-BOI-CHANNEL") String boiChannel,
			@RequestHeader(defaultValue="PSD2API",  required = true, value = "X-BOI-PLATFORM") String boiPlatform,
			@RequestHeader(required = true, value = "X-CORRELATION-ID") String correlationID) throws Exception {

		if (NullCheckUtils.isNullOrEmpty(boiPlatform) || NullCheckUtils.isNullOrEmpty(correlationID)) {
			
			throw MockFoundationServiceException
					.populateMockFoundationServiceException(ErrorCodeEnum.AUTHENTICATION_FAILURE_PMR);
		}
		if (NullCheckUtils.isNullOrEmpty(paymentId)) {
			throw MockFoundationServiceException
			.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_PMR);
		}
		PaymentInstruction instructions = null;
		try {
			instructions = retrivepaymentservice.retrievePayment(paymentId);
		} catch (Exception e) {
			if(e.getMessage().contains(CONNECTION_REFUSED)){
				 throw new RuntimeException();
			}else
				throw e;
		}
		return  instructions;

	}
	

}
