package com.capgemini.psd2.payment.retrieval.mock.foundationservice.repository;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.payment.retrieval.mock.foundationservice.domain.Payment;
import com.capgemini.psd2.payment.retrieval.mock.foundationservice.domain.PaymentInstruction;

/**
 * The Interface PaymentRetrievalRepository.
 */
public interface PaymentRetrievalRepository extends MongoRepository<PaymentInstruction , String> {

	/**
	 * Find by paymentId.
	 *
	
	 * @param paymentId
	 * @return the payment
	 */
	public PaymentInstruction findByPaymentPaymentId(String paymentId);
	

}
