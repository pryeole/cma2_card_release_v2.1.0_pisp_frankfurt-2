package com.capgemini.psd2.pisp.dspc.comparator;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import org.springframework.stereotype.Component;

import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBDomesticScheduled1;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDomesticScheduledConsentResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class DSPConsentsPayloadComparator implements Comparator<OBWriteDomesticScheduledConsentResponse1> {

	@Override
	public int compare(OBWriteDomesticScheduledConsentResponse1 response,
			OBWriteDomesticScheduledConsentResponse1 adaptedScheduledPaymentConsentsResponse) {

		int value = 1;

		if (Double.compare(Double.parseDouble(response.getData().getInitiation().getInstructedAmount().getAmount()), Double
				.parseDouble(adaptedScheduledPaymentConsentsResponse.getData().getInitiation().getInstructedAmount()
						.getAmount()))==0) {
			adaptedScheduledPaymentConsentsResponse.getData().getInitiation().getInstructedAmount()
					.setAmount(response.getData().getInitiation().getInstructedAmount().getAmount());
		}

		if (response.getData().getInitiation().equals(adaptedScheduledPaymentConsentsResponse.getData().getInitiation())
				&& response.getRisk().equals(adaptedScheduledPaymentConsentsResponse.getRisk()))
			value = 0;

		OBAuthorisation1 responseAuth = response.getData().getAuthorisation();
		OBAuthorisation1 adaptedResponseAuth = adaptedScheduledPaymentConsentsResponse.getData().getAuthorisation();

		if (!NullCheckUtils.isNullOrEmpty(responseAuth) && !NullCheckUtils.isNullOrEmpty(adaptedResponseAuth)
				&& value != 1) {
			if (responseAuth.equals(adaptedResponseAuth))
				value = 0;
			else
				value = 1;
		}

		return value;
	}

	public int compareScheduledPaymentDetails(CustomDSPConsentsPOSTResponse response,
			CustomDSPConsentsPOSTRequest request, PaymentConsentsPlatformResource paymentSetupPlatformResource) {

		String strPaymentConsentsRequest = JSONUtilities.getJSONOutPutFromObject(request);
		CustomDSPConsentsPOSTResponse adaptedScheduledPaymentConsentsResponse = JSONUtilities.getObjectFromJSONString(
				PispUtilities.getObjectMapper(), strPaymentConsentsRequest, CustomDSPConsentsPOSTResponse.class);

		if (!validateDebtorDetails(response.getData().getInitiation(),
				adaptedScheduledPaymentConsentsResponse.getData().getInitiation(), paymentSetupPlatformResource))
			return 1;

		compareAmount(response.getData().getInitiation().getInstructedAmount(),
				adaptedScheduledPaymentConsentsResponse.getData().getInitiation().getInstructedAmount());
		/*
		 * Date: 13-Feb-2018 Changes are made for CR-10
		 */
		checkAndModifyEmptyFields(adaptedScheduledPaymentConsentsResponse, response);

		if (!NullCheckUtils.isNullOrEmpty(response.getData().getAuthorisation()) && (NullCheckUtils
				.isNullOrEmpty(adaptedScheduledPaymentConsentsResponse.getData().getAuthorisation()))) {
			return 1;
		}

		if (NullCheckUtils.isNullOrEmpty(response.getData().getAuthorisation()) && !NullCheckUtils
				.isNullOrEmpty(adaptedScheduledPaymentConsentsResponse.getData().getAuthorisation())) {
			return 1;
		}

		int returnValue = compare(response, adaptedScheduledPaymentConsentsResponse);

		response.getData().getInitiation()
				.setRemittanceInformation(request.getData().getInitiation().getRemittanceInformation());
		response.getRisk().setDeliveryAddress(request.getRisk().getDeliveryAddress());

		// End of CR-10
		return returnValue;

	}

	private void checkAndModifyEmptyFields(CustomDSPConsentsPOSTResponse adaptedScheduledPaymentConsentsResponse,
			CustomDSPConsentsPOSTResponse response) {

		checkAndModifyRemittanceInformation(adaptedScheduledPaymentConsentsResponse.getData().getInitiation(),
				response);
		checkAndModifyCreditorPostalAddressInformation(
				adaptedScheduledPaymentConsentsResponse.getData().getInitiation(), response);

		if (adaptedScheduledPaymentConsentsResponse.getRisk().getDeliveryAddress() != null) {
			checkAndModifyAddressLine(adaptedScheduledPaymentConsentsResponse.getRisk().getDeliveryAddress(), response);
		}

		if (adaptedScheduledPaymentConsentsResponse.getData().getInitiation().getCreditorPostalAddress() != null
				&& adaptedScheduledPaymentConsentsResponse.getData().getInitiation().getCreditorPostalAddress()
						.getAddressLine() != null) {
			checkAndModifyCreditorPostalAddressLine(
					adaptedScheduledPaymentConsentsResponse.getData().getInitiation().getCreditorPostalAddress(),
					response);
		}

	}

	private void checkAndModifyRemittanceInformation(OBDomesticScheduled1 obDomesticScheduled1,
			CustomDSPConsentsPOSTResponse response) {

		OBRemittanceInformation1 remittanceInformation = obDomesticScheduled1.getRemittanceInformation();
		if (remittanceInformation != null && response.getData().getInitiation().getRemittanceInformation() == null
				&& NullCheckUtils.isNullOrEmpty(remittanceInformation.getReference())
				&& NullCheckUtils.isNullOrEmpty(remittanceInformation.getUnstructured())) {

			obDomesticScheduled1.setRemittanceInformation(null);
		}
	}

	private void checkAndModifyCreditorPostalAddressInformation(OBDomesticScheduled1 responseInitiation,
			CustomDSPConsentsPOSTResponse response) {

		OBPostalAddress6 crPostaladdInformation = responseInitiation.getCreditorPostalAddress();
		if (crPostaladdInformation != null && response.getData().getInitiation().getCreditorPostalAddress() == null
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getAddressLine())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getAddressType())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getDepartment())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getSubDepartment())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getStreetName())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getBuildingNumber())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getPostCode())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getTownName())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getCountrySubDivision())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getCountry())) {

			responseInitiation.setCreditorPostalAddress(null);
		}
	}

	private void checkAndModifyAddressLine(OBRisk1DeliveryAddress obRisk1DeliveryAddress,
			CustomDSPConsentsPOSTResponse response) {

		// addressLine List to String
		if (obRisk1DeliveryAddress.getAddressLine() != null) {
			List<String> addressLineList = new ArrayList<>();
			for (String addressLine : obRisk1DeliveryAddress.getAddressLine()) {
				if (NullCheckUtils.isNullOrEmpty(addressLine))
					continue;
				addressLineList.add(addressLine);
			}
			if (addressLineList.isEmpty() && response.getRisk().getDeliveryAddress().getAddressLine() == null)
				addressLineList = null;

			obRisk1DeliveryAddress.setAddressLine(addressLineList);
		}
	}

	private void checkAndModifyCreditorPostalAddressLine(OBPostalAddress6 obPostalAddress6,
			CustomDSPConsentsPOSTResponse response) {
		List<String> addressLineList = new ArrayList<>();
		for (String addressLine : (obPostalAddress6.getAddressLine())) {
			if (NullCheckUtils.isNullOrEmpty(addressLine))
				continue;
			addressLineList.add(addressLine);
		}
		if (addressLineList.isEmpty()
				&& response.getData().getInitiation().getCreditorPostalAddress().getAddressLine() == null)
			addressLineList = null;

		obPostalAddress6.setAddressLine(addressLineList);
	}

	private boolean validateDebtorDetails(OBDomesticScheduled1 obDomesticScheduled1,
			OBDomesticScheduled1 obDomesticScheduled12, PaymentConsentsPlatformResource paymentSetupPlatformResource) {

		if (Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())) {
			/*
			 * If DebtorAccount.Name is sent by TPP,tppDebtoNameDetails would be true, then
			 * name will participate in comparison else product will set foundation response
			 * DebtorAccount.Name as null so that it can be passed in comparison. Both
			 * fields would have the value as 'Null'.
			 */
			if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorNameDetails()))
				obDomesticScheduled1.getDebtorAccount().setName(null);

			// DebtorAgent is not present in CMA3
			return Objects.equals(obDomesticScheduled1.getDebtorAccount(), obDomesticScheduled12.getDebtorAccount());
		}
		if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())
				&& obDomesticScheduled12.getDebtorAccount() != null) {
			return Boolean.FALSE;
		}
		if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())
				&& obDomesticScheduled1.getDebtorAccount() != null) {
			obDomesticScheduled1.setDebtorAccount(null);
			return Boolean.TRUE;
		}
		return Boolean.TRUE;

	}

	/*
	 * Date: 12-Jan-2018, SIT defect#956 Comparing two float values received from FS
	 * and TPP. Both values must be treated equal mathematically if values are in
	 * below format. i.e 1.2 == 1.20, 0.20000 == 0.20, 0.2 == 00.20000, 0.2 ==
	 * 0.20000, 001.00 == 1.00 etc. If values are equal then set the amount received
	 * from TPP to the amount object of FS response. By doing this, comparison would
	 * be passed in swagger's java file(compare method of this class) and same
	 * amount would be available to TPP also in response of idempotent request.
	 */
	private void compareAmount(OBDomestic1InstructedAmount obDomestic1InstructedAmount,
			OBDomestic1InstructedAmount obDomestic1InstructedAmount2) {
		if (Double.compare(Double.parseDouble(obDomestic1InstructedAmount.getAmount()),
				Double.parseDouble(obDomestic1InstructedAmount2.getAmount())) == 0)
			obDomestic1InstructedAmount.setAmount(obDomestic1InstructedAmount2.getAmount());
	}

}
