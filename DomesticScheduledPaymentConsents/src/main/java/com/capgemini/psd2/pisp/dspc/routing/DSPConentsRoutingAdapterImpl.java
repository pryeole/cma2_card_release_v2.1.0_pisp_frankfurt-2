package com.capgemini.psd2.pisp.dspc.routing;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.GenericPaymentConsentResponse;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.operations.adapter.DomesticScheduledPaymentStagingAdapter;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component("dspConsentsStagingRoutingAdapter")
public class DSPConentsRoutingAdapterImpl implements DomesticScheduledPaymentStagingAdapter, ApplicationContextAware {

	private DomesticScheduledPaymentStagingAdapter beanInstance;

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Value("${app.paymentSetupStagingAdapter}")
	private String beanName;

	/** The application context. */
	private ApplicationContext applicationContext;

	@Override
	public GenericPaymentConsentResponse processDomesticScheduledPaymentConsents(
			CustomDSPConsentsPOSTRequest domesticScheduledPaymentRequest,
			CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params,
			OBExternalConsentStatus1Code successStatus, OBExternalConsentStatus1Code failureStatus) {
		return getRoutingInstance(beanName).processDomesticScheduledPaymentConsents(domesticScheduledPaymentRequest,
				customStageIdentifiers, addHeaderParams(params), successStatus, failureStatus);
	}

	@Override
	public CustomDSPConsentsPOSTResponse retrieveStagedDomesticScheduledPaymentConsents(
			CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
		return getRoutingInstance(beanName)
				.retrieveStagedDomesticScheduledPaymentConsents(customPaymentStageIdentifiers, addHeaderParams(params));

	}

	@Override
	public void setApplicationContext(ApplicationContext context) {
		this.applicationContext = context;
	}

	public DomesticScheduledPaymentStagingAdapter getRoutingInstance(String adapterName) {
		if (beanInstance == null)
			beanInstance = (DomesticScheduledPaymentStagingAdapter) applicationContext.getBean(adapterName);
		return beanInstance;
	}

	private Map<String, String> addHeaderParams(Map<String, String> params) {
		if (NullCheckUtils.isNullOrEmpty(params))
			params = new HashMap<>();

		if (null != reqHeaderAtrributes.getToken() && null != reqHeaderAtrributes.getToken().getSeviceParams()) {
			params.put(PSD2Constants.CHANNEL_IN_REQ_HEADER,
					reqHeaderAtrributes.getToken().getSeviceParams().get(PSD2Constants.CHANNEL_NAME));
		}
		params.put(PSD2Constants.USER_IN_REQ_HEADER, reqHeaderAtrributes.getPsuId());
		params.put(PSD2Constants.CORRELATION_REQ_HEADER, reqHeaderAtrributes.getCorrelationId());
		params.put(PSD2Constants.TENANT_ID, reqHeaderAtrributes.getTenantId());
		return params;
	}
}
