package com.capgemini.psd2.pisp.dspc.test.routing;

import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;

import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.GenericPaymentConsentResponse;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.dspc.routing.DSPConentsRoutingAdapterImpl;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.operations.adapter.DomesticScheduledPaymentStagingAdapter;
import com.capgemini.psd2.token.Token;

public class DSPConentsRoutingAdapterImplTest {

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Mock
	private ApplicationContext applicationContext;

	@InjectMocks
	private DSPConentsRoutingAdapterImpl adapter;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testProcessDomesticScheduledPaymentConsents() {
		CustomDSPConsentsPOSTRequest request = new CustomDSPConsentsPOSTRequest();
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();

		Map<String, String> params = new HashMap<>();

		Token token = new Token();
		token.setSeviceParams(params);

		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(token);
		params.put(PSD2Constants.USER_IN_REQ_HEADER,
				reqHeaderAtrributes.getToken().getSeviceParams().get(PSD2Constants.CHANNEL_NAME));

		DomesticScheduledPaymentStagingAdapter stagingAdapter = new DomesticScheduledPaymentStagingAdapter() {

			@Override
			public CustomDSPConsentsPOSTResponse retrieveStagedDomesticScheduledPaymentConsents(
					CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
				return null;
			}

			@Override
			public GenericPaymentConsentResponse processDomesticScheduledPaymentConsents(
					CustomDSPConsentsPOSTRequest domesticScheduledPaymentRequest,
					CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params,
					OBExternalConsentStatus1Code successStatus, OBExternalConsentStatus1Code failureStatus) {
				return null;
			}
		};

		when(applicationContext.getBean(anyString())).thenReturn(stagingAdapter);
		adapter.processDomesticScheduledPaymentConsents(request, stageIdentifiers, new HashMap<>(),
				OBExternalConsentStatus1Code.AUTHORISED, OBExternalConsentStatus1Code.REJECTED);
	}

	@Test
	public void testRetrieveStagedDomesticScheduledPaymentConsentsNullParams() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();

		Map<String, String> params = null;

		Token token = new Token();
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(token);

		DomesticScheduledPaymentStagingAdapter stagingAdapter = new DomesticScheduledPaymentStagingAdapter() {

			@Override
			public CustomDSPConsentsPOSTResponse retrieveStagedDomesticScheduledPaymentConsents(
					CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
				return null;
			}

			@Override
			public GenericPaymentConsentResponse processDomesticScheduledPaymentConsents(
					CustomDSPConsentsPOSTRequest domesticScheduledPaymentRequest,
					CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params,
					OBExternalConsentStatus1Code successStatus, OBExternalConsentStatus1Code failureStatus) {
				return null;
			}
		};

		when(applicationContext.getBean(anyString())).thenReturn(stagingAdapter);
		adapter.retrieveStagedDomesticScheduledPaymentConsents(stageIdentifiers, params);
		assertNull(params);
	}

	@Test
	public void testRetrieveStagedDomesticScheduledPaymentConsents() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();

		Map<String, String> params = new HashMap<>();

		DomesticScheduledPaymentStagingAdapter stagingAdapter = new DomesticScheduledPaymentStagingAdapter() {

			@Override
			public CustomDSPConsentsPOSTResponse retrieveStagedDomesticScheduledPaymentConsents(
					CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
				return null;
			}

			@Override
			public GenericPaymentConsentResponse processDomesticScheduledPaymentConsents(
					CustomDSPConsentsPOSTRequest domesticScheduledPaymentRequest,
					CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params,
					OBExternalConsentStatus1Code successStatus, OBExternalConsentStatus1Code failureStatus) {
				return null;
			}
		};

		when(applicationContext.getBean(anyString())).thenReturn(stagingAdapter);
		adapter.retrieveStagedDomesticScheduledPaymentConsents(stageIdentifiers, params);
	}

	@Test
	public void testSetApplicationContext() {
		adapter.setApplicationContext(new ApplicationContextMock());
	}

	@After
	public void tearDown() {
		reqHeaderAtrributes = null;
		applicationContext = null;
		adapter = null;
	}
}
