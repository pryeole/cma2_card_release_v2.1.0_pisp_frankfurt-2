package com.capgemini.psd2.pisp.dspc.test.transformer.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBDomesticScheduled1;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticScheduledConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticScheduledConsentResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseLinks;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseMeta;
import com.capgemini.psd2.pisp.dspc.transformer.impl.DSPConsentsResponseTransformerImpl;
import com.capgemini.psd2.pisp.utilities.PispDateUtility;

@RunWith(SpringJUnit4ClassRunner.class)
public class DSPConsentsResponseTransformerImplTest {

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Mock
	private PispDateUtility pispDateUtility;

	@InjectMocks
	private DSPConsentsResponseTransformerImpl transformer;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testPaymentConsentsResponseTransformer() {
		CustomDSPConsentsPOSTResponse response = new CustomDSPConsentsPOSTResponse();
		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();

		response.setData(new OBWriteDataDomesticScheduledConsentResponse1());
		response.getData().setCreationDateTime("2017-06-05T15:15:13+00:00");
		response.getData().setInitiation(new OBDomesticScheduled1());
		response.getData().getInitiation().setCreditorAccount(new OBCashAccountCreditor2());
		response.getData().getInitiation().getCreditorAccount().setSchemeName("IBAN");
		response.getData().getInitiation().setDebtorAccount(new OBCashAccountDebtor3());
		response.getData().getInitiation().getDebtorAccount().setSchemeName("IBAN");
		OBRisk1 risk = new OBRisk1();
		response.setRisk(risk);

		resource.setStatus("ACCEPTEDTECHNICALVALIDATION");
		resource.setStatusUpdateDateTime("2017-06-05T15:18:13+00:00");
		resource.setCreatedAt("2017-06-05T15:15:13+00:00");
		resource.setTppDebtorDetails("true");
		resource.setTppDebtorNameDetails("testDebtor");

		response.setLinks(null);
		response.setMeta(null);
		
		Mockito.when(reqHeaderAtrributes.getSelfUrl()).thenReturn("/testSelfUrl");
		CustomDSPConsentsPOSTResponse x = transformer.paymentConsentsResponseTransformer(response, resource, "GET");
		assertEquals(OBExternalConsentStatus1Code.AWAITINGAUTHORISATION, x.getData().getStatus());
		assertEquals("2017-06-05T15:18:13+00:00", x.getData().getStatusUpdateDateTime());
	}

	@Test
	public void testPaymentConsentsResponseTransformer_FalseDebtorDetails() {
		CustomDSPConsentsPOSTResponse response = new CustomDSPConsentsPOSTResponse();
		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();

		response.setData(new OBWriteDataDomesticScheduledConsentResponse1());
		response.getData().setCreationDateTime("2017-06-05T15:15:13+00:00");
		response.getData().setInitiation(new OBDomesticScheduled1());
		response.getData().getInitiation().setCreditorAccount(new OBCashAccountCreditor2());
		response.getData().getInitiation().setDebtorAccount(new OBCashAccountDebtor3());
		OBRisk1 risk = new OBRisk1();
		response.setRisk(risk);

		resource.setStatus("ACCEPTEDTECHNICALVALIDATION");
		resource.setStatusUpdateDateTime("2017-06-05T15:18:13+00:00");
		resource.setCreatedAt("2017-01-05T15:19:15+00:00");

		response.setLinks(null);
		PaymentSetupPOSTResponseLinks links = new PaymentSetupPOSTResponseLinks();
		links.getSelf();

		PaymentSetupPOSTResponseMeta meta = new PaymentSetupPOSTResponseMeta();
		meta.setTotalPages(100);
		Mockito.when(reqHeaderAtrributes.getSelfUrl()).thenReturn("/testSelfUrl");
		CustomDSPConsentsPOSTResponse x = transformer.paymentConsentsResponseTransformer(response, resource, "GET");
		assertEquals(OBExternalConsentStatus1Code.AWAITINGAUTHORISATION, x.getData().getStatus());
		assertEquals("2017-06-05T15:18:13+00:00", x.getData().getStatusUpdateDateTime());
	}

	@Test
	public void testpaymentConsentRequestTransformer_RequestedExecutionDateTime() {
		CustomDSPConsentsPOSTRequest request = new CustomDSPConsentsPOSTRequest();
		request.setData(new OBWriteDataDomesticScheduledConsent1());
		request.getData().setInitiation(new OBDomesticScheduled1());
		request.getData().setAuthorisation(new OBAuthorisation1());
		request.getData().getInitiation().setRequestedExecutionDateTime("2019-01-31T00:00:00+00:00");

		when(pispDateUtility.transformDateTimeInRequest(anyString())).thenReturn("2019-01-31T00:00:00+00:00");
		transformer.paymentConsentRequestTransformer(request);
	}

	@Test
	public void testpaymentConsentRequestTransformer_CompletionDateTime() {
		CustomDSPConsentsPOSTRequest request = new CustomDSPConsentsPOSTRequest();
		request.setData(new OBWriteDataDomesticScheduledConsent1());
		request.getData().setInitiation(new OBDomesticScheduled1());
		request.getData().setAuthorisation(new OBAuthorisation1());
		request.getData().getAuthorisation().setCompletionDateTime("2019-12-31T00:00:00+00:00");

		when(pispDateUtility.transformDateTimeInRequest(anyString())).thenReturn("2019-12-31T00:00:00+00:00");
		transformer.paymentConsentRequestTransformer(request);
	}

	@After
	public void tearDown() {
		reqHeaderAtrributes = null;
		pispDateUtility = null;
		transformer = null;
	}

}