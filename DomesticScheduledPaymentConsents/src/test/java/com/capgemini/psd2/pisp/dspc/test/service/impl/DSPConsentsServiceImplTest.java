package com.capgemini.psd2.pisp.dspc.test.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;

import java.util.HashMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPlatformDetails;
import com.capgemini.psd2.pisp.domain.GenericPaymentConsentResponse;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBDomestic1;
import com.capgemini.psd2.pisp.domain.OBDomesticScheduled1;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticScheduledConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticScheduledConsentResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentResponseInfo;
import com.capgemini.psd2.pisp.dspc.comparator.DSPConsentsPayloadComparator;
import com.capgemini.psd2.pisp.dspc.service.impl.DSPConsentsServiceImpl;
import com.capgemini.psd2.pisp.enums.ProcessConsentStatusEnum;
import com.capgemini.psd2.pisp.processing.adapter.service.PaymentConsentProcessingAdapter;
import com.capgemini.psd2.pisp.stage.operations.adapter.DomesticScheduledPaymentStagingAdapter;

@RunWith(SpringJUnit4ClassRunner.class)
public class DSPConsentsServiceImplTest {

	@Mock
	private DSPConsentsPayloadComparator dScheduledPaymentConsentsComparator;

	@Mock
	private DomesticScheduledPaymentStagingAdapter dspStagingAdapter;

	@Mock
	private PaymentConsentProcessingAdapter paymemtRequestAdapter;

	@InjectMocks
	private DSPConsentsServiceImpl service;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testCreateDomesticScheduledPaymentConsentsResource_WithNullPaymentId() {
		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();
		resource.setPaymentConsentId(null);
		resource.setCreatedAt("2019-08-06T06:06:06.666+00:00");

		GenericPaymentConsentResponse value = new GenericPaymentConsentResponse();
		value.setConsentId("1423");

		CustomDSPConsentsPOSTResponse response = new CustomDSPConsentsPOSTResponse();
		response.setData(new OBWriteDataDomesticScheduledConsentResponse1());
		response.getData().setConsentId("14563");
		response.getData().setInitiation(new OBDomesticScheduled1());
		response.setConsentPorcessStatus(ProcessConsentStatusEnum.PASS);

		CustomDSPConsentsPOSTRequest request = new CustomDSPConsentsPOSTRequest();
		request.setData(new OBWriteDataDomesticScheduledConsent1());
		request.getData().setInitiation(new OBDomesticScheduled1());
		OBRemittanceInformation1 remittanceInfo = new OBRemittanceInformation1();
		request.getData().getInitiation().setRemittanceInformation(remittanceInfo);
		request.getData().getInitiation().getRemittanceInformation().setReference("1");
		response.getData().getInitiation().setRemittanceInformation(remittanceInfo);
		OBPostalAddress6 postalAddress = new OBPostalAddress6();
		request.getData().getInitiation().setCreditorPostalAddress(postalAddress);
		request.getData().getInitiation().getCreditorPostalAddress().setCountry("Ireland");
		response.getData().getInitiation().setCreditorPostalAddress(postalAddress);
		OBRisk1 requestRisk = new OBRisk1();
		requestRisk.setDeliveryAddress(new OBRisk1DeliveryAddress());
		requestRisk.getDeliveryAddress().buildingNumber("1");
		request.setRisk(requestRisk);
		response.setRisk(requestRisk);
		Mockito.when(paymemtRequestAdapter.preConsentProcessFlows(anyObject(), anyObject())).thenReturn(resource);

		Mockito.when(paymemtRequestAdapter.postConsentProcessFlows(anyObject(), anyObject(), anyObject(), anyObject()))
				.thenReturn(response);

		Mockito.when(dspStagingAdapter.processDomesticScheduledPaymentConsents(anyObject(), anyObject(), anyObject(),
				anyObject(), anyObject())).thenReturn(value);

		service.createDomesticScheduledPaymentConsentsResource(request);
		assertNull(resource.getPaymentConsentId());
	}

	@Test
	public void testCreateDomesticScheduledPaymentConsentsResource_WithPaymentId_TrueDebtorDetails() {
		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();
		resource.setPaymentConsentId("1458");
		resource.setCreatedAt("2019-08-06T06:06:06.666+00:00");

		CustomDSPConsentsPOSTResponse response = new CustomDSPConsentsPOSTResponse();
		response.setData(new OBWriteDataDomesticScheduledConsentResponse1());
		response.getData().setConsentId("14563");
		response.getData().setInitiation(new OBDomesticScheduled1());
		response.setConsentPorcessStatus(ProcessConsentStatusEnum.PASS);

		CustomDSPConsentsPOSTRequest request = new CustomDSPConsentsPOSTRequest();
		request.setData(new OBWriteDataDomesticScheduledConsent1());
		request.getData().setInitiation(new OBDomesticScheduled1());
		OBRemittanceInformation1 remittanceInfo = new OBRemittanceInformation1();
		request.getData().getInitiation().setRemittanceInformation(remittanceInfo);
		request.getData().getInitiation().getRemittanceInformation().setReference("1");
		response.getData().getInitiation().setRemittanceInformation(remittanceInfo);
		OBPostalAddress6 postalAddress = new OBPostalAddress6();
		request.getData().getInitiation().setCreditorPostalAddress(postalAddress);
		request.getData().getInitiation().getCreditorPostalAddress().setCountry("Ireland");
		response.getData().getInitiation().setCreditorPostalAddress(postalAddress);
		OBRisk1 requestRisk = new OBRisk1();
		requestRisk.setDeliveryAddress(new OBRisk1DeliveryAddress());
		requestRisk.getDeliveryAddress().buildingNumber("1");
		request.setRisk(requestRisk);
		response.setRisk(requestRisk);

		CustomPaymentSetupPlatformDetails platformDetails = new CustomPaymentSetupPlatformDetails();
		platformDetails.setTppDebtorDetails("true");
		platformDetails.setTppDebtorNameDetails("true");

		Mockito.when(paymemtRequestAdapter.preConsentProcessFlows(anyObject(), anyObject())).thenReturn(resource);

		Mockito.when(dspStagingAdapter.retrieveStagedDomesticScheduledPaymentConsents(anyObject(), anyObject()))
				.thenReturn(response);

		Mockito.when(paymemtRequestAdapter.postConsentProcessFlows(anyObject(), anyObject(), anyObject(), anyObject()))
				.thenReturn(response);

		service.createDomesticScheduledPaymentConsentsResource(request);
		assertNotNull(resource.getPaymentConsentId());
	}

	@Test(expected = PSD2Exception.class)
	public void testCreateDomesticScheduledPaymentConsentsResource_WithPaymentId_FalseDebtorDetails_ComparatorException() {
		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();
		resource.setPaymentConsentId("1458");
		resource.setCreatedAt("2019-08-06T06:06:06.666+00:00");

		CustomDSPConsentsPOSTResponse response = new CustomDSPConsentsPOSTResponse();
		response.setData(new OBWriteDataDomesticScheduledConsentResponse1());
		response.getData().setConsentId("14563");
		response.setConsentPorcessStatus(ProcessConsentStatusEnum.PASS);

		CustomDSPConsentsPOSTRequest request = new CustomDSPConsentsPOSTRequest();
		request.setData(new OBWriteDataDomesticScheduledConsent1());
		request.getData().setInitiation(new OBDomesticScheduled1());
		request.getData().getInitiation().setDebtorAccount(null);

		CustomPaymentSetupPlatformDetails platformDetails = new CustomPaymentSetupPlatformDetails();
		platformDetails.setTppDebtorDetails("false");
		platformDetails.setTppDebtorNameDetails("false");

		Mockito.when(paymemtRequestAdapter.preConsentProcessFlows(anyObject(), anyObject())).thenReturn(resource);

		Mockito.when(dspStagingAdapter.retrieveStagedDomesticScheduledPaymentConsents(anyObject(), anyObject()))
				.thenReturn(response);

		Mockito.when(paymemtRequestAdapter.postConsentProcessFlows(anyObject(), anyObject(), anyObject(), anyObject()))
				.thenReturn(response);

		Mockito.when(dScheduledPaymentConsentsComparator.compareScheduledPaymentDetails(response, request, resource))
				.thenReturn(1);
		service.createDomesticScheduledPaymentConsentsResource(request);
		assertNotNull(resource.getPaymentConsentId());
		assertEquals(1,
				dScheduledPaymentConsentsComparator.compareScheduledPaymentDetails(response, request, resource));
	}

	@Test
	public void testRetrieveScheduledDomesticPaymentConsentsResource() {
		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();
		resource.setSetupCmaVersion(PSD2Constants.CMAVERSION);

		CustomDSPConsentsPOSTResponse response = new CustomDSPConsentsPOSTResponse();
		response.setData(new OBWriteDataDomesticScheduledConsentResponse1());
		response.getData().setConsentId("14563");

		Mockito.when(paymemtRequestAdapter.preConsentProcessGETFlow(anyObject())).thenReturn(resource);

		Mockito.when(dspStagingAdapter.retrieveStagedDomesticScheduledPaymentConsents(anyObject(), anyObject()))
				.thenReturn(response);

		Mockito.when(paymemtRequestAdapter.postConsentProcessFlows(anyObject(), anyObject(), anyObject(), anyObject()))
				.thenReturn(response);

		service.retrieveDomesticScheduledPaymentConsentsResource(anyString());
		assertNotNull(resource.getSetupCmaVersion());
	}

	@Test
	public void testRetrieveScheduledDomesticPaymentConsentsResource_NullCMAVersion() {
		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();
		resource.setSetupCmaVersion(null);

		CustomDSPConsentsPOSTResponse response = new CustomDSPConsentsPOSTResponse();
		response.setData(new OBWriteDataDomesticScheduledConsentResponse1());
		response.getData().setConsentId("14563");

		Mockito.when(paymemtRequestAdapter.preConsentProcessGETFlow(anyObject())).thenReturn(resource);

		Mockito.when(dspStagingAdapter.retrieveStagedDomesticScheduledPaymentConsents(anyObject(), anyObject()))
				.thenReturn(response);

		Mockito.when(paymemtRequestAdapter.postConsentProcessFlows(anyObject(), anyObject(), anyObject(), anyObject()))
				.thenReturn(response);

		service.retrieveDomesticScheduledPaymentConsentsResource(anyString());
		assertNull(resource.getSetupCmaVersion());
	}

	@Test(expected = PSD2Exception.class)
	public void testRetrieveScheduledDomesticPaymentConsentsResource_Exception() {
		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();
		resource.setSetupCmaVersion(PSD2Constants.CMAVERSION);

		CustomDSPConsentsPOSTResponse response = new CustomDSPConsentsPOSTResponse();
		response.setData(new OBWriteDataDomesticScheduledConsentResponse1());
		response.getData().setConsentId("14563");

		Mockito.when(paymemtRequestAdapter.preConsentProcessGETFlow(anyObject())).thenReturn(resource);

		Mockito.when(dspStagingAdapter.retrieveStagedDomesticScheduledPaymentConsents(anyObject(), anyObject()))
				.thenReturn(null);

		Mockito.when(paymemtRequestAdapter.postConsentProcessFlows(anyObject(), anyObject(), anyObject(), anyObject()))
				.thenReturn(response);

		service.retrieveDomesticScheduledPaymentConsentsResource(anyString());
		assertNotNull(resource.getSetupCmaVersion());
		assertNull(dspStagingAdapter.retrieveStagedDomesticScheduledPaymentConsents(anyObject(), anyObject()));
	}

	@After
	public void tearDown() {
		dScheduledPaymentConsentsComparator = null;
		dspStagingAdapter = null;
		paymemtRequestAdapter = null;
		service = null;
	}
}