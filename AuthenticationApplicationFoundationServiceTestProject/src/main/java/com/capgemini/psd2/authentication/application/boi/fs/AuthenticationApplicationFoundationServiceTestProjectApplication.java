package com.capgemini.psd2.authentication.application.boi.fs;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.AuthenticationApplicationFoundationServiceAdapter;


@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
@ComponentScan("com.capgemini.psd2")
@EnableMongoRepositories("com.capgemini.psd2")
public class AuthenticationApplicationFoundationServiceTestProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuthenticationApplicationFoundationServiceTestProjectApplication.class, args);
	}
}

@RestController
@ResponseBody
class TestAuthenticationApplicationFSAdapter {


	@Autowired
	AuthenticationApplicationFoundationServiceAdapter adapter;

	@RequestMapping("/testAuthenticationApplicationAdapter")
	public String getResponse(@RequestHeader Map<String, String> params) {

		PrincipalImpl principal = new PrincipalImpl("12345");
		CredentialsImpl credentials = new CredentialsImpl("123456");
		
		AuthenticationImpl authentication = new AuthenticationImpl(principal, credentials);
		adapter.authenticate(authentication, params);
		return "Authentication sucessful";

	}
}