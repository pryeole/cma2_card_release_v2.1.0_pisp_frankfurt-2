package com.capgemini.psd2.validate.pre.stage.payment.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;

import com.capgemini.psd2.foundationservice.payment.submission.boi.adapter.client.PaymentSubmissionFoundationServiceClientImpl;
import com.capgemini.psd2.foundationservice.payment.submission.boi.adapter.domain.PaymentInstruction;
import com.capgemini.psd2.foundationservice.payment.submission.boi.adapter.domain.ValidationPassed;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;

public class PaymentSubmissionFoundationServiceClientTest {

	@InjectMocks
	private PaymentSubmissionFoundationServiceClientImpl paymentSubmissionFoundationServiceClientImpl;
	
	@Mock
	private RestClientSync restClient;
	
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
		
		/**
		 * Context loads.
		 */
	@Test
	public void contextLoads() {
		
	}
	

	@Test
	public void testExecutePaymentSubmission() {
		
		ValidationPassed validationPassed = new ValidationPassed();
        HttpHeaders headers = new HttpHeaders();
        RequestInfo reqInfo = new RequestInfo();
        PaymentInstruction paymentInstruction = new PaymentInstruction();
        
        Mockito.when(restClient.callForPost(anyObject(), anyObject(), anyObject(), anyObject())).thenReturn(validationPassed);
        
        validationPassed = paymentSubmissionFoundationServiceClientImpl.executePaymentSubmission(reqInfo, paymentInstruction, ValidationPassed.class, headers);
        assertNotNull(validationPassed);
	}

}
