package com.capgemini.psd2.pisp.payments.standing.order.service;

import com.capgemini.psd2.pisp.domain.CustomDStandingOrderPOSTRequest;
import com.capgemini.psd2.pisp.domain.DStandingOrderPOST201Response;

public interface DomesticStandingOrderService {

	public DStandingOrderPOST201Response createDomesticStandingOrderResource(CustomDStandingOrderPOSTRequest request);
	
	public DStandingOrderPOST201Response retrieveDomesticStandingOrderResource(String consentId);
}
