package com.capgemini.psd2.pisp.dsp.test.routing;

import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.DomesticScheduledPaymentsAdapter;
import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.GenericPaymentsResponse;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.dsp.routing.DSPaymentsRoutingAdapterImpl;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.token.Token;

@RunWith(SpringJUnit4ClassRunner.class)
public class DSPaymentsRoutingAdapterImplTest {

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Mock
	private ApplicationContext applicationContext;

	@InjectMocks
	private DSPaymentsRoutingAdapterImpl adapter;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testProcessDSPayments() {
		CustomDSPaymentsPOSTRequest request = new CustomDSPaymentsPOSTRequest();
		Map<String, OBExternalStatus1Code> paymentStatusMap = new HashMap<>();

		Map<String, String> params = new HashMap<>();

		Token token = new Token();
		token.setSeviceParams(params);

		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(token);
		params.put(PSD2Constants.USER_IN_REQ_HEADER,
				reqHeaderAtrributes.getToken().getSeviceParams().get(PSD2Constants.CHANNEL_NAME));

		DomesticScheduledPaymentsAdapter paymentsAdapter = new DomesticScheduledPaymentsAdapter() {

			@Override
			public CustomDSPaymentsPOSTResponse retrieveStagedDSPaymentsResponse(
					CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
				return null;
			}

			@Override
			public GenericPaymentsResponse processDSPayments(CustomDSPaymentsPOSTRequest customRequest,
					Map<String, OBExternalStatus1Code> paymentStatusMap, Map<String, String> params) {
				return null;
			}
		};

		when(applicationContext.getBean(anyString())).thenReturn(paymentsAdapter);
		adapter.processDSPayments(request, paymentStatusMap, new HashMap<>());
	}

	@Test
	public void testRetrieveStagedDSPaymentsResponseNullParams() {

		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();

		Map<String, String> params = null;

		Token token = new Token();
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(token);

		DomesticScheduledPaymentsAdapter paymentsAdapter = new DomesticScheduledPaymentsAdapter() {

			@Override
			public CustomDSPaymentsPOSTResponse retrieveStagedDSPaymentsResponse(
					CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
				return null;
			}

			@Override
			public GenericPaymentsResponse processDSPayments(CustomDSPaymentsPOSTRequest customRequest,
					Map<String, OBExternalStatus1Code> paymentStatusMap, Map<String, String> params) {
				return null;
			}
		};

		when(applicationContext.getBean(anyString())).thenReturn(paymentsAdapter);
		adapter.retrieveStagedDSPaymentsResponse(stageIdentifiers, params);
		assertNull(params);
	}
	
	@Test
	public void testRetrieveStagedDSPaymentsResponse() {

		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();

		Map<String, String> params = new HashMap<>();

		DomesticScheduledPaymentsAdapter paymentsAdapter = new DomesticScheduledPaymentsAdapter() {

			@Override
			public CustomDSPaymentsPOSTResponse retrieveStagedDSPaymentsResponse(
					CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
				return null;
			}

			@Override
			public GenericPaymentsResponse processDSPayments(CustomDSPaymentsPOSTRequest customRequest,
					Map<String, OBExternalStatus1Code> paymentStatusMap, Map<String, String> params) {
				return null;
			}
		};

		when(applicationContext.getBean(anyString())).thenReturn(paymentsAdapter);
		adapter.retrieveStagedDSPaymentsResponse(stageIdentifiers, params);
	}

	@Test
	public void testSetApplicationContext() {
		adapter.setApplicationContext(new ApplicationContextMock());
	}

	@Test
	public void contextLoads() {
	}

	@After
	public void tearDown() {
		reqHeaderAtrributes = null;
		applicationContext = null;
		adapter = null;
	}
}
