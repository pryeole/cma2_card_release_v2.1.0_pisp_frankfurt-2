package com.capgemini.psd2.pisp.dsp.test.comparator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBDomesticScheduled1;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticScheduled1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticScheduledConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticScheduledConsentResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.dsp.comparator.DSPaymentsPayloadComparator;

@RunWith(SpringJUnit4ClassRunner.class)
public class DSPaymentsPayloadComparatorTest {

	@InjectMocks
	private DSPaymentsPayloadComparator comparator;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void compareTestValue0() {
		CustomDSPConsentsPOSTResponse paymentResponse = new CustomDSPConsentsPOSTResponse();
		CustomDSPConsentsPOSTResponse adaptedPaymentResponse = new CustomDSPConsentsPOSTResponse();

		paymentResponse.setData(new OBWriteDataDomesticScheduledConsentResponse1());
		OBDomesticScheduled1 initiation = new OBDomesticScheduled1();
		paymentResponse.getData().setInitiation(initiation);
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		initiation.setInstructedAmount(instructedAmount);
		instructedAmount.setAmount("20.0");
		paymentResponse.setRisk(new OBRisk1());

		adaptedPaymentResponse.setData(new OBWriteDataDomesticScheduledConsentResponse1());
		adaptedPaymentResponse.getData().setInitiation(initiation);

		adaptedPaymentResponse.setRisk(new OBRisk1());

		int i = comparator.compare(paymentResponse, adaptedPaymentResponse);
		assertEquals(0, i);
	}

	@Test
	public void compareTestValue1() {
		CustomDSPConsentsPOSTResponse paymentResponse = new CustomDSPConsentsPOSTResponse();
		CustomDSPConsentsPOSTResponse adaptedPaymentResponse = new CustomDSPConsentsPOSTResponse();

		paymentResponse.setData(new OBWriteDataDomesticScheduledConsentResponse1());
		OBDomesticScheduled1 initiation = new OBDomesticScheduled1();
		paymentResponse.getData().setInitiation(initiation);
		paymentResponse.getData().getInitiation().setLocalInstrument("LI");
		paymentResponse.setRisk(new OBRisk1());
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		initiation.setInstructedAmount(instructedAmount);
		instructedAmount.setAmount("20.0");

		adaptedPaymentResponse.setData(new OBWriteDataDomesticScheduledConsentResponse1());
		adaptedPaymentResponse.getData().setInitiation(initiation);
		adaptedPaymentResponse.getData().getInitiation().setLocalInstrument("LocalInstrument");
		adaptedPaymentResponse.setRisk(new OBRisk1());

		int i = comparator.compare(paymentResponse, adaptedPaymentResponse);
		assertEquals(0, i);
	}

	@Test
	public void comparePaymentDetails_TrueDebtorDetails() {

		CustomDSPConsentsPOSTResponse response = new CustomDSPConsentsPOSTResponse();
		CustomDSPConsentsPOSTRequest request = new CustomDSPConsentsPOSTRequest();

		response.setData(new OBWriteDataDomesticScheduledConsentResponse1());
		response.getData().setInitiation(new OBDomesticScheduled1());
		response.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		response.getData().getInitiation().getInstructedAmount().setAmount("142");
		response.getData().getInitiation().setDebtorAccount(new OBCashAccountDebtor3());
		response.getData().getInitiation().getDebtorAccount().setName("test");

		request.setData(new OBWriteDataDomesticScheduledConsent1());
		request.getData().setInitiation(new OBDomesticScheduled1());
		request.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		request.getData().getInitiation().getInstructedAmount().setAmount("142");
		request.getData().getInitiation().setDebtorAccount(new OBCashAccountDebtor3());
		request.getData().getInitiation().getDebtorAccount().setName("test1");

		OBRisk1 risk = new OBRisk1();
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		List<String> addressLine = new ArrayList<>();
		deliveryAddress.setAddressLine(addressLine);
		addressLine.add("dublin east");
		List<String> countrySubDivision = new ArrayList<>();
		countrySubDivision.add("james street");
		deliveryAddress.setCountrySubDivision("warner bros2");
		request.setRisk(risk);

		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();
		paymentSetupPlatformResource.setTppDebtorDetails("true");
		paymentSetupPlatformResource.setTppDebtorNameDetails("testdebtor");

		int i = comparator.comparePaymentDetails(response, request, paymentSetupPlatformResource);
		assertEquals(1, i);
	}

	@Test
	public void comparePaymentDetails_FalseDebtorDetails() {

		CustomDSPConsentsPOSTResponse response = new CustomDSPConsentsPOSTResponse();
		CustomDSPaymentsPOSTRequest request = new CustomDSPaymentsPOSTRequest();

		response.setData(new OBWriteDataDomesticScheduledConsentResponse1());
		response.getData().setInitiation(new OBDomesticScheduled1());
		response.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		response.getData().getInitiation().getInstructedAmount().setAmount("142");
		response.getData().getInitiation().setDebtorAccount(new OBCashAccountDebtor3());
		response.getData().getInitiation().getDebtorAccount().setName("test");

		request.setData(new OBWriteDataDomesticScheduled1());
		request.getData().setInitiation(new OBDomesticScheduled1());
		request.getData().getInitiation().setDebtorAccount(null);
		request.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		request.getData().getInitiation().getInstructedAmount().setAmount("142");
		request.getData().getInitiation().setRemittanceInformation(new OBRemittanceInformation1());
		request.getData().getInitiation().getRemittanceInformation().setReference("testReference");
		request.getData().getInitiation().getRemittanceInformation().setUnstructured("testunstructured");

		OBRisk1 risk = new OBRisk1();
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		deliveryAddress.setBuildingNumber("572");
		deliveryAddress.setCountry("Ireland");
		List<String> addressLine = new ArrayList<>();
		deliveryAddress.setAddressLine(addressLine);
		addressLine.add("warner bros");
		addressLine.add("warner bros2");
		List<String> countrySubDivision = new ArrayList<>();
		deliveryAddress.setCountrySubDivision("warner bros2");
		countrySubDivision.add("Dublin west");
		request.setRisk(risk);

		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();
		paymentSetupPlatformResource.setTppDebtorDetails("false");
		paymentSetupPlatformResource.setTppDebtorNameDetails("testdebtor");

		int i = comparator.comparePaymentDetails(response, request, paymentSetupPlatformResource);
		assertEquals(1, i);
	}

	@Test
	public void comparePaymentDetails_NullRemittance() {

		CustomDSPConsentsPOSTResponse response = new CustomDSPConsentsPOSTResponse();
		CustomDSPaymentsPOSTRequest request = new CustomDSPaymentsPOSTRequest();

		response.setData(new OBWriteDataDomesticScheduledConsentResponse1());
		response.getData().setInitiation(new OBDomesticScheduled1());
		response.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		response.getData().getInitiation().getInstructedAmount().setAmount("142");
		response.getData().getInitiation().setDebtorAccount(new OBCashAccountDebtor3());
		response.getData().getInitiation().getDebtorAccount().setName("test");
		response.setRisk(new OBRisk1());
		response.getRisk().setDeliveryAddress(new OBRisk1DeliveryAddress());
		List<String> addressLineResponse = new ArrayList<>();
		String addresses = "Line 0";
		addressLineResponse.add(addresses);
		response.getRisk().getDeliveryAddress().setAddressLine(addressLineResponse);

		request.setData(new OBWriteDataDomesticScheduled1());
		request.getData().setInitiation(new OBDomesticScheduled1());
		request.getData().getInitiation().setDebtorAccount(null);
		request.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		request.getData().getInitiation().getInstructedAmount().setAmount("142");
		request.getData().getInitiation().setRemittanceInformation(new OBRemittanceInformation1());
		request.getData().getInitiation().getRemittanceInformation().setReference("testReference");
		request.getData().getInitiation().getRemittanceInformation().setUnstructured("testunstructured");
		request.getData().getInitiation().setCreditorPostalAddress(new OBPostalAddress6());

		OBRisk1 risk = new OBRisk1();
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		deliveryAddress.setBuildingNumber("572");
		deliveryAddress.setCountry("Ireland");
		List<String> addressLine = new ArrayList<>();
		String address = "Line 1";
		addressLine.add(address);
		deliveryAddress.setAddressLine(addressLine);
		request.getData().getInitiation().getCreditorPostalAddress().setAddressLine(addressLine);
		List<String> countrySubDivision = new ArrayList<>();
		deliveryAddress.setCountrySubDivision("warner bros2");
		countrySubDivision.add("Dublin west");
		request.setRisk(risk);

		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();
		paymentSetupPlatformResource.setId("144");
		paymentSetupPlatformResource.setPaymentId("58923-001");
		paymentSetupPlatformResource.setPaymentConsentId("58923");
		paymentSetupPlatformResource.setSetupCmaVersion("3");
		String tppDebtorDetails = "false";
		paymentSetupPlatformResource.setTppDebtorDetails(tppDebtorDetails);
		paymentSetupPlatformResource.setTppDebtorNameDetails("Debtor");

		int i = comparator.comparePaymentDetails(response, request, paymentSetupPlatformResource);
		assertEquals(1, i);
	}

	@Test
	public void validateDebtorDetailsTest() {
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		OBDomesticScheduled1 responseInitiation = new OBDomesticScheduled1();
		OBDomesticScheduled1 requestInitiation = new OBDomesticScheduled1();
		PaymentConsentsPlatformResource PaymentConsentsPlatformResource = new PaymentConsentsPlatformResource();
		PaymentConsentsPlatformResource.setTppDebtorDetails("false");
		requestInitiation.setDebtorAccount(debtorAccount);
		boolean result = comparator.validateDebtorDetails(responseInitiation, requestInitiation,
				PaymentConsentsPlatformResource);
		assertThat(result).isEqualTo(false);
		requestInitiation.setDebtorAccount(null);
		responseInitiation.setDebtorAccount(null);
		result = comparator.validateDebtorDetails(responseInitiation, requestInitiation,
				PaymentConsentsPlatformResource);
		assertThat(result).isEqualTo(true);
	}

	@Test
	public void comparetest_nullAddressLine1() {

		CustomDSPConsentsPOSTRequest request = new CustomDSPConsentsPOSTRequest();
		OBRisk1 risk = new OBRisk1();
		OBWriteDataDomesticScheduledConsent1 data = new OBWriteDataDomesticScheduledConsent1();

		OBDomesticScheduled1 initiation = new OBDomesticScheduled1();
		data.setInitiation(initiation);
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("20.0");
		initiation.setInstructedAmount(instructedAmount);
		OBPostalAddress6 creditorPostalAddressD = new OBPostalAddress6();
		List<String> addressLineList = new ArrayList<String>();
		addressLineList.add("");
		creditorPostalAddressD.setAddressLine(addressLineList);
		initiation.setCreditorPostalAddress(creditorPostalAddressD);

		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		initiation.setCreditorAccount(creditorAccount);
		creditorAccount.setIdentification("08080021325698");
		creditorAccount.setName("ACME Inc");
		creditorAccount.setSecondaryIdentification("0002");

		request.setData(data);
		request.setRisk(risk);

		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		deliveryAddress.setBuildingNumber("572");
		deliveryAddress.setCountry("Ireland");
		List<String> addressLine = new ArrayList<>();
		addressLine.add("");
		deliveryAddress.setAddressLine(addressLine);
		String countrySubDivision = "Dublin west";
		deliveryAddress.setCountrySubDivision(countrySubDivision);

		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();
		paymentSetupPlatformResource.setId("144");
		paymentSetupPlatformResource.setPaymentId("58923-001");
		paymentSetupPlatformResource.setPaymentConsentId("58923");
		paymentSetupPlatformResource.setSetupCmaVersion("3");

		CustomDSPConsentsPOSTResponse response = new CustomDSPConsentsPOSTResponse();
		OBWriteDataDomesticScheduledConsentResponse1 data1 = new OBWriteDataDomesticScheduledConsentResponse1();
		response.setData(data1);
		OBRisk1 risk1 = new OBRisk1();
		response.setRisk(risk1);
		OBRisk1DeliveryAddress deliveryAddress1 = new OBRisk1DeliveryAddress();
		List<String> addressLine1 = null;
		deliveryAddress1.setAddressLine(addressLine1);
		risk1.setDeliveryAddress(deliveryAddress1);

		OBDomesticScheduled1 initiation1 = new OBDomesticScheduled1();
		data1.setInitiation(initiation1);
		initiation1.setCreditorPostalAddress(new OBPostalAddress6());
		initiation1.setInstructionIdentification("9999");
		OBDomestic1InstructedAmount instructedAmount1 = new OBDomestic1InstructedAmount();
		instructedAmount1.setAmount("20.0");
		initiation1.setInstructedAmount(instructedAmount1);
		OBCashAccountDebtor3 debtorAccount1 = new OBCashAccountDebtor3();
		initiation1.setDebtorAccount(debtorAccount1);
		debtorAccount1.setName("PSD2");

		comparator.comparePaymentDetails(response, request, paymentSetupPlatformResource);
	}
	
	@Test
	public void checkCreditorPostalAddress() {
		CustomDSPConsentsPOSTRequest request = new CustomDSPConsentsPOSTRequest();
		OBRisk1 risk = new OBRisk1();
		OBWriteDataDomesticScheduledConsent1 data = new OBWriteDataDomesticScheduledConsent1();

		OBDomesticScheduled1 initiation = new OBDomesticScheduled1();
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("123.223");
		initiation.setInstructedAmount(instructedAmount);
		initiation.setCreditorPostalAddress(null);
		initiation.setDebtorAccount(new OBCashAccountDebtor3());
		initiation.getDebtorAccount().setName("Test");
		data.setInitiation(initiation);
		
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		initiation.setCreditorAccount(creditorAccount);
		creditorAccount.setIdentification("08080021325698");
		creditorAccount.setName("ACME Inc");
		creditorAccount.setSecondaryIdentification("0002");

		request.setData(data);
		request.setRisk(risk);
		
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		deliveryAddress.setBuildingNumber("572");
		deliveryAddress.setCountry("Ireland");
		List<String> addressLine = new ArrayList<>();
		deliveryAddress.setAddressLine(addressLine);
		addressLine.add("warner bros");
		addressLine.add("warner bros2");
		String countrySubDivision = "Dublin west";
		deliveryAddress.setCountrySubDivision(countrySubDivision);
		
		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();
		paymentSetupPlatformResource.setId("144");
		paymentSetupPlatformResource.setPaymentId("58923-001");
		paymentSetupPlatformResource.setPaymentConsentId("58923");
		paymentSetupPlatformResource.setSetupCmaVersion("3");

		CustomDSPConsentsPOSTResponse response = new CustomDSPConsentsPOSTResponse();
		OBWriteDataDomesticScheduledConsentResponse1 data1 = new OBWriteDataDomesticScheduledConsentResponse1();
		response.setData(data1);
		response.setRisk(null);
		
		comparator.comparePaymentDetails(response, request, paymentSetupPlatformResource);
		
	}

	@Test
	public void comparetest_checkRemittanceInfo() {
		CustomDSPConsentsPOSTRequest request = new CustomDSPConsentsPOSTRequest();
		OBRisk1 risk = new OBRisk1();
		OBWriteDataDomesticScheduledConsent1 data = new OBWriteDataDomesticScheduledConsent1();

		OBDomesticScheduled1 initiation = new OBDomesticScheduled1();
		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("123.223");
		initiation.setInstructedAmount(instructedAmount);
		data.setInitiation(initiation);
		remittanceInformation.setReference(null);
		remittanceInformation.setUnstructured(null);
		initiation.setRemittanceInformation(remittanceInformation);

		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		initiation.setCreditorAccount(creditorAccount);
		creditorAccount.setIdentification("08080021325698");
		creditorAccount.setName("ACME Inc");
		creditorAccount.setSecondaryIdentification("0002");

		request.setData(data);
		request.setRisk(risk);

		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		deliveryAddress.setBuildingNumber("572");
		deliveryAddress.setCountry("Ireland");
		List<String> addressLine = new ArrayList<>();
		deliveryAddress.setAddressLine(addressLine);
		addressLine.add("warner bros");
		addressLine.add("warner bros2");
		String countrySubDivision = "Dublin west";
		deliveryAddress.setCountrySubDivision(countrySubDivision);

		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();
		paymentSetupPlatformResource.setId("144");
		paymentSetupPlatformResource.setPaymentId("58923-001");
		paymentSetupPlatformResource.setPaymentConsentId("58923");
		paymentSetupPlatformResource.setSetupCmaVersion("3");

		CustomDSPConsentsPOSTResponse response = new CustomDSPConsentsPOSTResponse();
		OBWriteDataDomesticScheduledConsentResponse1 data1 = new OBWriteDataDomesticScheduledConsentResponse1();
		response.setData(data1);

		OBRisk1 risk1 = new OBRisk1();
		response.setRisk(risk1);
		OBDomesticScheduled1 initiation1 = new OBDomesticScheduled1();
		OBDomestic1InstructedAmount instructedAmount1 = new OBDomestic1InstructedAmount();
		instructedAmount1.setAmount("123.223");
		initiation1.setInstructedAmount(instructedAmount1);
		initiation1.setRemittanceInformation(null);

		initiation1.setInstructionIdentification("9999");
		data1.setInitiation(initiation1);

		comparator.comparePaymentDetails(response, request, paymentSetupPlatformResource);
	}
	
	@After
	public void tearDown() {
		comparator = null;
	}
}