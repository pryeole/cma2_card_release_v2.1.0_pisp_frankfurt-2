package com.capgemini.psd2.pisp.file.payments.routing.adapter.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.FilePaymentsAdapter;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.domain.PaymentFileSubmitPOST201Response;
import com.capgemini.psd2.pisp.domain.PlatformFilePaymentsFileResponse;
import com.capgemini.psd2.pisp.file.payment.routing.adapter.routing.FPaymentsAdapterFactory;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component("filePaymentsStagingRoutingAdapter")
public class FilePaymentsRoutingAdapterImpl implements FilePaymentsAdapter {

	private FilePaymentsAdapter beanInstance;

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;
	
	/** The default adapter. */
	@Value("${app.fpaymentsStagingAdapter}")
	private String paymentSetupStagingAdapter;

	@Autowired
	private FPaymentsAdapterFactory factory;

	private FilePaymentsAdapter getFileRoutingAdapter() {
		if (beanInstance == null)
			beanInstance = factory.getFilePaymentsStagingInstance(paymentSetupStagingAdapter);
		return beanInstance;
	}

	@Override
	public PlatformFilePaymentsFileResponse downloadFilePaymentsReport(String paymentId) {
		return getFileRoutingAdapter().downloadFilePaymentsReport(paymentId);
	}

	@Override
	public CustomFilePaymentsPOSTResponse findFilePaymentsResourceStatus(
			CustomPaymentStageIdentifiers stageIdentifiers) {
		return getFileRoutingAdapter().findFilePaymentsResourceStatus(stageIdentifiers);
	}

	@Override
	public PaymentFileSubmitPOST201Response processPayments(CustomFilePaymentsPOSTRequest paymentConsentsRequest,
			Map<String, OBExternalStatus1Code> paymentStatusMap, Map<String, String> params) {
		return getFileRoutingAdapter().processPayments(paymentConsentsRequest, paymentStatusMap, addHeaderParams(params));
	}

	@Override
	public PaymentFileSubmitPOST201Response retrieveStagedFilePayments(
			CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
		return getFileRoutingAdapter().retrieveStagedFilePayments(customPaymentStageIdentifiers, addHeaderParams(params));
	}

	private Map<String, String> addHeaderParams(Map<String, String> mapParam) {
		/*
		 * Below code will be used when update service will be called from
		 * payment submission API. For submission flow -> update, PSUId and
		 * ChannelName are mandatory fields for FS
		 */
		if (NullCheckUtils.isNullOrEmpty(mapParam))
			mapParam = new HashMap<>();

		if (null != reqHeaderAtrributes.getToken() && null != reqHeaderAtrributes.getToken().getSeviceParams()) {
			mapParam.put(PSD2Constants.CHANNEL_IN_REQ_HEADER,
					reqHeaderAtrributes.getToken().getSeviceParams().get(PSD2Constants.CHANNEL_NAME));
		}
		mapParam.put(PSD2Constants.USER_IN_REQ_HEADER, reqHeaderAtrributes.getPsuId());
		mapParam.put(PSD2Constants.CORRELATION_REQ_HEADER, reqHeaderAtrributes.getCorrelationId());
		mapParam.put(PSD2Constants.TENANT_ID, reqHeaderAtrributes.getTenantId());
		return mapParam;
	}
}
