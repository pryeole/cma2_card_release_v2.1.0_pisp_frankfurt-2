package com.capgemini.psd2.pisp.file.payments.mongo.db.adapter.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Conditional;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.stereotype.Component;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.S3Object;
import com.capgemini.psd2.conditions.MongoDbMockCondition;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.FilePaymentsAdapter;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.FPaymentsFoundationResource;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBCharge1Amount;
import com.capgemini.psd2.pisp.domain.OBChargeBearerType1Code;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.OBMultiAuthorisation1;
import com.capgemini.psd2.pisp.domain.PaymentFileSubmitPOST201Response;
import com.capgemini.psd2.pisp.domain.PlatformFilePaymentsFileResponse;
import com.capgemini.psd2.pisp.enums.ProcessExecutionStatusEnum;
import com.capgemini.psd2.pisp.file.payments.mongo.db.adapter.config.PaymentAwsObject;
import com.capgemini.psd2.pisp.file.payments.mongo.db.adapter.repository.FPaymentsDataRepository;
import com.capgemini.psd2.pisp.file.payments.mongo.db.adapter.repository.FilePaymentsFoundationRepository;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.status.PaymentConstants;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.SandboxValidationConfig;
import com.capgemini.psd2.utilities.SandboxValidationUtility;

@Conditional(MongoDbMockCondition.class)
@Component("filePaymentsStagingMongoDbAdapter")
public class FilePaymentsMongoDbAdapterImpl implements FilePaymentsAdapter {

	@Value("${app.isSandboxEnabled:false}")
	private boolean isSandboxEnabled;

	@Autowired
	private FPaymentsDataRepository fPaymentsDataRepository;

	@Autowired
	private ClientConfiguration clientConfig;

	@Autowired
	private FilePaymentsFoundationRepository fPaymentsFoundationRepository;

	@Autowired
	private SandboxValidationUtility utility;

	@Autowired
	private PaymentAwsObject awsObject;

	@Autowired
	private SandboxValidationConfig sandboxValidation;

	@Autowired
	private RequestHeaderAttributes reqAttributes;

	@Value("${app.awsRegion:null}")
	private String awsRegion;

	@Value("${app.awsS3BucketName:null}")
	private String awsS3BucketName;

	@Value("${app.awsS3BucketFilePath:null}")
	private String awsS3BucketFilePath;

	@Value("${app.awsReportfileName:null}")
	private String awsReportfileName;

	@Value("${app.sandboxValidationPolicies.charges.borneByCreditor:null}")
	private String borneByCreditorRegex;

	@Value("${app.sandboxValidationPolicies.charges.borneByDebtor:null}")
	private String borneByDebtorRegex;

	@Value("${app.sandboxValidationPolicies.charges.followingServiceLevel:null}")
	private String followServiceLevelRegex;

	@Value("${app.sandboxValidationPolicies.charges.shared:null}")
	private String sharedRegex;

	@Value("${app.sandboxValidationPolicies.chargeAmount:null}")
	private String chargeAmount;

	private String decimalPlaces = "%.2f";

	@Override
	public PlatformFilePaymentsFileResponse downloadFilePaymentsReport(String paymentId) {

		try {

			PlatformFilePaymentsFileResponse platformFilePaymentsFileResponse = new PlatformFilePaymentsFileResponse();
			S3Object fullObject = null;

			AmazonS3 s3client = null;

			if (clientConfig != null) {
				s3client = awsObject.getPaymentAwsClient(null, clientConfig, awsRegion);

			} else {
				s3client = AmazonS3ClientBuilder.standard()
						.withCredentials(DefaultAWSCredentialsProviderChain.getInstance()).withRegion(awsRegion)
						.build();
			}
			fullObject = awsObject.getPaymentAwsObjectS3Object(s3client, awsS3BucketName, awsS3BucketFilePath);

			if (fullObject != null && fullObject.getObjectContent() != null && fullObject.getObjectMetadata() != null) {
				InputStream is = fullObject.getObjectContent();
				byte[] byteArray = getPDFByteArray(is);

				platformFilePaymentsFileResponse.setFileByteArray(byteArray);
				platformFilePaymentsFileResponse.setFileName(awsReportfileName);
			}
			return platformFilePaymentsFileResponse;
		} catch (Exception exception) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID, exception.getMessage()));
		}
	}

	private byte[] getPDFByteArray(InputStream stream) throws IOException {
		byte[] buffer = new byte[8192];
		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		int bytesRead;
		while ((bytesRead = stream.read(buffer)) != -1) {
			baos.write(buffer, 0, bytesRead);
		}
		return baos.toByteArray();
	}

	@Override
	public CustomFilePaymentsPOSTResponse findFilePaymentsResourceStatus(
			CustomPaymentStageIdentifiers stageIdentifiers) {
		try {
			return fPaymentsDataRepository.findOneByDataFilePaymentId(stageIdentifiers.getPaymentSubmissionId());
		} catch (Exception exp) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID, ErrorMapKeys.RESOURCE_NOT_FOUND));
		}
	}

	private List<OBCharge1> getMockedFilePaymentsCharges(String currency, String controlSum) {
		List<OBCharge1> charges = new ArrayList<>();
		OBCharge1 obCharge1 = new OBCharge1();
		OBChargeBearerType1Code obj = null;

		/*
		 * By default it is set to BORNEBYCREDITOR bcz its a mandatory field in response
		 * object
		 */
		obj = OBChargeBearerType1Code.BORNEBYCREDITOR;
		obCharge1.setChargeBearer(obj);

		if (Pattern.compile(borneByCreditorRegex).matcher(controlSum).find()) {
			obj = OBChargeBearerType1Code.BORNEBYCREDITOR;
		} else if (Pattern.compile(borneByDebtorRegex).matcher(controlSum).find()) {
			obj = OBChargeBearerType1Code.BORNEBYDEBTOR;
		} else if (Pattern.compile(followServiceLevelRegex).matcher(controlSum).find()) {
			obj = OBChargeBearerType1Code.FOLLOWINGSERVICELEVEL;
		} else if (Pattern.compile(sharedRegex).matcher(controlSum).find()) {
			obj = OBChargeBearerType1Code.SHARED;
		}

		if (obj != null) {

			double chargedAmount = Double.parseDouble(chargeAmount) / 100;
			double chargeLimit = Double.parseDouble(sandboxValidation.getChargeAmountLimit());
			Double d = Double.parseDouble(controlSum) * chargedAmount;
			String chargeAmount = null;
			if (d > chargeLimit) {
				chargeAmount = Double.toString(chargeLimit);
			} else {
				chargeAmount = String.format(decimalPlaces, d);
			}
			OBCharge1Amount amount = new OBCharge1Amount();
			amount.setAmount(chargeAmount);
			amount.setCurrency(sandboxValidation.getChargeCurrency());
			obCharge1.setAmount(amount);
			obCharge1.setType("Type1");
			obCharge1.setChargeBearer(obj);

			charges.add(obCharge1);
		}
		return charges;
	}

	@Override
	public PaymentFileSubmitPOST201Response processPayments(CustomFilePaymentsPOSTRequest customRequest,
			Map<String, OBExternalStatus1Code> paymentStatusMap, Map<String, String> params) {

		String paymentRequest = JSONUtilities.getJSONOutPutFromObject(customRequest);
		FPaymentsFoundationResource bankResponse = JSONUtilities.getObjectFromJSONString(
				PispUtilities.getObjectMapper(), paymentRequest, FPaymentsFoundationResource.class);

		String currency = (sandboxValidation != null && sandboxValidation.getChargeCurrency() != null)
				? sandboxValidation.getChargeCurrency()
				: "EUR";
		String initiationAmount = customRequest.getData().getInitiation().getControlSum().toString();

		// SandboxMocking for post
		if (utility.isValidAmount(initiationAmount, PSD2Constants.PROCESS_EXEC_INCOMPLETE)) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.SANDBOX_MOCKED_ERROR));
		}

		/* 900 Scenario */
		if (utility.isValidAmount(initiationAmount, PSD2Constants.CUT_OFF_DATE_EXCEPTION)) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(
					OBErrorCodeEnum.UK_OBIE_RULES_AFTERCUTOFFDATETIME, ErrorMapKeys.AFTER_CUTOFF_DATE));
		}

		String submissionId = null;

		ProcessExecutionStatusEnum processStatusEnum = utility.getMockedProcessExecStatus(currency, initiationAmount);
		OBMultiAuthorisation1 multiAuthorisation = utility.getMockedMultiAuthBlock(currency, initiationAmount);

		if (processStatusEnum == ProcessExecutionStatusEnum.PASS
				|| processStatusEnum == ProcessExecutionStatusEnum.FAIL)
			submissionId = UUID.randomUUID().toString();

		bankResponse.getData().setFilePaymentId(submissionId);
		bankResponse.getData().setCreationDateTime(customRequest.getCreatedOn());

		// removing charges block as this is not returned for BOI on domestic payment
		// APIs
		if (!isSandboxEnabled) {
			List<OBCharge1> charges = getMockedFilePaymentsCharges(currency, initiationAmount);
			bankResponse.getData().setCharges(charges);
		}

		bankResponse.setProcessExecutionStatus(processStatusEnum);

		bankResponse.getData().setMultiAuthorisation(multiAuthorisation);
		if (submissionId != null) {
			OBExternalStatus1Code status = calculateCMAStatus(processStatusEnum, multiAuthorisation, paymentStatusMap);
			bankResponse.getData().setStatus(status);
			bankResponse.getData().setStatusUpdateDateTime(customRequest.getCreatedOn());
		}

		try {

			fPaymentsFoundationRepository.save(bankResponse);
		} catch (DataAccessResourceFailureException exception) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, exception.getMessage()));
		}
		return bankResponse;
	}

	@Override
	public PaymentFileSubmitPOST201Response retrieveStagedFilePayments(
			CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
		PaymentFileSubmitPOST201Response response = new PaymentFileSubmitPOST201Response();
		FPaymentsFoundationResource filePaymentFoundationResponse = fPaymentsFoundationRepository
				.findOneByDataConsentId(customPaymentStageIdentifiers.getPaymentConsentId());
		// Sanbox Mocking For get
		if (reqAttributes.getMethodType().equals("GET") && utility.isValidAmount(
				filePaymentFoundationResponse.getData().getInitiation().getControlSum().toString(),
				PSD2Constants.SANDBOX_PISP_GET_MOCKING)) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.SANDBOX_MOCKED_ERROR));
		}
		response.setData(filePaymentFoundationResponse.getData());
		return response;
	}

	private OBExternalStatus1Code calculateCMAStatus(ProcessExecutionStatusEnum processExecutionStatusEnum,
			OBMultiAuthorisation1 multiAuthorisation, Map<String, OBExternalStatus1Code> paymentStatusMap) {

		OBExternalStatus1Code cmaStatus = paymentStatusMap.get(PaymentConstants.INITIAL);

		if (processExecutionStatusEnum == ProcessExecutionStatusEnum.FAIL)
			cmaStatus = paymentStatusMap.get(PaymentConstants.FAIL);
		else if (processExecutionStatusEnum == ProcessExecutionStatusEnum.PASS) {
			if (multiAuthorisation == null || multiAuthorisation.getStatus() == OBExternalStatus2Code.AUTHORISED)
				cmaStatus = paymentStatusMap.get(PaymentConstants.PASS_M_AUTH);
			else if (multiAuthorisation.getStatus() == OBExternalStatus2Code.AWAITINGFURTHERAUTHORISATION)
				cmaStatus = paymentStatusMap.get(PaymentConstants.PASS_M_AWAIT);
			else if (multiAuthorisation.getStatus() == OBExternalStatus2Code.REJECTED)
				cmaStatus = paymentStatusMap.get(PaymentConstants.PASS_M_REJECT);
		}
		return cmaStatus;
	}
}
