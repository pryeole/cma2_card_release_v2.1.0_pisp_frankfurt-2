package com.capgemini.psd2.pisp.file.payments.mongo.db.adapter.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.auth.BasicAWSCredentials;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;

@Configuration
public class FPaymentsConfiguration {

	@Bean
	public BasicAWSCredentials getAWSCredentials(
			@Value("${app.awsAccesskey:null}") String accessKey, @Value("${app.awsSecretkey:null}")String secretKey) throws Exception {
		
		BasicAWSCredentials awsCredentials = null;
		try {
			awsCredentials = new BasicAWSCredentials(accessKey, secretKey);
			return awsCredentials;

		}catch(Exception exception) {
			throw PSD2Exception.populatePSD2Exception(exception.getMessage(),
					ErrorCodeEnum.PISP_TECHNICAL_ERROR_CONSENT_UPDATION);
		}
		
	}

}
