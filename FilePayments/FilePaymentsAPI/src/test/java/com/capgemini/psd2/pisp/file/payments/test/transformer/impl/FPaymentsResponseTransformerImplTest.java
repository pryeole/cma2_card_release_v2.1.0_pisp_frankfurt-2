package com.capgemini.psd2.pisp.file.payments.test.transformer.impl;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.annotation.RequestMethod;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBDomestic1;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.OBFile1;
import com.capgemini.psd2.pisp.domain.OBMultiAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBTransactionIndividualStatus1Code;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticResponse1;
import com.capgemini.psd2.pisp.domain.OBWriteDataFile1;
import com.capgemini.psd2.pisp.domain.OBWriteDataFileResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentDomesticSubmitPOST201Response;
import com.capgemini.psd2.pisp.domain.PaymentFileSubmitPOST201Response;
import com.capgemini.psd2.pisp.domain.PaymentsPlatformResource;
import com.capgemini.psd2.pisp.file.payments.transformer.impl.FPaymentsResponseTransformerImpl;
import com.capgemini.psd2.pisp.payment.submission.platform.adapter.PaymentsPlatformAdapter;
import com.capgemini.psd2.pisp.utilities.PispDateUtility;

@RunWith(SpringJUnit4ClassRunner.class)
public class FPaymentsResponseTransformerImplTest {

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Mock
	private PaymentsPlatformAdapter paymentsPlatformAdapter;

	@Mock
	private PispDateUtility pispDateUtility;

	private MockMvc mockMvc;

	@InjectMocks
	private FPaymentsResponseTransformerImpl transformer;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(transformer).dispatchOptions(true).build();
	}

	@Test
	public void contextLoads() {

	}

	@Test
	public void testPaymentSubmissionResponseTransformer() throws Exception {

		PaymentFileSubmitPOST201Response paymentSubmissionResponse = new PaymentFileSubmitPOST201Response();
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<String, Object>();

		PaymentsPlatformResource platResource = new PaymentsPlatformResource();
		platResource.setCreatedAt("");
		platResource.setStatus(OBExternalStatus1Code.INITIATIONPENDING.toString());
		platResource.setStatusUpdateDateTime("");
		OBWriteDataFileResponse1 data = new OBWriteDataFileResponse1();
		paymentSubmissionResponse.setData(data);
		paymentSubmissionResponse.getData().setCreationDateTime("");
		paymentSubmissionResponse.getData().setStatus(OBExternalStatus1Code.INITIATIONCOMPLETED);
		paymentSubmissionResponse.getData().setStatusUpdateDateTime("");
		paymentSubmissionResponse.getData().setMultiAuthorisation(new OBMultiAuthorisation1());
		OBFile1 initiation = new OBFile1();
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setSchemeName("SortCodeAccountNumber");
		initiation.setDebtorAccount(debtorAccount);
		paymentSubmissionResponse.getData().setInitiation(initiation);
		PaymentConsentsPlatformResource paymentConsentsPlatformResource = new PaymentConsentsPlatformResource();
		paymentConsentsPlatformResource.setTppDebtorDetails(Boolean.FALSE.toString());
		paymentsPlatformResourceMap.put("submission", platResource);
		paymentsPlatformResourceMap.put("consent", paymentConsentsPlatformResource);

		transformer.paymentSubmissionResponseTransformer(paymentSubmissionResponse, paymentsPlatformResourceMap,
				RequestMethod.POST.toString());
	}

	@Test
	public void testPaymentSubmissionResponseTransformerGetStatus() throws Exception {

		PaymentFileSubmitPOST201Response paymentSubmissionResponse = new PaymentFileSubmitPOST201Response();
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<String, Object>();

		PaymentsPlatformResource platResource = new PaymentsPlatformResource();
		platResource.setCreatedAt("");
		platResource.setStatus(OBExternalStatus1Code.INITIATIONPENDING.toString());
		platResource.setStatusUpdateDateTime("");
		OBWriteDataFileResponse1 data = new OBWriteDataFileResponse1();
		paymentSubmissionResponse.setData(data);
		paymentSubmissionResponse.getData().setCreationDateTime("");
		paymentSubmissionResponse.getData().setStatus(OBExternalStatus1Code.INITIATIONCOMPLETED);
		paymentSubmissionResponse.getData().setStatusUpdateDateTime("");
		paymentSubmissionResponse.getData().setMultiAuthorisation(new OBMultiAuthorisation1());
		OBFile1 initiation = new OBFile1();
		paymentSubmissionResponse.getData().setInitiation(initiation);
		PaymentConsentsPlatformResource paymentConsentsPlatformResource = new PaymentConsentsPlatformResource();
		paymentConsentsPlatformResource.setTppDebtorDetails(Boolean.FALSE.toString());
		paymentsPlatformResourceMap.put("submission", platResource);
		paymentsPlatformResourceMap.put("consent", paymentConsentsPlatformResource);

		transformer.paymentSubmissionResponseTransformer(paymentSubmissionResponse, paymentsPlatformResourceMap,
				RequestMethod.GET.toString());
	}

	@Test
	public void testPaymentSubmissionResponseTransformerGetAuth() throws Exception {

		PaymentFileSubmitPOST201Response paymentSubmissionResponse = new PaymentFileSubmitPOST201Response();
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<String, Object>();
		OBFile1 initiation = new OBFile1();
		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		PaymentsPlatformResource platResource = new PaymentsPlatformResource();
		OBWriteDataFileResponse1 data = new OBWriteDataFileResponse1();
		PaymentConsentsPlatformResource paymentConsentsPlatformResource = new PaymentConsentsPlatformResource();

		platResource.setCreatedAt("");
		platResource.setStatus(OBExternalStatus1Code.INITIATIONPENDING.toString());
		platResource.setStatusUpdateDateTime("");
		paymentSubmissionResponse.setData(data);
		paymentSubmissionResponse.getData().setCreationDateTime("");
		paymentSubmissionResponse.getData().setStatus(OBExternalStatus1Code.INITIATIONPENDING);
		paymentSubmissionResponse.getData().setStatusUpdateDateTime("");

		multiAuthorisation.setStatus(OBExternalStatus2Code.AUTHORISED);
		paymentSubmissionResponse.getData().setMultiAuthorisation(multiAuthorisation);
		paymentSubmissionResponse.getData().setInitiation(initiation);
		paymentConsentsPlatformResource.setTppDebtorDetails(Boolean.FALSE.toString());
		paymentsPlatformResourceMap.put("submission", platResource);
		paymentsPlatformResourceMap.put("consent", paymentConsentsPlatformResource);

		when(paymentsPlatformAdapter.updatePaymentsPlatformResource(anyObject())).thenReturn(platResource);

		transformer.paymentSubmissionResponseTransformer(paymentSubmissionResponse, paymentsPlatformResourceMap,
				RequestMethod.GET.toString());
	}

	@Test
	public void testPaymentSubmissionResponseTransformerGetReject() throws Exception {

		PaymentFileSubmitPOST201Response paymentSubmissionResponse = new PaymentFileSubmitPOST201Response();
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<String, Object>();
		OBFile1 initiation = new OBFile1();
		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		PaymentsPlatformResource platResource = new PaymentsPlatformResource();
		OBWriteDataFileResponse1 data = new OBWriteDataFileResponse1();
		PaymentConsentsPlatformResource paymentConsentsPlatformResource = new PaymentConsentsPlatformResource();

		platResource.setCreatedAt("");
		platResource.setStatus(OBExternalStatus1Code.INITIATIONPENDING.toString());
		platResource.setStatusUpdateDateTime("");
		paymentSubmissionResponse.setData(data);
		paymentSubmissionResponse.getData().setCreationDateTime("");
		paymentSubmissionResponse.getData().setStatus(OBExternalStatus1Code.INITIATIONPENDING);
		paymentSubmissionResponse.getData().setStatusUpdateDateTime("");

		multiAuthorisation.setStatus(OBExternalStatus2Code.REJECTED);
		paymentSubmissionResponse.getData().setMultiAuthorisation(multiAuthorisation);
		paymentSubmissionResponse.getData().setInitiation(initiation);
		paymentConsentsPlatformResource.setTppDebtorDetails(Boolean.FALSE.toString());
		paymentsPlatformResourceMap.put("submission", platResource);
		paymentsPlatformResourceMap.put("consent", paymentConsentsPlatformResource);

		when(paymentsPlatformAdapter.updatePaymentsPlatformResource(anyObject())).thenReturn(platResource);

		transformer.paymentSubmissionResponseTransformer(paymentSubmissionResponse, paymentsPlatformResourceMap,
				RequestMethod.GET.toString());
	}

	@Test
	public void testPaymentSubmissionRequestTransformer() throws Exception {
		CustomFilePaymentsPOSTRequest paymentSubmissionRequest = new CustomFilePaymentsPOSTRequest();
		OBWriteDataFile1 data = new OBWriteDataFile1();
		OBFile1 initiation = new OBFile1();
		initiation.setRequestedExecutionDateTime("2019-01-31T00:00:00");
		data.setInitiation(initiation);
		paymentSubmissionRequest.setData(data);
		when(pispDateUtility.transformDateTimeInRequest(anyString())).thenReturn("2019-01-31T00:00:00+00:00");
		transformer.paymentSubmissionRequestTransformer(paymentSubmissionRequest);
	}
}
