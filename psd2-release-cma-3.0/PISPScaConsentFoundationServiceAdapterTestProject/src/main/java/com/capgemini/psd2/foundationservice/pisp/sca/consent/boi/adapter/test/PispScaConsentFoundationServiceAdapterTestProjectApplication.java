package com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.test;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.PispScaConsentFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.constants.PispScaConsentFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.ChannelCode;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.FraudSystemResponse;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.PaymentConsentsValidationResponse;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.stage.domain.CustomConsentAppViewData;
import com.capgemini.psd2.pisp.stage.domain.CustomFraudSystemPaymentData;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageUpdateData;
import com.capgemini.psd2.pisp.stage.domain.CustomPreAuthorizeAdditionalInfo;

@SpringBootApplication
@ComponentScan("com.capgemini.psd2")
public class PispScaConsentFoundationServiceAdapterTestProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(PispScaConsentFoundationServiceAdapterTestProjectApplication.class, args);
	}
}




@RestController
@ResponseBody
class TestPispScaConsentController
{
	@Autowired
	private PispScaConsentFoundationServiceAdapter pispScaConsentFoundationServiceAdapter;
	
	
	@RequestMapping("/testPispScaConsentController")
	public CustomConsentAppViewData getData()
	{
		CustomPaymentStageIdentifiers customPaymentStageIdentifiers=new CustomPaymentStageIdentifiers();
		customPaymentStageIdentifiers.setPaymentConsentId("1234");
		customPaymentStageIdentifiers.setPaymentSetupVersion("v2.0");
		customPaymentStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		
		Map<String,String> params=new HashMap<String ,String>();
		 params.put("sourceSystemReqHeader", "PSD2API");
		    params.put("correlationMuleReqHeader", "2345678652002");
		    params.put("transactioReqHeader", "BOI999");
		    params.put("partysourceReqHeader", "platform");
		    params.put("sourceUserReqHeader", "87654321");
		    params.put("apiChannelCode","");
		return pispScaConsentFoundationServiceAdapter.retrieveConsentAppStagedViewData( customPaymentStageIdentifiers, params);
		
	}
	
	@RequestMapping("/testValidatePispScaConsentController")
	public PaymentConsentsValidationResponse validateData()
	{	
		
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentConsentId("58924");
		stageIdentifiers.setPaymentSetupVersion("v2.0");
		stageIdentifiers.setPaymentSubmissionId("12345");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		
		Map<String,String> params=new HashMap<String ,String>();
		 params.put("sourceSystemReqHeader", "PSD2API");
		    params.put("correlationMuleReqHeader", "");
		    params.put("transactioReqHeader", "BOI999");
		    params.put("partysourceReqHeader", "platform");
		    params.put("sourceUserReqHeader", "87654321");
		    params.put("apiChannelCode","BOI");
		    
		   OBCashAccountDebtor3 selectedDebtorDetails = new OBCashAccountDebtor3();
		   selectedDebtorDetails.setIdentification("validatorIdentification");
		   selectedDebtorDetails.setName("validatorName");
		   selectedDebtorDetails.setSchemeName("validatorSchemeName");
		   selectedDebtorDetails.setSecondaryIdentification("validatorSecondaryIdentification");
		   
		   CustomPreAuthorizeAdditionalInfo preAuthAdditionalInfo = new CustomPreAuthorizeAdditionalInfo();
		   preAuthAdditionalInfo.setAccountBic("02113sds");
		   preAuthAdditionalInfo.setAccountIban("123abc");
		   preAuthAdditionalInfo.setAccountNSC("abc123");
		   preAuthAdditionalInfo.setAccountNumber("abc123");
		   preAuthAdditionalInfo.setAccountPan("abc123");
		   preAuthAdditionalInfo.setPayerCurrency("payerCurrency");
		   preAuthAdditionalInfo.setPayerJurisdiction("payerJurisdiction");
		   
		return pispScaConsentFoundationServiceAdapter.validatePreAuthorisation(selectedDebtorDetails,
				 preAuthAdditionalInfo, stageIdentifiers ,params);
		
	}
	
	
	@RequestMapping("/testPispScaConsentControllerForUpdate")
    public void getDataForUpdate()
    {
          CustomPaymentStageIdentifiers customPaymentStageIdentifiers=new CustomPaymentStageIdentifiers();
          customPaymentStageIdentifiers.setPaymentConsentId("4ac7fd67-b8d6-46b5-9391-2e7305bed280");
          customPaymentStageIdentifiers.setPaymentSetupVersion("1.0");
          customPaymentStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);

          OBCashAccountDebtor3 debtorDetails = new OBCashAccountDebtor3();
          debtorDetails.setIdentification("12345678");
          debtorDetails.setName("Update");
          debtorDetails.setSchemeName("PISP");
          debtorDetails.setSecondaryIdentification("456789");

          CustomPaymentStageUpdateData updateData = new CustomPaymentStageUpdateData();
          Object fraud = FraudSystemResponse.APPROVE;
          updateData.setDebtorDetails(debtorDetails);
          updateData.setDebtorDetailsUpdated(true);
          updateData.setFraudScoreUpdated(true);
          updateData.setFraudScore(fraud);
          updateData.setSetupStatus("Authorised");
          updateData.setSetupStatusUpdated(true);
          updateData.setSetupStatusUpdateDateTime("2019-01-22T15:15:37+00:00");

          Map<String,String> params=new HashMap<String,String>();
          params.put("x-api-source-system", "PSD2API");
          params.put("x-api-correlation-id", "");
          params.put("x-api-transaction-id", "BOI999");
          params.put("x-api-party-source-id-number", "platform");
          params.put("x-api-source-user", "87654321");
          params.put("x-api-channel-code", "BOL");

          pispScaConsentFoundationServiceAdapter.updatePaymentStageData(customPaymentStageIdentifiers, updateData, params);

    }
	
	@RequestMapping("/testPispScheduledScaConsentControllerForUpdate")
    public void getDataScheduledForUpdate()
    {
          CustomPaymentStageIdentifiers customPaymentStageIdentifiers=new CustomPaymentStageIdentifiers();
          customPaymentStageIdentifiers.setPaymentConsentId("80179af0-cf31-459a-96c4-9947b59f05ee");
          customPaymentStageIdentifiers.setPaymentSetupVersion("1.0");
          customPaymentStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_SCH_PAY);

          OBCashAccountDebtor3 debtorDetails = new OBCashAccountDebtor3();
          debtorDetails.setIdentification("08080021325698");
          debtorDetails.setName("Update");
          debtorDetails.setSchemeName("UK.OBIE.SortCodeAccountNumber");
          debtorDetails.setSecondaryIdentification("456789");

          CustomPaymentStageUpdateData updateData = new CustomPaymentStageUpdateData();
          updateData.setDebtorDetails(debtorDetails);
          updateData.setDebtorDetailsUpdated(true);
          updateData.setFraudScoreUpdated(true);
          updateData.setSetupStatus("Authorised");
          updateData.setSetupStatusUpdated(true);
          updateData.setSetupStatusUpdateDateTime("2019-01-22T15:15:37+00:00");

          Map<String,String> params=new HashMap<String,String>();
          params.put("X-API-SOURCE-SYSTEM", "PSD2API");
          params.put("X-API-CORRELATION-ID", "");
          params.put("X-API-TRANSACTION-ID", "BOI999");
          params.put("X-API-PARTY-SOURCE-ID-NUMBER", "platform");
          params.put("X-API-SOURCE-USER", "87654321");
          params.put("X-API-CHANNEL-CODE", "BOL");

          pispScaConsentFoundationServiceAdapter.updatePaymentStageData(customPaymentStageIdentifiers, updateData, params);

    }

	@RequestMapping("/testValidateScheduledPispScaConsentController")
	public void validateScheduledData()
	{	
		 
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentConsentId("80179af0-cf31-459a-96c4-9947b59f05ee");
		stageIdentifiers.setPaymentSetupVersion("v1.0");
		stageIdentifiers.setPaymentSubmissionId("12345");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_SCH_PAY);
		
		Map<String,String> params=new HashMap<String ,String>();
		 params.put("sourceSystemReqHeader", "PSD2API");
		    params.put("correlationMuleReqHeader", "");
		    params.put("transactioReqHeader", "BOI999");
		    params.put("partysourceReqHeader", "platform");
		    params.put("sourceUserReqHeader", "87654321");
		    params.put("apiChannelCode","BOI");
		    
		   OBCashAccountDebtor3 selectedDebtorDetails = new OBCashAccountDebtor3();
		   selectedDebtorDetails.setIdentification("validatorIdentification");
		   selectedDebtorDetails.setName("validatorName");
		   selectedDebtorDetails.setSchemeName("validatorSchemeName");
		   selectedDebtorDetails.setSecondaryIdentification("validatorSecondaryIdentification");
		   
		   CustomPreAuthorizeAdditionalInfo preAuthAdditionalInfo = new CustomPreAuthorizeAdditionalInfo();
		   preAuthAdditionalInfo.setAccountBic("02113sds");
		   preAuthAdditionalInfo.setAccountIban("123abc");
		   preAuthAdditionalInfo.setAccountNSC("abc123");
		   preAuthAdditionalInfo.setAccountNumber("abc123");
		   preAuthAdditionalInfo.setAccountPan("abc123");
		   preAuthAdditionalInfo.setPayerCurrency("payerCurrency");
		   preAuthAdditionalInfo.setPayerJurisdiction("payerJurisdiction");
		   
		 pispScaConsentFoundationServiceAdapter.validatePreAuthorisation(selectedDebtorDetails,
				 preAuthAdditionalInfo, stageIdentifiers ,params);
		
	}
	
	@RequestMapping("/testPispScaConsentControllerForStandingOrder")
    public CustomConsentAppViewData getDataForStandingOrder()
    {
		CustomPaymentStageIdentifiers customPaymentStageIdentifiers=new CustomPaymentStageIdentifiers();
        customPaymentStageIdentifiers.setPaymentConsentId("26789");
        customPaymentStageIdentifiers.setPaymentSetupVersion("1.0");
        customPaymentStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_ST_ORD);
        Map<String,String> params=new HashMap<String,String>();
        params.put("x-api-source-system", "PSD2API");
        params.put("x-api-correlation-id", "");
        params.put("x-api-transaction-id", "BOI999");
        params.put("x-api-party-source-id-number", "platform");
        params.put("x-api-source-user", "87654321");
        params.put("x-api-channel-code", "BOL");
		
		return pispScaConsentFoundationServiceAdapter.retrieveConsentAppStagedViewData(customPaymentStageIdentifiers, params);
    }
	
	
	@RequestMapping("/testFraudSystemDomesticPaymentStagedData")
	public CustomFraudSystemPaymentData getDataForFraudSystemDomesticPaymentStagedData()
	{
		CustomPaymentStageIdentifiers customPaymentStageIdentifiers=new CustomPaymentStageIdentifiers();
		customPaymentStageIdentifiers.setPaymentConsentId("4ac7fd67-b8d6-46b5-9391-2e7305bed280");
		customPaymentStageIdentifiers.setPaymentSetupVersion("v1.0");
		customPaymentStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		
		Map<String,String> params=new HashMap<String ,String>();
		 params.put("sourceSystemReqHeader", "PSD2API");
		    params.put("correlationMuleReqHeader", "2345678652002");
		    params.put("transactioReqHeader", "BOI999");
		    params.put("partysourceReqHeader", "platform");
		    params.put("sourceUserReqHeader", "87654321");
		    params.put("apiChannelCode","");
		return pispScaConsentFoundationServiceAdapter.retrieveFraudSystemPaymentStagedData(customPaymentStageIdentifiers, params);
	}
	
	@RequestMapping("/testFraudSystemDomesticStandingOrderPaymentStagedData")
    public CustomFraudSystemPaymentData getFraudSystemDomesticStandingOrderPaymentStagedData()
    {
		CustomPaymentStageIdentifiers customPaymentStageIdentifiers=new CustomPaymentStageIdentifiers();
        customPaymentStageIdentifiers.setPaymentConsentId("123456");
        customPaymentStageIdentifiers.setPaymentSetupVersion("1.0");
        customPaymentStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_ST_ORD);
        Map<String,String> params=new HashMap<String,String>();
        params.put("x-api-source-system", "PSD2API");
        params.put("x-api-correlation-id", "");
        params.put("x-api-transaction-id", "BOI999");
        params.put("x-api-party-source-id-number", "platform");
        params.put("x-api-source-user", "87654321");
        params.put("x-api-channel-code", "BOL");
		
		return pispScaConsentFoundationServiceAdapter.retrieveFraudSystemPaymentStagedData(customPaymentStageIdentifiers, params);
    }
	
	@RequestMapping("/testFraudSystemDomesticSchedulePaymentStagedData")
    public CustomFraudSystemPaymentData getFraudSystemDomesticSchedulePaymentStagedData()
    {
		
		CustomPaymentStageIdentifiers customPaymentStageIdentifiers=new CustomPaymentStageIdentifiers();
        customPaymentStageIdentifiers.setPaymentConsentId("65e940da-c445-42c9-abd3-d360a5a1b0a2");
        customPaymentStageIdentifiers.setPaymentSetupVersion("1.0");
        customPaymentStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_SCH_PAY);
        Map<String,String> params=new HashMap<String,String>();
        params.put("x-api-source-system", "PSD2API");
        params.put("x-api-correlation-id", "");
        params.put("x-api-transaction-id", "BOI999");
        params.put("x-api-party-source-id-number", "platform");
        params.put("x-api-source-user", "87654321");
        params.put("x-api-channel-code", "BOL");
		
		return pispScaConsentFoundationServiceAdapter.retrieveFraudSystemPaymentStagedData(customPaymentStageIdentifiers, params);
    }
}


