package com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.CustomerAccountsFilterFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.ChannelProfile;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.AdapterFilterUtility;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Account;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountBrand;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountEntitlement;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.BrandCode;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUserProfile;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.EntitlementsAccount;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.CustomerAccountProfileFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.client.CustomerAccountProfileFoundationServiceClient;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.delegate.CustomerAccountProfileFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.transformer.CustomerAccountProfileFoundationServiceTransformer;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;
import com.capgemini.psd2.validator.PSD2Validator;

@RunWith(SpringJUnit4ClassRunner.class)
public class CustomerAccountProfileFoundationServiceAdapterTests {

	@InjectMocks
	private CustomerAccountProfileFoundationServiceAdapter customerAccProFoundServiceAdapter;

	@Mock
	private CustomerAccountProfileFoundationServiceDelegate cusAccProFoundServiceDelegate;

	@Mock 
	private CustomerAccountsFilterFoundationServiceAdapter commonFilterUtility;
	
	@Mock
	private CustomerAccountProfileFoundationServiceTransformer custAccntProfileSerTrans;
	
	@Mock
	private CustomerAccountProfileFoundationServiceClient custAccntProfileFouServiceClient;
	
	@Mock
	private AdapterFilterUtility adapterFilterUtility;
	
	@Mock
	private RestClientSyncImpl restClient;

	@Mock
	private PSD2Validator validator;
	/**
	 * Sets the up.
	 */
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}

	@Test(expected = AdapterException.class)
	public void paramsNullTest(){
		String userId = "test";
		customerAccProFoundServiceAdapter.retrieveCustomerAccountList(userId, null);
	}
	
	@Test(expected = AdapterException.class)
	public void userIdNullTest(){
		Map<String , String> params = new HashMap<>();
		customerAccProFoundServiceAdapter.retrieveCustomerAccountList(null, params);
	}
	
 	
	@Test
	public void getAccountNullTest(){
		Map<String , String> params = new HashMap<>();
		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(any(), any())).thenReturn(null);
		customerAccProFoundServiceAdapter.retrieveCustomerAccountList("test", params);
	}
	
	
	
	@Test
	public void testCustomerAccountInfoList1() {
	/** Test account list */
	/** Creating First Account */
		String userId = "test";
		Map<String , String> params = new HashMap<>();
		DigitalUserProfile filteraccount = new DigitalUserProfile();
		List<EntitlementsAccount> accountList = new ArrayList<>();
		EntitlementsAccount acen = new EntitlementsAccount();
		AccountBrand ab = new AccountBrand();
		BrandCode brandCode = BrandCode.fromValue("AA");
		ab.setBrandCode(brandCode);
		Account acc = new Account();
		acc.setAccountBrand(ab);
		acen.setAccount(acc);
		accountList.add(acen);
		DigitalUserProfile digiUserProfile = new DigitalUserProfile();
		digiUserProfile.setAccountEntitlements(accountList);
		String jurisdictionType = null;
		Mockito.when(adapterFilterUtility.prevalidateJurisdiction(digiUserProfile, jurisdictionType)).thenReturn(filteraccount);
		customerAccProFoundServiceAdapter.retrieveCustomerAccountList(userId, params);
	}
	
	@Test
	public void testCustomerAccountInfoList2() {
	/** Test account list */
	/** Creating First Account */
		String userId = "test";
		Map<String , String> params = new HashMap<>();
		DigitalUserProfile filteraccount = new DigitalUserProfile();
		EntitlementsAccount acen = new EntitlementsAccount();
		List<EntitlementsAccount> accountList = new ArrayList<EntitlementsAccount>();
		AccountBrand ab = new AccountBrand();
		BrandCode brandCode = BrandCode.fromValue("AA");
		ab.brandCode(brandCode);
		Account acc = new Account();
		acc.setAccountBrand(ab);
		acen.setAccount(acc);
		accountList.add(acen);
		DigitalUserProfile digiUserProfile = new DigitalUserProfile();
		digiUserProfile.setAccountEntitlements(accountList);
		String jurisdictionType = "abc";
		Mockito.when(adapterFilterUtility.prevalidateJurisdiction(digiUserProfile, jurisdictionType)).thenReturn(filteraccount);
		customerAccProFoundServiceAdapter.retrieveCustomerAccountList(userId, params);
	}
    
	@Test
	public void testCustomerAccountInfoList3() {
	/** Test account list */
	/** Creating First Account */
		String userId = "test";
		Map<String , String> params = new HashMap<>();
		DigitalUserProfile filteraccount = new DigitalUserProfile();
		EntitlementsAccount acen = new EntitlementsAccount();
		List<EntitlementsAccount> accountList = new ArrayList<EntitlementsAccount>();
		AccountBrand ab = new AccountBrand();
		BrandCode brandCode = BrandCode.fromValue("AA");
		ab.brandCode(brandCode);
		Account acc = new Account();
		acc.setAccountBrand(ab);
		acen.setAccount(acc);
		accountList.add(acen);
		DigitalUserProfile digiUserProfile = new DigitalUserProfile();
		digiUserProfile.setAccountEntitlements(accountList);
		String jurisdictionType = "abc";
		customerAccProFoundServiceAdapter.retrieveCustomerAccountList(userId, params);
		Mockito.when(adapterFilterUtility.prevalidateJurisdiction(digiUserProfile, jurisdictionType)).thenReturn(filteraccount);
	}
	
	@Test
	public void testCustomerAccountInfoList4() {
	/** Test account list */
	/** Creating First Account */
		String userId = "test";
		Map<String , String> params = new HashMap<>();
		DigitalUserProfile filteraccount = new DigitalUserProfile();
		EntitlementsAccount acen = new EntitlementsAccount();
		List<EntitlementsAccount> accountList = new ArrayList<EntitlementsAccount>();
		AccountBrand ab = new AccountBrand();
		BrandCode brandCode = BrandCode.fromValue("AA");
		ab.brandCode(brandCode);
		Account acc = new Account();
		acc.setAccountBrand(ab);
		acen.setAccount(acc);
		accountList.add(acen);
		DigitalUserProfile digiUserProfile = new DigitalUserProfile();
		digiUserProfile.setAccountEntitlements(accountList);
		String jurisdictionType = null;
		customerAccProFoundServiceAdapter.retrieveCustomerAccountList(userId, params);
		Mockito.when(adapterFilterUtility.prevalidateJurisdiction(digiUserProfile, jurisdictionType)).thenReturn(filteraccount);
	}
	
	@Test
	public void testCustomerAccountInfoFraudnetList() {
	/** Test account list 
	/** Creating First Account */
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt accnt = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
		accnt.setAccountNumber("acct8895");
		accnt.setAccountNSC("123456");
		accnt.setAccountName("darshan");
		accnt.setBic("SC802001");
		accnt.setIban("1022675");
		accnt.setCurrency("GBP");
		accnt.setAccountPermission("V");
		DigitalUserProfile filteraccount =new DigitalUserProfile();
		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(any(), any())).thenReturn(filteraccount);
		assertNotNull(filteraccount);
	}
    
	@Test
	public void testFilterAccountListNull() {
		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.USER_ID, "user");
		params.put(PSD2Constants.CHANNEL_ID, "channel");
		params.put(PSD2Constants.CORRELATION_ID, "correlation");
		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(any(), any())).thenReturn(null);
		customerAccProFoundServiceAdapter.retrieveCustomerInfo("BOI999", params);
	
	}
	@Test
	public void testFilterAccountListwithsuccess() {
		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.USER_ID, "user");
		params.put(PSD2Constants.CHANNEL_ID, "channel");
		params.put(PSD2Constants.CORRELATION_ID, "correlation");
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt accnt = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
		accnt.setAccountNumber("acct8895");
		accnt.setAccountNSC("123456");
		accnt.setAccountName("darshan");
		accnt.setBic("SC802001");
		accnt.setIban("1022675");
		accnt.setCurrency("GBP");
		accnt.setAccountPermission("V");
		DigitalUserProfile filteraccount =new DigitalUserProfile();
		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(any(), any())).thenReturn(filteraccount);
		Mockito.when(cusAccProFoundServiceDelegate.getFoundationServiceMuleURL(any(), any())).thenReturn("http://localhost:8082/fs-accountEnquiry-service/services/AccountEnquiry/business/nsc1234/acc1234/account");
		com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.raml.domain.DigitalUserProfile channelProfile=new com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.raml.domain.DigitalUserProfile();
		Mockito.when(custAccntProfileFouServiceClient.restTransportForParty(any(), any(), any())).thenReturn(channelProfile);
		customerAccProFoundServiceAdapter.retrieveCustomerInfo("BOI999", params);
	
	}
	@Test
	public void testFilterAccountListwithNull() {
		customerAccProFoundServiceAdapter.retrieveCustomerInfo("BOI999", null);
	}
	}
