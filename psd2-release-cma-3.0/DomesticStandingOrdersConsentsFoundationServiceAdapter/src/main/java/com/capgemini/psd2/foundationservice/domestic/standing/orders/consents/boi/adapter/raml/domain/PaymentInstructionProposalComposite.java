package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Payment Instruction-Proposal composite object
 */
@ApiModel(description = "Payment Instruction-Proposal composite object")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-01-16T16:32:31.691+05:30")

public class PaymentInstructionProposalComposite   {
  @JsonProperty("paymentInstructionProposal")
  private PaymentInstructionProposal paymentInstructionProposal = null;

  @JsonProperty("paymentInstruction")
  private PaymentInstruction paymentInstruction = null;

  public PaymentInstructionProposalComposite paymentInstructionProposal(PaymentInstructionProposal paymentInstructionProposal) {
    this.paymentInstructionProposal = paymentInstructionProposal;
    return this;
  }

  /**
   * Get paymentInstructionProposal
   * @return paymentInstructionProposal
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public PaymentInstructionProposal getPaymentInstructionProposal() {
    return paymentInstructionProposal;
  }

  public void setPaymentInstructionProposal(PaymentInstructionProposal paymentInstructionProposal) {
    this.paymentInstructionProposal = paymentInstructionProposal;
  }

  public PaymentInstructionProposalComposite paymentInstruction(PaymentInstruction paymentInstruction) {
    this.paymentInstruction = paymentInstruction;
    return this;
  }

  /**
   * Get paymentInstruction
   * @return paymentInstruction
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public PaymentInstruction getPaymentInstruction() {
    return paymentInstruction;
  }

  public void setPaymentInstruction(PaymentInstruction paymentInstruction) {
    this.paymentInstruction = paymentInstruction;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PaymentInstructionProposalComposite paymentInstructionProposalComposite = (PaymentInstructionProposalComposite) o;
    return Objects.equals(this.paymentInstructionProposal, paymentInstructionProposalComposite.paymentInstructionProposal) &&
        Objects.equals(this.paymentInstruction, paymentInstructionProposalComposite.paymentInstruction);
  }

  @Override
  public int hashCode() {
    return Objects.hash(paymentInstructionProposal, paymentInstruction);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PaymentInstructionProposalComposite {\n");
    
    sb.append("    paymentInstructionProposal: ").append(toIndentedString(paymentInstructionProposal)).append("\n");
    sb.append("    paymentInstruction: ").append(toIndentedString(paymentInstruction)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

