package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Standing Order Instruction Proposal
 */
@ApiModel(description = "Standing Order Instruction Proposal")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-01-16T16:32:31.691+05:30")

public class StandingOrderInstructionProposal   {
  @JsonProperty("paymentInstructionProposalId")
  private String paymentInstructionProposalId = null;

  @JsonProperty("proposalCreationDatetime")
  private OffsetDateTime proposalCreationDatetime = null;

  @JsonProperty("proposalStatus")
  private ProposalStatus proposalStatus = null;

  @JsonProperty("proposalStatusUpdateDatetime")
  private OffsetDateTime proposalStatusUpdateDatetime = null;

  @JsonProperty("authorisationType")
  private AuthorisationType authorisationType = null;

  @JsonProperty("authorisationDatetime")
  private OffsetDateTime authorisationDatetime = null;

  @JsonProperty("proposingParty")
  private Party proposingParty = null;

  @JsonProperty("proposingPartyPostalAddress")
  private PaymentInstructionPostalAddress proposingPartyPostalAddress = null;

  @JsonProperty("proposingPartyAccount")
  private ProposingPartyAccount proposingPartyAccount = null;

  @JsonProperty("authorisingParty")
  private Party authorisingParty = null;

  @JsonProperty("authorisingPartyAccount")
  private AuthorisingPartyAccount authorisingPartyAccount = null;

  @JsonProperty("paymentInstructionRiskFactorReference")
  private PaymentInstrumentRiskFactor paymentInstructionRiskFactorReference = null;

  @JsonProperty("paymentType")
  private PaymentType paymentType = null;

  @JsonProperty("charges")
  @Valid
  private List<PaymentInstructionCharge> charges = null;

  @JsonProperty("fraudSystemResponse")
  private FraudSystemResponse fraudSystemResponse = null;

  @JsonProperty("reference")
  private String reference = null;

  @JsonProperty("frequency")
  private String frequency = null;

  @JsonProperty("numberOfPayments")
  private Double numberOfPayments = null;

  @JsonProperty("firstPaymentDateTime")
  private OffsetDateTime firstPaymentDateTime = null;

  @JsonProperty("recurringPaymentDateTime")
  private OffsetDateTime recurringPaymentDateTime = null;

  @JsonProperty("finalPaymentDateTime")
  private OffsetDateTime finalPaymentDateTime = null;

  @JsonProperty("firstPaymentAmount")
  private PaymentTransaction firstPaymentAmount = null;

  @JsonProperty("recurringPaymentAmount")
  private PaymentTransaction recurringPaymentAmount = null;

  @JsonProperty("finalPaymentAmount")
  private PaymentTransaction finalPaymentAmount = null;

  @JsonProperty("permission")
  private String permission = "Create";

  public StandingOrderInstructionProposal paymentInstructionProposalId(String paymentInstructionProposalId) {
    this.paymentInstructionProposalId = paymentInstructionProposalId;
    return this;
  }

  /**
   * The identifier of the Payment Instruction Proposal in the Banking System
   * @return paymentInstructionProposalId
  **/
  @ApiModelProperty(required = true, value = "The identifier of the Payment Instruction Proposal in the Banking System")
  @NotNull

@Size(min=0,max=35) 
  public String getPaymentInstructionProposalId() {
    return paymentInstructionProposalId;
  }

  public void setPaymentInstructionProposalId(String paymentInstructionProposalId) {
    this.paymentInstructionProposalId = paymentInstructionProposalId;
  }

  public StandingOrderInstructionProposal proposalCreationDatetime(OffsetDateTime proposalCreationDatetime) {
    this.proposalCreationDatetime = proposalCreationDatetime;
    return this;
  }

  /**
   * The risk profile associated
   * @return proposalCreationDatetime
  **/
  @ApiModelProperty(required = true, value = "The risk profile associated")
  @NotNull

  @Valid

  public OffsetDateTime getProposalCreationDatetime() {
    return proposalCreationDatetime;
  }

  public void setProposalCreationDatetime(OffsetDateTime proposalCreationDatetime) {
    this.proposalCreationDatetime = proposalCreationDatetime;
  }

  public StandingOrderInstructionProposal proposalStatus(ProposalStatus proposalStatus) {
    this.proposalStatus = proposalStatus;
    return this;
  }

  /**
   * Get proposalStatus
   * @return proposalStatus
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public ProposalStatus getProposalStatus() {
    return proposalStatus;
  }

  public void setProposalStatus(ProposalStatus proposalStatus) {
    this.proposalStatus = proposalStatus;
  }

  public StandingOrderInstructionProposal proposalStatusUpdateDatetime(OffsetDateTime proposalStatusUpdateDatetime) {
    this.proposalStatusUpdateDatetime = proposalStatusUpdateDatetime;
    return this;
  }

  /**
   * Reflects the last time proposalStatus is updated.
   * @return proposalStatusUpdateDatetime
  **/
  @ApiModelProperty(value = "Reflects the last time proposalStatus is updated.")

  @Valid

  public OffsetDateTime getProposalStatusUpdateDatetime() {
    return proposalStatusUpdateDatetime;
  }

  public void setProposalStatusUpdateDatetime(OffsetDateTime proposalStatusUpdateDatetime) {
    this.proposalStatusUpdateDatetime = proposalStatusUpdateDatetime;
  }

  public StandingOrderInstructionProposal authorisationType(AuthorisationType authorisationType) {
    this.authorisationType = authorisationType;
    return this;
  }

  /**
   * Get authorisationType
   * @return authorisationType
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public AuthorisationType getAuthorisationType() {
    return authorisationType;
  }

  public void setAuthorisationType(AuthorisationType authorisationType) {
    this.authorisationType = authorisationType;
  }

  public StandingOrderInstructionProposal authorisationDatetime(OffsetDateTime authorisationDatetime) {
    this.authorisationDatetime = authorisationDatetime;
    return this;
  }

  /**
   * Moment on which the proposal was authorised.
   * @return authorisationDatetime
  **/
  @ApiModelProperty(value = "Moment on which the proposal was authorised.")

  @Valid

  public OffsetDateTime getAuthorisationDatetime() {
    return authorisationDatetime;
  }

  public void setAuthorisationDatetime(OffsetDateTime authorisationDatetime) {
    this.authorisationDatetime = authorisationDatetime;
  }

  public StandingOrderInstructionProposal proposingParty(Party proposingParty) {
    this.proposingParty = proposingParty;
    return this;
  }

  /**
   * Get proposingParty
   * @return proposingParty
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Party getProposingParty() {
    return proposingParty;
  }

  public void setProposingParty(Party proposingParty) {
    this.proposingParty = proposingParty;
  }

  public StandingOrderInstructionProposal proposingPartyPostalAddress(PaymentInstructionPostalAddress proposingPartyPostalAddress) {
    this.proposingPartyPostalAddress = proposingPartyPostalAddress;
    return this;
  }

  /**
   * Get proposingPartyPostalAddress
   * @return proposingPartyPostalAddress
  **/
  @ApiModelProperty(value = "")

  @Valid

  public PaymentInstructionPostalAddress getProposingPartyPostalAddress() {
    return proposingPartyPostalAddress;
  }

  public void setProposingPartyPostalAddress(PaymentInstructionPostalAddress proposingPartyPostalAddress) {
    this.proposingPartyPostalAddress = proposingPartyPostalAddress;
  }

  public StandingOrderInstructionProposal proposingPartyAccount(ProposingPartyAccount proposingPartyAccount) {
    this.proposingPartyAccount = proposingPartyAccount;
    return this;
  }

  /**
   * Get proposingPartyAccount
   * @return proposingPartyAccount
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public ProposingPartyAccount getProposingPartyAccount() {
    return proposingPartyAccount;
  }

  public void setProposingPartyAccount(ProposingPartyAccount proposingPartyAccount) {
    this.proposingPartyAccount = proposingPartyAccount;
  }

  public StandingOrderInstructionProposal authorisingParty(Party authorisingParty) {
    this.authorisingParty = authorisingParty;
    return this;
  }

  /**
   * Get authorisingParty
   * @return authorisingParty
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Party getAuthorisingParty() {
    return authorisingParty;
  }

  public void setAuthorisingParty(Party authorisingParty) {
    this.authorisingParty = authorisingParty;
  }

  public StandingOrderInstructionProposal authorisingPartyAccount(AuthorisingPartyAccount authorisingPartyAccount) {
    this.authorisingPartyAccount = authorisingPartyAccount;
    return this;
  }

  /**
   * Get authorisingPartyAccount
   * @return authorisingPartyAccount
  **/
  @ApiModelProperty(value = "")

  @Valid

  public AuthorisingPartyAccount getAuthorisingPartyAccount() {
    return authorisingPartyAccount;
  }

  public void setAuthorisingPartyAccount(AuthorisingPartyAccount authorisingPartyAccount) {
    this.authorisingPartyAccount = authorisingPartyAccount;
  }

  public StandingOrderInstructionProposal paymentInstructionRiskFactorReference(PaymentInstrumentRiskFactor paymentInstructionRiskFactorReference) {
    this.paymentInstructionRiskFactorReference = paymentInstructionRiskFactorReference;
    return this;
  }

  /**
   * Get paymentInstructionRiskFactorReference
   * @return paymentInstructionRiskFactorReference
  **/
  @ApiModelProperty(value = "")

  @Valid

  public PaymentInstrumentRiskFactor getPaymentInstructionRiskFactorReference() {
    return paymentInstructionRiskFactorReference;
  }

  public void setPaymentInstructionRiskFactorReference(PaymentInstrumentRiskFactor paymentInstructionRiskFactorReference) {
    this.paymentInstructionRiskFactorReference = paymentInstructionRiskFactorReference;
  }

  public StandingOrderInstructionProposal paymentType(PaymentType paymentType) {
    this.paymentType = paymentType;
    return this;
  }

  /**
   * Get paymentType
   * @return paymentType
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public PaymentType getPaymentType() {
    return paymentType;
  }

  public void setPaymentType(PaymentType paymentType) {
    this.paymentType = paymentType;
  }

  public StandingOrderInstructionProposal charges(List<PaymentInstructionCharge> charges) {
    this.charges = charges;
    return this;
  }

  public StandingOrderInstructionProposal addChargesItem(PaymentInstructionCharge chargesItem) {
    if (this.charges == null) {
      this.charges = new ArrayList<PaymentInstructionCharge>();
    }
    this.charges.add(chargesItem);
    return this;
  }

  /**
   * Get charges
   * @return charges
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<PaymentInstructionCharge> getCharges() {
    return charges;
  }

  public void setCharges(List<PaymentInstructionCharge> charges) {
    this.charges = charges;
  }

  public StandingOrderInstructionProposal fraudSystemResponse(FraudSystemResponse fraudSystemResponse) {
    this.fraudSystemResponse = fraudSystemResponse;
    return this;
  }

  /**
   * Get fraudSystemResponse
   * @return fraudSystemResponse
  **/
  @ApiModelProperty(value = "")

  @Valid

  public FraudSystemResponse getFraudSystemResponse() {
    return fraudSystemResponse;
  }

  public void setFraudSystemResponse(FraudSystemResponse fraudSystemResponse) {
    this.fraudSystemResponse = fraudSystemResponse;
  }

  public StandingOrderInstructionProposal reference(String reference) {
    this.reference = reference;
    return this;
  }

  /**
   * Payment Reference.
   * @return reference
  **/
  @ApiModelProperty(value = "Payment Reference.")

@Size(min=0,max=70) 
  public String getReference() {
    return reference;
  }

  public void setReference(String reference) {
    this.reference = reference;
  }

  public StandingOrderInstructionProposal frequency(String frequency) {
    this.frequency = frequency;
    return this;
  }

  /**
   * Get frequency
   * @return frequency
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

@Pattern(regexp="^(EvryDay)$|^(EvryWorkgDay)$|^(IntrvlWkDay:0[1-9]:0[1-7])$|^(WkInMnthDay:0[1-5]:0[1-7])$|^(IntrvlMnthDay:(0[1-6]|12|24):(-0[1-5]|0[1-9]|[12][0-9]|3[01]))$|^(QtrDay:(ENGLISH|SCOTTISH|RECEIVED))$") 
  public String getFrequency() {
    return frequency;
  }

  public void setFrequency(String frequency) {
    this.frequency = frequency;
  }

  public StandingOrderInstructionProposal numberOfPayments(Double numberOfPayments) {
    this.numberOfPayments = numberOfPayments;
    return this;
  }

  /**
   * Get numberOfPayments
   * @return numberOfPayments
  **/
  @ApiModelProperty(value = "")


  public Double getNumberOfPayments() {
    return numberOfPayments;
  }

  public void setNumberOfPayments(Double numberOfPayments) {
    this.numberOfPayments = numberOfPayments;
  }

  public StandingOrderInstructionProposal firstPaymentDateTime(OffsetDateTime firstPaymentDateTime) {
    this.firstPaymentDateTime = firstPaymentDateTime;
    return this;
  }

  /**
   * Get firstPaymentDateTime
   * @return firstPaymentDateTime
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public OffsetDateTime getFirstPaymentDateTime() {
    return firstPaymentDateTime;
  }

  public void setFirstPaymentDateTime(OffsetDateTime firstPaymentDateTime) {
    this.firstPaymentDateTime = firstPaymentDateTime;
  }

  public StandingOrderInstructionProposal recurringPaymentDateTime(OffsetDateTime recurringPaymentDateTime) {
    this.recurringPaymentDateTime = recurringPaymentDateTime;
    return this;
  }

  /**
   * Get recurringPaymentDateTime
   * @return recurringPaymentDateTime
  **/
  @ApiModelProperty(value = "")

  @Valid

  public OffsetDateTime getRecurringPaymentDateTime() {
    return recurringPaymentDateTime;
  }

  public void setRecurringPaymentDateTime(OffsetDateTime recurringPaymentDateTime) {
    this.recurringPaymentDateTime = recurringPaymentDateTime;
  }

  public StandingOrderInstructionProposal finalPaymentDateTime(OffsetDateTime finalPaymentDateTime) {
    this.finalPaymentDateTime = finalPaymentDateTime;
    return this;
  }

  /**
   * Get finalPaymentDateTime
   * @return finalPaymentDateTime
  **/
  @ApiModelProperty(value = "")

  @Valid

  public OffsetDateTime getFinalPaymentDateTime() {
    return finalPaymentDateTime;
  }

  public void setFinalPaymentDateTime(OffsetDateTime finalPaymentDateTime) {
    this.finalPaymentDateTime = finalPaymentDateTime;
  }

  public StandingOrderInstructionProposal firstPaymentAmount(PaymentTransaction firstPaymentAmount) {
    this.firstPaymentAmount = firstPaymentAmount;
    return this;
  }

  /**
   * Get firstPaymentAmount
   * @return firstPaymentAmount
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public PaymentTransaction getFirstPaymentAmount() {
    return firstPaymentAmount;
  }

  public void setFirstPaymentAmount(PaymentTransaction firstPaymentAmount) {
    this.firstPaymentAmount = firstPaymentAmount;
  }

  public StandingOrderInstructionProposal recurringPaymentAmount(PaymentTransaction recurringPaymentAmount) {
    this.recurringPaymentAmount = recurringPaymentAmount;
    return this;
  }

  /**
   * Get recurringPaymentAmount
   * @return recurringPaymentAmount
  **/
  @ApiModelProperty(value = "")

  @Valid

  public PaymentTransaction getRecurringPaymentAmount() {
    return recurringPaymentAmount;
  }

  public void setRecurringPaymentAmount(PaymentTransaction recurringPaymentAmount) {
    this.recurringPaymentAmount = recurringPaymentAmount;
  }

  public StandingOrderInstructionProposal finalPaymentAmount(PaymentTransaction finalPaymentAmount) {
    this.finalPaymentAmount = finalPaymentAmount;
    return this;
  }

  /**
   * Get finalPaymentAmount
   * @return finalPaymentAmount
  **/
  @ApiModelProperty(value = "")

  @Valid

  public PaymentTransaction getFinalPaymentAmount() {
    return finalPaymentAmount;
  }

  public void setFinalPaymentAmount(PaymentTransaction finalPaymentAmount) {
    this.finalPaymentAmount = finalPaymentAmount;
  }

  public StandingOrderInstructionProposal permission(String permission) {
    this.permission = permission;
    return this;
  }

  /**
   * Specifies the service request type
   * @return permission
  **/
  @ApiModelProperty(example = "Create", required = true, value = "Specifies the service request type")
  @NotNull


  public String getPermission() {
    return permission;
  }

  public void setPermission(String permission) {
    this.permission = permission;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    StandingOrderInstructionProposal standingOrderInstructionProposal = (StandingOrderInstructionProposal) o;
    return Objects.equals(this.paymentInstructionProposalId, standingOrderInstructionProposal.paymentInstructionProposalId) &&
        Objects.equals(this.proposalCreationDatetime, standingOrderInstructionProposal.proposalCreationDatetime) &&
        Objects.equals(this.proposalStatus, standingOrderInstructionProposal.proposalStatus) &&
        Objects.equals(this.proposalStatusUpdateDatetime, standingOrderInstructionProposal.proposalStatusUpdateDatetime) &&
        Objects.equals(this.authorisationType, standingOrderInstructionProposal.authorisationType) &&
        Objects.equals(this.authorisationDatetime, standingOrderInstructionProposal.authorisationDatetime) &&
        Objects.equals(this.proposingParty, standingOrderInstructionProposal.proposingParty) &&
        Objects.equals(this.proposingPartyPostalAddress, standingOrderInstructionProposal.proposingPartyPostalAddress) &&
        Objects.equals(this.proposingPartyAccount, standingOrderInstructionProposal.proposingPartyAccount) &&
        Objects.equals(this.authorisingParty, standingOrderInstructionProposal.authorisingParty) &&
        Objects.equals(this.authorisingPartyAccount, standingOrderInstructionProposal.authorisingPartyAccount) &&
        Objects.equals(this.paymentInstructionRiskFactorReference, standingOrderInstructionProposal.paymentInstructionRiskFactorReference) &&
        Objects.equals(this.paymentType, standingOrderInstructionProposal.paymentType) &&
        Objects.equals(this.charges, standingOrderInstructionProposal.charges) &&
        Objects.equals(this.fraudSystemResponse, standingOrderInstructionProposal.fraudSystemResponse) &&
        Objects.equals(this.reference, standingOrderInstructionProposal.reference) &&
        Objects.equals(this.frequency, standingOrderInstructionProposal.frequency) &&
        Objects.equals(this.numberOfPayments, standingOrderInstructionProposal.numberOfPayments) &&
        Objects.equals(this.firstPaymentDateTime, standingOrderInstructionProposal.firstPaymentDateTime) &&
        Objects.equals(this.recurringPaymentDateTime, standingOrderInstructionProposal.recurringPaymentDateTime) &&
        Objects.equals(this.finalPaymentDateTime, standingOrderInstructionProposal.finalPaymentDateTime) &&
        Objects.equals(this.firstPaymentAmount, standingOrderInstructionProposal.firstPaymentAmount) &&
        Objects.equals(this.recurringPaymentAmount, standingOrderInstructionProposal.recurringPaymentAmount) &&
        Objects.equals(this.finalPaymentAmount, standingOrderInstructionProposal.finalPaymentAmount) &&
        Objects.equals(this.permission, standingOrderInstructionProposal.permission);
  }

  @Override
  public int hashCode() {
    return Objects.hash(paymentInstructionProposalId, proposalCreationDatetime, proposalStatus, proposalStatusUpdateDatetime, authorisationType, authorisationDatetime, proposingParty, proposingPartyPostalAddress, proposingPartyAccount, authorisingParty, authorisingPartyAccount, paymentInstructionRiskFactorReference, paymentType, charges, fraudSystemResponse, reference, frequency, numberOfPayments, firstPaymentDateTime, recurringPaymentDateTime, finalPaymentDateTime, firstPaymentAmount, recurringPaymentAmount, finalPaymentAmount, permission);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class StandingOrderInstructionProposal {\n");
    
    sb.append("    paymentInstructionProposalId: ").append(toIndentedString(paymentInstructionProposalId)).append("\n");
    sb.append("    proposalCreationDatetime: ").append(toIndentedString(proposalCreationDatetime)).append("\n");
    sb.append("    proposalStatus: ").append(toIndentedString(proposalStatus)).append("\n");
    sb.append("    proposalStatusUpdateDatetime: ").append(toIndentedString(proposalStatusUpdateDatetime)).append("\n");
    sb.append("    authorisationType: ").append(toIndentedString(authorisationType)).append("\n");
    sb.append("    authorisationDatetime: ").append(toIndentedString(authorisationDatetime)).append("\n");
    sb.append("    proposingParty: ").append(toIndentedString(proposingParty)).append("\n");
    sb.append("    proposingPartyPostalAddress: ").append(toIndentedString(proposingPartyPostalAddress)).append("\n");
    sb.append("    proposingPartyAccount: ").append(toIndentedString(proposingPartyAccount)).append("\n");
    sb.append("    authorisingParty: ").append(toIndentedString(authorisingParty)).append("\n");
    sb.append("    authorisingPartyAccount: ").append(toIndentedString(authorisingPartyAccount)).append("\n");
    sb.append("    paymentInstructionRiskFactorReference: ").append(toIndentedString(paymentInstructionRiskFactorReference)).append("\n");
    sb.append("    paymentType: ").append(toIndentedString(paymentType)).append("\n");
    sb.append("    charges: ").append(toIndentedString(charges)).append("\n");
    sb.append("    fraudSystemResponse: ").append(toIndentedString(fraudSystemResponse)).append("\n");
    sb.append("    reference: ").append(toIndentedString(reference)).append("\n");
    sb.append("    frequency: ").append(toIndentedString(frequency)).append("\n");
    sb.append("    numberOfPayments: ").append(toIndentedString(numberOfPayments)).append("\n");
    sb.append("    firstPaymentDateTime: ").append(toIndentedString(firstPaymentDateTime)).append("\n");
    sb.append("    recurringPaymentDateTime: ").append(toIndentedString(recurringPaymentDateTime)).append("\n");
    sb.append("    finalPaymentDateTime: ").append(toIndentedString(finalPaymentDateTime)).append("\n");
    sb.append("    firstPaymentAmount: ").append(toIndentedString(firstPaymentAmount)).append("\n");
    sb.append("    recurringPaymentAmount: ").append(toIndentedString(recurringPaymentAmount)).append("\n");
    sb.append("    finalPaymentAmount: ").append(toIndentedString(finalPaymentAmount)).append("\n");
    sb.append("    permission: ").append(toIndentedString(permission)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

