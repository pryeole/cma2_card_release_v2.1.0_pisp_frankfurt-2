package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import java.util.Objects;

import javax.validation.constraints.DecimalMax;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * BalanceAmount
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-01-16T16:32:31.691+05:30")

public class BalanceAmount   {
  @JsonProperty("groupReportingCurrency")
  private Double groupReportingCurrency = null;

  @JsonProperty("localReportingCurrency")
  private Double localReportingCurrency = null;

  @JsonProperty("transactionCurrency")
  private Double transactionCurrency = null;

  public BalanceAmount groupReportingCurrency(Double groupReportingCurrency) {
    this.groupReportingCurrency = groupReportingCurrency;
    return this;
  }

  /**
   * The value in group reporting currency
   * maximum: 1.0E+15
   * @return groupReportingCurrency
  **/
  @ApiModelProperty(value = "The value in group reporting currency")

 @DecimalMax("1.0E+15") 
  public Double getGroupReportingCurrency() {
    return groupReportingCurrency;
  }

  public void setGroupReportingCurrency(Double groupReportingCurrency) {
    this.groupReportingCurrency = groupReportingCurrency;
  }

  public BalanceAmount localReportingCurrency(Double localReportingCurrency) {
    this.localReportingCurrency = localReportingCurrency;
    return this;
  }

  /**
   * The value in Local reporting currency
   * maximum: 1.0E+15
   * @return localReportingCurrency
  **/
  @ApiModelProperty(value = "The value in Local reporting currency")

 @DecimalMax("1.0E+15") 
  public Double getLocalReportingCurrency() {
    return localReportingCurrency;
  }

  public void setLocalReportingCurrency(Double localReportingCurrency) {
    this.localReportingCurrency = localReportingCurrency;
  }

  public BalanceAmount transactionCurrency(Double transactionCurrency) {
    this.transactionCurrency = transactionCurrency;
    return this;
  }

  /**
   * The actual value in the transaction currency
   * maximum: 1.0E+15
   * @return transactionCurrency
  **/
  @ApiModelProperty(value = "The actual value in the transaction currency")

 @DecimalMax("1.0E+15") 
  public Double getTransactionCurrency() {
    return transactionCurrency;
  }

  public void setTransactionCurrency(Double transactionCurrency) {
    this.transactionCurrency = transactionCurrency;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BalanceAmount balanceAmount = (BalanceAmount) o;
    return Objects.equals(this.groupReportingCurrency, balanceAmount.groupReportingCurrency) &&
        Objects.equals(this.localReportingCurrency, balanceAmount.localReportingCurrency) &&
        Objects.equals(this.transactionCurrency, balanceAmount.transactionCurrency);
  }

  @Override
  public int hashCode() {
    return Objects.hash(groupReportingCurrency, localReportingCurrency, transactionCurrency);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class BalanceAmount {\n");
    
    sb.append("    groupReportingCurrency: ").append(toIndentedString(groupReportingCurrency)).append("\n");
    sb.append("    localReportingCurrency: ").append(toIndentedString(localReportingCurrency)).append("\n");
    sb.append("    transactionCurrency: ").append(toIndentedString(transactionCurrency)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

