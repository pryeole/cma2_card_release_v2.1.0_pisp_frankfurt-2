package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Gets or Sets creditDebitEventCode
 */
public enum CreditDebitEventCode {
  
  DR("Dr"),
  
  CR("Cr");

  private String value;

  CreditDebitEventCode(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static CreditDebitEventCode fromValue(String text) {
    for (CreditDebitEventCode b : CreditDebitEventCode.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

