package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import java.util.Objects;

import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * PaymentInstruction2
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-01-16T16:32:31.691+05:30")

public class PaymentInstruction2   {
  @JsonProperty("paymentInstructionNumber")
  private String paymentInstructionNumber = null;

  public PaymentInstruction2 paymentInstructionNumber(String paymentInstructionNumber) {
    this.paymentInstructionNumber = paymentInstructionNumber;
    return this;
  }

  /**
   * Get paymentInstructionNumber
   * @return paymentInstructionNumber
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getPaymentInstructionNumber() {
    return paymentInstructionNumber;
  }

  public void setPaymentInstructionNumber(String paymentInstructionNumber) {
    this.paymentInstructionNumber = paymentInstructionNumber;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PaymentInstruction2 paymentInstruction2 = (PaymentInstruction2) o;
    return Objects.equals(this.paymentInstructionNumber, paymentInstruction2.paymentInstructionNumber);
  }

  @Override
  public int hashCode() {
    return Objects.hash(paymentInstructionNumber);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PaymentInstruction2 {\n");
    
    sb.append("    paymentInstructionNumber: ").append(toIndentedString(paymentInstructionNumber)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

