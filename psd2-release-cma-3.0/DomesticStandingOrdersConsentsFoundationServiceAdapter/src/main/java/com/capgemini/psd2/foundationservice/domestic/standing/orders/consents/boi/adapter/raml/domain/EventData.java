package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import java.time.OffsetDateTime;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * EventData
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-01-16T16:32:31.691+05:30")

public class EventData   {
  @JsonProperty("originatingChannel")
  private Channel originatingChannel = null;

  @JsonProperty("originatingChannelBrand")
  private Brand originatingChannelBrand = null;

  @JsonProperty("eventSubject")
  private EventSubject eventSubject = null;

  @JsonProperty("eventAction")
  private EventAction eventAction = null;

  @JsonProperty("eventOutcome")
  private EventOutcome eventOutcome = null;

  @JsonProperty("eventDateTime")
  private OffsetDateTime eventDateTime = null;

  @JsonProperty("eventData")
  private EventData1 eventData = null;

  public EventData originatingChannel(Channel originatingChannel) {
    this.originatingChannel = originatingChannel;
    return this;
  }

  /**
   * Get originatingChannel
   * @return originatingChannel
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public Channel getOriginatingChannel() {
    return originatingChannel;
  }

  public void setOriginatingChannel(Channel originatingChannel) {
    this.originatingChannel = originatingChannel;
  }

  public EventData originatingChannelBrand(Brand originatingChannelBrand) {
    this.originatingChannelBrand = originatingChannelBrand;
    return this;
  }

  /**
   * Get originatingChannelBrand
   * @return originatingChannelBrand
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Brand getOriginatingChannelBrand() {
    return originatingChannelBrand;
  }

  public void setOriginatingChannelBrand(Brand originatingChannelBrand) {
    this.originatingChannelBrand = originatingChannelBrand;
  }

  public EventData eventSubject(EventSubject eventSubject) {
    this.eventSubject = eventSubject;
    return this;
  }

  /**
   * Get eventSubject
   * @return eventSubject
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public EventSubject getEventSubject() {
    return eventSubject;
  }

  public void setEventSubject(EventSubject eventSubject) {
    this.eventSubject = eventSubject;
  }

  public EventData eventAction(EventAction eventAction) {
    this.eventAction = eventAction;
    return this;
  }

  /**
   * Get eventAction
   * @return eventAction
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public EventAction getEventAction() {
    return eventAction;
  }

  public void setEventAction(EventAction eventAction) {
    this.eventAction = eventAction;
  }

  public EventData eventOutcome(EventOutcome eventOutcome) {
    this.eventOutcome = eventOutcome;
    return this;
  }

  /**
   * Get eventOutcome
   * @return eventOutcome
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public EventOutcome getEventOutcome() {
    return eventOutcome;
  }

  public void setEventOutcome(EventOutcome eventOutcome) {
    this.eventOutcome = eventOutcome;
  }

  public EventData eventDateTime(OffsetDateTime eventDateTime) {
    this.eventDateTime = eventDateTime;
    return this;
  }

  /**
   * The time when the event occurred
   * @return eventDateTime
  **/
  @ApiModelProperty(required = true, value = "The time when the event occurred")
  @NotNull

  @Valid

  public OffsetDateTime getEventDateTime() {
    return eventDateTime;
  }

  public void setEventDateTime(OffsetDateTime eventDateTime) {
    this.eventDateTime = eventDateTime;
  }

  public EventData eventData(EventData1 eventData) {
    this.eventData = eventData;
    return this;
  }

  /**
   * Get eventData
   * @return eventData
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public EventData1 getEventData() {
    return eventData;
  }

  public void setEventData(EventData1 eventData) {
    this.eventData = eventData;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    EventData eventData = (EventData) o;
    return Objects.equals(this.originatingChannel, eventData.originatingChannel) &&
        Objects.equals(this.originatingChannelBrand, eventData.originatingChannelBrand) &&
        Objects.equals(this.eventSubject, eventData.eventSubject) &&
        Objects.equals(this.eventAction, eventData.eventAction) &&
        Objects.equals(this.eventOutcome, eventData.eventOutcome) &&
        Objects.equals(this.eventDateTime, eventData.eventDateTime) &&
        Objects.equals(this.eventData, eventData.eventData);
  }

  @Override
  public int hashCode() {
    return Objects.hash(originatingChannel, originatingChannelBrand, eventSubject, eventAction, eventOutcome, eventDateTime, eventData);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class EventData {\n");
    
    sb.append("    originatingChannel: ").append(toIndentedString(originatingChannel)).append("\n");
    sb.append("    originatingChannelBrand: ").append(toIndentedString(originatingChannelBrand)).append("\n");
    sb.append("    eventSubject: ").append(toIndentedString(eventSubject)).append("\n");
    sb.append("    eventAction: ").append(toIndentedString(eventAction)).append("\n");
    sb.append("    eventOutcome: ").append(toIndentedString(eventOutcome)).append("\n");
    sb.append("    eventDateTime: ").append(toIndentedString(eventDateTime)).append("\n");
    sb.append("    eventData: ").append(toIndentedString(eventData)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

