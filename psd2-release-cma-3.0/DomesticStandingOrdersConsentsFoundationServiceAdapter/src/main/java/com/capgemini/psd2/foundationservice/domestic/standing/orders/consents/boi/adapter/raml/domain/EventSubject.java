package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * The entity or resource which acts as the subject of the event from the source / source sub system
 */
public enum EventSubject {
  
  DIGITAL_USER("Digital User"),
  
  PARTY("Party"),
  
  ACCOUNT("Account"),
  
  COUNTER_PARTY("Counter Party"),
  
  PAYMENT("Payment"),
  
  STANDING_ORDER_SCHEDULE_INSTRUCTION("Standing Order Schedule Instruction"),
  
  ELECTRONIC_DEVICE("Electronic Device"),
  
  CARD("Card");

  private String value;

  EventSubject(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static EventSubject fromValue(String text) {
    for (EventSubject b : EventSubject.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

