package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Digital User object
 */
@ApiModel(description = "Digital User object")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-01-16T16:32:31.691+05:30")

public class DigitalUser   {
  @JsonProperty("digitalUserIdentifier")
  private String digitalUserIdentifier = null;

  @JsonProperty("authenticationSystemCode")
  private AuthenticationSystemCode authenticationSystemCode = null;

  @JsonProperty("digitalUserLockedOutIndicator")
  private Boolean digitalUserLockedOutIndicator = null;

  @JsonProperty("secureKeyAttemptsRemainingCount")
  private Double secureKeyAttemptsRemainingCount = null;

  @JsonProperty("personInformation")
  private PersonBasicInformation personInformation = null;

  public DigitalUser digitalUserIdentifier(String digitalUserIdentifier) {
    this.digitalUserIdentifier = digitalUserIdentifier;
    return this;
  }

  /**
   * Get digitalUserIdentifier
   * @return digitalUserIdentifier
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getDigitalUserIdentifier() {
    return digitalUserIdentifier;
  }

  public void setDigitalUserIdentifier(String digitalUserIdentifier) {
    this.digitalUserIdentifier = digitalUserIdentifier;
  }

  public DigitalUser authenticationSystemCode(AuthenticationSystemCode authenticationSystemCode) {
    this.authenticationSystemCode = authenticationSystemCode;
    return this;
  }

  /**
   * Get authenticationSystemCode
   * @return authenticationSystemCode
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public AuthenticationSystemCode getAuthenticationSystemCode() {
    return authenticationSystemCode;
  }

  public void setAuthenticationSystemCode(AuthenticationSystemCode authenticationSystemCode) {
    this.authenticationSystemCode = authenticationSystemCode;
  }

  public DigitalUser digitalUserLockedOutIndicator(Boolean digitalUserLockedOutIndicator) {
    this.digitalUserLockedOutIndicator = digitalUserLockedOutIndicator;
    return this;
  }

  /**
   * Get digitalUserLockedOutIndicator
   * @return digitalUserLockedOutIndicator
  **/
  @ApiModelProperty(value = "")


  public Boolean isDigitalUserLockedOutIndicator() {
    return digitalUserLockedOutIndicator;
  }

  public void setDigitalUserLockedOutIndicator(Boolean digitalUserLockedOutIndicator) {
    this.digitalUserLockedOutIndicator = digitalUserLockedOutIndicator;
  }

  public DigitalUser secureKeyAttemptsRemainingCount(Double secureKeyAttemptsRemainingCount) {
    this.secureKeyAttemptsRemainingCount = secureKeyAttemptsRemainingCount;
    return this;
  }

  /**
   * Get secureKeyAttemptsRemainingCount
   * @return secureKeyAttemptsRemainingCount
  **/
  @ApiModelProperty(value = "")


  public Double getSecureKeyAttemptsRemainingCount() {
    return secureKeyAttemptsRemainingCount;
  }

  public void setSecureKeyAttemptsRemainingCount(Double secureKeyAttemptsRemainingCount) {
    this.secureKeyAttemptsRemainingCount = secureKeyAttemptsRemainingCount;
  }

  public DigitalUser personInformation(PersonBasicInformation personInformation) {
    this.personInformation = personInformation;
    return this;
  }

  /**
   * Get personInformation
   * @return personInformation
  **/
  @ApiModelProperty(value = "")

  @Valid

  public PersonBasicInformation getPersonInformation() {
    return personInformation;
  }

  public void setPersonInformation(PersonBasicInformation personInformation) {
    this.personInformation = personInformation;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DigitalUser digitalUser = (DigitalUser) o;
    return Objects.equals(this.digitalUserIdentifier, digitalUser.digitalUserIdentifier) &&
        Objects.equals(this.authenticationSystemCode, digitalUser.authenticationSystemCode) &&
        Objects.equals(this.digitalUserLockedOutIndicator, digitalUser.digitalUserLockedOutIndicator) &&
        Objects.equals(this.secureKeyAttemptsRemainingCount, digitalUser.secureKeyAttemptsRemainingCount) &&
        Objects.equals(this.personInformation, digitalUser.personInformation);
  }

  @Override
  public int hashCode() {
    return Objects.hash(digitalUserIdentifier, authenticationSystemCode, digitalUserLockedOutIndicator, secureKeyAttemptsRemainingCount, personInformation);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DigitalUser {\n");
    
    sb.append("    digitalUserIdentifier: ").append(toIndentedString(digitalUserIdentifier)).append("\n");
    sb.append("    authenticationSystemCode: ").append(toIndentedString(authenticationSystemCode)).append("\n");
    sb.append("    digitalUserLockedOutIndicator: ").append(toIndentedString(digitalUserLockedOutIndicator)).append("\n");
    sb.append("    secureKeyAttemptsRemainingCount: ").append(toIndentedString(secureKeyAttemptsRemainingCount)).append("\n");
    sb.append("    personInformation: ").append(toIndentedString(personInformation)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

