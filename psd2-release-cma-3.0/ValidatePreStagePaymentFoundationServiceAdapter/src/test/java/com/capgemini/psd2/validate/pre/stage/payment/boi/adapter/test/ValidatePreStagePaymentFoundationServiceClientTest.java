package com.capgemini.psd2.validate.pre.stage.payment.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter.client.ValidatePreStagePaymentFoundationServiceClientImpl;
import com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter.domain.PaymentInstruction;
import com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter.domain.ValidationPassed;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;

@RunWith(SpringJUnit4ClassRunner.class)
public class ValidatePreStagePaymentFoundationServiceClientTest {
	
	@InjectMocks
	ValidatePreStagePaymentFoundationServiceClientImpl validatePreStagePaymentFoundationServiceClient;
	
	@Mock
	@Qualifier("restClientFoundation")
	private RestClientSync restClient;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void validatePreStagePaymentTest(){
		
		ValidationPassed validationPassed = new ValidationPassed();
		RequestInfo requestInfo = new RequestInfo();
		PaymentInstruction paymentInstruction = new PaymentInstruction();
		HttpHeaders httpHeaders = new HttpHeaders();
		
		Mockito.when(restClient.callForPost(anyObject(), anyObject(), anyObject(), anyObject())).thenReturn(validationPassed);
		
		validationPassed = validatePreStagePaymentFoundationServiceClient.validatePreStagePayment(requestInfo, paymentInstruction, ValidationPassed.class, httpHeaders);
		assertNotNull(validationPassed);
	}

}
