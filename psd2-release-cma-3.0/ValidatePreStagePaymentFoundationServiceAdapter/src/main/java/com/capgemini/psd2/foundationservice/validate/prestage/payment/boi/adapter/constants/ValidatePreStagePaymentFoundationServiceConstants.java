
package com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter.constants;

public class ValidatePreStagePaymentFoundationServiceConstants {

	public static final String CHANNEL_ID = "X-BOI-CHANNEL";
	public static final String USER_ID = "X-BOI-USER";
	public static final String PLATFORM_ID = "X-BOI-PLATFORM";
	public static final String CORRELATION_ID = "X-CORRELATION-ID";
	public static final String BENEFICIARY_COUNTRY = "GB";
	public static final String VALIDATION_STATUS_PENDING = "Pending";
	public static final String VALIDATION_STATUS_REJECTED = "Rejected";

	
}
