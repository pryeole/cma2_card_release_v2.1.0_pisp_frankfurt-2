package com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.OBExternalAccountSubType1Code;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountBeneficiariesResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.AccountBeneficiariesFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.client.AccountBeneficiariesFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.delegate.AccountBeneficiariesFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.AddressCountry;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.AddressReference;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.BeneficiaryAccount;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.ExternalPaymentBeneficiaryIndicator;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.OperationalOrganisationUnit;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.PartiesPaymentBeneficiariesresponse;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.PaymentBeneficiary;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.PaymentBeneficiaryLocaleTypeCode;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountBeneficiariesFoundationServiceAdapterTest {

	@InjectMocks
	private AccountBeneficiariesFoundationServiceAdapter accountBeneficiariesFoundationServiceAdapter;

	@Mock
	private AccountBeneficiariesFoundationServiceDelegate accountBeneficiaryFoundationServiceDelegate;

	@Mock
	private AccountBeneficiariesFoundationServiceClient accountBeneficiaryFoundationServiceClient;

	@Test
	public void testAccountBeneficiaryFS1() {
		PartiesPaymentBeneficiariesresponse partiesPaymentBeneficiariesresponse = new PartiesPaymentBeneficiariesresponse();
		List<PaymentBeneficiary> paymentBeneficiaryList = new ArrayList<PaymentBeneficiary>();
		PaymentBeneficiary paymentBeneficiary = new PaymentBeneficiary();
		BeneficiaryAccount beneficiaryAccount = new BeneficiaryAccount();
		OperationalOrganisationUnit operationalOrganisationUnit = new OperationalOrganisationUnit();
		AddressReference addressReference = new AddressReference();
		AddressCountry addressCountry = new AddressCountry();

		addressReference.setGeoCodeBuildingName("New Century House");
		addressReference.setFirstAddressLine("Mayor Street Lower");
		addressReference.setSecondAddressLine("International Financial Services Centre");
		addressReference.setThirdAddressLine("Dublin 1");

		addressCountry.setIsoCountryAlphaTwoCode("IE");
		addressCountry.setCountryName("Republic of Ireland");
		addressReference.setAddressCountry(addressCountry);

		operationalOrganisationUnit.setAddressReference(addressReference);

		beneficiaryAccount.setAccountName("ABC Corporation");
		beneficiaryAccount.setAccountNumber("24512786");
		beneficiaryAccount.setParentNationalSortCodeNSCNumber("902127");
		beneficiaryAccount.setInternationalBankAccountNumberIBAN("GB58BOFI90212724512786");
		beneficiaryAccount.setSwiftBankIdentifierCodeBIC("BOFIGB2BXXX");
		beneficiaryAccount.setOperationalOrganisationUnit(operationalOrganisationUnit);

		paymentBeneficiary.setPaymentBeneficiaryIdentifier("1234");
		paymentBeneficiary.setPaymentBeneficiaryLabel("ACME Co. Invoices");
		paymentBeneficiary.setPaymentBeneficiaryNarrativeText("October Invoices");
		paymentBeneficiary.setPaymentBeneficiaryBankName("Bank of Ireland");
		paymentBeneficiary.setPaymentBeneficiaryLocaleTypeCode(PaymentBeneficiaryLocaleTypeCode.SEPA);
		paymentBeneficiary.setExternalPaymentBeneficiaryIndicator(ExternalPaymentBeneficiaryIndicator.N);
		paymentBeneficiary.setBeneficiaryAccount(beneficiaryAccount);

		paymentBeneficiaryList.add(paymentBeneficiary);

		partiesPaymentBeneficiariesresponse.setPaymentBeneficiaryList(paymentBeneficiaryList);
		partiesPaymentBeneficiariesresponse.setTotal(1d);

		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setCorrelationId("test");
		accountMapping.setPsuId("test");

		Mockito.when(accountBeneficiaryFoundationServiceDelegate.getFoundationServiceURL(any(), any(), any()))
				.thenReturn("http://10.102.19.131:8081/psd2-abt-service/services/abt/accounts/nsc1234/acct1234");
		Mockito.when(accountBeneficiaryFoundationServiceClient.restTransportForAccountBeneficiaryFS(any(), any(),
				any())).thenReturn(partiesPaymentBeneficiariesresponse);
		Mockito.when(accountBeneficiaryFoundationServiceDelegate.transform((any()), any()))
				.thenReturn(new PlatformAccountBeneficiariesResponse());

		Map<String, String> params = new HashMap<String, String>();
		accountBeneficiariesFoundationServiceAdapter.retrieveAccountBeneficiaries(accountMapping, params);
	}

	@Test
	public void testAccountBeneficiaryFS2() {
		PartiesPaymentBeneficiariesresponse partiesPaymentBeneficiariesresponse = new PartiesPaymentBeneficiariesresponse();
		List<PaymentBeneficiary> paymentBeneficiaryList = new ArrayList<PaymentBeneficiary>();
		PaymentBeneficiary paymentBeneficiary = new PaymentBeneficiary();
		BeneficiaryAccount beneficiaryAccount = new BeneficiaryAccount();
		OperationalOrganisationUnit operationalOrganisationUnit = new OperationalOrganisationUnit();
		AddressReference addressReference = new AddressReference();
		AddressCountry addressCountry = new AddressCountry();

		addressReference.setGeoCodeBuildingName("New Century House");
		addressReference.setFirstAddressLine("Mayor Street Lower");
		addressReference.setSecondAddressLine("International Financial Services Centre");
		addressReference.setThirdAddressLine("Dublin 1");

		addressCountry.setIsoCountryAlphaTwoCode("IE");
		addressCountry.setCountryName("Republic of Ireland");
		addressReference.setAddressCountry(addressCountry);

		operationalOrganisationUnit.setAddressReference(addressReference);

		beneficiaryAccount.setAccountName("ABC Corporation");
		beneficiaryAccount.setAccountNumber("24512786");
		beneficiaryAccount.setParentNationalSortCodeNSCNumber("902127");
		beneficiaryAccount.setInternationalBankAccountNumberIBAN("GB58BOFI90212724512786");
		beneficiaryAccount.setSwiftBankIdentifierCodeBIC("BOFIGB2BXXX");
		beneficiaryAccount.setOperationalOrganisationUnit(operationalOrganisationUnit);

		paymentBeneficiary.setPaymentBeneficiaryIdentifier("1234");
		paymentBeneficiary.setPaymentBeneficiaryLabel("ACME Co. Invoices");
		paymentBeneficiary.setPaymentBeneficiaryNarrativeText("October Invoices");
		paymentBeneficiary.setPaymentBeneficiaryBankName("Bank of Ireland");
		paymentBeneficiary.setPaymentBeneficiaryLocaleTypeCode(PaymentBeneficiaryLocaleTypeCode.SEPA);
		paymentBeneficiary.setExternalPaymentBeneficiaryIndicator(ExternalPaymentBeneficiaryIndicator.N);
		paymentBeneficiary.setBeneficiaryAccount(beneficiaryAccount);

		paymentBeneficiaryList.add(paymentBeneficiary);

		partiesPaymentBeneficiariesresponse.setPaymentBeneficiaryList(paymentBeneficiaryList);
		partiesPaymentBeneficiariesresponse.setTotal(1d);

		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountSubType(OBExternalAccountSubType1Code.SAVINGS);
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setCorrelationId("test");
		accountMapping.setPsuId("test");

		Mockito.when(accountBeneficiaryFoundationServiceDelegate.getFoundationServiceURL(any(), any(), any()))
				.thenReturn("http://10.102.19.131:8081/psd2-abt-service/services/abt/accounts/nsc1234/acct1234");
		Mockito.when(accountBeneficiaryFoundationServiceClient.restTransportForAccountBeneficiaryFS(any(), any(),
				any())).thenReturn(partiesPaymentBeneficiariesresponse);
		Mockito.when(accountBeneficiaryFoundationServiceDelegate.transform((any()), any()))
				.thenReturn(new PlatformAccountBeneficiariesResponse());

		Map<String, String> params = new HashMap<String, String>();
		accountBeneficiariesFoundationServiceAdapter.retrieveAccountBeneficiaries(accountMapping, params);
	}

	@Test
	public void testAccountBeneficiaryFS3() {
		PartiesPaymentBeneficiariesresponse partiesPaymentBeneficiariesresponse = new PartiesPaymentBeneficiariesresponse();
		List<PaymentBeneficiary> paymentBeneficiaryList = new ArrayList<PaymentBeneficiary>();
		PaymentBeneficiary paymentBeneficiary = new PaymentBeneficiary();
		BeneficiaryAccount beneficiaryAccount = new BeneficiaryAccount();
		OperationalOrganisationUnit operationalOrganisationUnit = new OperationalOrganisationUnit();
		AddressReference addressReference = new AddressReference();
		AddressCountry addressCountry = new AddressCountry();

		addressReference.setGeoCodeBuildingName("New Century House");
		addressReference.setFirstAddressLine("Mayor Street Lower");
		addressReference.setSecondAddressLine("International Financial Services Centre");
		addressReference.setThirdAddressLine("Dublin 1");

		addressCountry.setIsoCountryAlphaTwoCode("IE");
		addressCountry.setCountryName("Republic of Ireland");
		addressReference.setAddressCountry(addressCountry);

		operationalOrganisationUnit.setAddressReference(addressReference);

		beneficiaryAccount.setAccountName("ABC Corporation");
		beneficiaryAccount.setAccountNumber("24512786");
		beneficiaryAccount.setParentNationalSortCodeNSCNumber("902127");
		beneficiaryAccount.setInternationalBankAccountNumberIBAN("GB58BOFI90212724512786");
		beneficiaryAccount.setSwiftBankIdentifierCodeBIC("BOFIGB2BXXX");
		beneficiaryAccount.setOperationalOrganisationUnit(operationalOrganisationUnit);

		paymentBeneficiary.setPaymentBeneficiaryIdentifier("1234");
		paymentBeneficiary.setPaymentBeneficiaryLabel("ACME Co. Invoices");
		paymentBeneficiary.setPaymentBeneficiaryNarrativeText("October Invoices");
		paymentBeneficiary.setPaymentBeneficiaryBankName("Bank of Ireland");
		paymentBeneficiary.setPaymentBeneficiaryLocaleTypeCode(PaymentBeneficiaryLocaleTypeCode.SEPA);
		paymentBeneficiary.setExternalPaymentBeneficiaryIndicator(ExternalPaymentBeneficiaryIndicator.N);
		paymentBeneficiary.setBeneficiaryAccount(beneficiaryAccount);

		paymentBeneficiaryList.add(paymentBeneficiary);

		partiesPaymentBeneficiariesresponse.setPaymentBeneficiaryList(paymentBeneficiaryList);
		partiesPaymentBeneficiariesresponse.setTotal(1d);

		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CREDITCARD);
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setCorrelationId("test");
		accountMapping.setPsuId("test");

		Mockito.when(accountBeneficiaryFoundationServiceDelegate.getFoundationServiceURL(any(), any(), any()))
				.thenReturn("http://10.102.19.131:8081/psd2-abt-service/services/abt/accounts/nsc1234/acct1234");
		Mockito.when(accountBeneficiaryFoundationServiceClient.restTransportForAccountBeneficiaryFS(any(), any(),
				any())).thenReturn(partiesPaymentBeneficiariesresponse);
		Mockito.when(accountBeneficiaryFoundationServiceDelegate.transform((any()), any()))
				.thenReturn(new PlatformAccountBeneficiariesResponse());

		Map<String, String> params = new HashMap<String, String>();
		accountBeneficiariesFoundationServiceAdapter.retrieveAccountBeneficiaries(accountMapping, params);
	}

	@Test(expected = AdapterException.class)
	public void testExceptionNullCheckUtil1() {
		AccountMapping accountMapping = new AccountMapping();
		Map<String, String> params = new HashMap<>();
		accountMapping.setPsuId("test");
		accountMapping.setCorrelationId("test");
		Mockito.when(accountBeneficiaryFoundationServiceClient.restTransportForAccountBeneficiaryFS(any(), any(),
				any())).thenReturn(null);
		accountBeneficiariesFoundationServiceAdapter.retrieveAccountBeneficiaries(accountMapping, params);
	}

	@Test(expected = AdapterException.class)
	public void testExceptionNullParams() {
		AccountMapping accountMapping = new AccountMapping();
		Map<String, String> params = null;
		accountMapping.setPsuId("test");
		accountMapping.setCorrelationId("test");
		Mockito.when(accountBeneficiaryFoundationServiceClient.restTransportForAccountBeneficiaryFS(any(), any(),
				any())).thenReturn(null);
		accountBeneficiariesFoundationServiceAdapter.retrieveAccountBeneficiaries(accountMapping, params);
	}

	@Test(expected = AdapterException.class)
	public void testExceptionNullCheckUtil2() {
		AccountMapping accountMapping = new AccountMapping();
		Map<String, String> params = new HashMap<>();
		accountMapping.setPsuId(null);
		Mockito.when(accountBeneficiaryFoundationServiceClient.restTransportForAccountBeneficiaryFS(any(), any(),
				any())).thenReturn(null);
		accountBeneficiariesFoundationServiceAdapter.retrieveAccountBeneficiaries(accountMapping, params);
	}

	@Test(expected = AdapterException.class)
	public void testExceptionNullAccountMappingPsu() {
		AccountMapping accountMapping = null;
		Map<String, String> params = new HashMap<>();
		Mockito.when(accountBeneficiaryFoundationServiceClient.restTransportForAccountBeneficiaryFS(any(), any(),
				any())).thenReturn(null);
		accountBeneficiariesFoundationServiceAdapter.retrieveAccountBeneficiaries(accountMapping, params);
	}

	@Test(expected = AdapterException.class)
	public void testExceptionNullCheckUtil3() {
		AccountMapping accountMapping = new AccountMapping();
		Map<String, String> params = new HashMap<>();
		accountMapping.setPsuId(null);
		Mockito.when(
				accountBeneficiaryFoundationServiceClient.restTransportForAccountBeneficiaryFS(null, null, null))
				.thenReturn(null);
		accountBeneficiariesFoundationServiceAdapter.retrieveAccountBeneficiaries(accountMapping, params);
	}

	@Test(expected = Exception.class)
	public void testRestTransportForBeneficiary() {
		PlatformAccountBeneficiariesResponse partiesPaymentBeneficiariesresponse = null;
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("test");
		Mockito.when(accountBeneficiaryFoundationServiceClient.restTransportForAccountBeneficiaryFS(anyObject(),
				anyObject(), anyObject()))
				.thenThrow(PSD2Exception.populatePSD2Exception("", ErrorCodeEnum.TECHNICAL_ERROR));
		Mockito.when(accountBeneficiaryFoundationServiceDelegate.transform((any()), any()))
				.thenReturn(partiesPaymentBeneficiariesresponse);
		accountBeneficiariesFoundationServiceAdapter.retrieveAccountBeneficiaries(accountMapping,
				new HashMap<String, String>());
	}

//	@Test(expected = AdapterException.class)
//	public void testRestTransportForAccountBeneficiary() {
//		AccountMapping accountMapping = new AccountMapping();
//		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
//		AccountDetails accDet = new AccountDetails();
//		accDet.setAccountId("12345");
//		accDet.setAccountNSC("nsc1234");
//		accDet.setAccountNumber("acct1234");
//		accDetList.add(accDet);
//		PlatformAccountBeneficiariesResponse productGETResponse = null;
//
//		accountMapping.setAccountDetails(accDetList);
//		accountMapping.setTppCID("test");
//		accountMapping.setPsuId("test");
//		accountMapping.setCorrelationId("test");
//
//		ErrorInfo errorInfo = new ErrorInfo();
//		errorInfo.setErrorCode("571");
//		errorInfo.setErrorMessage("This request cannot be processed. No Beneficiary found");
//		Mockito.when(accountBeneficiaryFoundationServiceClient.restTransportForAccountBeneficiaryFS(anyObject(),
//				anyObject(), anyObject(), anyObject()))
//				.thenThrow((new AdapterException("Adapter Exception", errorInfo)));
//		PlatformAccountBeneficiariesResponse getresponse = accountBeneficiariesFoundationServiceAdapter
//				.retrieveAccountBeneficiaries(accountMapping, new HashMap<String, String>());
//		assertEquals(productGETResponse, getresponse);
//
//	}

}