
package com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.delegate;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountBeneficiariesResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.constants.AccountBeneficiariesFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.PartiesPaymentBeneficiariesresponse;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.transformer.AccountBeneficiariesFoundationServiceTransformer;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class AccountBeneficiariesFoundationServiceDelegate {

	@Autowired
	private AccountBeneficiariesFoundationServiceTransformer accountBeneficiariesTransformer;

	@Value("${foundationService.userInReqHeader:#{x-api-source-user}}")
	private String userInReqHeader;

	@Value("${foundationService.channelInReqHeader:#{x-api-channel-Code}}")
	private String channelInReqHeader;

	@Value("${foundationService.platformInReqHeader:#{x-api-source-system}}")
	private String platformInReqHeader;

	@Value("${foundationService.transactionReqHeader:#{x-api-transaction-id}}")
	private String transactionReqHeader;

	/** The platform. */
	@Value("${foundationService.platform}")
	private String platform;

	public PlatformAccountBeneficiariesResponse transform(PartiesPaymentBeneficiariesresponse beneficiaries,
			Map<String, String> params) {
		return accountBeneficiariesTransformer.transformAccountBeneficiaries(beneficiaries, params);
	}

	public String getFoundationServiceURL(String version, String userId, String baseURL) {
		if (NullCheckUtils.isNullOrEmpty(userId)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		if (NullCheckUtils.isNullOrEmpty(version)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		return baseURL + "/" + version + "/" + "parties" + "/" + userId + "/" + "payment-beneficiaries";
	}

	public HttpHeaders createRequestHeaders(RequestInfo requestInfo, AccountMapping accountMapping,
			Map<String, String> params) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(userInReqHeader, accountMapping.getPsuId());
		httpHeaders.add(channelInReqHeader, params.get(PSD2Constants.CHANNEL_NAME));
		String platformId = params.get(PSD2Constants.PLATFORM_IN_REQ_HEADER);
		httpHeaders.add(platformInReqHeader, NullCheckUtils.isNullOrEmpty(platformId) ? platform : platformId);
		httpHeaders.add(transactionReqHeader, accountMapping.getCorrelationId());
		httpHeaders.add(AccountBeneficiariesFoundationServiceConstants.PARENTASORTCODENSCNUMBERHEADER, accountMapping.getAccountDetails().get(0).getAccountNSC());
		httpHeaders.add( AccountBeneficiariesFoundationServiceConstants.ACCOUNTNUMBERHEADER, accountMapping.getAccountDetails().get(0).getAccountNumber());
		return httpHeaders;
	}

}
