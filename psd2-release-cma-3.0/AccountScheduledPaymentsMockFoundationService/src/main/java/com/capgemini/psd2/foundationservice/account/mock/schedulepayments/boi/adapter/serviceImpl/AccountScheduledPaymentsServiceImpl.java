/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.mock.schedulepayments.boi.adapter.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.foundationservice.account.mock.schedulepayments.boi.adapter.domain.PartiesOnceOffPaymentScheduleScheduleItemsresponse;
import com.capgemini.psd2.foundationservice.account.mock.schedulepayments.boi.adapter.repository.AccountScheduledPaymentsRepository;
import com.capgemini.psd2.foundationservice.account.mock.schedulepayments.boi.adapter.service.AccountScheduledPaymentsService;
import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;


//import com.capgemini.psd2.account.information.mock.foundationservice.exception.handler.RecordNotFoundException;


/**
 * The Class AccountInformationServiceImpl.
 */
@Service
public class AccountScheduledPaymentsServiceImpl implements AccountScheduledPaymentsService {

	/** The repository. */
	@Autowired
	private AccountScheduledPaymentsRepository repository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.capgemini.psd2.account.information.mock.foundationservice.service.
	 * AccountInformationService#retrieveAccountInformation(java.lang.String,
	 * java.lang.String)
	 */
	public PartiesOnceOffPaymentScheduleScheduleItemsresponse retrieveAccountInformation(
			String userId) throws Exception {
		PartiesOnceOffPaymentScheduleScheduleItemsresponse party= new PartiesOnceOffPaymentScheduleScheduleItemsresponse();
		/*party.setAccountId("1234");
		party.setTotal(2.0);
		repository.save(party);*/
		
		party=repository.findByUserId(userId);
		if (party == null) {
			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.NO_SCHEDULED_STMT_NOT_FOUND_SCP);
		}
		return party;

	}

}
