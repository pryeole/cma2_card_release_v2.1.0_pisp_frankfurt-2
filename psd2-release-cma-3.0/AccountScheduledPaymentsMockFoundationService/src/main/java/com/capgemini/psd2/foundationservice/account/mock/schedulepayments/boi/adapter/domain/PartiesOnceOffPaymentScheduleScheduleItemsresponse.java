package com.capgemini.psd2.foundationservice.account.mock.schedulepayments.boi.adapter.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * PartiesOnceOffPaymentScheduleScheduleItemsresponse
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-12-03T11:38:41.280+05:30")
public class PartiesOnceOffPaymentScheduleScheduleItemsresponse {
  //@SerializedName("total")
  private Double total = null;
  
  private String userId;

  //@SerializedName("scheduleItemsList")
  private List<ScheduleItem> scheduleItemsList = new ArrayList<ScheduleItem>();

  public String getUserId() {
	return userId;
}

public void setUserId(String userId) {
	this.userId = userId;
}

public PartiesOnceOffPaymentScheduleScheduleItemsresponse total(Double total) {
    this.total = total;
    return this;
  }

   /**
   * Get total
   * @return total
  **/
  //@ApiModelProperty(required = true, value = "")
  public Double getTotal() {
    return total;
  }



public void setTotal(Double total) {
    this.total = total;
  }

  public PartiesOnceOffPaymentScheduleScheduleItemsresponse scheduleItemsList(List<ScheduleItem> scheduleItemsList) {
    this.scheduleItemsList = scheduleItemsList;
    return this;
  }

  public PartiesOnceOffPaymentScheduleScheduleItemsresponse addScheduleItemsListItem(ScheduleItem scheduleItemsListItem) {
    this.scheduleItemsList.add(scheduleItemsListItem);
    return this;
  }

   /**
   * Get scheduleItemsList
   * @return scheduleItemsList
  **/
  //@ApiModelProperty(required = true, value = "")
  public List<ScheduleItem> getScheduleItemsList() {
    return scheduleItemsList;
  }

  public void setScheduleItemsList(List<ScheduleItem> scheduleItemsList) {
    this.scheduleItemsList = scheduleItemsList;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PartiesOnceOffPaymentScheduleScheduleItemsresponse partiesOnceOffPaymentScheduleScheduleItemsresponse = (PartiesOnceOffPaymentScheduleScheduleItemsresponse) o;
    return Objects.equals(this.total, partiesOnceOffPaymentScheduleScheduleItemsresponse.total) &&
        Objects.equals(this.scheduleItemsList, partiesOnceOffPaymentScheduleScheduleItemsresponse.scheduleItemsList);
  }

  @Override
  public int hashCode() {
    return Objects.hash(total, scheduleItemsList);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PartiesOnceOffPaymentScheduleScheduleItemsresponse {\n");
    
    sb.append("    total: ").append(toIndentedString(total)).append("\n");
    sb.append("    scheduleItemsList: ").append(toIndentedString(scheduleItemsList)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

