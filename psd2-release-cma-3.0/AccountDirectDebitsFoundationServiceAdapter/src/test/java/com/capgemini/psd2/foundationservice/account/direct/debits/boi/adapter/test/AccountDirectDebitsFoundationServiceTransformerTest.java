package com.capgemini.psd2.foundationservice.account.direct.debits.boi.adapter.test;

import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.aisp.domain.OBDirectDebit1;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountDirectDebitsResponse;
import com.capgemini.psd2.foundationservice.account.direct.debits.boi.adapter.transformer.AccountDirectDebitsFoundationServiceTransformer;
import com.capgemini.psd2.validator.PSD2Validator;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountDirectDebitsFoundationServiceTransformerTest {
	@InjectMocks
	AccountDirectDebitsFoundationServiceTransformer accountDirectDebitsFoundationServiceTransformer;
	
	@Mock
	@Qualifier("PSD2ResponseValidator")
	private PSD2Validator validator;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testTransformAccountDirectDebits(){
		OBDirectDebit1 directDebit = new OBDirectDebit1();
		Map<String, String> params = new HashMap<>();
		PlatformAccountDirectDebitsResponse response = accountDirectDebitsFoundationServiceTransformer.transformAccountDirectDebits(directDebit, params);
		assertNotNull(response);
	}
}
