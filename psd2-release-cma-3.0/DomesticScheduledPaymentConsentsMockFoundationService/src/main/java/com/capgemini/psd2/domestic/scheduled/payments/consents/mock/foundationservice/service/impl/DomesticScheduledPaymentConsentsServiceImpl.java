/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.domestic.scheduled.payments.consents.mock.foundationservice.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.domestic.scheduled.payments.consents.mock.foundationservice.domain.Amount;
import com.capgemini.psd2.domestic.scheduled.payments.consents.mock.foundationservice.domain.ChargeBearer;
import com.capgemini.psd2.domestic.scheduled.payments.consents.mock.foundationservice.domain.Currency;
import com.capgemini.psd2.domestic.scheduled.payments.consents.mock.foundationservice.domain.PaymentInstructionCharge;
import com.capgemini.psd2.domestic.scheduled.payments.consents.mock.foundationservice.domain.ProposalStatus;
import com.capgemini.psd2.domestic.scheduled.payments.consents.mock.foundationservice.domain.ScheduledPaymentInstructionProposal;
import com.capgemini.psd2.domestic.scheduled.payments.consents.mock.foundationservice.exception.handler.RecordNotFoundException;
import com.capgemini.psd2.domestic.scheduled.payments.consents.mock.foundationservice.repository.DomesticScheduledPaymentConsentsRepository;
import com.capgemini.psd2.domestic.scheduled.payments.consents.mock.foundationservice.service.DomesticaScheduledPaymentConsentsService;
import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;




// TODO: Auto-generated Javadoc
/**
 * The Class DomesticPaymentConsentsServiceImpl.
 */
@Service
public class DomesticScheduledPaymentConsentsServiceImpl implements DomesticaScheduledPaymentConsentsService {

	/** The repository. */
	@Autowired
	private DomesticScheduledPaymentConsentsRepository repository;

	@Override
	public ScheduledPaymentInstructionProposal retrieveAccountInformation(String consentId) throws Exception {

		ScheduledPaymentInstructionProposal response1 = repository.findByPaymentInstructionProposalId(consentId);
		if (null == response1) {
			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.SERVER_RESOURCE_NOT_FOUND_PPS_PIPR);
		}

		return response1;
	}

	@Override
	public ScheduledPaymentInstructionProposal createDomesticScheduledPaymentConsentsResource(
			ScheduledPaymentInstructionProposal paymentInstProposalReq) {
		String consentID = null;
		if (null == paymentInstProposalReq) {

			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.SERVER_RESOURCE_NOT_FOUND_PPS_PIPR);
		}

		consentID = UUID.randomUUID().toString();
		paymentInstProposalReq.setPaymentInstructionProposalId(consentID);

		PaymentInstructionCharge charge= new PaymentInstructionCharge();
		Amount amount = new Amount();
		charge.setChargeBearer(ChargeBearer.BORNEBYDEBTOR);
		charge.setType("UK.OBIE.ChapsOut");
		amount.setTransactionCurrency(500.01);
		charge.setAmount(amount);
		Currency currency= new Currency();
		currency.setIsoAlphaCode("GBP");
		charge.setCurrency(currency);
		List<PaymentInstructionCharge> List= new ArrayList();
		List.add(charge);
		paymentInstProposalReq.setCharges(List);
		//----Reject status for testing...
		if (paymentInstProposalReq.getInstructionReference()!=null && paymentInstProposalReq.getInstructionReference().equalsIgnoreCase("REJECTED"))
		{
			paymentInstProposalReq.setProposalStatus(ProposalStatus.REJECTED);
		}
		else
		{
			paymentInstProposalReq.setProposalStatus(ProposalStatus.AWAITINGAUTHORISATION);
		}
		paymentInstProposalReq.setPaymentInstructionProposalId(consentID);
		return repository.save(paymentInstProposalReq);

	}

	@Override
	public ScheduledPaymentInstructionProposal validateDomesticScheduledPaymentConsentsResource(
			ScheduledPaymentInstructionProposal paymentInstProposalReq) throws RecordNotFoundException {
		// TODO Auto-generated method stub
		repository.save(paymentInstProposalReq);
		return paymentInstProposalReq;
	}

	@Override
	public ScheduledPaymentInstructionProposal updateDomesticScheduledPaymentConsentsResource(
			String paymentInstructionProposalId, ScheduledPaymentInstructionProposal paymentInstProposalReq) throws Exception {
		repository.save(paymentInstProposalReq);
		return paymentInstProposalReq;
	}

}
