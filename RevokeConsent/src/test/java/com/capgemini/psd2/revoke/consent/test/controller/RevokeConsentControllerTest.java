package com.capgemini.psd2.revoke.consent.test.controller;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.revoke.consent.controller.RevokeConsentController;
import com.capgemini.psd2.revoke.consent.service.RevokeConsentService;

public class RevokeConsentControllerTest {


	@InjectMocks
	private RevokeConsentController controller;


	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Mock
	private RevokeConsentService revokeRequestService;

	@Test
	public void deleteAccountRequest(){
		controller.deleteAccountRequest("1234545", "BOIUK");
	}
}
