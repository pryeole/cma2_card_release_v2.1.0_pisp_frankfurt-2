package com.capgemini.psd2.pisp.payment.setup.platform.adapter.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;

public interface PaymentSetupPlatformRepository extends MongoRepository<PaymentConsentsPlatformResource, String> {

	public PaymentConsentsPlatformResource findOneByPaymentTypeAndTppCIDAndIdempotencyKeyAndIdempotencyRequestAndCreatedAtGreaterThanAndSetupCmaVersionIn(
			String paymentType, String tppCID, String idempotencyKey, String idempotencyRequest, String date , List<String> cmaVersion);

	public PaymentConsentsPlatformResource findOneByPaymentIdOrPaymentConsentIdAndSetupCmaVersionIn(String paymentId, String paymentConsentId, List<String> cmaVersion);

	public PaymentConsentsPlatformResource findOneByPaymentConsentIdAndSetupCmaVersionIn(String paymentConsentId , List<String> cmaVersion);
	
	public PaymentConsentsPlatformResource findOneByPaymentTypeAndTppCIDAndIdempotencyKeyAndIdempotencyRequestAndFileHashAndCreatedAtGreaterThanAndSetupCmaVersionIn(
			String paymentType, String tppCID, String idempotencyKey, String idempotencyRequest, String fileHash, String date, List<String> cmaVersion);
	
	public PaymentConsentsPlatformResource findOneByPaymentTypeAndTppCIDAndIdempotencyKeyAndIdempotencyRequestAndPaymentConsentIdAndCreatedAtGreaterThanAndSetupCmaVersionIn(
			String paymentType, String tppCID, String consentId, String idempotencyRequest, String fileHash, String date, List<String> cmaVersion);

	public PaymentConsentsPlatformResource findOneByPaymentConsentIdAndPaymentTypeAndSetupCmaVersionIn(String paymentConsentId, String paymentType, List<String> cmaVersion);
	
	public PaymentConsentsPlatformResource findOneByPaymentIdAndSetupCmaVersionIn(String paymentId, List<String> cmaVersion);
}
