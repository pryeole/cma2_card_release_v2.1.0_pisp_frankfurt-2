package com.capgemini.psd2.aisp.validation.test.adapter.impl;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBAddressTypeCode;
import com.capgemini.psd2.aisp.domain.OBExternalPartyType1Code;
import com.capgemini.psd2.aisp.domain.OBParty1;
import com.capgemini.psd2.aisp.domain.OBPostalAddress8;
import com.capgemini.psd2.aisp.domain.OBReadParty1;
import com.capgemini.psd2.aisp.domain.OBReadParty1Data;
import com.capgemini.psd2.aisp.validation.adapter.impl.AccountPartyValidatorImpl;
import com.capgemini.psd2.aisp.validation.adapter.utilities.CommonAccountValidations;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.validator.PSD2Validator;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountPartyValidatorTest {

	@InjectMocks
	AccountPartyValidatorImpl accountPartyValidatorImpl;
	@Mock
	private CommonAccountValidations commonAccountValidations;
	@Mock
	private PSD2Validator psd2Validator;
	
	public  OBReadParty1 getAccountPartyGETResponse() 
	{

		OBReadParty1Data data = new OBReadParty1Data();
		OBParty1 party = new OBParty1();
		data.setParty(party);
		party.setPartyId("2345");
		party.setPartyNumber("024");
		party.setPartyType(OBExternalPartyType1Code.DELEGATE);
		party.setPhone("+91-8004142576");
		party.setName("Keli");
		party.setEmailAddress("keli.34@gmail.com");
		party.setMobile("+91-8004142576");
		List<OBPostalAddress8> partyList = new ArrayList<>();
		List<String> addressLines = new ArrayList<>();
		addressLines.add("KenCircle");
		OBPostalAddress8 oBPostalAddress8 = new OBPostalAddress8();
		oBPostalAddress8.setAddressLine(addressLines);
		oBPostalAddress8.setAddressType(OBAddressTypeCode.BUSINESS);
		oBPostalAddress8.setBuildingNumber("502");
		oBPostalAddress8.setCountry("IN");
		oBPostalAddress8.setCountrySubDivision("Atlas");
		oBPostalAddress8.setPostCode("12345");
		oBPostalAddress8.setStreetName("HighStreet");
		oBPostalAddress8.setTownName("DownTown");
		data.getParty().addAddressItem(oBPostalAddress8);
		partyList.add(oBPostalAddress8);
		OBReadParty1 oBReadParty1 = new OBReadParty1();
		oBReadParty1.setData(data);
		Links links = new Links();
		Meta meta = new Meta();
		meta.setTotalPages(1);
		oBReadParty1.setLinks(links);
		oBReadParty1.setMeta(meta);
		return oBReadParty1;
	
	}

	@Test
	public void validateResponseParamsTest() {
		Field reqValidationEnabled = null;
			try {
				reqValidationEnabled = AccountPartyValidatorImpl.class.getDeclaredField("resValidationEnabled");
				reqValidationEnabled.setAccessible(true);
				reqValidationEnabled.set(accountPartyValidatorImpl, true);
			} catch (NoSuchFieldException | SecurityException e) {
				e.printStackTrace();
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			assertTrue(accountPartyValidatorImpl.validateResponseParams(getAccountPartyGETResponse()));
	}
	
	@Test
	public void validateResponseParamsTestSetResValidationFalse() {
		Field reqValidationEnabled = null;
			try {
				reqValidationEnabled = AccountPartyValidatorImpl.class.getDeclaredField("resValidationEnabled");
				reqValidationEnabled.setAccessible(true);
				reqValidationEnabled.set(accountPartyValidatorImpl, false);
			} catch (NoSuchFieldException | SecurityException e) {
				e.printStackTrace();
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			assertTrue(accountPartyValidatorImpl.validateResponseParams(getAccountPartyGETResponse()));
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateUniqueIdTestForNullParam()
	{
		accountPartyValidatorImpl.validateUniqueId(null);
	}
	@Test
	public void validateUniqueIdTest()
	{
		Mockito.doNothing().when(commonAccountValidations).validateUniqueUUID("asd");  
		accountPartyValidatorImpl.validateUniqueId("asdf");
	}
	@Test
	public void validateRequestParamsTest()
	{
		assertNull(accountPartyValidatorImpl.validateRequestParams(null));
	}
	
	
}
