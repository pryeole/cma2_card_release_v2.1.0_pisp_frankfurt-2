package com.capgemini.psd2.aisp.validation.adapter.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.domain.OBDirectDebit1;
import com.capgemini.psd2.aisp.domain.OBReadDirectDebit1;
import com.capgemini.psd2.aisp.validation.adapter.AISPCustomValidator;
import com.capgemini.psd2.aisp.validation.adapter.utilities.CommonAccountValidations;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;

@Component("accountDirectDebitsValidator")
@ConfigurationProperties("app")
public class AccountDirectDebitsValidatorImpl implements AISPCustomValidator<OBReadDirectDebit1, OBReadDirectDebit1> {

	@Autowired
	private PSD2Validator psd2Validator;

	@Value("${app.swaggerValidations.response:#{true}}")
	private Boolean resValidationEnabled;

	@Autowired
	private CommonAccountValidations commonAccountValidations;

	@Override
	public OBReadDirectDebit1 validateRequestParams(OBReadDirectDebit1 t) {
		return null;
	}

	@Override
	public boolean validateResponseParams(OBReadDirectDebit1 obReadDirectDebit1) {
		if (resValidationEnabled)
			executeAccountDirectDebitsResponseSwaggerValidations(obReadDirectDebit1);
		executeAccountDirectDebitsResponseCustomValidations(obReadDirectDebit1);
		return true;
	}

	private void executeAccountDirectDebitsResponseSwaggerValidations(OBReadDirectDebit1 obReadDirectDebit1) {
		psd2Validator.validate(obReadDirectDebit1);
	}

	private void executeAccountDirectDebitsResponseCustomValidations(OBReadDirectDebit1 obReadDirectDebit1) {
		if (!NullCheckUtils.isNullOrEmpty(obReadDirectDebit1)
				&& !NullCheckUtils.isNullOrEmpty(obReadDirectDebit1.getData())) {
			for (OBDirectDebit1 obDirectDebit1 : obReadDirectDebit1.getData().getDirectDebit()) {
				if(!NullCheckUtils.isNullOrEmpty(obDirectDebit1)) {
					if (!NullCheckUtils.isNullOrEmpty(obDirectDebit1.getPreviousPaymentDateTime())) {
						commonAccountValidations
								.validateAndParseDateTimeFormatForResponse(obDirectDebit1.getPreviousPaymentDateTime());
					}
					if (!NullCheckUtils.isNullOrEmpty(obDirectDebit1.getPreviousPaymentAmount())) {
						if (!NullCheckUtils.isNullOrEmpty(obDirectDebit1.getPreviousPaymentAmount().getAmount())) {
							commonAccountValidations.validateAmount(obDirectDebit1.getPreviousPaymentAmount().getAmount());
						}
						if (!NullCheckUtils.isNullOrEmpty(obDirectDebit1.getPreviousPaymentAmount().getCurrency())) {
							commonAccountValidations
									.isValidCurrency(obDirectDebit1.getPreviousPaymentAmount().getCurrency());
						}
					}
				}
			}
		}
	}

	@Override
	public void validateUniqueId(String consentId) {
		if (NullCheckUtils.isNullOrEmpty(consentId)) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID, ErrorMapKeys.VALIDATION_ERROR));
		}
		commonAccountValidations.validateUniqueUUID(consentId);
	}

}