package com.capgemini.psd2.aisp.validation.adapter.utilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.domain.OBExternalPermissions1Code;

@Component
@Configuration
@EnableConfigurationProperties
@ConfigurationProperties("app.permissionValidationRules")
public class PermissionsValidationRules {
	
	private Map<String, List<OBExternalPermissions1Code>> excludedPermission = new HashMap<>();
	
	/*Required Permisson included in CMA 2.0*/
	private List<OBExternalPermissions1Code> requiredPermission = new ArrayList<>();
	
	/** The mandatoryRules. */
	private Map<String, List<OBExternalPermissions1Code>> mandatoryRules = new HashMap<>();

	/** The orConditonRules */
	private Map<String, List<OBExternalPermissions1Code>> orConditonRules = new HashMap<>();

	public Map<String, List<OBExternalPermissions1Code>> getMandatoryRules() {
		return mandatoryRules;
	}

	public void setMandatoryRules(Map<String, List<OBExternalPermissions1Code>> mandatoryRules) {
		this.mandatoryRules = mandatoryRules;
	}

	public Map<String, List<OBExternalPermissions1Code>> getOrConditonRules() {
		return orConditonRules;
	}

	public void setOrConditonRules(Map<String, List<OBExternalPermissions1Code>> orConditonRules) {
		this.orConditonRules = orConditonRules;
	}

	public List<OBExternalPermissions1Code> getRequiredPermission() {
		return requiredPermission;
	}

	public void setRequiredPermission(List<OBExternalPermissions1Code> requiredPermission) {
		this.requiredPermission = requiredPermission;
	}

	public Map<String, List<OBExternalPermissions1Code>> getExcludedPermission() {
		return excludedPermission;
	}

	public void setExcludedPermission(Map<String, List<OBExternalPermissions1Code>> excludedPermission) {
		this.excludedPermission = excludedPermission;
	}

	public List<OBExternalPermissions1Code> getExcludedPermissionForSpecificTenant(String key) {
		return this.excludedPermission.get(key);
	}

}
