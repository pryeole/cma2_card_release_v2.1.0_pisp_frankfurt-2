package com.capgemini.psd2.aisp.validation.adapter.impl;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.validation.adapter.AISPCustomValidator;

@Component("accountsRoutingValidator")
public class AISPValidatorRoutingImpl<T, V> implements AISPCustomValidator<T, V>, ApplicationContextAware {
	
	@Value("${app.customValidationBean:null}")
	private String beanName;
	
	private ApplicationContext applicationContext;

	@SuppressWarnings("unchecked")
	public AISPCustomValidator<T, V> getAccountsValidatorInstance(String beanName) {
		return (AISPCustomValidator<T, V>) applicationContext.getBean(beanName);
	}
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
	
	@Override
	public T validateRequestParams(T t) {
		return getAccountsValidatorInstance(beanName).validateRequestParams(t);
	}
	
	@Override
	public boolean validateResponseParams(V v) {
		return getAccountsValidatorInstance(beanName).validateResponseParams(v);
	}

	@Override
	public void validateUniqueId(String consentId) {
		getAccountsValidatorInstance(beanName).validateUniqueId(consentId);
	}

}
