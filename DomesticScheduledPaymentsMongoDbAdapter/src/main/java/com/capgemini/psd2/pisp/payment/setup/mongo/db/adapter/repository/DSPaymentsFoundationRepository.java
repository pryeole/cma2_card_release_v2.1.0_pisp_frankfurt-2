package com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTResponse;

public interface DSPaymentsFoundationRepository extends MongoRepository<CustomDSPaymentsPOSTResponse, String> {

	public CustomDSPaymentsPOSTResponse findOneByDataDomesticScheduledPaymentId(String submissionId);

}