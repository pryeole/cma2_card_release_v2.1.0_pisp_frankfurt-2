package com.capgemini.psd2.pisp.processing.adapter;

import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;

public interface PaymentConsentsCustomValidator<T,V> {
	// ValidateSetupPOSTRequest
	public boolean validateConsentRequest(T t);

	// ValidateSetupGETResponse
	public boolean validateConsentsGETRequest(PaymentRetrieveGetRequest paymentRetrieveRequest);

	// ValidateSetupResponse
	public boolean validatePaymentConsentResponse(V v);
}
