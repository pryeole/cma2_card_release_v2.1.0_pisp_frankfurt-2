package com.capgemini.psd2.pisp.processing.adapter.service;

import java.util.Map;

import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPlatformDetails;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.PaymentsPlatformResource;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.enums.ProcessExecutionStatusEnum;

public interface PaymentSubmissionProcessingAdapter<T, U> {

	public Map<String, Object> prePaymentProcessGETFlows(String submissionId, PaymentTypeEnum domesticPay);

	public Map<String, Object> preSubmissionProcessing(T request,
			CustomPaymentSetupPlatformDetails platformResourceDetails, String consentIdInRequest);

	public PaymentsPlatformResource createInitialPaymentsPlatformResource(String paymentConsentId,
			String paymentVersion, PaymentTypeEnum paymentTypeEnum);

	public U postPaymentProcessFlows(Map<String, Object> paymentsPlatformResourceMap, U paymentsFoundationResponse,
			ProcessExecutionStatusEnum processStateEnum, String submissionId, OBExternalStatus2Code multiAuthStatus,
			String methodType);

}
