package com.capgemini.psd2.config;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;

import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.support.PersistenceExceptionTranslator;
import org.springframework.data.mapping.context.MappingContext;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.convert.MongoConverter;
import org.springframework.data.mongodb.core.mapping.MongoPersistentEntity;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.mask.DataMaskRules;
import com.capgemini.psd2.mongo.config.PSD2MongoTemplate;
import com.mongodb.DB;
import com.mongodb.DBCollection;


public class PSD2MongoTemplateTest {

	PSD2MongoTemplate psd2MongoTemplate;

	@Mock
	MappingContext mappingContext;
	
	@Mock
	DBCollection collection;
	@Mock
	RuntimeException exp;
	
	@Mock 
	MongoPersistentEntity<?> mongo;
	
	@Mock
	PersistenceExceptionTranslator translator;
	
	@Mock
	MongoTemplate mongoTemplate;
	
	@Mock
	DBCollection dbCollection;

	@Mock
	DB db;
	@Mock
	MongoConverter mongoConverter;
	@Before
	public void before() {
		psd2MongoTemplate = Mockito.spy(new PSD2MongoTemplate(Mockito.mock(MongoDbFactory.class), mongoConverter));
		MockitoAnnotations.initMocks(this);
	}
	@Test(expected = DataIntegrityViolationException.class)
	public void saveTest() throws JSONException{
		ReflectionTestUtils.setField(psd2MongoTemplate, "suppressedError", "suppressedError");
		DataMaskRules objectToSave = new DataMaskRules();
		doThrow(new DataIntegrityViolationException("Test")).when(psd2MongoTemplate).save(any(),any());
		doReturn(mappingContext).when(mongoConverter).getMappingContext();
		psd2MongoTemplate.save(objectToSave);
	}

	@Test
	public void saveExceptionTest() throws JSONException{
		ReflectionTestUtils.setField(psd2MongoTemplate, "suppressedError", "suppressedError");
		DataMaskRules objectToSave = new DataMaskRules();
		doThrow(new DataIntegrityViolationException("suppressedError")).when(psd2MongoTemplate).save(any(),any());
		doReturn(mappingContext).when(mongoConverter).getMappingContext();
		psd2MongoTemplate.save(objectToSave);
	}
	
	@Test
	public void save1Test() throws JSONException{
		ReflectionTestUtils.setField(psd2MongoTemplate, "suppressedError", "suppressedError");
		DataMaskRules objectToSave = new DataMaskRules();
		doThrow(new DataIntegrityViolationException("suppressedError")).when(psd2MongoTemplate).execute(any(String.class),any());
		psd2MongoTemplate.save(objectToSave, "collectionName");
	}
	
	@Test(expected = DataIntegrityViolationException.class)
	public void save1ExceptionTest() throws JSONException{
		ReflectionTestUtils.setField(psd2MongoTemplate, "suppressedError", "suppressedError");
		DataMaskRules objectToSave = new DataMaskRules();
		doThrow(new DataIntegrityViolationException("test")).when(psd2MongoTemplate).execute(any(String.class),any());
		psd2MongoTemplate.save(objectToSave, "collectionName");
	}

	@Test
	public void insertTest() throws JSONException{
		ReflectionTestUtils.setField(psd2MongoTemplate, "suppressedError", "suppressedError");
		DataMaskRules objectToSave = new DataMaskRules();
		doReturn(mappingContext).when(mongoConverter).getMappingContext();
		doThrow(new DataIntegrityViolationException("suppressedError")).when(psd2MongoTemplate).insert(any(Class.class), any());
		psd2MongoTemplate.insert(objectToSave);
	}
	
	@Test(expected = DataIntegrityViolationException.class)
	public void insertExceptionTest() throws JSONException{
		ReflectionTestUtils.setField(psd2MongoTemplate, "suppressedError", "suppressedError");
		DataMaskRules objectToSave = new DataMaskRules();
		doReturn(mappingContext).when(mongoConverter).getMappingContext();
		doThrow(new DataIntegrityViolationException("test")).when(psd2MongoTemplate).insert(any(Class.class), any());
		psd2MongoTemplate.insert(objectToSave);
	}
	
	
}
