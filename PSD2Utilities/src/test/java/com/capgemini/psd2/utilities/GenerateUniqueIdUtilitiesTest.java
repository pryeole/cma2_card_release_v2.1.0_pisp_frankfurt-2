/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.utilities;

import static org.junit.Assert.assertNotEquals;

import java.util.UUID;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;

/**
 * The Class GenerateUniqueIdUtilitiesTest.
 */
public class GenerateUniqueIdUtilitiesTest {

	/**
	 * Test get unique id.
	 */
	@SuppressWarnings("static-access")
	@Test
	public void testGetUniqueId() {
		UUID uid = UUID.fromString("38400000-8cf0-11bd-b23e-10b96e4ef00d");
		UUID uniqueId = uid.randomUUID();
		assertNotEquals(uniqueId, GenerateUniqueIdUtilities.getUniqueId());
	}

	/**
	 * Test generate random unique ID.
	 */
	@Test
	public void testGenerateRandomUniqueID() {
		String uniqueNumber = RandomStringUtils.random(8, false, true);
		assertNotEquals(uniqueNumber, GenerateUniqueIdUtilities.generateRandomUniqueID());
	}
	
	@Test
	public void generateRandomClientSeceret(){
		String uniqueNumber = RandomStringUtils.random(8, false, true);
		assertNotEquals(uniqueNumber, GenerateUniqueIdUtilities.generateRandomClientSeceret());
	}
}
