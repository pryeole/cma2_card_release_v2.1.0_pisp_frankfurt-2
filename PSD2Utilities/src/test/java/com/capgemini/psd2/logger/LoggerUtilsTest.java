/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.logger;

import java.util.UUID;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

/**
 * The Class LoggerUtilsTest.
 */
public class LoggerUtilsTest {
	
	/** The request header attributes. */
	@Mock
	RequestHeaderAttributes requestHeaderAttributes;
		
	/** The logger attribute. */
	@Mock
	private LoggerAttribute loggerAttribute;
	
	/** The logger utils. */
	@InjectMocks
	private LoggerUtils loggerUtils = new LoggerUtils();
	
	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Test populate logger data.
	 */
	@Test
	public void testPopulateLoggerData() {
		Mockito.when(requestHeaderAttributes.getCorrelationId()).thenReturn("ba4f73f8-9a60-425b-aed8-2a7ef2509fea");
		Mockito.when(requestHeaderAttributes.getFinancialId()).thenReturn("finance123");
		Mockito.when(requestHeaderAttributes.getCustomerIPAddress()).thenReturn("10.1.1.2");
		Mockito.when(requestHeaderAttributes.getCustomerLastLoggedTime()).thenReturn("10-10-2017T10:10");
		UUID uuid = UUID.randomUUID();
		Mockito.when(requestHeaderAttributes.getRequestId()).thenReturn(uuid);
		Mockito.when(requestHeaderAttributes.getTppLegalEntityName()).thenReturn("Moneywise");
		Mockito.when(requestHeaderAttributes.getTppCID()).thenReturn("Moneywise");
		Mockito.when(requestHeaderAttributes.getPsuId()).thenReturn("psuid");
		loggerAttribute = loggerUtils.populateLoggerData("doFilterInternal()");
	}
	
	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		requestHeaderAttributes = null;
		loggerAttribute = null;
	}

}
