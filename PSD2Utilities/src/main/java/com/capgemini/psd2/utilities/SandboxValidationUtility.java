package com.capgemini.psd2.utilities;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBCharge1Amount;
import com.capgemini.psd2.pisp.domain.OBChargeBearerType1Code;
import com.capgemini.psd2.pisp.domain.OBExchangeRate1;
import com.capgemini.psd2.pisp.domain.OBExchangeRate2;
import com.capgemini.psd2.pisp.domain.OBExchangeRateType2Code;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.OBMultiAuthorisation1;
import com.capgemini.psd2.pisp.enums.ProcessConsentStatusEnum;
import com.capgemini.psd2.pisp.enums.ProcessExecutionStatusEnum;

@Component
public class SandboxValidationUtility {

	@Autowired
	private SandboxValidationConfig sandboxValidationConfig;

	@Value("${app.exchangeRate.expirationTime:10}")
	private String expirationTime;

	@Value("${app.exchangeRate.rate:1.5}")
	private String mockedExchangeRate;

	public boolean isValidCurrency(String currency) {
		/* Check for allowed currencies in domestic apis */
		if (!sandboxValidationConfig.getAllowedCurrencies().isEmpty()) {
			if (!NullCheckUtils.isNullOrEmpty(currency)) {
				if (!sandboxValidationConfig.getAllowedCurrencies().contains(currency)) {
					return false;
				}
			}
		}
		/* Check for unsupported currencies in international apis */
		if (!sandboxValidationConfig.getUnsupportedCurrencies().isEmpty()) {
			if (!NullCheckUtils.isNullOrEmpty(currency)) {
				if (sandboxValidationConfig.getUnsupportedCurrencies().contains(currency)) {
					return false;
				}
			}
		}
		return true;
	}

	public boolean isValidAmount(String amount, String regexString) {
		if (!sandboxValidationConfig.getAmountValidation().isEmpty()) {
			if (sandboxValidationConfig.getAmountValidation().containsKey(regexString)) {
				if (amount != null && !amount.isEmpty()) {

					if (Pattern.compile(sandboxValidationConfig.getAmountValidation().get(regexString)).matcher(amount)
							.find()) {
						// if(!accountConfig.getAmountValidationSandbox().get(0).matches(amount)){
						return true;

					}
				}

			}
		}

		return false;
	}

	public static String getTemporalAccessorInISOFormat(TemporalAccessor input) {
		String isoDateTime = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZ").format(input);
		isoDateTime = isoDateTime.substring(0, 22).concat(":").concat(isoDateTime.substring(22, 24));
		return isoDateTime;
	}

	public String getMockedExpectedExecDtTm() {
		return getTemporalAccessorInISOFormat(ZonedDateTime.now().plusDays(1));
	}

	public String getMockedExpectedSettlementDtTm() {
		return getTemporalAccessorInISOFormat(ZonedDateTime.now().plusDays(2));
	}

	public String getMockedCutOffDtTm() {
		return getTemporalAccessorInISOFormat(ZonedDateTime.now().plusDays(3));
	}
	
	public String addDaysToCurrentDtTm(int days){
		return getTemporalAccessorInISOFormat(ZonedDateTime.now().plusDays(days));
	}

	public List<OBCharge1> getMockedOBChargeList(String currency, String initiationAmount,
			OBChargeBearerType1Code chargeBearer) {

		OBCharge1 obCharge1 = new OBCharge1();

		if (null == chargeBearer) {
			if (sandboxValidationConfig.getCharges().get(PSD2Constants.BORNEBYCREDITOR).contains(currency)) {
				obCharge1.setChargeBearer(OBChargeBearerType1Code.BORNEBYCREDITOR);
				return populateCharge(obCharge1, sandboxValidationConfig.getChargeCurrency(), initiationAmount);
			} else if (sandboxValidationConfig.getCharges().get(PSD2Constants.BORNEBYDEBTOR).contains(currency)) {
				obCharge1.setChargeBearer(OBChargeBearerType1Code.BORNEBYDEBTOR);
				return populateCharge(obCharge1, sandboxValidationConfig.getChargeCurrency(), initiationAmount);
			} else if (sandboxValidationConfig.getCharges().get(PSD2Constants.FOLLOWINGSERVICELEVEL)
					.contains(currency)) {
				obCharge1.setChargeBearer(OBChargeBearerType1Code.FOLLOWINGSERVICELEVEL);
				return populateCharge(obCharge1, sandboxValidationConfig.getChargeCurrency(), initiationAmount);
			} else if (sandboxValidationConfig.getCharges().get(PSD2Constants.SHARED).contains(currency)) {
				obCharge1.setChargeBearer(OBChargeBearerType1Code.SHARED);
				return populateCharge(obCharge1, sandboxValidationConfig.getChargeCurrency(), initiationAmount);
			}
		} else {
			obCharge1.setChargeBearer(chargeBearer);
			return populateCharge(obCharge1, currency, initiationAmount);
		}

		return null;
	}

	private List<OBCharge1> populateCharge(OBCharge1 obCharge1, String currency, String initiationAmount) {
		double charge = Double.parseDouble(sandboxValidationConfig.getChargeAmount()) / 100;
		double chargeLimit = Double.parseDouble(sandboxValidationConfig.getChargeAmountLimit());
		Double d = Double.parseDouble(initiationAmount) * charge;
		String chargeAmount = null;
		if( d > chargeLimit){
			chargeAmount = Double.toString(chargeLimit);
		}else{
			chargeAmount = String.format("%.2f", d);
		}
		OBCharge1Amount amount = new OBCharge1Amount();
		amount.setAmount(chargeAmount);
		amount.setCurrency(sandboxValidationConfig.getChargeCurrency());
		obCharge1.setAmount(amount);
		obCharge1.setType("Type1");

		List<OBCharge1> charges = new ArrayList<>();
		charges.add(obCharge1);
		return charges;
	}

	public ProcessExecutionStatusEnum getMockedProcessExecStatus(String currency, String initiationAmount) {

		/* 900 Scenario */
		if (isValidAmount(initiationAmount, PSD2Constants.CUT_OFF_DATE_EXCEPTION)) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(
					OBErrorCodeEnum.UK_OBIE_RULES_AFTERCUTOFFDATETIME, ErrorMapKeys.AFTER_CUTOFF_DATE));
		}

		/* Incomplete mocked condition */
		if (isValidAmount(initiationAmount, PSD2Constants.PROCESS_EXEC_INCOMPLETE))
			return ProcessExecutionStatusEnum.INCOMPLETE;

		/* Abort mocked condition */
		if (isValidAmount(initiationAmount, PSD2Constants.PROCESS_EXEC_ABORT))
			return ProcessExecutionStatusEnum.ABORT;

		/* Failure mocked condition for sandbox */
		if (!isValidCurrency(currency) || isValidAmount(initiationAmount, PSD2Constants.REJECTED_CONDITION))
			return ProcessExecutionStatusEnum.FAIL;

		/* default PASS mocked condition */
		return ProcessExecutionStatusEnum.PASS;

	}

	public ProcessConsentStatusEnum getMockedConsentProcessExecStatus(String currency, String initiationAmount) {
		/* Failure mocked condition for sandbox */
		if (!isValidCurrency(currency) || isValidAmount(initiationAmount, PSD2Constants.REJECTED_CONDITION)) {
			return ProcessConsentStatusEnum.FAIL;
		}
		/* Internal Server Error */
		if (isValidAmount(initiationAmount, PSD2Constants.PROCESS_EXEC_INCOMPLETE_CONSENT)) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.PISP_NO_PAYMENT_SETUP_RESOURCES_FOUND));
		}
		return ProcessConsentStatusEnum.PASS;
	}

	public OBMultiAuthorisation1 getMockedMultiAuthBlock(String currency, String initiationAmount) {
		OBMultiAuthorisation1 multiAuthBlock = null;

		/* Multi Authorisation mocked "Authorised" condition */
		if (isValidAmount(initiationAmount, PSD2Constants.M_AUTHORISED)) {
			multiAuthBlock = new OBMultiAuthorisation1();
			multiAuthBlock.setStatus(OBExternalStatus2Code.AUTHORISED);
			multiAuthBlock.setNumberRequired(2);
			multiAuthBlock.setNumberReceived(2);
			multiAuthBlock.setLastUpdateDateTime(DateUtilites.getCurrentDateInISOFormat());
			multiAuthBlock.setExpirationDateTime(getMockedCutOffDtTm());
		}

		/* Multi Authorisation mocked "Awaiting Multi Auth" condition */
		if (isValidAmount(initiationAmount, PSD2Constants.M_AWAITINGFURTHERAUTHORISATION)) {
			multiAuthBlock = new OBMultiAuthorisation1();
			multiAuthBlock.setStatus(OBExternalStatus2Code.AWAITINGFURTHERAUTHORISATION);
			multiAuthBlock.setNumberRequired(2);
			multiAuthBlock.setNumberReceived(1);
			multiAuthBlock.setLastUpdateDateTime(DateUtilites.getCurrentDateInISOFormat());
			multiAuthBlock.setExpirationDateTime(getMockedCutOffDtTm());
		}

		/* Multi Authorisation mocked "Rejected" condition */
		if (isValidAmount(initiationAmount, PSD2Constants.M_REJECTED)) {
			multiAuthBlock = new OBMultiAuthorisation1();
			multiAuthBlock.setStatus(OBExternalStatus2Code.REJECTED);
			multiAuthBlock.setNumberRequired(2);
			multiAuthBlock.setNumberReceived(1);
			multiAuthBlock.setLastUpdateDateTime(DateUtilites.getCurrentDateInISOFormat());
			multiAuthBlock.setExpirationDateTime(getMockedCutOffDtTm());

		}
		return multiAuthBlock;
	}

	public OBExchangeRate2 getMockedExchangeRateInformation(OBExchangeRate1 tppExchangeRateBlock,
			String initiationAmount) {
		if (null != tppExchangeRateBlock) {
			OBExchangeRate2 mockedExchangeRateBlock = new OBExchangeRate2();
			mockedExchangeRateBlock.setRateType(tppExchangeRateBlock.getRateType());
			mockedExchangeRateBlock.setUnitCurrency(tppExchangeRateBlock.getUnitCurrency());

			/* Expiration date only for actual type */
			if (tppExchangeRateBlock.getRateType() == OBExchangeRateType2Code.ACTUAL)
				mockedExchangeRateBlock.setExpirationDateTime(getTemporalAccessorInISOFormat(
						ZonedDateTime.now().plusMinutes(Long.parseLong(expirationTime))));

			/*
			 * If its aggreed, bank will populate same exchange rate block, tpp
			 * provided.
			 */
			if (tppExchangeRateBlock.getRateType() == OBExchangeRateType2Code.AGREED) {
				mockedExchangeRateBlock.setContractIdentification(tppExchangeRateBlock.getContractIdentification());
				mockedExchangeRateBlock.setExchangeRate(tppExchangeRateBlock.getExchangeRate());
			}
			/* Mock the fields */
			else {
				String mockedIdentification = "mockedIdentification";
				mockedExchangeRateBlock.setContractIdentification(mockedIdentification);
				mockedExchangeRateBlock.setExchangeRate(BigDecimal.valueOf(Double.parseDouble(mockedExchangeRate)));
			}
			return mockedExchangeRateBlock;
		}
		return null;
	}

	public boolean isValidPsuId(String psuId) {
		if (!sandboxValidationConfig.getPsuId().isEmpty()) {
			if (psuId != null && !psuId.isEmpty()) {
				if (sandboxValidationConfig.getPsuId().equals(psuId)) {
					return true;
				}

			}

		}
		return false;
	}

	/* Check for rejected PSUIds in accountStatement api download endpoint */
	public boolean isValidPsuIdForDownload(String psuId) {
		if (!sandboxValidationConfig.getRejectedPSUIds().isEmpty()) {
			if (!NullCheckUtils.isNullOrEmpty(psuId)) {
				if (sandboxValidationConfig.getRejectedPSUIds().contains(psuId)) {
					return true;
				}
			}

		}
		return false;
	}

	public boolean isValidSecIdentification(String secondaryIdentification) {
		/* Check for allowed currencies in domestic apis */
		if (!sandboxValidationConfig.getSecondaryIdentificationRule().isEmpty()) {
			if (!NullCheckUtils.isNullOrEmpty(secondaryIdentification)) {
				if (Pattern.compile(sandboxValidationConfig.getSecondaryIdentificationRule())
						.matcher(secondaryIdentification).find()) {
					// if(!accountConfig.getAmountValidationSandbox().get(0).matches(amount)){
					return true;

				}
			}
		}
		return false;
	}

}
