package com.capgemini.psd2.utilities;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
@Component
public  class ValidationUtility {


	private static String uuIdRegexPattern;
	
	@Value("${app.regex.uuId:#{null}}")
	public  void setUuIdRegexPattern(String uuIdRegexPtrn) {
		uuIdRegexPattern = uuIdRegexPtrn;
	}
	 
	public static void isValidUUID(String uuidString) {
		if(uuidString != null && uuIdRegexPattern != null && !uuidString.matches(uuIdRegexPattern)) {
			throw PSD2Exception.populatePSD2Exception("The provided input string is not valid UUID",ErrorCodeEnum.VALIDATION_ERROR);
		}
	}
	
	public static void isValidField(String field, String regEx, int minLength, int maxLength ){

		if (field.length() < minLength || field.length() > maxLength) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.VALIDATION_ERROR);
		}

		Pattern p = Pattern.compile(regEx);
		Matcher m = p.matcher(field);
		if (m.find()) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.VALIDATION_ERROR);
		}
	
	}
}
