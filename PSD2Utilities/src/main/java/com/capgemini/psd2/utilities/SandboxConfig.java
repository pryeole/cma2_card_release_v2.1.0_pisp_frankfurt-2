package com.capgemini.psd2.utilities;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Component
@Configuration
@EnableConfigurationProperties
@ConfigurationProperties("app")
public class SandboxConfig {

	@Value("${app.isSandboxEnabled:#{false}}")
	private boolean isSandboxEnabled;

	@Value("${app.sandboxPassword:#{'331947'}}")
	private String sandboxPassword;

	public boolean isSandboxEnabled() {
		return isSandboxEnabled;
	}

	public void setSandboxEnabled(boolean isSandboxEnabled) {
		this.isSandboxEnabled = isSandboxEnabled;
	}

	public String getSandboxPassword() {
		return sandboxPassword;
	}

	public void setSandboxPassword(String sandboxPassword) {
		this.sandboxPassword = sandboxPassword;
	}


}
