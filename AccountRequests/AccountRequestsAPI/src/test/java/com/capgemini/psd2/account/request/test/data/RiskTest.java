package com.capgemini.psd2.account.request.test.data;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.capgemini.psd2.account.request.data.Risk;


public class RiskTest {

	@Test
	public void testRisk() {
		Risk risk = new Risk();
		risk.setRiskData("");
		assertTrue(risk.getRiskData().equals(""));
	}
}