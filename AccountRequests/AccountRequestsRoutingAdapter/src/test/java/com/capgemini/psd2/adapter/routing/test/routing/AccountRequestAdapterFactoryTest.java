package com.capgemini.psd2.adapter.routing.test.routing;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;

import com.capgemini.psd2.account.request.routing.adapter.impl.AccountRequestRoutingAdapter;
import com.capgemini.psd2.account.request.routing.adapter.routing.AccountRequestCoreSystemAdapterFactory;

public class AccountRequestAdapterFactoryTest {
	
	@Mock
	private ApplicationContext applicationContext;
	
	@InjectMocks
	private AccountRequestCoreSystemAdapterFactory accountRequestCoreSystemAdapterFactory;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testAccountBalanceAdapter() {
		AccountRequestRoutingAdapter accountRequestRoutingAdapter = new AccountRequestRoutingAdapter();
		when(applicationContext.getBean(anyString())).thenReturn(accountRequestRoutingAdapter);
		AccountRequestRoutingAdapter accountRequestRoutingAdapterResult = (AccountRequestRoutingAdapter) accountRequestCoreSystemAdapterFactory.getAdapterInstance(anyString());
		assertEquals(accountRequestRoutingAdapter, accountRequestRoutingAdapterResult);
	}
}
