<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<%@ page session="false" %>
<html lang="en">
<head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
      <title>BOI: Error</title>
<base
	href="/<%=request.getAttribute("applicationName")%><%=request.getContextPath() %>/">
<link rel="apple-touch-icon"
	href="${cdnBaseURL}/consent-ui/${buildVersion}/img/apple-touch-icon.png">
<link rel="stylesheet"
	href="${cdnBaseURL}/consent-ui/${buildVersion}/css/error_main.css">
<link rel="shortcut icon" type="image/x-icon"
	href="${cdnBaseURL}/consent-ui/${buildVersion}/img/favicon.ico?rev=16" />

</head>
   <body>
      <div  role="alert" class="details-container sr-only">
                 	<div class="account-Details-section">
                    	<div role="alert" class="alert page-alert">
							<p class="msg">
								<span class="alert-prefix">Error:</span><span>${errorMessage}</span>
							</p>
							<c:if test="${not empty errorDescription}">
								<p><span>${errorDescription}</span></p>
							</c:if>
                        </div>
                    </div>                                                                        
               </div>
      <header>
         <div class="header-container">
            <div class="container">
				<img src="${LOGOURL}" title="Bank of Demo logo"
					alt="Bank of Demo logo" class="logo" />
               <div class="portal-details">
                  <h1>Account Access</h1>
                  <h2>Manage third party access to your Bank of Demo accounts</h2>
               </div>
            </div>
         </div>
      </header>
      <!--Error container-->         
      <section class="container main-container" aria-hidden="true">
            <div class="row">
               <div class="col-md-10 col-md-offset-1">
                  <div class="table-header-container">
                        <h3 class="table-header">Error</h3>
                  </div>
                  <div class="details-container">
                 	<div class="account-Details-section">
                    	<div role="alert" class="alert page-alert">
							<p class="msg">
								<span class="alert-prefix">Error:</span><span>${errorMessage}</span>
							</p>
							<c:if test="${not empty errorDescription}">
								<p><span>${errorDescription}</span></p>
							</c:if>
                        </div>
                    </div>                                                                        
               </div>
            </div>
         </section>
         <!--Error container Ends-->      
   </body>
</html>