"use strict";
describe("ConsentApp.modalPopUp_test", function() {
    beforeEach(module("consentTemplates", "consentApp"));
    var compile, ctrl, scope, element, modalData, fakeModal;
    beforeEach(inject(function($controller, $rootScope, $compile) {
        scope = $rootScope.$new();
        scope.modalData = {
            open: function() { /**/ },
            "modelpopupTitle": "Title",
            "modelpopupBodyContent": "Content",
            "btn": {
                "okbtn": { "visible": true, "label": "", "action": function() { /**/ } },
                "cancelbtn": { "visible": true, "label": "", "action": null }
            }
        };
        fakeModal = {
            // create a mock object using spies
            close: jasmine.createSpy("modalInstance.close"),
            dismiss: jasmine.createSpy("modalInstance.dismiss"),
            result: {
                then: jasmine.createSpy("modalInstance.result.then")
            }
        };
        element = angular.element('<model-pop-up modal="modalData"></model-pop-up>');
        $compile(element)(scope);
        //scope.$digest();
    }));

    describe("test modal popup", function() {
        it("should add modal HTML to the DOM", function() {
            element.isolateScope().modal.open();
            scope.$digest();
            expect($('div[uib-modal-window="modal-window"]')[0]).toBeInDOM();
            expect($('div[uib-modal-window="modal-window"]')).toHaveClass("pop-confirm");
        });

        it("should close the modal", function() {
            inject(function($controller, $uibModal) {
                spyOn(element.isolateScope().modal, "open").and.returnValue(fakeModal);
                element.isolateScope().modal.open();
                scope.$digest();
                ctrl = $controller("modalCtrl", { $scope: scope, $uibModalInstance: fakeModal });
                scope.cancelBtnClicked();
                expect(fakeModal.dismiss).toHaveBeenCalled();
            });
        });

        it("should call action method on ok button click", function() {
            inject(function($controller, $uibModal) {
                spyOn(element.isolateScope().modal, "open").and.returnValue(fakeModal);
                element.isolateScope().modal.open();
                scope.$digest();
                ctrl = $controller("modalCtrl", { $scope: element.isolateScope(), $uibModalInstance: fakeModal });
                spyOn(element.isolateScope().modal.btn.okbtn, "action").and.returnValue(true);
                element.isolateScope().okBtnClicked();
                expect(fakeModal.dismiss).toHaveBeenCalled();
            });
        });
    });
});