package com.capgemini.psd2.security.consent.aisp.helpers;

import java.util.List;

import javax.naming.NamingException;

import com.capgemini.psd2.aisp.domain.OBAccount2;
import com.capgemini.psd2.aisp.domain.OBReadAccount2;

public interface AispConsentCreationDataHelper {

	public String retrieveAccountRequestSetupData(String intentId);

	public OBReadAccount2 retrieveCustomerAccountListInfo(String userId, String clientId, String flowType,
			String correlationId, String channelId, String tenantId, String intentId);

	public void createConsent(List<OBAccount2> accountList, String userId, String cid, String intentId,
			String channelId, String headers, String tppAppName, Object tppInformation, String tennantId) throws NamingException;

	public void cancelAccountRequest(String intentId);

	public OBReadAccount2 findExistingConsentAccounts(String intentId);
	
	public String getCMAVersion(String intentId);

}
