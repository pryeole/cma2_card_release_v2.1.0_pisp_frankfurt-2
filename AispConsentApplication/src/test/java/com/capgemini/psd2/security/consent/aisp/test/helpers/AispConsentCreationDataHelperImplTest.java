package com.capgemini.psd2.security.consent.aisp.test.helpers;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.adapter.CustomerAccountListAdapter;
import com.capgemini.psd2.aisp.domain.OBAccount2;
import com.capgemini.psd2.aisp.domain.OBBranchAndFinancialInstitutionIdentification4;
import com.capgemini.psd2.aisp.domain.OBCashAccount3;
import com.capgemini.psd2.aisp.domain.OBExternalRequestStatus1Code;
import com.capgemini.psd2.aisp.domain.OBReadAccount2;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.fraudsystem.helper.FraudSystemHelper;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.security.consent.aisp.helpers.AispConsentCreationDataHelperImpl;

@RunWith(SpringJUnit4ClassRunner.class)
public class AispConsentCreationDataHelperImplTest {

	@Mock
	private LoggerUtils loggerUtils;
	
	@Mock
	private AccountRequestAdapter accountRequestAdapter;

	@Mock
	private AispConsentAdapter aispConsentAdapter;

	@Mock
	private CustomerAccountListAdapter customerAccountListAdapter;

	@Mock
	private RequestHeaderAttributes reqAttributes;

	@Mock
	private FraudSystemHelper fraudSystemHelper;

	// @Mock
	// private DatatypeConverter dateConverter;

	@InjectMocks
	private AispConsentCreationDataHelperImpl consentCreationDataHelperImpl = new AispConsentCreationDataHelperImpl();

	@Before
	public void setUp() {
		Map<String, String> map=new HashMap<>();
		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");
		Map<String, String> specificErrorMessageMap=new HashMap<>();
		specificErrorMessageMap.put("signature_invalid_content", "Invalid content prsent in joseheader of x-jws-signature header");
		specificErrorMessageMap.put("signature_missing", "signature header missing in request");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
		MockitoAnnotations.initMocks(this);
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put("AISP", "AISP");
		paramMap.put("client_id", "client123");
		paramMap.put("AccountRequestId", "accReqId123");
	}

	@Test
	public void testRetrieveAccountRequestSetupDataAISPCrationDateBeforeException() {
		OBReadConsentResponse1 accountRequestPOSTResponse = new OBReadConsentResponse1();
		OBReadConsentResponse1Data data = new OBReadConsentResponse1Data();
		data.setCreationDateTime("2017-02-25T00:00:00-00:00");
		accountRequestPOSTResponse.data(data);
		when(accountRequestAdapter.getAccountRequestGETResponse(anyString())).thenReturn(accountRequestPOSTResponse);
		consentCreationDataHelperImpl.retrieveAccountRequestSetupData("dummy");
	}

	@Test(expected = PSD2Exception.class)
	public void testRetrieveAccountRequestSetupForException() {
		OBReadConsentResponse1 accountRequestPOSTResponse = new OBReadConsentResponse1();
		OBReadConsentResponse1Data data = new OBReadConsentResponse1Data();
		data.setCreationDateTime("2017-02-25T00:00:00-00:00");
		accountRequestPOSTResponse.data(data);
		when(accountRequestAdapter.getAccountRequestGETResponse(anyString())).thenReturn(null);
		consentCreationDataHelperImpl.retrieveAccountRequestSetupData("dummy");
	}

	@Test
	public void testCreateConsent() throws NamingException {
		doNothing().when(aispConsentAdapter).createConsent(any(AispConsent.class));
		when(accountRequestAdapter.getAccountRequestGETResponse(anyString()))
				.thenReturn(getAccountRequestPOSTResponseInvalidStatus());

		doNothing().when(fraudSystemHelper).captureFraudEvent(anyObject(), anyObject(), anyObject());

		List<OBAccount2> acctList = new ArrayList<>();

		PSD2Account customerAccountInfo1 = new PSD2Account();
		List<OBCashAccount3> accountList = new ArrayList<>();
		OBCashAccount3 account = new OBCashAccount3();
		account.setIdentification("IE29AIBK93115212345678");
		account.setSchemeName("IBAN");
		accountList.add(account);
		OBBranchAndFinancialInstitutionIdentification4 servicer = new OBBranchAndFinancialInstitutionIdentification4();
		servicer.setIdentification("123456");
		customerAccountInfo1.setServicer(servicer);
		customerAccountInfo1.setAccount(accountList);
		customerAccountInfo1.setCurrency("EUR");
		customerAccountInfo1.setNickname("John");
		customerAccountInfo1.setHashedValue("123");
		PSD2Account customerAccountInfo2 = new PSD2Account();

		customerAccountInfo2.setCurrency("EUR");
		customerAccountInfo2.setNickname("John");
		customerAccountInfo2.setAccount(accountList);
		customerAccountInfo2.setHashedValue("123");
		customerAccountInfo2.setServicer(servicer);
		Map<String, String> additionalInformation = new HashMap<>();
		additionalInformation.put(PSD2Constants.ACCOUNT_NSC, "123");
		additionalInformation.put(PSD2Constants.ACCOUNT_NUMBER, "456");
		customerAccountInfo1.setAdditionalInformation(additionalInformation);
		customerAccountInfo2.setAdditionalInformation(additionalInformation);
		acctList.add(customerAccountInfo1);
		acctList.add(customerAccountInfo2);

		consentCreationDataHelperImpl.createConsent(acctList, null, null, null, null, "eC1mYXBpLWZpbmFuY2lhbElk", null,
				null,null);
	}

	@Test
	public void testCreateConsent4() throws NamingException {
		doNothing().when(aispConsentAdapter).createConsent(any(AispConsent.class));
		when(accountRequestAdapter.getAccountRequestGETResponse(anyString()))
				.thenReturn(getAccountRequestPOSTResponseInvalidStatus());

		doNothing().when(fraudSystemHelper).captureFraudEvent(anyObject(), anyObject(), anyObject());

		List<OBAccount2> acctList = new ArrayList<>();

		PSD2Account customerAccountInfo1 = new PSD2Account();

		List<OBCashAccount3> accountList = new ArrayList<>();
		OBCashAccount3 account = new OBCashAccount3();
		account.setIdentification("IE29AIBK93115212345678");
		account.setSchemeName("IBAN");
		OBBranchAndFinancialInstitutionIdentification4 servicer = new OBBranchAndFinancialInstitutionIdentification4();
		servicer.setIdentification("123456");
		customerAccountInfo1.setServicer(servicer);
		accountList.add(account);
		customerAccountInfo1.setAccount(accountList);
		customerAccountInfo1.setCurrency("EUR");
		customerAccountInfo1.setNickname("John");
		customerAccountInfo1.setHashedValue("123");
		PSD2Account customerAccountInfo2 = new PSD2Account();

		customerAccountInfo2.setCurrency("EUR");
		customerAccountInfo2.setNickname("John");
		customerAccountInfo2.setAccount(accountList);
		customerAccountInfo2.setHashedValue("123");
		customerAccountInfo2.setServicer(servicer);
		Map<String, String> additionalInformation = new HashMap<>();
		additionalInformation.put(PSD2Constants.ACCOUNT_NSC, "123");
		additionalInformation.put(PSD2Constants.ACCOUNT_NUMBER, "456");
		customerAccountInfo1.setAdditionalInformation(additionalInformation);
		customerAccountInfo2.setAdditionalInformation(additionalInformation);
		acctList.add(customerAccountInfo1);
		acctList.add(customerAccountInfo2);

		consentCreationDataHelperImpl.createConsent(acctList, null, null, null, null, "eC1mYXBpLWZpbmFuY2lhbElk", null,
				null,null);
	}

	@Test(expected = Exception.class)
	public void testCreateConsentBranches() throws NamingException {
		doNothing().when(aispConsentAdapter).createConsent(any(AispConsent.class));
		when(accountRequestAdapter.getAccountRequestGETResponse(anyString()))
				.thenReturn(getAccountRequestPOSTResponseInvalidStatus());

		doNothing().when(fraudSystemHelper).captureFraudEvent(anyObject(), anyObject(), anyObject());

		List<OBAccount2> acctList = new ArrayList<>();

		PSD2Account customerAccountInfo1 = new PSD2Account();
		List<OBCashAccount3> accountList = new ArrayList<>();
		OBCashAccount3 account = new OBCashAccount3();
		account.setIdentification("12345");
		account.setSchemeName("SortCodeAccountNumber");
		accountList.add(account);
		OBBranchAndFinancialInstitutionIdentification4 servicer = new OBBranchAndFinancialInstitutionIdentification4();
		servicer.setIdentification("12345678901234");
		customerAccountInfo1.setServicer(servicer);
		customerAccountInfo1.setAccount(accountList);
		customerAccountInfo1.setCurrency("EUR");
		customerAccountInfo1.setNickname("John");
		customerAccountInfo1.setHashedValue("123");
		PSD2Account customerAccountInfo2 = new PSD2Account();

		customerAccountInfo2.setCurrency("EUR");
		customerAccountInfo2.setNickname("John");
		customerAccountInfo2.setAccount(accountList);
		customerAccountInfo2.setHashedValue("123");
		customerAccountInfo2.setServicer(servicer);
		Map<String, String> additionalInformation = new HashMap<>();
		additionalInformation.put(PSD2Constants.ACCOUNT_NSC, "123");
		additionalInformation.put(PSD2Constants.ACCOUNT_NUMBER, "456");
		customerAccountInfo1.setAdditionalInformation(additionalInformation);
		customerAccountInfo2.setAdditionalInformation(additionalInformation);
		acctList.add(customerAccountInfo1);
		acctList.add(customerAccountInfo2);

		consentCreationDataHelperImpl.createConsent(acctList, null, null, null, null, null, null, null,null);
	}

	@Test
	public void testCreateConsentBranches3() throws NamingException {
		doNothing().when(aispConsentAdapter).createConsent(any(AispConsent.class));
		when(accountRequestAdapter.getAccountRequestGETResponse(anyString()))
				.thenReturn(getAccountRequestPOSTResponseInvalidStatus());

		doNothing().when(fraudSystemHelper).captureFraudEvent(anyObject(), anyObject(), anyObject());

		List<OBAccount2> acctList = new ArrayList<>();

		PSD2Account customerAccountInfo1 = new PSD2Account();

		List<OBCashAccount3> accountList = new ArrayList<>();
		OBCashAccount3 account = new OBCashAccount3();
		account.setIdentification("12345678901234");
		account.setSchemeName("SortCodeAccountNumber");
		OBBranchAndFinancialInstitutionIdentification4 servicer = new OBBranchAndFinancialInstitutionIdentification4();
		servicer.setIdentification("12345678901234");
		accountList.add(account);
		customerAccountInfo1.setServicer(servicer);
		customerAccountInfo1.setAccount(accountList);
		customerAccountInfo1.setCurrency("EUR");
		customerAccountInfo1.setNickname("John");
		customerAccountInfo1.setHashedValue("123");
		PSD2Account customerAccountInfo2 = new PSD2Account();

		customerAccountInfo2.setCurrency("EUR");
		customerAccountInfo2.setNickname("John");
		customerAccountInfo2.setAccount(accountList);
		customerAccountInfo2.setHashedValue("123");
		customerAccountInfo2.setServicer(servicer);
		Map<String, String> additionalInformation = new HashMap<>();
		additionalInformation.put(PSD2Constants.ACCOUNT_NSC, "123");
		additionalInformation.put(PSD2Constants.ACCOUNT_NUMBER, "456");
		customerAccountInfo1.setAdditionalInformation(additionalInformation);
		customerAccountInfo2.setAdditionalInformation(additionalInformation);
		acctList.add(customerAccountInfo1);
		acctList.add(customerAccountInfo2);

		consentCreationDataHelperImpl.createConsent(acctList, null, null, null, null, "eC1mYXBpLWZpbmFuY2lhbElk", null,
				null,null);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testRetrieveCustomerAccountListInfoSuccessFlow() {
		// when(securityRequestAttributes.getFlowType()).thenReturn("AISP");
		when(reqAttributes.getCorrelationId()).thenReturn("123445");
		when(customerAccountListAdapter.retrieveCustomerAccountList(anyString(), anyMap()))
				.thenReturn(new OBReadAccount2());
		when(accountRequestAdapter.getAccountRequestGETResponse(anyString()))
		.thenReturn(getAccountRequestPOSTResponseInvalidStatus());
		consentCreationDataHelperImpl.retrieveCustomerAccountListInfo(null, null, null, null, null, null,null);
	}

	@Test
	public void testCancelAccountRequest() {
		when(aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(anyString(), any(ConsentStatusEnum.class)))
				.thenReturn(new AispConsent());
		doNothing().when(aispConsentAdapter).updateConsentStatus(anyString(), any(ConsentStatusEnum.class));
		when(accountRequestAdapter.updateAccountRequestResponse(anyString(), any(OBExternalRequestStatus1Code.class)))
				.thenReturn(new OBReadConsentResponse1());

		consentCreationDataHelperImpl.cancelAccountRequest("");
	}

	public static List<PSD2Account> getCustomerAccountsNew() {
		List<PSD2Account> mockCustomerAccountList = new ArrayList<>();
		PSD2Account customerAccountInfo1 = new PSD2Account();
		List<OBCashAccount3> accountList = new ArrayList<>();
		OBCashAccount3 account = new OBCashAccount3();
		account.setIdentification("12345");
		account.setSchemeName("IBAN");
		accountList.add(account);
		OBBranchAndFinancialInstitutionIdentification4 servicer = new OBBranchAndFinancialInstitutionIdentification4();
		servicer.setIdentification("12345");
		customerAccountInfo1.setServicer(servicer);
		customerAccountInfo1.setAccount(accountList);
		customerAccountInfo1.setCurrency("EUR");
		customerAccountInfo1.setNickname("John");
		customerAccountInfo1.setHashedValue("123");
		PSD2Account customerAccountInfo2 = new PSD2Account();

		customerAccountInfo2.setCurrency("EUR");
		customerAccountInfo2.setNickname("John");
		customerAccountInfo2.setAccount(accountList);
		customerAccountInfo2.setHashedValue("123");
		customerAccountInfo2.setServicer(servicer);

		mockCustomerAccountList.add(customerAccountInfo1);
		mockCustomerAccountList.add(customerAccountInfo2);
		return mockCustomerAccountList;
	}

	public static OBReadConsentResponse1 getAccountRequestPOSTResponseInvalidStatus() {
		OBReadConsentResponse1 mockAccountRequestPOSTResponse = new OBReadConsentResponse1();

		OBReadConsentResponse1Data data = new OBReadConsentResponse1Data();
		List<com.capgemini.psd2.aisp.domain.OBExternalPermissions1Code> permissions = new ArrayList<>();
		permissions.add(com.capgemini.psd2.aisp.domain.OBExternalPermissions1Code.READACCOUNTSBASIC);
		permissions.add(com.capgemini.psd2.aisp.domain.OBExternalPermissions1Code.READACCOUNTSDETAIL);
		permissions.add(com.capgemini.psd2.aisp.domain.OBExternalPermissions1Code.READBALANCES);
		data.setPermissions(permissions);
		data.setCreationDateTime("2017-12-25T00:00:00-00:00");
		data.setExpirationDateTime("2017-05-02T00:00:00-00:00");
		data.setTransactionFromDateTime("2017-05-03T00:00:00-00:00");
		data.setTransactionToDateTime("2017-12-03T00:00:00-00:00");
		data.consentId("af5b90c1-64b5-4a52-ba55-5eed68b2a269");
		data.setStatus(OBExternalRequestStatus1Code.AUTHORISED);
		mockAccountRequestPOSTResponse.setData(data);
		mockAccountRequestPOSTResponse.setRisk(null);
		return mockAccountRequestPOSTResponse;
	}
}
