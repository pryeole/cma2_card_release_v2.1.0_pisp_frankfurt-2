/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.security.saas.mongo.db.adapter;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.domain.LoginDetails;
import com.capgemini.psd2.authentication.adapter.AuthenticationAdapter;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.security.exceptions.PSD2SecurityException;
import com.capgemini.psd2.security.saas.mongo.db.adapter.repository.SaaSRepository;
import com.capgemini.psd2.utilities.SandboxConfig;

/**
 * The Class AccountBalanceMongoDbAdaptorImpl.
 */
@Component
public class SaaSMongoDbAdaptorImpl implements AuthenticationAdapter{

	/** The repository. */
	@Autowired
	private SaaSRepository repository;

	@Autowired
	private SandboxConfig sandboxConfig;

	@Override
	public <T> T authenticate(T authenticaiton, Map<String, String> headers) {
		LoginDetails loginDetails;
		try{
			Authentication authenticationObject = (Authentication)authenticaiton;

			String loginId = authenticationObject.getPrincipal().toString();
			String password = null;
			if(Boolean.valueOf(sandboxConfig.isSandboxEnabled())){
				loginDetails = repository.findByLoginIdAndPassword(loginId, sandboxConfig.getSandboxPassword());
			}
			else{
				password = authenticationObject.getCredentials().toString();
				loginDetails = repository.findByLoginIdAndPassword(loginId, password);
			}
		}
		catch(DataAccessResourceFailureException e){
			throw PSD2SecurityException.populatePSD2Exception(e.getMessage(),ErrorCodeEnum.CONNECTION_ERROR); 
		}
		if(loginDetails==null)
			throw PSD2SecurityException.populatePSD2Exception(ErrorCodeEnum.NO_RECORD_FOUND_FOR_LOGINID_PASSWORD);

		return authenticaiton;
	}
}
