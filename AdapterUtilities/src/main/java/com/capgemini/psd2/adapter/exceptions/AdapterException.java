/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.adapter.exceptions;

import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.OBError1;
import com.capgemini.psd2.pisp.domain.OBErrorResponse1;

/**
 * The Class AdapterException.
 */
public class AdapterException extends PSD2Exception{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	private transient Object foundationError;
	/**
	 * Instantiates a new adapter exception.
	 *
	 * @param message the message
	 * @param errorInfo the error info
	 */
	public AdapterException(String message, ErrorInfo errorInfo){
		super(message,errorInfo);
	}		
	
	
	public Object getFoundationError() {
		return foundationError;
	}

	public AdapterException(String message, ErrorInfo errorInfo, Object foundationError){
		super(message,errorInfo);
		this.foundationError = foundationError;
	}
	
	public AdapterException(String message, OBErrorResponse1 obErrorResponse1, Object foundationError){
		super(message,obErrorResponse1);
		this.foundationError = foundationError;
	}
	/**
	 * Populate PSD 2 exception.
	 *
	 * @param detailErrorMessage the detail error message
	 * @param errorCodeEnum the error code enum
	 * @return the adapter exception
	 */
	
	public static AdapterException populatePSD2Exception(String detailErrorMessage, String reponceBody, String status){
		ErrorInfo errorInfo = new ErrorInfo(status, detailErrorMessage, reponceBody, status);
		/*errorInfo.setErrorCode(errorCodeEnum.getErrorCode());			
		errorInfo.setErrorMessage(errorCodeEnum.getErrorMessage());
		errorInfo.setDetailErrorMessage(detailErrorMessage);
		errorInfo.setStatusCode(errorCodeEnum.getStatusCode());*/
		return new AdapterException(detailErrorMessage, errorInfo);
	}
	
	
	public static AdapterException populatePSD2Exception(String detailErrorMessage,AdapterErrorCodeEnum errorCodeEnum){
		ErrorInfo errorInfo = new ErrorInfo(errorCodeEnum.getErrorCode(), errorCodeEnum.getErrorMessage(), errorCodeEnum.getDetailErrorMessage(), errorCodeEnum.getStatusCode(),errorCodeEnum.getObErrorCode() );
		/*errorInfo.setErrorCode(errorCodeEnum.getErrorCode());			
		errorInfo.setErrorMessage(errorCodeEnum.getErrorMessage());
		errorInfo.setDetailErrorMessage(detailErrorMessage);
		errorInfo.setStatusCode(errorCodeEnum.getStatusCode());*/
		return new AdapterException(detailErrorMessage, errorInfo);
	}
	
	public static AdapterException populatePSD2Exception(String detailErrorMessage,AdapterErrorCodeEnum errorCodeEnum, Object foundationError){
		ErrorInfo errorInfo = new ErrorInfo(errorCodeEnum.getErrorCode(), errorCodeEnum.getErrorMessage(), errorCodeEnum.getDetailErrorMessage(), errorCodeEnum.getStatusCode(), errorCodeEnum.getObErrorCode());
		return new AdapterException(detailErrorMessage, errorInfo, foundationError);
	}
	
	
	public static AdapterException populatePSD2Exception(String detailErrorMessage, AdapterOBErrorCodeEnum errorCodeEnum, Object foundationError) {
		OBErrorResponse1 obErrorResponse1 = new OBErrorResponse1();
		OBError1 obError1 = new OBError1();
		obError1.setErrorCode(errorCodeEnum.getObErrorCode());
		obError1.setMessage(errorCodeEnum.getSpecificErrorMessage());
		obErrorResponse1.getErrors().add(obError1);
		obErrorResponse1.setMessage(errorCodeEnum.getGeneralErrorMessage());
		obErrorResponse1.setCode(errorCodeEnum.getCode());
		obErrorResponse1.setHttpStatus(errorCodeEnum.getHttpStatusCode());	
		return new AdapterException(detailErrorMessage, obErrorResponse1, foundationError);

	}

	/**
	 * Populate PSD 2 exception.
	 *
	 * @param errorCodeEnum the error code enum
	 * @return the adapter exception
	 */
	public static AdapterException populatePSD2Exception(AdapterErrorCodeEnum errorCodeEnum){
		ErrorInfo errorInfo = new ErrorInfo(errorCodeEnum.getErrorCode(), errorCodeEnum.getErrorMessage(), errorCodeEnum.getDetailErrorMessage(), errorCodeEnum.getStatusCode(), errorCodeEnum.getObErrorCode());
		/*errorInfo.setErrorCode(errorCodeEnum.getErrorCode());			
		errorInfo.setErrorMessage(errorCodeEnum.getErrorMessage());
		errorInfo.setDetailErrorMessage(errorCodeEnum.getErrorMessage());
		errorInfo.setStatusCode(errorCodeEnum.getStatusCode());*/
		return new AdapterException(errorCodeEnum.getErrorMessage(), errorInfo);
	}
}
