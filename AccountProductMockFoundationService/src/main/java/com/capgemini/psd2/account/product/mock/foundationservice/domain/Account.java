/*
 * Account Process API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package com.capgemini.psd2.account.product.mock.foundationservice.domain;

import java.util.Objects;

import com.google.gson.annotations.SerializedName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Account object
 */
@ApiModel(description = "Account object")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-11-27T14:59:26.897+05:30")
public class Account {
	@SerializedName("accountName")
	private String accountName = null;

	@SerializedName("accountNickName")
	private String accountNickName = null;

	@SerializedName("accountNumber")
	private String accountNumber = null;

	@SerializedName("retailNonRetailCode")
	private RetailNonRetailCode retailNonRetailCode = null;

	@SerializedName("sourceSystemAccountType")
	private SourceSystemAccountType sourceSystemAccountType = null;

	@SerializedName("accountBrand")
	private AccountBrand accountBrand = null;

	@SerializedName("accountCurrency")
	private AccountCurrency accountCurrency = null;

	@SerializedName("currentAccountInformation")
	private CurrentAccountInformation currentAccountInformation = null;

	@SerializedName("savingsAccountInformation")
	private SavingsAccountInformation savingsAccountInformation = null;

	@SerializedName("creditCardAccountInformation")
	private CreditCardAccountInformation creditCardAccountInformation = null;

	@SerializedName("schemeName")
	private String schemeName = null;

	@SerializedName("product")
	private Product product = null;

	public Account accountName(String accountName) {
		this.accountName = accountName;
		return this;
	}

	/**
	 * Name of the account
	 * 
	 * @return accountName
	 **/
	@ApiModelProperty(value = "Name of the account")
	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public Account accountNickName(String accountNickName) {
		this.accountNickName = accountNickName;
		return this;
	}

	/**
	 * Nickname that the customer entered for the account
	 * 
	 * @return accountNickName
	 **/
	@ApiModelProperty(value = "Nickname that the customer entered for the account")
	public String getAccountNickName() {
		return accountNickName;
	}

	public void setAccountNickName(String accountNickName) {
		this.accountNickName = accountNickName;
	}

	public Account accountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
		return this;
	}

	/**
	 * The identifier of the Account. This will be unique within its Parent NSC,
	 * but will not be globally unique.
	 * 
	 * @return accountNumber
	 **/
	@ApiModelProperty(required = true, value = "The identifier of the Account. This will be unique within its Parent NSC, but will not be globally unique.")
	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Account retailNonRetailCode(RetailNonRetailCode retailNonRetailCode) {
		this.retailNonRetailCode = retailNonRetailCode;
		return this;
	}

	/**
	 * Get retailNonRetailCode
	 * 
	 * @return retailNonRetailCode
	 **/
	@ApiModelProperty(required = true, value = "")
	public RetailNonRetailCode getRetailNonRetailCode() {
		return retailNonRetailCode;
	}

	public void setRetailNonRetailCode(RetailNonRetailCode retailNonRetailCode) {
		this.retailNonRetailCode = retailNonRetailCode;
	}

	public Account sourceSystemAccountType(SourceSystemAccountType sourceSystemAccountType) {
		this.sourceSystemAccountType = sourceSystemAccountType;
		return this;
	}

	/**
	 * Get sourceSystemAccountType
	 * 
	 * @return sourceSystemAccountType
	 **/
	@ApiModelProperty(required = true, value = "")
	public SourceSystemAccountType getSourceSystemAccountType() {
		return sourceSystemAccountType;
	}

	public void setSourceSystemAccountType(SourceSystemAccountType sourceSystemAccountType) {
		this.sourceSystemAccountType = sourceSystemAccountType;
	}

	public Account accountBrand(AccountBrand accountBrand) {
		this.accountBrand = accountBrand;
		return this;
	}

	/**
	 * Get accountBrand
	 * 
	 * @return accountBrand
	 **/
	@ApiModelProperty(value = "")
	public AccountBrand getAccountBrand() {
		return accountBrand;
	}

	public void setAccountBrand(AccountBrand accountBrand) {
		this.accountBrand = accountBrand;
	}

	public Account accountCurrency(AccountCurrency accountCurrency) {
		this.accountCurrency = accountCurrency;
		return this;
	}

	/**
	 * Get accountCurrency
	 * 
	 * @return accountCurrency
	 **/
	@ApiModelProperty(value = "")
	public AccountCurrency getAccountCurrency() {
		return accountCurrency;
	}

	public void setAccountCurrency(AccountCurrency accountCurrency) {
		this.accountCurrency = accountCurrency;
	}

	public Account currentAccountInformation(CurrentAccountInformation currentAccountInformation) {
		this.currentAccountInformation = currentAccountInformation;
		return this;
	}

	/**
	 * Get currentAccountInformation
	 * 
	 * @return currentAccountInformation
	 **/
	@ApiModelProperty(value = "")
	public CurrentAccountInformation getCurrentAccountInformation() {
		return currentAccountInformation;
	}

	public void setCurrentAccountInformation(CurrentAccountInformation currentAccountInformation) {
		this.currentAccountInformation = currentAccountInformation;
	}

	public Account savingsAccountInformation(SavingsAccountInformation savingsAccountInformation) {
		this.savingsAccountInformation = savingsAccountInformation;
		return this;
	}

	/**
	 * Get savingsAccountInformation
	 * 
	 * @return savingsAccountInformation
	 **/
	@ApiModelProperty(value = "")
	public SavingsAccountInformation getSavingsAccountInformation() {
		return savingsAccountInformation;
	}

	public void setSavingsAccountInformation(SavingsAccountInformation savingsAccountInformation) {
		this.savingsAccountInformation = savingsAccountInformation;
	}

	public Account creditCardAccountInformation(CreditCardAccountInformation creditCardAccountInformation) {
		this.creditCardAccountInformation = creditCardAccountInformation;
		return this;
	}

	/**
	 * Get creditCardAccountInformation
	 * 
	 * @return creditCardAccountInformation
	 **/
	@ApiModelProperty(value = "")
	public CreditCardAccountInformation getCreditCardAccountInformation() {
		return creditCardAccountInformation;
	}

	public void setCreditCardAccountInformation(CreditCardAccountInformation creditCardAccountInformation) {
		this.creditCardAccountInformation = creditCardAccountInformation;
	}

	public Account schemeName(String schemeName) {
		this.schemeName = schemeName;
		return this;
	}

	/**
	 * Get schemeName
	 * 
	 * @return schemeName
	 **/
	@ApiModelProperty(value = "")
	public String getSchemeName() {
		return schemeName;
	}

	public void setSchemeName(String schemeName) {
		this.schemeName = schemeName;
	}

	public Account product(Product product) {
		this.product = product;
		return this;
	}

	/**
	 * Get product
	 * 
	 * @return product
	 **/
	@ApiModelProperty(value = "")
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Account account = (Account) o;
		return Objects.equals(this.accountName, account.accountName)
				&& Objects.equals(this.accountNickName, account.accountNickName)
				&& Objects.equals(this.accountNumber, account.accountNumber)
				&& Objects.equals(this.retailNonRetailCode, account.retailNonRetailCode)
				&& Objects.equals(this.sourceSystemAccountType, account.sourceSystemAccountType)
				&& Objects.equals(this.accountBrand, account.accountBrand)
				&& Objects.equals(this.accountCurrency, account.accountCurrency)
				&& Objects.equals(this.currentAccountInformation, account.currentAccountInformation)
				&& Objects.equals(this.savingsAccountInformation, account.savingsAccountInformation)
				&& Objects.equals(this.creditCardAccountInformation, account.creditCardAccountInformation)
				&& Objects.equals(this.schemeName, account.schemeName) && Objects.equals(this.product, account.product);
	}

	@Override
	public int hashCode() {
		return Objects.hash(accountName, accountNickName, accountNumber, retailNonRetailCode, sourceSystemAccountType,
				accountBrand, accountCurrency, currentAccountInformation, savingsAccountInformation,
				creditCardAccountInformation, schemeName, product);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Account {\n");

		sb.append("    accountName: ").append(toIndentedString(accountName)).append("\n");
		sb.append("    accountNickName: ").append(toIndentedString(accountNickName)).append("\n");
		sb.append("    accountNumber: ").append(toIndentedString(accountNumber)).append("\n");
		sb.append("    retailNonRetailCode: ").append(toIndentedString(retailNonRetailCode)).append("\n");
		sb.append("    sourceSystemAccountType: ").append(toIndentedString(sourceSystemAccountType)).append("\n");
		sb.append("    accountBrand: ").append(toIndentedString(accountBrand)).append("\n");
		sb.append("    accountCurrency: ").append(toIndentedString(accountCurrency)).append("\n");
		sb.append("    currentAccountInformation: ").append(toIndentedString(currentAccountInformation)).append("\n");
		sb.append("    savingsAccountInformation: ").append(toIndentedString(savingsAccountInformation)).append("\n");
		sb.append("    creditCardAccountInformation: ").append(toIndentedString(creditCardAccountInformation))
				.append("\n");
		sb.append("    schemeName: ").append(toIndentedString(schemeName)).append("\n");
		sb.append("    product: ").append(toIndentedString(product)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

}
