<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>com.capgemini.psd2</groupId>
	<artifactId>ConsentList</artifactId>
	<version>2.0.0</version>
	<name>ConsentList</name>
	<description>Internal Api for listing consent details</description>

	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>1.5.6.RELEASE</version>
	</parent>

	<properties>
		<java.version>1.8</java.version>
		<sonar.core.codeCoveragePlugin>jacoco</sonar.core.codeCoveragePlugin>
		<sonar.jacoco.reportPath>${project.basedir}/../coverage-reports/jacoco.exec</sonar.jacoco.reportPath>
		<sonar.exclusions>**/config/*,
			**/ConsentListApplication.*
		</sonar.exclusions>
	</properties>

	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.springframework.cloud</groupId>
				<artifactId>spring-cloud-dependencies</artifactId>
				<version>Edgware.RELEASE</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>

	<dependencies>
		<!-- To use the project specific cutomized utilities like Exceptionhandling, 
			logging etc -->
		<dependency>
			<groupId>com.capgemini.psd2</groupId>
			<artifactId>PSD2Utilities</artifactId>
			<version>1901</version>
		</dependency>

		<!-- For web integration specifically Restful -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>

		<!-- To register the application to eureka server -->
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-eureka</artifactId>
		</dependency>

		<!-- To connect the api with config server -->
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-config</artifactId>
		</dependency>

		<!-- Dependency for AWS KMS -->
		<dependency>
			<groupId>com.capgemini.psd2</groupId>
			<artifactId>PSD2KMSUtilities</artifactId>
			<version>2.0.0</version>
		</dependency>

		<!-- LDAP Authentication Security -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-security</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-ldap</artifactId>
		</dependency>

		<!-- To get account request setup data -->
		<dependency>
			<groupId>com.capgemini.psd2</groupId>
			<artifactId>AccountRequestsRoutingAdapter</artifactId>
			<version>1901</version>
		</dependency>

		<!-- To get payment setup data -->
		<dependency>
			<groupId>com.capgemini.psd2</groupId>
			<artifactId>PaymentSetupPlatformAdapter</artifactId>
			<version>1901</version>
		</dependency>

		<!-- For fetching AISP consents details -->
		<dependency>
			<groupId>com.capgemini.psd2</groupId>
			<artifactId>AISPConsentAdapter</artifactId>
			<version>2.0.0</version>
		</dependency>

		<!-- For fetching PISP consents details -->
		<dependency>
			<groupId>com.capgemini.psd2</groupId>
			<artifactId>PISPConsentAdapter</artifactId>
			<version>2.0.0</version>
		</dependency>

		<!-- For fetching CISP consents details -->
		<dependency>
			<groupId>com.capgemini.psd2</groupId>
			<artifactId>FundsConfirmationConsentRoutingAdapter</artifactId>
			<version>1901</version>
		</dependency>

		<!-- For restart endpoint enablement with config server -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId> spring-boot-starter-actuator</artifactId>
		</dependency>

		<!-- Creating test cases using jUnit -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>

		<!-- For Splunk Logging of the api -->
		<dependency>
			<groupId>com.capgemini.psd2</groupId>
			<artifactId>PSD2SplunkLogger</artifactId>
			<version>2.0.0</version>
		</dependency>
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<configuration>
					<source>1.8</source>
					<target>1.8</target>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
				<configuration>
					<layout>ZIP</layout>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.jacoco</groupId>
				<artifactId>jacoco-maven-plugin</artifactId>
				<version>0.7.8</version>
				<configuration>
					<destFile>${sonar.jacoco.reportPath}</destFile>
					<dataFile>${sonar.jacoco.reportPath}</dataFile>
					<append>true</append>
				</configuration>
				<executions>
					<execution>
						<id>jacoco-initialize</id>
						<goals>
							<goal>prepare-agent</goal>
						</goals>
					</execution>
					<execution>
						<id>jacoco-site</id>
						<phase>package</phase>
						<goals>
							<goal>report</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>
</project>