package com.capgemini.psd2.mock.foundationservice.account.transactions.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.mock.foundationservice.account.transactions.domain.Accnt;

public interface AccountTransactionsRepository extends MongoRepository<Accnt, String> {

	/**
	 * Find by nsc and account number.
	 *
	 * @param nsc the nsc
	 * @param accountNumber the account number
	 * @return the accnt
	 */
	public Accnt findByNscAndAccountNumber(String nsc, String accountNumber);
	public Accnt findByPlApplId(String refID);
	
}

