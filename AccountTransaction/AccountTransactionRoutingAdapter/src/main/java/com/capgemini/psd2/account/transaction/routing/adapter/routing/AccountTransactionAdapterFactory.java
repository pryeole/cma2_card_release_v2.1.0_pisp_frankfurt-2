package com.capgemini.psd2.account.transaction.routing.adapter.routing;

import com.capgemini.psd2.aisp.adapter.AccountTransactionAdapter;

@FunctionalInterface
public interface AccountTransactionAdapterFactory {
	public AccountTransactionAdapter getAdapterInstance(String coreSystemName);
}
