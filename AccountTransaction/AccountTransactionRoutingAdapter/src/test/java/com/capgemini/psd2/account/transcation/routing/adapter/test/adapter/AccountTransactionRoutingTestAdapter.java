package com.capgemini.psd2.account.transcation.routing.adapter.test.adapter;

import java.util.Map;

import com.capgemini.psd2.account.transaction.test.mock.data.AccountTransactionRoutingAdapterTestMockData;
import com.capgemini.psd2.aisp.adapter.AccountTransactionAdapter;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountTransactionResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;

public class AccountTransactionRoutingTestAdapter implements AccountTransactionAdapter {

	@Override
	public PlatformAccountTransactionResponse retrieveAccountTransaction(AccountMapping accountMapping,
			Map<String, String> params) {
		return AccountTransactionRoutingAdapterTestMockData.getMockTransactionGETResponse();
	}

}
