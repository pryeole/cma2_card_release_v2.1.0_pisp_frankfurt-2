/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.transcation.routing.adapter.test.adapter;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.account.transaction.routing.adapter.impl.AccountTransactionRoutingAdapter;
import com.capgemini.psd2.account.transaction.routing.adapter.routing.AccountTransactionAdapterFactory;
import com.capgemini.psd2.aisp.adapter.AccountTransactionAdapter;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountTransactionResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.PSD2Exception;

public class AccountTransactionTestRoutingAdapter{

	@Mock
	private AccountTransactionAdapterFactory adapters;
	
	@InjectMocks
	private AccountTransactionRoutingAdapter accountTransactionRoutingAdapter;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	@Test
	public void testRetriveAccountTransaction() {
		AccountTransactionAdapter adapter=new AccountTransactionAdapter() {
			
			@Override
			public PlatformAccountTransactionResponse retrieveAccountTransaction(AccountMapping accountMapping,
					Map<String, String> params) {
				return null;
			}
		};
		
		
		when(adapters.getAdapterInstance(anyString())).thenReturn(adapter);
		ReflectionTestUtils.setField(accountTransactionRoutingAdapter, "defaultAdapter", "abc");
		accountTransactionRoutingAdapter.retrieveAccountTransaction(null, null);
	}

	@Test
	public void testRetriveAccountTransactionNullParam() {
		AccountTransactionAdapter adapter=new AccountTransactionAdapter() {
			
			@Override
			public PlatformAccountTransactionResponse retrieveAccountTransaction(AccountMapping accountMapping,
					Map<String, String> params) {
				return null;
			}
		};
		
		
		when(adapters.getAdapterInstance(anyString())).thenReturn(adapter);
		ReflectionTestUtils.setField(accountTransactionRoutingAdapter, "defaultAdapter", "accountTransactionMongoDbAdapter");
		accountTransactionRoutingAdapter.retrieveAccountTransaction(null, null);
	}
	
}
