package com.capgemini.psd2.account.transaction.service;

import com.capgemini.psd2.aisp.domain.OBReadTransaction3;

public interface AccountTransactionService {
	public OBReadTransaction3 retrieveAccountTransaction(String paramString1, String paramString2,
			String paramString3, Integer paramInteger, Integer pageSize, String txnKey);

	public OBReadTransaction3 retrieveAccountTransactions(String paramString1, String paramString2,
			String paramString3, Integer paramInteger, Integer pageSize, String txnKey);
}