package com.capgemini.psd2.foundationservice.sca.authentication.retrieve.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;

import java.lang.reflect.Method;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.capgemini.psd2.adapter.security.domain.AuthenticationParameters;
import com.capgemini.psd2.adapter.security.domain.DigitalUser;
import com.capgemini.psd2.foundationservice.sca.authentication.retrieve.boi.adapter.client.SCAAuthenticationRetrieveFoundationServiceClient;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;

@RunWith(SpringJUnit4ClassRunner.class)
public class SCAAuthenticationRetrieveFoundationServiceClientTest {

	@InjectMocks
	private SCAAuthenticationRetrieveFoundationServiceClient client;

	@Mock
	private RestClientSyncImpl restClient;

	@Mock
	private RestTemplate restTemplate;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		Method method1 = SCAAuthenticationRetrieveFoundationServiceClient.class.getDeclaredMethod("init", null);
		method1.setAccessible(true);
		method1.invoke(client);
		assertNotNull(method1);

	}

	
	@Test
	public void restTransportForSCAPinApplication() {

		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = new HttpHeaders();
		MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();

		AuthenticationParameters authenticationParameters = new AuthenticationParameters();
		DigitalUser digitalUser = new DigitalUser();

		queryParams.add("digitalUserIdentifier", "guy57");
		queryParams.add("authenticationSystemCode", "B365");
		queryParams.add("digitalUserLockedOutIndicator", "true");

		httpHeaders.add("X-BOI-USER", "header user");
		httpHeaders.add("X-BOI-CHANNEL", "header channel");
		httpHeaders.add("X-BOI-PLATFORM", "header platform");
		httpHeaders.add("X-CORRELATION-ID", "header correlation Id");
		requestInfo.setUrl(
				"http://localhost:8182//mocking/api/v1/links/29932b82-4b1a-43e4-8208-5c5349f9738e/it-boi/p/authentication/v2.0/digital-user/authentication");
		RequestHeaderAttributes requestHeaderAttributes = new RequestHeaderAttributes();
		requestHeaderAttributes.setChannelId("B365");
		requestHeaderAttributes.setCorrelationId("ggy89978");
		requestHeaderAttributes.setIdempotencyKey("gyu7876");
		requestHeaderAttributes.setIntentId("yhgh68");
		requestHeaderAttributes.setPsuId("675ssd");
		

		assertNotNull(requestHeaderAttributes);
		requestInfo.setRequestHeaderAttributes(requestHeaderAttributes);
		authenticationParameters = client.restTransportForRetrieveAuthenticationService(requestInfo,
				AuthenticationParameters.class, queryParams, httpHeaders);
		Mockito.when(restClient.callForGet(anyObject(), anyObject(), anyObject())).thenReturn(authenticationParameters);

	}

}