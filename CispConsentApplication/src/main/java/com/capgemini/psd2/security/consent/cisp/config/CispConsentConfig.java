package com.capgemini.psd2.security.consent.cisp.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.capgemini.psd2.cisp.adapter.FundsConfirmationConsentAdapter;
import com.capgemini.psd2.funds.confirmation.consent.routing.adapter.impl.FundsConfirmationConsentRoutingAdapter;
import com.capgemini.psd2.scaconsenthelper.config.helpers.ConsentAuthorizationHelper;
import com.capgemini.psd2.scaconsenthelper.filters.ConsentBlockBackActionFilter;
import com.capgemini.psd2.scaconsenthelper.filters.ConsentCookieHandlerFilter;
import com.capgemini.psd2.scaconsenthelper.filters.CookieHandlerFilter;
import com.capgemini.psd2.scaconsenthelper.models.IntentTypeEnum;
import com.capgemini.psd2.security.consent.cisp.helpers.CispConsentAuthorizationHelper;

@Configuration
public class CispConsentConfig {

	@Bean(name = "FundsConfirmationConsentAdapter")
	public FundsConfirmationConsentAdapter fundsConfirmationConsentAdapter(){
		return new FundsConfirmationConsentRoutingAdapter();
	}
	
	@Bean
	public CookieHandlerFilter buildCispCookieHandlerFilter(){
		return new ConsentCookieHandlerFilter(IntentTypeEnum.CISP_INTENT_TYPE.getIntentType());
	}
	
	@Bean
	public ConsentBlockBackActionFilter buildCispBlockActionFilter(){
		return new ConsentBlockBackActionFilter();		
	}
	
	@Bean
	public FilterRegistrationBean blockCISPRefreshEventFilter(){
		FilterRegistrationBean filterRegBean = new FilterRegistrationBean();
		filterRegBean.setFilter(buildCispBlockActionFilter());
		List<String> urlPatterns = new ArrayList<>();
		urlPatterns.add("/cisp/home");
		filterRegBean.setName("cispBlockBackEventFilter");
		filterRegBean.setUrlPatterns(urlPatterns);
		return filterRegBean;
	}	
	
	@Bean
	public FilterRegistrationBean cispFilter(){
		FilterRegistrationBean filterRegBean = new FilterRegistrationBean();
		filterRegBean.setFilter(buildCispCookieHandlerFilter());
		List<String> urlPatterns = new ArrayList<>();
		urlPatterns.add("/cisp/customerconsentview");
		urlPatterns.add("/cisp/consent");
		urlPatterns.add("/cisp/cancelConsent");		
		urlPatterns.add("/cisp/checkSession");
		filterRegBean.setName("cispCookieFilter");
		filterRegBean.setUrlPatterns(urlPatterns);
		return filterRegBean;
	}	
	
	@Bean(name = "cispConsentAuthorizationHelper")
	public ConsentAuthorizationHelper cispConsentAuthorizationHelper(){
		return new CispConsentAuthorizationHelper();
	}
	
}
