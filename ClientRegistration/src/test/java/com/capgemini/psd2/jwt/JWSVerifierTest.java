package com.capgemini.psd2.jwt;

import java.security.InvalidKeyException;
import java.text.ParseException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import com.capgemini.psd2.exception.ClientRegistrationException;
import com.capgemini.psd2.mockdata.DynamicClientMockData;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
@RunWith(SpringJUnit4ClassRunner.class)
public class JWSVerifierTest {

	@InjectMocks
	JWSVerifierImpl jWSVerifier;
	@Test
	public void testCreateJWSVerifier() throws ParseException, InvalidKeyException {
		Assert.notNull(jWSVerifier.createJWSVerifier(DynamicClientMockData.rsaKeyFormat(), DynamicClientMockData.signedJWT().getHeader()),"");
	}
	@Test(expected= ClientRegistrationException.class)
	public void testCreateJWSVerifierException() throws ParseException, InvalidKeyException {
		Assert.notNull(jWSVerifier.createJWSVerifier(DynamicClientMockData.rsaKeyFormat(), new JWSHeader(JWSAlgorithm.ES512)),"");
	}

}
