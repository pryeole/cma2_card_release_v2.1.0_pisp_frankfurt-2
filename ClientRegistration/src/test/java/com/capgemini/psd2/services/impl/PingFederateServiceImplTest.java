package com.capgemini.psd2.services.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.security.cert.CertificateException;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.config.PFClientRegConfig;
import com.capgemini.psd2.exception.ClientRegistrationException;
import com.capgemini.psd2.helper.CertExtractor;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.mockdata.TppPortalMockData;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.service.impl.PingFederateServiceImpl;
import com.capgemini.tpp.ldap.client.model.ClientModel;

@RunWith(SpringJUnit4ClassRunner.class)
public class PingFederateServiceImplTest {

	@Mock
	private RestClientSync restClient;

	
	@Mock
	private RequestHeaderAttributes requestHeaderAttributes;
	
	
	@InjectMocks
	private PingFederateServiceImpl pingFederateService;
	
	

	@Mock
	private CertExtractor certExtractor;
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		PFClientRegConfig pfClientRegConfig = new PFClientRegConfig();
		pfClientRegConfig
				.setClientByIdUrl("https://LIN51003588.corp.capgemini.com:9999/pf-admin-api/v1/oauth/clients/{clientId}");
		pfClientRegConfig.setClientsUrl("https://LIN51003588.corp.capgemini.com:9999/pf-admin-api/v1/oauth/clients");
		//pfClientRegConfig.setAdminuser("ApiAdmin");
		//pfClientRegConfig.setAdminpwd("Test@2017");
		pfClientRegConfig.getAdminuser().put("Admin", "ApiAdmin");
		pfClientRegConfig.getAdminpwdV2().put("pwd", "Test@2017");
		pfClientRegConfig.getAdminpwd("pwd");
		pfClientRegConfig.getAdminuser("Admin");
		pfClientRegConfig.getTokenMgrRefId().put("BOIUK","INTREFATM");
		pfClientRegConfig.setCaImportUrl("https://test.com");
		pfClientRegConfig.setReplicateUrl("https://replicateurl");
		pfClientRegConfig.getPrefix().put("PSD2", "https://localhost:8000/123455");
		pfClientRegConfig.getPrefix("PSD2");
		
		
		ReflectionTestUtils.setField(pingFederateService, "pfClientRegConfig", pfClientRegConfig);
		
	/*	RequestHeaderAttributes attr=new  RequestHeaderAttributes();
		attr.setTenantId("PSD2");
		attr.getTenantId();
		ReflectionTestUtils.setField(tppSoftwareHelper,"attributes",attr);*/
		
	//	this.tppSoftwareHelper.init();
		// TppSoftwareHelper helper=null;
		//TppSoftwareHelper.init();
	//	TppSoftwareHelper.encodeCredentials(pfClientRegConfig);
		
	//	TppSoftwareHelper helper=new TppSoftwareHelper();
		
		
		
		
	}

	@Test
	@Ignore
	public void testRegisterTPPApplicationinPF() {
		ClientModel clientModelExpected = TppPortalMockData.getClientModel();
		when(restClient.callForPost(any(), any(), any(), any())).thenReturn(clientModelExpected);
		Mockito.when(requestHeaderAttributes.getTenantId()).thenReturn("PSD2");
	
			ClientModel clientModel = pingFederateService.registerTPPApplicationinPF(
					TppPortalMockData.getSSADataElements(), TppPortalMockData.getClientSecret());
			assertEquals(clientModelExpected.getClientAuth().getSecret(), clientModel.getClientAuth().getSecret());
			assertEquals(clientModelExpected.getClientId(), clientModel.getClientId());
			assertEquals(clientModelExpected.getDefaultAccessTokenManagerRef().getId(),
					clientModel.getDefaultAccessTokenManagerRef().getId());
			
		
	}

	@Test
	@Ignore
	public void testRollBackPFAppRegisteration() {
		when(restClient.callForDelete(any(), any(), any())).thenReturn(Void.class);
		pingFederateService.deletePFAppRegisteration(TppPortalMockData.getClientId());
	}

	@Test(expected = ClientRegistrationException.class)
	public void testRecoverRollBackPFAppRegisteration() {
		Exception e = new Exception();
		pingFederateService.recoverDeletePFAppRegisteration(e);
	}

	@Test(expected = ClientRegistrationException.class)
	public void testRecoverRegisterTPPApplicationinPF() {
		Exception e = new Exception();
		pingFederateService.recoverRegisterTPPApplicationinPF(e);
	}

	@Test
	@Ignore
	public void testFetchClientDetailsByIdFromPF() {
		ClientModel clientModelExpected = TppPortalMockData.getClientModel();
		when(restClient.callForGet(any(), any(), any())).thenReturn(clientModelExpected);
		ClientModel clientModel = pingFederateService.fetchClientDetailsByIdFromPF(TppPortalMockData.getClientSecret());
		assertEquals(clientModelExpected.getClientAuth().getSecret(), clientModel.getClientAuth().getSecret());
		assertEquals(clientModelExpected.getClientId(), clientModel.getClientId());
		assertEquals(clientModelExpected.getDefaultAccessTokenManagerRef().getId(),
				clientModel.getDefaultAccessTokenManagerRef().getId());
	}
	
	
	@Test
	@Ignore
	public void testUpdateTPPApplicationinPF() {
		ClientModel clientModelExpected = TppPortalMockData.getClientModel();
		when(restClient.callForPut(any(), any(), any(), any())).thenReturn(clientModelExpected);
		ClientModel clientModel = pingFederateService.updateTPPApplicationinPF(TppPortalMockData.getClientModel(), TppPortalMockData.getClientId(), TppPortalMockData.getClientSecret());
		assertEquals(clientModelExpected.getClientAuth().getSecret(), clientModel.getClientAuth().getSecret());
		assertEquals(clientModelExpected.getClientId(), clientModel.getClientId());
		assertEquals(clientModelExpected.getDefaultAccessTokenManagerRef().getId(),
				clientModel.getDefaultAccessTokenManagerRef().getId());
	}
	@Test
	@Ignore
	public void testaddTrustedCAinPF() throws CertificateException, IOException {
		HashMap<String, String> map=new HashMap<>();
		when(certExtractor.intermediateCertificate(any())).thenReturn("");
		map.put("fileData", certExtractor.intermediateCertificate(any()));
		when(restClient.callForPost(any(), any(), any(), any())).thenReturn(new HashMap());
		pingFederateService.addTrustedCAinPF(any());
	}
	
	@Test
	@Ignore
	public void testreplicatePFConfiguration() {
		when(restClient.callForPost(any(), any(), any(), any())).thenReturn(new HashMap());
		pingFederateService.replicatePFConfiguration();
	}
}
