package com.capgemini.psd2.integration.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.integration.service.CPNPOperationsService;

@RestController
public class CPNPOperationsController {

	@Autowired
	private CPNPOperationsService service;

	@RequestMapping(value = "/verifyCPNP", method = RequestMethod.GET)
	public void verifyCPNP(@RequestHeader("psuId") String psuId, @RequestHeader("X-SSL-Client-OU") String tppId) {
		service.verifyClient(tppId, psuId);
	}
}
