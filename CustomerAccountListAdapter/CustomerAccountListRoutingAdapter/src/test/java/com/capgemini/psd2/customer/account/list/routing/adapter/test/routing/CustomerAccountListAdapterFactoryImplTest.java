package com.capgemini.psd2.customer.account.list.routing.adapter.test.routing;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;

import com.capgemini.psd2.customer.account.list.routing.adapter.impl.CustomerAccountListRoutingAdapter;
import com.capgemini.psd2.customer.account.list.routing.adapter.routing.CustomerListAdapterFactoryImpl;

public class CustomerAccountListAdapterFactoryImplTest {

	@Mock
	private ApplicationContext applicationContext;

	@InjectMocks
	private CustomerListAdapterFactoryImpl customerListAdapterFactoryImpl = new CustomerListAdapterFactoryImpl();

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testGetAdapterInstance() {
		CustomerAccountListRoutingAdapter customerAccountListRoutingAdapter = new CustomerAccountListRoutingAdapter();
		when(applicationContext.getBean(anyString())).thenReturn(customerAccountListRoutingAdapter);
		CustomerAccountListRoutingAdapter customerAccountListRoutingAdapterResult = (CustomerAccountListRoutingAdapter) customerListAdapterFactoryImpl
				.getAdapterInstance("customerMongoDBAdapter");
		assertEquals(customerAccountListRoutingAdapter, customerAccountListRoutingAdapterResult);
	}
}