package com.capgemini.psd2.account.product.boi.fs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.aisp.platform.domain.PlatformAccountProductsResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.product.boi.adapter.AccountProductFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.account.product.boi.adapter.domain.Product;

@SpringBootApplication
@ComponentScan("com.capgemini.psd2")
public class AccountProductFoundationServiceTestProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccountProductFoundationServiceTestProjectApplication.class, args);
	}
}

@RestController
@ResponseBody
class TestAccountProductFSController{
	
	@Autowired
	AccountProductFoundationServiceAdapter adapter;
	
	@RequestMapping("/testProduct")
	public PlatformAccountProductsResponse getResponse(){
		Product product = new Product();
		product.setProductType("BusinessCurrentAccount");
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		
		AccountDetails accDet1 = new AccountDetails();
		accDet1.setAccountId("56789");
		accDet1.setAccountNSC("903779");
		accDet1.setAccountNumber("76528776");
		accDetList.add(accDet1);

		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("BOI999");
		accountMapping.setCorrelationId("23456778");
		Map<String, String> params = new HashMap<>();
		params.put("channelId", "BOL");
	    params.put("consentFlowType", "AISP");
	    params.put("x-channel-id", "BOL");
	    params.put("x-user-id", "BOI999");
	    params.put("X-BOI-PLATFORM", "platform");
	    params.put("x-fapi-interaction-id", "12345678"); 
		return adapter.retrieveAccountProducts(accountMapping, params);
		
		
		
	}

	
}
