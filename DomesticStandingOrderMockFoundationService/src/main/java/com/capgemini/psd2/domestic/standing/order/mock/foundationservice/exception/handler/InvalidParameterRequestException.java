package com.capgemini.psd2.domestic.standing.order.mock.foundationservice.exception.handler;

public class InvalidParameterRequestException extends Exception{
	
	
	private static final long serialVersionUID = 1L;
	
	public InvalidParameterRequestException(String s){
		super(s);
		
	}

}
