package com.capgemini.psd2.domestic.standing.order.mock.foundationservice.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.capgemini.psd2.domestic.standing.order.mock.foundationservice.raml.domain.StandingOrderInstructionComposite;
import com.capgemini.psd2.domestic.standing.order.mock.foundationservice.raml.domain.StandingOrderInstructionProposal;

@Repository
public interface DomesticStandingOrderFoundationRepository extends MongoRepository<StandingOrderInstructionComposite, String> {

	
	public StandingOrderInstructionComposite findByPaymentInstructionPaymentInstructionNumber(String PaymentInstructionNumber);

}
