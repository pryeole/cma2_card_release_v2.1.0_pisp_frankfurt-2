package com.capgemini.psd2.account.offers.mongo.db.adapter.test.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.account.offer.mongo.db.adapter.impl.AccountOffersMongoDbAdapterImpl;
import com.capgemini.psd2.account.offer.mongo.db.adapter.repository.AccountOffersRepository;
import com.capgemini.psd2.account.offers.mongo.db.adapter.test.mock.data.AccountOffersAdapterTestMockData;
import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountOffersCMA2;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountOffersResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerUtils;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountOffersMongoDBAdapterImplTest {

	/**
	 * Sets the up.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		Map<String, String> map=new HashMap<>();
		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");
		Map<String, String> specificErrorMessageMap=new HashMap<>();
		specificErrorMessageMap.put("NO_ACCOUNT_ID_FOUND", "signature_invalid_content");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap); 
	}

	@InjectMocks
	private AccountOffersMongoDbAdapterImpl accountOffersMongoDbAdapterImpl;

	@Mock
	AccountOffersRepository accountOffersRepository;

	@Mock
	LoggerUtils loggerUtils;

	@Test
	public void testRetrieveAccountOffersSuccessFlow() {
		AccountMapping accountMapping = new AccountMapping();
		ArrayList<AccountDetails> accountDetailsList = new ArrayList<>();

		AccountDetails accountDetails = new AccountDetails();
		Map<String, String> params = new HashMap<>();
		accountDetails.setAccountId("d9d9c3dc-1427-4f03-92a7-7916fcf4cc0d");
		accountDetailsList.add(accountDetails);
		accountMapping.setAccountDetails(accountDetailsList);
		List<AccountOffersCMA2> offersList = new ArrayList<>();
		offersList.add(new AccountOffersCMA2());

		Mockito.when(accountOffersRepository.findByAccountNumberAndAccountNSC(anyString(), anyString()))
				.thenReturn(offersList);

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountOffersAdapterTestMockData.getMockLoggerData());

		PlatformAccountOffersResponse actualResponse = accountOffersMongoDbAdapterImpl
				.retrieveAccountOffers(accountMapping, params);

		assertEquals(AccountOffersAdapterTestMockData.getMockExpectedAccountOffersResponse(),
				actualResponse.getoBReadOffer1());

	}

	@Test(expected = PSD2Exception.class)
	public void testRetrieveAccountOfferExceptionFlow() {
		AccountMapping accountMapping = new AccountMapping();
		ArrayList<AccountDetails> accountDetailsList = new ArrayList<>();

		AccountDetails accountDetails = new AccountDetails();
		Map<String, String> params = new HashMap<>();
		accountDetails.setAccountId("d9d9c3dc-1427-4f03-92a7-7916fcf4cc0d");
		accountDetailsList.add(accountDetails);
		accountMapping.setAccountDetails(accountDetailsList);
		List<AccountOffersCMA2> offersList = new ArrayList<>();
		offersList.add(new AccountOffersCMA2());

		Mockito.when(accountOffersRepository.findByAccountNumberAndAccountNSC(anyString(), anyString()))
				.thenThrow(DataAccessResourceFailureException.class);

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountOffersAdapterTestMockData.getMockLoggerData());

		PlatformAccountOffersResponse actualResponse = accountOffersMongoDbAdapterImpl
				.retrieveAccountOffers(accountMapping, params);

		assertEquals(AccountOffersAdapterTestMockData.getMockExpectedAccountOffersResponse(),
				actualResponse.getoBReadOffer1());
	}

	@Test
	public void testRetrieveAccountOfferNullException() {

		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accountDetailsList = new ArrayList<>();
		AccountDetails accountDetails = new AccountDetails();
		Map<String, String> params = new HashMap<>();

		accountDetails.setAccountId("123");
		accountDetailsList.add(accountDetails);
		accountMapping.setAccountDetails(accountDetailsList);
		List<AccountOffersCMA2> offersList = new ArrayList<>();
		offersList.add(new AccountOffersCMA2());

		Mockito.when(accountOffersRepository.findByAccountNumberAndAccountNSC(anyString(), anyString()))
				.thenReturn(offersList);

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountOffersAdapterTestMockData.getMockLoggerData());

		PlatformAccountOffersResponse actualResponse = accountOffersMongoDbAdapterImpl
				.retrieveAccountOffers(accountMapping, params);
		assertEquals(AccountOffersAdapterTestMockData.getMockExpectedAccountOffersNullResponse(),
				actualResponse.getoBReadOffer1());

	}

}
