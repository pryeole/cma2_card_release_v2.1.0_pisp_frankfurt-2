package com.capgemini.psd2.tpp.block.ldap.adapter.test.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.query.LdapQuery;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.internal.apis.domain.ActionEnum;
import com.capgemini.psd2.tpp.block.config.TppBlockLdapConfiguration;
import com.capgemini.psd2.tpp.block.constants.TppBlockConstants;
import com.capgemini.psd2.tpp.block.ldap.adapter.impl.TppBlockLdapAdapter;

public class TppBlockLdapAdapterTest {

	@Mock
	private LdapTemplate ldapTemplate;
	
	@Mock
	private Attribute attribute;
	
	@Mock
	private Attributes attributes;
	
	@Mock
	private TppBlockLdapConfiguration tppBlockLdapConfiguration;

	@InjectMocks
	TppBlockLdapAdapter obj = new TppBlockLdapAdapter();

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testUpdateTppStatus() {
		doNothing().when(ldapTemplate).modifyAttributes(anyString(), anyObject());
		obj.updateTppStatus("123456", ActionEnum.UNBLOCK, "BOI");
	}

	@Test(expected = PSD2Exception.class)
	public void testUpdateTppStatusForException() {
		doThrow(Exception.class).when(ldapTemplate).modifyAttributes(anyString(),anyObject());
		obj.updateTppStatus("123456", ActionEnum.UNBLOCK, "BOI");
	}

	@Test(expected=Exception.class)
	public void testFetchTppStatusDetails() {
		
		List<Object> tppList = new ArrayList<>();
		attribute.add("a:b, d:c");
		attributes.put(TppBlockConstants.BLOCK_ATTRIBUTE,attribute);
		//Temporary Fix
		when(attributes.get(anyString())).thenReturn(attribute);
		tppList.add(attributes);
		when(tppBlockLdapConfiguration.getTenantSpecificTppBaseDn("BOIUK")).thenReturn("ou=tpp,ou=groups,dc=boiuk,dc=co.uk,dc=capgeminibank,dc=com");
		when(ldapTemplate.search(any(LdapQuery.class), any(AttributesMapper.class))).thenReturn(tppList);
		obj.fetchTppStatusDetails("123456","BOIUK");
	}
	
	@Test(expected=Exception.class)
	public void testFetchTppStatusDetailsForException() {
		when(ldapTemplate.search(any(LdapQuery.class), any(AttributesMapper.class))).thenReturn(null);
		obj.fetchTppStatusDetails("123456","BOI");
	}
	
	@Test(expected=PSD2Exception.class)
	public void testFetchTppStatusDetailsForException1() {
		when(tppBlockLdapConfiguration.getTenantSpecificTppBaseDn("BOIUK")).thenReturn("ou=tpp,ou=groups,dc=boiuk,dc=co.uk,dc=capgeminibank,dc=com");
		when(ldapTemplate.search(any(LdapQuery.class), any(AttributesMapper.class))).thenThrow(PSD2Exception.class);
		obj.fetchTppStatusDetails("123456","BOIUK");
	}
}
