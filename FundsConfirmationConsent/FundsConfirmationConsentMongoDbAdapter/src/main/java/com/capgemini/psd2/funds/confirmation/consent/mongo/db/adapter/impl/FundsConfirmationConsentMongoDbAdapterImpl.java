package com.capgemini.psd2.funds.confirmation.consent.mongo.db.adapter.impl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;
import com.capgemini.psd2.cisp.adapter.CispConsentAdapter;
import com.capgemini.psd2.cisp.adapter.FundsConfirmationConsentAdapter;
import com.capgemini.psd2.cisp.domain.OBExternalRequestStatus1Code;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentDataResponse1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1;
import com.capgemini.psd2.consent.domain.CispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.funds.confirmation.consent.mongo.db.adapter.repository.FundsConfirmationConsentRepository;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.product.common.CompatibleVersionList;
import com.capgemini.psd2.utilities.DateUtilites;
public class FundsConfirmationConsentMongoDbAdapterImpl implements FundsConfirmationConsentAdapter {

	@Autowired
	private FundsConfirmationConsentRepository fundsConfirmationConsentRepository;

	@Autowired
	private CispConsentAdapter cispConsentAdapter;
	
	@Autowired
	private CompatibleVersionList compatibleVersionList;
	
	@Autowired
	private RequestHeaderAttributes req;

	@Override
	public OBFundsConfirmationConsentResponse1 createFundsConfirmationConsentPOSTResponse(
			OBFundsConfirmationConsentDataResponse1 data1) {
		OBFundsConfirmationConsentResponse1 fundsConfirmationConsentPOSTResponse = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentDataResponse1 data = null;
		data1.setTenantId(req.getTenantId());
		try {
			data = fundsConfirmationConsentRepository.save(data1);
		} catch (DataAccessResourceFailureException e) {
			
		 throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,e.getMessage()));
			
		}

		fundsConfirmationConsentPOSTResponse.setData(data);
		return fundsConfirmationConsentPOSTResponse;
	}

	@Override
	public OBFundsConfirmationConsentResponse1 getFundsConfirmationConsentPOSTResponse(String consentId) {
		OBFundsConfirmationConsentResponse1 fundsConfirmationConsentPOSTResponse = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentDataResponse1 data = null;
		try {
			data = fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(consentId, compatibleVersionList.fetchVersionList());
		} catch (DataAccessResourceFailureException e) {
	
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,e.getMessage()));
			
		}
		if (null == data) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND,ErrorMapKeys.RESOURCE_NOT_FOUND));
			
		}
		
		/* Fix for Sandbox defect #2716 */
		String cid = req.getTppCID();
		if (null != cid && !cid.equals(data.getTppCID())) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.INVALID_TPP_USER_ACCESS);
		}
		
		fundsConfirmationConsentPOSTResponse.setData(data);
		return fundsConfirmationConsentPOSTResponse;
	}

	@Override
	public void removeFundsConfirmationConsent(String consentId, String cId) {
		OBFundsConfirmationConsentDataResponse1 data = null;
		try {
			data = fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(consentId, compatibleVersionList.fetchVersionList());
		} catch (DataAccessResourceFailureException e) {
			
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,e.getMessage()));
		}

		if (null == data) {
			
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND, ErrorMapKeys.INVALID_INTENT_ID));
		}
		
		if (null != cId && !cId.equals(data.getTppCID())) {
		   throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.INVALID_TPP_USER_ACCESS);
		}
		
		data.setStatus(OBExternalRequestStatus1Code.REVOKED);
		data.setStatusUpdateDateTime(DateUtilites.getCurrentDateInISOFormat());
		try {
			revokeConsent(consentId);
			fundsConfirmationConsentRepository.save(data);
		} catch (DataAccessResourceFailureException e) {
			
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,e.getMessage()));
		}

	}

	private void revokeConsent(String consentId) {
		CispConsent cispConsent = cispConsentAdapter.retrieveConsentByFundsIntentIdAndStatus(consentId,
				ConsentStatusEnum.AUTHORISED);
		if (cispConsent == null)
			return;
		cispConsentAdapter.updateConsentStatus(cispConsent.getConsentId(), ConsentStatusEnum.REVOKED);
	}

	@Override
	public OBFundsConfirmationConsentResponse1 updateFundsConfirmationConsentResponse(String consentId,
			OBExternalRequestStatus1Code statusEnum) {

		OBFundsConfirmationConsentResponse1 updateFundsConfirmationConsentPOSTResponse = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentDataResponse1 data = null;
		try {
			data = fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(consentId, compatibleVersionList.fetchVersionList());
		} catch (DataAccessResourceFailureException e) {
			
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,e.getMessage()));
		}

		if (null == data) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND, ErrorMapKeys.RES_NOTFOUND));
			
		}
		data.setStatusUpdateDateTime(DateUtilites.getCurrentDateInISOFormat());
		data.setStatus(statusEnum);
		try {
			data = fundsConfirmationConsentRepository.save(data);
		} catch (DataAccessResourceFailureException e) {
			
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,e.getMessage()));
		}
		updateFundsConfirmationConsentPOSTResponse.setData(data);
		return updateFundsConfirmationConsentPOSTResponse;
	}

}