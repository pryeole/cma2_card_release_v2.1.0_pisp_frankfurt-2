package com.capgemini.psd2.funds.confirmation.transformer;

public interface FundsConfirmationConsentTransformer<T> {

	public T fundsConfirmationConsentResponseTransformer(T response);

}
