/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.balance.service;

import com.capgemini.psd2.cma2.aisp.domain.OBReadBalance1;

/**
 * The Interface AccountBalanceService.
 */
public interface AccountBalanceService {
	
	/**
	 * Retrieve account balance.
	 *
	 * @param accountId the account id
	 * @return the balances GET response
	 */
	public OBReadBalance1 retrieveAccountBalance(String accountId);
}
