/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.balance.routing.adapter.routing;

import com.capgemini.psd2.aisp.adapter.AccountBalanceAdapter;

/**
 * A factory for creating AccountBalanceAdapter objects.
 */
@FunctionalInterface
public interface AccountBalanceAdapterFactory {
	
	/**
	 * Gets the adapter instance.
	 *
	 * @param coreSystemName the core system name
	 * @return the adapter instance
	 */
public AccountBalanceAdapter getAdapterInstance(String coreSystemName);

}
