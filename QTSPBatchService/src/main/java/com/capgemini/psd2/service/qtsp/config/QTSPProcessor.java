package com.capgemini.psd2.service.qtsp.config;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import com.capgemini.psd2.model.qtsp.QTSPResource;
import com.capgemini.psd2.service.qtsp.QTSPService;

public class QTSPProcessor implements ItemProcessor<List<QTSPResource>, Map<String, List<QTSPResource>>> {

	private static final Logger LOGGER = LoggerFactory.getLogger(QTSPProcessor.class);

	@Autowired
	private QTSPService qtspService;

	@Override
	public Map<String, List<QTSPResource>> process(List<QTSPResource> qtspResponses) throws Exception {

		LOGGER.info("Processing {} QTSPs", qtspResponses.size());
		return qtspService.processQTSPResponse(qtspResponses);
	}

}