package com.capgemini.psd2.service.qtsp.config;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.capgemini.psd2.model.qtsp.QTSPResource;
import com.capgemini.psd2.service.qtsp.restclient.QTSPRestClient;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class QTSPReader implements ItemReader<List<QTSPResource>> {

	private static final Logger LOGGER = LoggerFactory.getLogger(QTSPReader.class);

	@Autowired
	private QTSPRestClient qtspRestClient;

	@Autowired
	private QTSPServiceConfig qtspServiceConfig;

	@Value("${api.qtspFromIndexInclusive:0}")
	private String fromIndex;

	@Value("${api.qtspToIndexExclusive:0}")
	private String toIndex;

	@Override
	public List<QTSPResource> read() {

		int from = 0;
		int to = 0;
		if (!qtspServiceConfig.isBatchJobState()) {
			List<QTSPResource> qtspResponses = new ArrayList<>();
			if (qtspServiceConfig.isConnectToOB()) {
				qtspRestClient.buildSSLRequest();
				LOGGER.info("Retrieving OBAccessToken");
				String accessToken = qtspRestClient.retrieveOBAccessToken();
				LOGGER.info("Retrieving QTSPs from OB");
				qtspResponses.addAll(qtspRestClient.retrieveQTSPs(accessToken));
			} else {
				ObjectMapper mapper = new ObjectMapper();
				LOGGER.info("Retrieving QTSPs from JSON");
				List<QTSPResource> qtsps = mapper.convertValue(
						JSONUtilities.getObjectFromJSONString(readDataFromFile(), new ArrayList<>()),
						new TypeReference<List<QTSPResource>>() {
						});
				qtspResponses.addAll(qtsps);
			}
			qtspServiceConfig.setBatchJobState(true);

			LOGGER.info("Total Number of Certificates : {}", qtspResponses.size());
			try {
				from = Integer.valueOf(fromIndex);
				to = Integer.valueOf(toIndex);
			} catch (NumberFormatException e) {
				LOGGER.info("Invalid range specified");
			}

			if (from < 0 || to > qtspResponses.size() || from > to || to <= 0) {
				return qtspResponses;
			}
			return qtspResponses.subList(from, to);
		}
		return null;
	}

	private String readDataFromFile() {

		LOGGER.info("Reading Data from file");
		String fileName = "qtsps.json";
		String content = "";
		InputStream is = getClass().getResourceAsStream("/" + fileName);
		if (null != is) {
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			content = reader.lines().collect(Collectors.joining(System.lineSeparator()));
		}
		return content;
	}
}