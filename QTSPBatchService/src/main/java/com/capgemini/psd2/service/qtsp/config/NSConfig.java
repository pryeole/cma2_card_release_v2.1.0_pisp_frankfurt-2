package com.capgemini.psd2.service.qtsp.config;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("qtspservice.writer.netscaler")
public class NSConfig {

	private Map<String, String> prefix = new HashMap<>();

	private Map<String, String> username = new HashMap<>();

	private Map<String, String> password = new HashMap<>();

	private Map<String, String> vserverName = new HashMap<>();

	private String login;

	private String uploadCert;

	private String addCertAsCACert;

	private String bindCACert;

	private String unbindCACert;

	private String unbindCACertParam;

	private String removeCertAsCACert;

	private String removeCert;

	private String logout;

	private String fileLocation;

	private String fileEncoding;

	private String timeout;
	
	private String saveUrl;

	public Map<String, String> getPrefix() {
		return prefix;
	}

	public void setPrefix(Map<String, String> prefix) {
		this.prefix = prefix;
	}

	public Map<String, String> getUsername() {
		return username;
	}

	public void setUsername(Map<String, String> username) {
		this.username = username;
	}

	public Map<String, String> getPassword() {
		return password;
	}

	public void setPassword(Map<String, String> password) {
		this.password = password;
	}

	public Map<String, String> getVserverName() {
		return vserverName;
	}

	public void setVserverName(Map<String, String> vserverName) {
		this.vserverName = vserverName;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getUploadCert() {
		return uploadCert;
	}

	public void setUploadCert(String uploadCert) {
		this.uploadCert = uploadCert;
	}

	public String getAddCertAsCACert() {
		return addCertAsCACert;
	}

	public void setAddCertAsCACert(String addCertAsCACert) {
		this.addCertAsCACert = addCertAsCACert;
	}

	public String getBindCACert() {
		return bindCACert;
	}

	public void setBindCACert(String bindCACert) {
		this.bindCACert = bindCACert;
	}

	public String getUnbindCACert() {
		return unbindCACert;
	}

	public void setUnbindCACert(String unbindCACert) {
		this.unbindCACert = unbindCACert;
	}

	public String getUnbindCACertParam() {
		return unbindCACertParam;
	}

	public void setUnbindCACertParam(String unbindCACertParam) {
		this.unbindCACertParam = unbindCACertParam;
	}

	public String getRemoveCertAsCACert() {
		return removeCertAsCACert;
	}

	public void setRemoveCertAsCACert(String removeCertAsCACert) {
		this.removeCertAsCACert = removeCertAsCACert;
	}

	public String getRemoveCert() {
		return removeCert;
	}

	public void setRemoveCert(String removeCert) {
		this.removeCert = removeCert;
	}

	public String getLogout() {
		return logout;
	}

	public void setLogout(String logout) {
		this.logout = logout;
	}

	public String getFileLocation() {
		return fileLocation;
	}

	public void setFileLocation(String fileLocation) {
		this.fileLocation = fileLocation;
	}

	public String getFileEncoding() {
		return fileEncoding;
	}

	public void setFileEncoding(String fileEncoding) {
		this.fileEncoding = fileEncoding;
	}

	public String getTimeout() {
		return timeout;
	}

	public void setTimeout(String timeout) {
		this.timeout = timeout;
	}

	public String getSaveUrl() {
		return saveUrl;
	}

	public void setSaveUrl(String saveUrl) {
		this.saveUrl = saveUrl;
	}
}