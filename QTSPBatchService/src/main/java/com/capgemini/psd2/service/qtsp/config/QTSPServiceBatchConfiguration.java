package com.capgemini.psd2.service.qtsp.config;

import java.util.List;
import java.util.Map;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import com.capgemini.psd2.model.qtsp.QTSPResource;

@Lazy
@Configuration
public class QTSPServiceBatchConfiguration {

	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	@Autowired
	private StepBuilderFactory stepBuilderFactory;

	@Bean
	public ItemReader<List<QTSPResource>> reader() {
		return new QTSPReader();
	}

	@Bean
	public ItemProcessor<List<QTSPResource>, Map<String, List<QTSPResource>>> processor() {
		return new QTSPProcessor();
	}

	@Bean
	public ItemWriter<Map<String, List<QTSPResource>>> writer() {
		return new QTSPWriter();
	}

	@Bean
	public Step step1() {

		return stepBuilderFactory.get("step1").<List<QTSPResource>, Map<String, List<QTSPResource>>>chunk(1)
				.reader(reader()).processor(processor()).writer(writer()).build();
	}

	@Bean
	public Job job1() {

		return jobBuilderFactory.get("job1").flow(step1()).end().build();
	}

}