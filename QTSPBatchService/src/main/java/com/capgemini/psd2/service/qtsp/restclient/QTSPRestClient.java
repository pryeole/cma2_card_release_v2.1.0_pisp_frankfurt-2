package com.capgemini.psd2.service.qtsp.restclient;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.config.OBConfig;
import com.capgemini.psd2.model.qtsp.OBResponse;
import com.capgemini.psd2.model.qtsp.QTSPResource;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.restclient.ScimRestClient;
import com.capgemini.psd2.util.JwtCreate;

@Component
public class QTSPRestClient {

	@Autowired
	private ScimRestClient scimRestClient;

	@Autowired
	private JwtCreate jwtCreate;

	@Autowired
	private RestClientSync restClientSync;

	@Autowired
	private OBConfig obConfig;

	private static final String AUTHORIZATION = "Authorization";

	private static final String BEARER = "Bearer ";

	private static final Logger LOGGER = LoggerFactory.getLogger(QTSPRestClient.class);

	public void buildSSLRequest() {

		restClientSync.buildSSLRequest(obConfig.getTransportkeyAlias());
	}

	public String retrieveOBAccessToken() {

		return scimRestClient.retrieveOBAccessToken(jwtCreate.createJWTToken());
	}

	public List<QTSPResource> retrieveQTSPs(String accessToken) {

		OBResponse obResponse = new OBResponse();
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.set(AUTHORIZATION, BEARER + accessToken);
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(obConfig.getApiUrl());
		try {
			obResponse = restClientSync.callForGet(requestInfo, OBResponse.class, httpHeaders);
		} catch (Exception e) {
			LOGGER.error("Failed to fetch QTSPs from OB, Exception : {}", e.getMessage());
		}
		return obResponse.getResources();
	}

}
