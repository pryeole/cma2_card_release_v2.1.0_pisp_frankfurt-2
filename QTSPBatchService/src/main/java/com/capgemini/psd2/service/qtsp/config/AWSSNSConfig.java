package com.capgemini.psd2.service.qtsp.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("qtspservice.writer.awssns")
public class AWSSNSConfig {

	private String accessKey;

	private String secretKey;

	private String region;

	private String topicArn;

	private String pfAdd;

	private String pfRemove;

	private String nsAdd;

	private String nsRemove;

	private String message;

	public String getAccessKey() {
		return accessKey;
	}

	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getTopicArn() {
		return topicArn;
	}

	public void setTopicArn(String topicArn) {
		this.topicArn = topicArn;
	}

	public String getPfAdd() {
		return pfAdd;
	}

	public void setPfAdd(String pfAdd) {
		this.pfAdd = pfAdd;
	}

	public String getPfRemove() {
		return pfRemove;
	}

	public void setPfRemove(String pfRemove) {
		this.pfRemove = pfRemove;
	}

	public String getNsAdd() {
		return nsAdd;
	}

	public void setNsAdd(String nsAdd) {
		this.nsAdd = nsAdd;
	}

	public String getNsRemove() {
		return nsRemove;
	}

	public void setNsRemove(String nsRemove) {
		this.nsRemove = nsRemove;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}