package com.capgemini.psd2.model.qtsp;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OBResponse {

	private List<String> schemas;

	private String totalResults;

	@JsonProperty("Resources")
	private List<QTSPResource> resources;

	public List<String> getSchemas() {
		return schemas;
	}

	public void setSchemas(List<String> schemas) {
		this.schemas = schemas;
	}

	public String getTotalResults() {
		return totalResults;
	}

	public void setTotalResults(String totalResults) {
		this.totalResults = totalResults;
	}

	public List<QTSPResource> getResources() {
		return resources;
	}

	public void setResources(List<QTSPResource> resources) {
		this.resources = resources;
	}
}