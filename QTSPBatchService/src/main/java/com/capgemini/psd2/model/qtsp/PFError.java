package com.capgemini.psd2.model.qtsp;

public class PFError {

	private String resultId;

	private String message;

	private PFValidationError[] validationErrors;

	public String getResultId() {
		return resultId;
	}

	public void setResultId(String resultId) {
		this.resultId = resultId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public PFValidationError[] getValidationErrors() {
		return validationErrors;
	}

	public void setValidationErrors(PFValidationError[] validationErrors) {
		this.validationErrors = validationErrors;
	}
}