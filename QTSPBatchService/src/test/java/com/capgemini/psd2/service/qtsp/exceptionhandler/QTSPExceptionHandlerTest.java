package com.capgemini.psd2.service.qtsp.exceptionhandler;

import java.nio.charset.Charset;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;

import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.utilities.JSONUtilities;

@RunWith(SpringJUnit4ClassRunner.class)
public class QTSPExceptionHandlerTest {

	@InjectMocks
	private QTSPExceptionHandler qtspExceptionHandler;

	@Before 
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this); 
	}

	@Test(expected = PSD2Exception.class)
	public void handleExceptionTest_serverErrorException_success() {
		qtspExceptionHandler.handleException(new HttpServerErrorException(HttpStatus.OK));
	}

	@Test(expected = PSD2Exception.class)
	public void handleExceptionTest_serverErrorException_failure() {
		ErrorInfo info = new ErrorInfo("9018", "message", HttpStatus.BAD_REQUEST.toString(),"400");
		byte[] responseBody = JSONUtilities.getJSONOutPutFromObject(info).getBytes();
		HttpServerErrorException e = new HttpServerErrorException(HttpStatus.BAD_REQUEST, "Bad Request", responseBody,
				Charset.forName("UTF-8"));
		qtspExceptionHandler.handleException(e);
	}

	@Test(expected = PSD2Exception.class)
	public void handleExceptionTest_clientErrorException_success() {

		qtspExceptionHandler.handleException(new HttpClientErrorException(HttpStatus.OK));
	}

	@Test(expected = PSD2Exception.class)
	public void handleExceptionTest_clientErrorException_failure() {
		ErrorInfo info = new ErrorInfo("9018", "message", HttpStatus.BAD_REQUEST.toString(),"400");
		byte[] responseBody = JSONUtilities.getJSONOutPutFromObject(info).getBytes();
		HttpClientErrorException e = new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Bad Request", responseBody,
				Charset.forName("UTF-8"));
		qtspExceptionHandler.handleException(e);
	}

	@Test(expected = PSD2Exception.class)
	public void handleExceptionTest_resourceAccessException() {

		qtspExceptionHandler.handleException(new ResourceAccessException("Could not access resource"));
	}

}
