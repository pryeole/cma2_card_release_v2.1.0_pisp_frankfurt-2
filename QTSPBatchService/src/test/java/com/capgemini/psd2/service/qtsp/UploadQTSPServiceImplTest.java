package com.capgemini.psd2.service.qtsp;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.model.qtsp.FailureInfo;
import com.capgemini.psd2.model.qtsp.QTSPResource;
import com.capgemini.psd2.service.qtsp.integration.AWSSNSAdapter;
import com.capgemini.psd2.service.qtsp.integration.NetScalerAdapter;
import com.capgemini.psd2.service.qtsp.integration.PingFederateAdapter;
import com.capgemini.psd2.service.qtsp.stub.QTSPStub;
import com.capgemini.psd2.service.qtsp.utility.QTSPServiceUtility;

@RunWith(SpringJUnit4ClassRunner.class)
public class UploadQTSPServiceImplTest {

	@InjectMocks
	private UploadQTSPServiceImpl uploadQTSPServiceImpl;

	@Mock
	private PingFederateAdapter pingFederateAdapter;

	@Mock
	private NetScalerAdapter netScalerAdapter;

	@Mock
	private QTSPServiceUtility utility;

	@Mock
	private AWSSNSAdapter awsSNSAdapter;

	@Mock
	private QTSPService service;
	
	@Mock
	private RequestHeaderAttributes requestHeaderAttributes;

	private QTSPResource qtsp;

	List<FailureInfo> failures;

	List<FailureInfo> emptyFailures;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		qtsp = QTSPStub.getCerts().get(0);
		failures = new ArrayList<>();
		FailureInfo failureInfo1 = new FailureInfo(qtsp, "asdasd", "fsdfs", QTSPServiceUtility.PF_FAILURE_STAGE_ADD);
		failures.add(failureInfo1);
		emptyFailures = new ArrayList<>();
		when(service.findQTSP(anyString())).thenReturn(QTSPStub.getCerts().get(0));
	}

	@Test
	public void validateRequestBodyTest() {
		uploadQTSPServiceImpl.validateRequestBody(qtsp);
	}

	@Test
	public void validateRequestBodyTest_null() {
		qtsp.setX509Certificate(null);
		uploadQTSPServiceImpl.validateRequestBody(qtsp);
	}

	@Test
	public void validateRequestBodyTest_empty() {
		qtsp.setX509Certificate("");
		uploadQTSPServiceImpl.validateRequestBody(qtsp);
	}

	@Test
	public void validateRequestBodyTest_parseError() {
		qtsp.setX509Certificate("123dfiusdf");
		uploadQTSPServiceImpl.validateRequestBody(qtsp);
	}

	@Test
	public void uploadQTSPTest() {
		when(requestHeaderAttributes.getTenantId()).thenReturn("BOI");
		uploadQTSPServiceImpl.uploadQTSP(qtsp);
	}

	@Test
	public void uploadQTSPTest_pfFailure() {
		when(requestHeaderAttributes.getTenantId()).thenReturn("BOI");
		when(utility.getFailures()).thenReturn(failures);
		uploadQTSPServiceImpl.uploadQTSP(qtsp);
	}

	@Test
	public void uploadQTSPTest_nsFailure() {
		when(requestHeaderAttributes.getTenantId()).thenReturn("BOI");
		when(utility.getFailures()).thenReturn(emptyFailures).thenReturn(failures);
		uploadQTSPServiceImpl.uploadQTSP(qtsp);
	}
}
