/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.mock.schedulepayments.boi.adapter.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.foundationservice.account.mock.schedulepayments.boi.adapter.domain.PartiesOneOffPaymentInstructionsresponse;
import com.capgemini.psd2.foundationservice.account.mock.schedulepayments.boi.adapter.service.AccountScheduledPaymentsService;
import com.capgemini.psd2.foundationservice.validator.ValidationUtility;

/**
 * The Class AccountInformationController.
 */
@RestController
@RequestMapping("/core-banking/p/party")
public class AccountScheduledPaymentsController {

	/** The account information service. */
	@Autowired
	private AccountScheduledPaymentsService accountSchedulePaymentsService;

	@Autowired
	private ValidationUtility validationUtility;

	/**
	 * 0 Channel A reterive account information.
	 *
	 * @param accountNsc
	 *            the account nsc
	 * @param accountNumber
	 *            the account number
	 * @param boiUser
	 *            the boi user
	 * @param boiChannel
	 *            the boi channel
	 * @param sourceSystem
	 *            the boi platform
	 * @param correlationID
	 *            the correlation ID
	 * @return the accounts
	 * @throws Exception
	 *             the exception
	 */
	//@RequestMapping(value = "/{userId}/accounts/{accountNSC}/{accountNumber}/once-off-payment-schedule/schedule-items", method = RequestMethod.GET, produces = "application/json")
	@RequestMapping(value = "/{version}/parties/{userId}/one-off-payment-instructions", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public PartiesOneOffPaymentInstructionsresponse channelAReteriveAccountInformation(
			@RequestParam(value = "parentNationalSortCodeNSCNumber", required = false) String parentNationalSortCodeNSCNumber,
			@RequestParam(value = "accountNumber", required = false) String accountNumber,
			@PathVariable("userId") String userId,
			@RequestHeader(required = false, value = "X-API-TRANSACTION-ID") String trasaction) throws Exception {
		validationUtility.validateErrorCode(trasaction);
		return accountSchedulePaymentsService.retrieveAccountInformation(userId);

	}

}