/*package com.capgemini.psd2.security.consent.test.handlers;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.AuthenticationException;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.security.consent.handlers.CustomAuthenticationFailureHandler;
import com.capgemini.psd2.security.exceptions.PSD2AuthenticationException;
import com.capgemini.psd2.security.models.SecurityRequestAttributes;

public class CustomAuthenticationFailureHandlerTest {

	@InjectMocks
	private CustomAuthenticationFailureHandler handler;
	
	private RequestHeaderAttributes requestHeaderAttributes;
	
	private SecurityRequestAttributes securityRequestAttributes;
	
	private HttpServletRequest request;
	private HttpServletResponse response;
	private AuthenticationException exception;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		requestHeaderAttributes = new RequestHeaderAttributes();
		requestHeaderAttributes.setCorrelationId("ba4f73f8-9a60-425b-aed8-2a7ef2509fea");
		ReflectionTestUtils.setField(handler, "requestHeaderAttributes",
				requestHeaderAttributes);
		securityRequestAttributes = new SecurityRequestAttributes();
		String callBackEncUrl = "Qxl7JY2bCzj/1O3i7r8U7FIGIeDodZEE7Yf70rLbH2hz8bK8PTtgSATm9ix61kE38Jpt/SS4Z1cXk+1RWeG1RCECy6u7FGgIzIdpco1MaE64vA7Pn3a11vv8ZmJ2y9skkVNcu9DXnL1fKzb2DylzGH9LWmVovRhU10at1NBRE0ERCB8dsT+xfuQ985E2MFrkWPUP/Kbz8iOa4dqY4WBxazQcy06dVUO7BQwmo9oYch8ubVW3ILdfbl7pfM7Z/wszl8cB8SPPTtLavvCfDNXRGUSZoYGGfK40F8TOvF/sYLeOx3E0HFNibWAx6f9JmhAZWO40a82nAYaY3BkTLY3uyQ8nZSQJ1oyW1VRa/U+Cig5lQ1W4BAH0KgYQzi9HkoF1KeP9ZE7WQlerJ2G8en9x5ewvgj11eF5L0zZ41oUNy41pso1lU+p+n0PCs6EngV+FImsx8pBLf6XN0rQ8LhW0z51I7MhKCBuyaeIw4zR6pWRBH9xcYKUgbkCvmLcM4MQRR7G+y0Jd5F/hLfhI60vTMZEntGexTanIZyOdEgPQnOQzTtqwmCmD8cQm8vxJIZGoi3xR36FWsvlLTbni1r9UaQZpB2P/tuRjxZtVveDuVeqZ2zfZ/M/xrMcpr6D9c7BSWiCs1PnVcDEWX4QwnZfk4FBD5Fl1NFI7b1fYsg0iZSkIU1L+bgt4RfZ8gGkzj9aiUx1f4OuzMu0m5A90tM2M8Q3YyKgNSzk3Ecji7t2o6rN3qkmYJihl/GhS+qTT5x9mT1FTtAQ/dQlQYReeKYA3LMP6f8SFEDGY+JvAjy3eb85BXCT1NEpjIz3oOd+RXh3bDUSKRAqhIRiPCZiGmQ/gnYJofJPYS3Q/WaeTnXA6KdUhLSz6AaNcf2datuut2mAgZdMQMRc4n9gGjn0tqOK/2eHy3rcs84ndOMhJY+pUGDE=#!/aisp-account";
		securityRequestAttributes.setCallBackEncUrl(callBackEncUrl);
		ReflectionTestUtils.setField(handler, "securityRequestAttributes",
				securityRequestAttributes);
	}
	
	@Test
	public void testOnAuthenticationFailure() throws IOException, ServletException{
		request = mock(HttpServletRequest.class);
		response = mock (HttpServletResponse.class);
		exception = mock(AuthenticationException.class);
		RequestDispatcher requestDispatcher = mock(RequestDispatcher.class);
		when(request.getRequestDispatcher(anyString())).thenReturn(requestDispatcher );
		handler.onAuthenticationFailure(request, response, exception);
	}
	
	@Test
	public void testPSD2AuthenticationException() throws IOException, ServletException{
		request = mock(HttpServletRequest.class);
		response = mock (HttpServletResponse.class);
		exception = mock(PSD2AuthenticationException.class);
		RequestDispatcher requestDispatcher = mock(RequestDispatcher.class);
		when(request.getRequestDispatcher(anyString())).thenReturn(requestDispatcher );
		handler.onAuthenticationFailure(request, response, exception);
	}
}
*/