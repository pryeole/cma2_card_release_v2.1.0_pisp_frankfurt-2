package com.capgemini.psd2.sca.authentication.service.mule.mock.foundationservice.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.sca.authentication.service.mule.mock.foundationservice.domain.CustomLoginResponse;

public interface SCAAuthenticationPOSTRepository extends MongoRepository<CustomLoginResponse, String>{
	
	public CustomLoginResponse findByDigitalUserIdentifier(String Id);
	

}
