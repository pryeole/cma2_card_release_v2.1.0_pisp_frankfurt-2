package com.capgemini.psd2.pisp.domestic.payments.routing.adapter.impl;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.DomesticPaymentsAdapter;
import com.capgemini.psd2.pisp.domain.CustomDPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBTransactionIndividualStatus1Code;
import com.capgemini.psd2.pisp.domain.PaymentDomesticSubmitPOST201Response;
import com.capgemini.psd2.pisp.domestic.payments.routing.adapter.routing.DomesticPaymentsAdapterFactory;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.utilities.JSONUtilities;

@Component("dPaymentsRoutingAdapter")
public class DomesticPaymentsRoutingAdapterImpl implements DomesticPaymentsAdapter {

	private static final Logger LOG = LoggerFactory.getLogger(DomesticPaymentsRoutingAdapterImpl.class);

	private DomesticPaymentsAdapter beanInstance;

	@Autowired
	private LoggerUtils loggerUtils;
	@Autowired
	private DomesticPaymentsAdapterFactory factory;

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Value("${app.paymentSubmissionExecutionAdapter}")
	private String paymentSubmissionExecutionAdapter;

	private DomesticPaymentsAdapter getRoutingAdapter() {
		if (beanInstance == null)
			beanInstance = factory.getDomesticPaymentsAdapterInstance(paymentSubmissionExecutionAdapter);
		return beanInstance;
	}

	@Override
	public PaymentDomesticSubmitPOST201Response processDomesticPayments(
			CustomDPaymentsPOSTRequest domesticPaymentsRequest,
			Map<String, OBTransactionIndividualStatus1Code> paymentStatusMap, Map<String, String> params) {

		LOG.info("{\"Enter\":\"{}\",\"{}\"}",
				"com.capgemini.psd2.pisp.domestic.payments.routing.adapter.impl.processDomesticPayments()",
				loggerUtils.populateLoggerData("processDomesticPayments"));

		PaymentDomesticSubmitPOST201Response x = getRoutingAdapter().processDomesticPayments(domesticPaymentsRequest,
				paymentStatusMap, getHeaderParams(params));

		LOG.info("{\"Exit\":\"{}\",\"{}\",\"tenantId\":{}}",
				"com.capgemini.psd2.pisp.domestic.payments.routing.adapter.impl.processDomesticPayments()",
				loggerUtils.populateLoggerData("processDomesticPayments"),
				JSONUtilities.getJSONOutPutFromObject(reqHeaderAtrributes.getTenantId()));

		return x;
	}

	@Override
	public PaymentDomesticSubmitPOST201Response retrieveStagedDomesticPayments(
			CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
		return getRoutingAdapter().retrieveStagedDomesticPayments(customPaymentStageIdentifiers,
				getHeaderParams(params));
	}

	private Map<String, String> getHeaderParams(Map<String, String> params) {
		if (params == null)
			params = new HashMap<>();

		if (null != reqHeaderAtrributes.getToken() && null != reqHeaderAtrributes.getToken().getSeviceParams()) {
			params.put(PSD2Constants.CHANNEL_IN_REQ_HEADER,
					reqHeaderAtrributes.getToken().getSeviceParams().get(PSD2Constants.CHANNEL_NAME));
		}

		params.put(PSD2Constants.USER_IN_REQ_HEADER, reqHeaderAtrributes.getPsuId());
		params.put(PSD2Constants.CORRELATION_REQ_HEADER, reqHeaderAtrributes.getCorrelationId());
		params.put(PSD2Constants.TENANT_ID, reqHeaderAtrributes.getTenantId());
		return params;
	}

}
