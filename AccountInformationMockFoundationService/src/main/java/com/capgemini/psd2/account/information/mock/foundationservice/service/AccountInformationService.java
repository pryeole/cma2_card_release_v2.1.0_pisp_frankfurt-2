/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.information.mock.foundationservice.service;

import com.capgemini.psd2.account.information.mock.foundationservice.domain.Accounts;

/**
 * The Interface AccountInformationService.
 */
public interface AccountInformationService {

	/**
	 * Retrieve account information.
	 *
	 * @param nsc the nsc
	 * @param accountNumber the account number
	 * @return the accounts
	 * @throws Exception the exception
	 */
	public Accounts  retrieveAccountInformation(String nsc, String accountNumber) throws Exception;

	
}
