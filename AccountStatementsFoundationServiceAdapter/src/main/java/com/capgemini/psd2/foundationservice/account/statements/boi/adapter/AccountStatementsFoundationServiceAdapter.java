package com.capgemini.psd2.foundationservice.account.statements.boi.adapter;

import java.text.SimpleDateFormat;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.capgemini.psd2.adapter.datetime.utility.StatementDateRange;
import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.adapter.AccountStatementsAdapter;
import com.capgemini.psd2.aisp.domain.OBReadStatement1;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountSatementsFileResponse;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountStatementsResponse;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountTransactionResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.client.AccountStatementsFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.constants.AccountStatementsFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.delegate.AccountStatementsFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.raml.domain.Statementsresponse;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.CustomerAccountsFilterFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.AdapterFilterUtility;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUserProfile;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class AccountStatementsFileFoundationServiceAdapter.
 */
@Component
@EnableConfigurationProperties
@ConfigurationProperties("foundationService")
public class AccountStatementsFoundationServiceAdapter implements AccountStatementsAdapter {
	/** The single account balance base URL. */
	@Value("${foundationService.singleAccountStatementsFileBaseURL}")
	private String singleAccountStatementsFileBaseURL;

	@Value("${foundationService.consentFlowType}")
	private String consentFlowType;

	@Autowired
	private AdapterFilterUtility adapterFilterUtility;

	/** The single account statements credit card file base URL. */
	@Value("${foundationService.singleAccountStatementsCreditCardFileBaseURL}")
	private String singleAccountStatementsCreditCardFileBaseURL;

	/** The statement base URL for account. */
	@Value("${foundationService.singleAccountStatementsFileBaseStmtURL}")
	private String singleAccountStatementsFileBaseStmtURL;
	/** The statement base URL for Creditcardaccount. */
	@Value("${foundationService.singleAccountStatementsCreditCardFileBaseStmtURL}")
	private String singleAccountStatementsCreditCardFileBaseStmtURL;

	/** The account statements file foundation service delegate. */
	@Autowired
	private AccountStatementsFoundationServiceDelegate accountStatementsFileFoundationServiceDelegate;

	/** The account balance foundation service client. */
	@Autowired
	private AccountStatementsFoundationServiceClient accountStatementsFileFoundationServiceClient;

	@Autowired
	private CustomerAccountsFilterFoundationServiceAdapter commonFilterUtility;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.capgemini.psd2.aisp.adapter.AccountStatementsAdapter#
	 * downloadStatementFileByStatementsId(com.capgemini.psd2.consent.domain.
	 * AccountMapping, java.util.Map)
	 */
	public PlatformAccountSatementsFileResponse downloadStatementFileByStatementsId(AccountMapping accountMapping,
			Map<String, String> params) {

		PlatformAccountSatementsFileResponse accountSatementsFileResponse = new PlatformAccountSatementsFileResponse();

		if (accountMapping == null || accountMapping.getPsuId() == null) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}

		if (params == null) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}

		params.put(PSD2Constants.CONSENT_FLOW_TYPE, consentFlowType);
		params.put(PSD2Constants.USER_ID, accountMapping.getPsuId());
		params.put(PSD2Constants.CHANNEL_ID, params.get("channelId"));
		params.put(PSD2Constants.CORRELATION_ID, accountMapping.getCorrelationId());

		String channelId = params.get("channelId");

		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = new HttpHeaders();

		AccountDetails accountDetails;
		if (accountMapping.getAccountDetails() != null && !accountMapping.getAccountDetails().isEmpty()) {
			accountDetails = accountMapping.getAccountDetails().get(0);
			params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE,
					accountDetails.getAccountSubType().toString());
			params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_NUMBER, accountDetails.getAccountNumber());

		} else
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);

		String finalURL = null;
		if ((NullCheckUtils.isNullOrEmpty(accountDetails.getAccountSubType())
				&& params.get(PSD2Constants.CMAVERSION) == null)
				|| accountDetails.getAccountSubType().toString()
						.contentEquals(AccountStatementsFoundationServiceConstants.CURRENT_ACCOUNT)
				|| accountDetails.getAccountSubType().toString()
						.contentEquals(AccountStatementsFoundationServiceConstants.SAVINGS)) {
			params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_NSC, accountDetails.getAccountNSC());
			params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_NUMBER, accountDetails.getAccountNumber());
			httpHeaders = accountStatementsFileFoundationServiceDelegate.createRequestHeadersFile(accountMapping,
					params);

			finalURL = accountStatementsFileFoundationServiceDelegate.getFoundationServiceURLFile(params,
					singleAccountStatementsFileBaseURL);
		} else if (accountDetails.getAccountSubType().toString()
				.equals(AccountStatementsFoundationServiceConstants.CREDIT_CARD)) {

			params.put("maskedPan", accountDetails.getAccount().getIdentification()
					.substring(accountDetails.getAccount().getIdentification().length() - 4));

			httpHeaders = accountStatementsFileFoundationServiceDelegate.createRequestHeadersFile(accountMapping,
					params);

			finalURL = accountStatementsFileFoundationServiceDelegate.getFoundationServiceCardURLFile(params,
					singleAccountStatementsCreditCardFileBaseURL);

		}

		requestInfo.setUrl(finalURL);
		byte[] byteArray = null;

		byteArray = accountStatementsFileFoundationServiceClient.restTransportForAccountStatementsFile(requestInfo,
				byte[].class, httpHeaders);
		accountSatementsFileResponse.setFileName("E-Statements.pdf");
		accountSatementsFileResponse.setFileByteArray(byteArray);

		if (NullCheckUtils.isNullOrEmpty(byteArray)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND_FOUNDATION_SERVICE);
		}

		return accountSatementsFileResponse;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.capgemini.psd2.aisp.adapter.AccountStatementsAdapter#
	 * retrieveAccountStatements(com.capgemini.psd2.consent.domain.AccountMapping,
	 * java.util.Map) To retrieve Account Statements for Current/Saving accounts and
	 * creditcard
	 */
	public PlatformAccountStatementsResponse retrieveAccountStatements(AccountMapping accountMapping,
			Map<String, String> params) {
		if (accountMapping == null || accountMapping.getPsuId() == null) {

			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		if (params == null) {

			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}

		params.put(PSD2Constants.CONSENT_FLOW_TYPE, consentFlowType);
		params.put(PSD2Constants.USER_ID, accountMapping.getPsuId());
		params.put(PSD2Constants.CHANNEL_ID, params.get("channelId"));
		params.put(PSD2Constants.CORRELATION_ID, accountMapping.getCorrelationId());

		String channelId = params.get("channelId");

		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = new HttpHeaders();

		Statementsresponse statementsResponse = null;

		String fromBookingDateTimeInString = null;
		String toBookingDateTimeInString = null;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		StatementDateRange statementDateRange = accountStatementsFileFoundationServiceDelegate
				.createTransactionDateRange(params);
		StatementDateRange decision = accountStatementsFileFoundationServiceDelegate.fsCallFilter(statementDateRange);

		if (decision.isEmptyResponse()) {
			// SITdefect2350-statement Null Response
			OBReadStatement1 oBReadstatement1 = new OBReadStatement1();
			PlatformAccountStatementsResponse PlatformAccountStatementsResponse = new PlatformAccountStatementsResponse();
			PlatformAccountStatementsResponse.setoBReadStatement1(oBReadstatement1);
			return PlatformAccountStatementsResponse;
		} else {
			fromBookingDateTimeInString = formatter.format(decision.getNewFilterFromDate());
			toBookingDateTimeInString = formatter.format(decision.getNewFilterToDate());
		}

		MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<>();
		queryParams.add(AccountStatementsFoundationServiceConstants.FROM_DATE, fromBookingDateTimeInString);
		queryParams.add(AccountStatementsFoundationServiceConstants.TO_DATE, toBookingDateTimeInString);

		/*
		 * HttpHeaders httpHeaders =
		 * accountStatementsFileFoundationServiceDelegate.createRequestHeadersStmt(
		 * requestInfo, accountMapping, params);
		 */
		AccountDetails accountDetails;

		if (accountMapping.getAccountDetails() != null && !accountMapping.getAccountDetails().isEmpty()) {
			accountDetails = accountMapping.getAccountDetails().get(0);
			params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_ID, accountDetails.getAccountId());
			params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE,
					String.valueOf(accountDetails.getAccountSubType()));

		} else
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		if (channelId.equalsIgnoreCase("B365")) {
			if ((NullCheckUtils.isNullOrEmpty(accountDetails.getAccountSubType())
					&& params.get(PSD2Constants.CMAVERSION) == null)
					|| accountDetails.getAccountSubType().toString()
							.equalsIgnoreCase(AccountStatementsFoundationServiceConstants.CURRENT_ACCOUNT)
					|| accountDetails.getAccountSubType().toString()
							.equalsIgnoreCase(AccountStatementsFoundationServiceConstants.SAVINGS)) {

				httpHeaders = accountStatementsFileFoundationServiceDelegate.createRequestHeadersStmt(accountMapping,
						params);
				
				String finalURL = accountStatementsFileFoundationServiceDelegate.getFoundationServiceURL(
						accountDetails.getAccountNSC(), accountDetails.getAccountNumber(),
						singleAccountStatementsFileBaseStmtURL);
				requestInfo.setUrl(finalURL);
				statementsResponse = accountStatementsFileFoundationServiceClient.restTransportForAccountStatementsStmt(
						requestInfo, Statementsresponse.class, queryParams, httpHeaders);
			} else if (accountDetails.getAccountSubType().toString()
					.equalsIgnoreCase(AccountStatementsFoundationServiceConstants.CREDIT_CARD)) {
				
				//ADDED DIGITALUSERPROFILE BLOCK
				DigitalUserProfile filteredAccounts = commonFilterUtility
						.retrieveCustomerAccountList(accountMapping.getPsuId(), params);
				if (filteredAccounts == null || filteredAccounts.getAccountEntitlements() == null
						|| filteredAccounts.getAccountEntitlements().isEmpty()) {
					throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.ACCOUNT_NOT_FOUND);
				}
				accountMapping = adapterFilterUtility.prevalidateAccounts(filteredAccounts, accountMapping);
				
				params.put("maskedPan", accountDetails.getAccount().getIdentification()
						.substring(accountDetails.getAccount().getIdentification().length() - 4));
				httpHeaders = accountStatementsFileFoundationServiceDelegate.createRequestHeadersStmt(accountMapping,
						params);

				String finalURL = accountStatementsFileFoundationServiceDelegate.getFoundationServiceCreditcardURL(
						accountDetails.getAccountNumber(), singleAccountStatementsCreditCardFileBaseStmtURL);
				requestInfo.setUrl(finalURL);
				statementsResponse = accountStatementsFileFoundationServiceClient.restTransportForAccountStatementsStmt(
						requestInfo, Statementsresponse.class, queryParams, httpHeaders);
			}
			
			if (statementsResponse.getStatementsList().isEmpty()) {
				statementsResponse = new Statementsresponse();
			}
		}

		else {

			if (((NullCheckUtils.isNullOrEmpty(accountDetails.getAccountSubType())
					&& params.get(PSD2Constants.CMAVERSION) == null)
					|| accountDetails.getAccountSubType().toString().contentEquals("CurrentAccount")
					|| accountDetails.getAccountSubType().toString().contentEquals("Savings"))
					|| (accountDetails.getAccountSubType().toString().contentEquals("CreditCard"))) {

				DigitalUserProfile filteredAccounts = commonFilterUtility
						.retrieveCustomerAccountList(accountMapping.getPsuId(), params);
				if (filteredAccounts == null || filteredAccounts.getAccountEntitlements() == null						|| filteredAccounts.getAccountEntitlements().isEmpty()) {
					throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.ACCOUNT_NOT_FOUND);
				}
				accountMapping = adapterFilterUtility.prevalidateAccounts(filteredAccounts, accountMapping);
				statementsResponse = new Statementsresponse();

			}
		}

		// MultiValueMap<String, String> queryParams =
		// accountStatementsFileFoundationServiceDelegate.getFromAndToDate(params);

		return accountStatementsFileFoundationServiceDelegate.transformAccountStatementResponse(statementsResponse,
				params);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.capgemini.psd2.aisp.adapter.AccountStatementsAdapter#
	 * retrieveAccountStatementsByStatementId(com.capgemini.psd2.consent.domain.
	 * AccountMapping, java.util.Map)
	 */
	public PlatformAccountStatementsResponse retrieveAccountStatementsByStatementId(AccountMapping accountMapping,
			Map<String, String> params) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.capgemini.psd2.aisp.adapter.AccountStatementsAdapter#
	 * retrieveAccountTransactionsByStatementsId(com.capgemini.psd2.consent.domain.
	 * AccountMapping, java.util.Map)
	 */
	public PlatformAccountTransactionResponse retrieveAccountTransactionsByStatementsId(AccountMapping accountMapping,
			Map<String, String> params) {
		return null;
	}
}
