package com.capgemini.psd2.sca.authentication.service.boi.fs;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.adapter.security.custom.domain.CustomAuthenticationServicePostRequest;
import com.capgemini.psd2.adapter.security.custom.domain.CustomAuthenticationServicePostResponse;
import com.capgemini.psd2.adapter.security.domain.DigitalUser13;
import com.capgemini.psd2.adapter.security.domain.Login;
import com.capgemini.psd2.adapter.security.domain.SelectedDeviceReference;
import com.capgemini.psd2.foundationservice.sca.authentication.service.boi.adapter.SCAAuthenticationServiceFoundationServiceAdapter;

@SpringBootApplication
@ComponentScan("com.capgemini.psd2")
@EnableMongoRepositories("com.capgemini.psd2")
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class SCAAuthenticationServiceFoundationServiceTestProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(SCAAuthenticationServiceFoundationServiceTestProjectApplication.class, args);
	}
}

@RestController
@ResponseBody
class TestAuthenticationApplicationFSAdapter {

	@Autowired
	SCAAuthenticationServiceFoundationServiceAdapter scaadapter;

	@RequestMapping(value = "/testSCAAuthenticationServiceAdapter", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	@ResponseBody
	public CustomAuthenticationServicePostResponse getResponse(
			@RequestBody CustomAuthenticationServicePostRequest custRequest) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("x-fapi-interaction-id", "2345678652002");
		params.put("X-BOI-USER", "BOI2002");		
		return scaadapter.retrieveAuthenticationService(custRequest, params);

	}
}