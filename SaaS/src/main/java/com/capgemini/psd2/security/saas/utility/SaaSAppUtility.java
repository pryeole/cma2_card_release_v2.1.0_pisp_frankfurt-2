package com.capgemini.psd2.security.saas.utility;

import java.util.Base64;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.rest.client.helpers.HeaderHelper;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.scaconsenthelper.config.PFConfig;
import com.capgemini.psd2.scaconsenthelper.constants.OIDCConstants;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.scaconsenthelper.models.DropOffRequest;
import com.capgemini.psd2.scaconsenthelper.models.DropOffResponse;
import com.capgemini.psd2.utilities.JSONUtilities;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class SaaSAppUtility {

	@Autowired
	private RequestHeaderAttributes requestHeaderAttributes;
	
	@Autowired
	private PFConfig pfConfig;

	@Autowired
	private RestClientSync restClientSyncImpl;
	
	@Value("${app.regex.username}")
	private String userNameRegex;
	
	public boolean validUserAndPassword(String username) {
		Boolean isValid = Boolean.FALSE;
		Pattern usenamePattern = Pattern.compile(userNameRegex);
		//Pattern passPattern = Pattern.compile(passwordRegex);
		Matcher m = usenamePattern.matcher(username);
		//Matcher m1 = passPattern.matcher((String) authentication.getCredentials());
		if (m.matches()) {
			isValid = true;
		}
		return isValid;
	}
	
	public String dropOff(String userId, String authenticationTime) {

		String correlationId = requestHeaderAttributes.getCorrelationId();

		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(pfConfig.getTenantSpecificDropOffUrl(requestHeaderAttributes.getTenantId()));

		DropOffRequest dropOffDataModel = new DropOffRequest();
		dropOffDataModel.setUsername(userId);
		dropOffDataModel.setAcr(OIDCConstants.SCA_ACR);
		dropOffDataModel.setChannel_id(requestHeaderAttributes.getChannelId());
		dropOffDataModel.setCorrelationId(correlationId);
		dropOffDataModel.setClient_id(requestHeaderAttributes.getTppCID());
		dropOffDataModel.setScope(requestHeaderAttributes.getScopes());

		dropOffDataModel.setAuthnInst(authenticationTime);

		HttpHeaders httpHeaders = HeaderHelper.populateJSONContentTypeHeader();

		String credentials = pfConfig.getScainstanceusername().concat(":").concat(pfConfig.getScainstancepassword());

		String encoderString = Base64.getEncoder().encodeToString(credentials.getBytes());

		httpHeaders.add(SCAConsentHelperConstants.AUTHORIZATION_HEADER,
				SCAConsentHelperConstants.BASIC + " ".concat(encoderString));

		httpHeaders.add(PFConstants.PING_INSTANCE_ID, pfConfig.getScainstanceId());

		String jsonResponse = restClientSyncImpl.callForPost(requestInfo, dropOffDataModel, String.class, httpHeaders);

		DropOffResponse dropOffResponse = JSONUtilities.getObjectFromJSONString(jsonResponse, DropOffResponse.class);

		return dropOffResponse.getRef();
	}
}
